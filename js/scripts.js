$(function() {
    var clock = document.querySelector('digiclock');
    var date = document.querySelector('digidate');
    var pad = function(x) {
        return x < 10 ? '0'+x : x;
    };

    var ticktock = function() {
        var now = new Date();
        var h = pad(now.getHours());
        var m = pad(now.getMinutes());
        var s = pad(now.getSeconds());

        var current_time = [h,m,s].join(':');
        clock.innerHTML = current_time;
    };

    var tickdate = function() {
        var now = new Date();
        var d_names = new Array("Chủ nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7");
        var t = d_names[now.getDay()];

        var d = pad(now.getDate());
        var m = pad(now.getMonth()+1);
        var y = pad(now.getFullYear());
        var g = pad(now.getFullYear());

        var current_date = t + ', ' + [d,m,y].join('/');

        date.innerHTML = current_date;

    };
    ticktock();
    tickdate();
    setInterval(ticktock, 1000);
    setInterval(tickdate, 1000);
});

function $$$(id) {
    return document.getElementById(id);
}
function Forward(url) {
    window.location.href = url;
}
function ForwardPlus(path) {
    window.location.pathname = path;
}
function _postback() {
    return void(1);
}

function ajaxFunction() {
    var xmlHttp=null;
    try {
        // Firefox, Internet Explorer 7. Opera 8.0+, Safari.
        xmlHttp = new XMLHttpRequest();
    }
    catch (e) {
        // Internet Explorer 6.
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e) {
            try{
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                return false;
            }
        }
    }
}

function $query(obj) {
    var query = "";
    $(obj).find("input").each(function(i){
        var t = $(obj).find("input").eq(i);
        if ((t.attr("type") != "checkbox") && (t.attr("type") != "radio") && (t.attr("type") != "file") ) {
            if (t.attr("type") != "password") {
                query += "&"+t.attr("name")+"="+encodeURIComponent(t.val());
            } else {
                query += "&"+t.attr("name")+"="+document.getElementsByName(t.attr("name"))[0].value;
            }
        }
        else {
            if(t.attr("type") == "checkbox") {
                if (t.is(":checked"))
                    query += "&"+t.attr("name")+"="+t.attr("value");
            } else if (t.attr("type") == "radio") {
                if (t.is(":checked"))
                    query += "&"+t.attr("name")+"="+t.attr("value");
            } else if (t.attr("type") == "file") {
                query += "&"+t.attr("name")+"="+document.getElementsByName(t.attr("name")).files;
            }
        }
    });
    $(obj).find("textarea").each(function(i) {
        var t = $(obj).find("textarea").eq(i);
        query += "&"+t.attr("name")+"="+encodeURIComponent(t.val());
    });
    $(obj).find("select").each(function(i) {
        var t = $(obj).find("select").eq(i);
        query += "&"+t.attr("name")+"="+encodeURIComponent(t.val());
    });

    return query.substring(1);
}

function $query_unt(obj) {
    var query = "";
    $(obj).find("input").each(function(i){
        var t = $(obj).find("input").eq(i);
        if((t.attr("type") != "button") && (t.attr("type") != "submit") && (t.attr("type") != "reset") && (t.attr("type") != "hidden")) {
            if ((t.attr("type") != "checkbox") && (t.attr("type") != "radio") ) {
                t.val('');
            } else {
                t.attr("checked", false);
            }
        } else {}
    });
    $(obj).find("textarea").each(function(i) {
        var t = $(obj).find("textarea").eq(i);
        t.val('');
    });
    return true;
}

function showLoader() {
    $("#_loading").html("<div class=\"loading-body\"><div class=\"windows8\"> <div class=\"wBall\" id=\"wBall_1\"> <div class=\"wInnerBall\"> </div> </div> <div class=\"wBall\" id=\"wBall_2\"> <div class=\"wInnerBall\"> </div> </div> <div class=\"wBall\" id=\"wBall_3\"> <div class=\"wInnerBall\"> </div> </div> <div class=\"wBall\" id=\"wBall_4\"> <div class=\"wInnerBall\"> </div> </div> <div class=\"wBall\" id=\"wBall_5\"> <div class=\"wInnerBall\"> </div> </div> </div></div>").hide().fadeIn(10);
    block = true;
}

function closeLoader() {
    $("#_loading").html("").hide().fadeOut();
    block = false;
}

function showResult(type,data) {
    closeLoader();
    $("#"+type+"").html(data).hide().fadeIn();
    block = false;
}

function header_notify() {
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=notify&type=header',
        success: function(data){
            $("#header_inbox_bar").html(data);
            return true;
        }
    });
    return false;
}

(function ($) {
    "use strict";
    $(document).ready(function () {
        if ($.fn.dcAccordion) {
            $('#nav-accordion').dcAccordion({
                idParent: 'sidebar',
                eventType: 'click',
                autoClose: true,
                saveState: true,
                speed: 'slow',
                autoExpand: true
            });
        }
        /*==Nice Scroll ==*/
        if ($.fn.niceScroll) {
            $(".leftside-navigation").niceScroll({
                cursorcolor: "#0092dd",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });
            $(".leftside-navigation").getNiceScroll().resize();
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();
        }

        /*==Sidebar Toggle==*/
        $(".leftside-navigation .sub-menu > a").click(function () {
            var o = ($(this).offset());
            var diff = 80 - o.top;
            if (diff > 0)
                $(".leftside-navigation").scrollTo("-=" + Math.abs(diff), 500);
            else
                $(".leftside-navigation").scrollTo("+=" + Math.abs(diff), 500);
        });

        $('.sidebar-toggle-box .fa-bars').click(function (e) {
            $(".leftside-navigation").niceScroll({
                cursorcolor: "#1FB5AD",
                cursorborder: "0px solid #fff",
                cursorborderradius: "0px",
                cursorwidth: "3px"
            });
            $('#sidebar').toggleClass('hide-left-bar');
            if ($('#sidebar').hasClass('hide-left-bar')) {
                $(".leftside-navigation").getNiceScroll().hide();
            }
            $(".leftside-navigation").getNiceScroll().show();
            $('#main-content').toggleClass('merge-left');
            e.stopPropagation();
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }
            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }
        });

        $('.header,#main-content,#sidebar').click(function () {
            if ($('#container').hasClass('open-right-panel')) {
                $('#container').removeClass('open-right-panel')
            }
            if ($('.right-sidebar').hasClass('open-right-bar')) {
                $('.right-sidebar').removeClass('open-right-bar')
            }
            if ($('.header').hasClass('merge-header')) {
                $('.header').removeClass('merge-header')
            }
        });

        $('.panel .tools .fa').click(function () {
            var panel = $(this).parents(".panel");
            var el = panel.children(".panel-body");
            panel.addClass('panel-loader-active');
            setTimeout(function() {
                panel.removeClass('panel-loader-active');
            }, 500);
            if ($(this).hasClass("fa-times")) {
                panel.parent().remove();
            } else if ($(this).hasClass("fa-chevron-down")) {
                $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
                el.slideUp(200);
            } else if ($(this).hasClass("fa-chevron-up")) {
                $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
                el.slideDown(200);
            } else if ($(this).hasClass("fa-eye")) {
                $(this).removeClass("fa-eye").addClass("fa-eye-slash");
                panel.find(".fa-times").addClass("link-disabled");
                panel.addClass("panel-focus-enabled");
                $('<div id="focus-overlay"></div>').hide().appendTo("body").fadeIn(300);
            } else if ($(this).hasClass("fa-eye-slash")) {
                $(this).removeClass("fa-eye-slash").addClass("fa-eye");
                panel.find(".fa-times").removeClass("link-disabled");
                $("body").find('#focus-overlay').fadeOut(function(){
                    $(this).remove();
                    panel.removeClass("panel-focus-enabled");
                });
            } else if ($(this).hasClass("fa-compress")) {
                $(this).removeClass("fa-compress").addClass("fa-expand");
                panel.find(".fa-times").addClass("link-disabled");
                $("body").addClass("panel-fullscreen-active");
                panel.addClass("panel-fullscreen");
            } else if ($(this).hasClass("fa-expand")) {
                $(this).removeClass("fa-expand").addClass("fa-compress");
                panel.find(".fa-times").removeClass("link-disabled");
                $("body").removeClass('panel-fullscreen-active');
                panel.removeClass("panel-fullscreen");
            }
        });

        $('#_search').on('keydown', 'input.search[name="key"]', function() {
            var key = $(this).val();
            $.ajax({
                url: '/action.php',
                type: 'POST',
                data: {
                    'url': 'search_fast',
                    'key': key
                },
                dataType: 'html',
                success: function(data) {
                    if(data == '')
                        $('#r_search').hide();
                    else {
                        showResult('r_search', data);
                        $('#r_search ul.list').niceScroll({
                            autohidemode: false,
                            cursorcolor: "#cfcfcf",
                            cursorborder: "0px solid #fff",
                            cursorborderradius: "0px",
                            cursorwidth: "3px"
                        });
                    }
                }
            });
        });
        $('body').click(function() {
            $('#r_search').hide();
            $('#_search input.search[name="key"]').val('');
        });
    });
})(jQuery);

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") {
    hidden = "hidden";
    visibilityChange = "visibilitychange";
} else if (typeof document.mozHidden !== "undefined") {
    hidden = "mozHidden";
    visibilityChange = "mozvisibilitychange";
} else if (typeof document.msHidden !== "undefined") {
    hidden = "msHidden";
    visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
    hidden = "webkitHidden";
    visibilityChange = "webkitvisibilitychange";
}

// toggle function
$.fn.clickToggle = function( f1, f2 ) {
    return this.each( function() {
        var clicked = false;
        $(this).bind('click', function() {
            if(clicked) {
                clicked = false;
                return f2.apply(this, arguments);
            }

            clicked = true;
            return f1.apply(this, arguments);
        });
    });

};

$.fn.shiftSelectable = function() {
    var lastChecked,
        $boxes = this;
    $boxes.click(function(evt) {
        if(!lastChecked) {
            lastChecked = this;
            return;
        }
        if(evt.shiftKey) {
            var start = $boxes.index(this),
                end = $boxes.index(lastChecked);
            $boxes.slice(Math.min(start, end), Math.max(start, end) + 1)
                .attr('checked', lastChecked.checked)
                .trigger('change');
        }

        lastChecked = this;
    });
};

$(document).ready(function() {
    $('#go-top').hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 0) {
                $('#go-top').removeClass().addClass('animated fadeInRight').show();
            } else {
                $('#go-top').removeClass().addClass('animated fadeOutRight').show();
            }
        });
        $('#go-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
    $('#container').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });
    $('#dataTablesList').find('input[type="checkbox"]').shiftSelectable();
    $('#_ol_select_all').change(function () {
        $("input.ol-checkbox-js").prop('checked', $(this).prop("checked"));
    });
    autosize($('textarea.form-control'));
    $('.auto-number').autoNumeric('init');

    $('.ol-list-views').on('click', 'a', function(){
        var el = $(this);
        $.ajax({
            url: '/action.php',
            type: 'POST',
            data: 'url=list_views&id='+parseInt(el.attr("rel")),
            dataType: 'html',
            success: function(data){
                $(el).parent('.ol-list-views').append(data);
                $(el).remove();
            },
            statusCode: {
                401: function() {
                    window.location.href = '/?ol=login';
                }
            }
        });
        return false;
    });
});

function load_comment(el, type, id) {
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=load_comment&type=' + type + '&id=' + parseInt(id),
        dataType: 'html',
        success: function(data){
            $(el).parents('.ol-cmt-rs').prepend(data);
            $(el).parent('.ol-cmt-load').remove();
        },
        statusCode: {
            401: function() {
                window.location.href = '/?ol=login';
            }
        }
    });
    return false;
}

function get_slug(table) {
    var name  = $('#name').val();
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=get_slug&table='+table+'&name='+name,
        dataType: 'html',
        success: function(data){
            $('#slug').val(data);
            closeLoader();
        }
    });
    return false;
}

function getSlugOther() {
    var name  = $('#name').val();
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=get_slug_other&name='+name,
        dataType: 'html',
        success: function(data){
            $('#slug').val(data);
            closeLoader();
        }
    });
    return false;
}

function performSort(id, sort, table) {
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=perform_sort&q='+id+'&sort='+sort+'&type='+table,
        dataType: 'html',
        success: function(data){
            window.location.reload();
        }
    });
    return false;
}

function performSortCore(id, sort, table) {
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=perform_sort_user&q='+id+'&sort='+sort+'&type='+table,
        dataType: 'html',
        success: function(data){
            window.location.reload();
        }
    });
    return false;
}

function edit_status(el, id, type, table) {
    var status = el.attr("rel");
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=edit_status&id='+id+'&type='+type+'&table='+table+'&status='+status,
        dataType: 'html',
        success: function(data){
            if(status==1) {
                el.removeClass("btn-event-close").addClass("btn-event-open");
                el.attr("rel","0");
                el.html(1);
                el.attr("data-original-title","Đóng");
            } else {
                el.removeClass("btn-event-open").addClass("btn-event-close");
                el.attr("rel","1");
                el.html(0);
                el.attr("data-original-title","Mở");
            }
        }
    });
    return false;
}

function edit_status_core(el, id, type, table, qr) {
    var status = el.attr("rel");
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=edit_status_core&id='+id+'&type='+type+'&table='+table+'&qr='+qr+'&status='+status,
        dataType: 'html',
        success: function(data){
            if(status==1) {
                el.removeClass("btn-event-close").addClass("btn-event-open");
                el.attr("rel","0");
	            el.html(1);
                el.attr("data-original-title","Đóng");
            } else {
                el.removeClass("btn-event-open").addClass("btn-event-close");
                el.attr("rel","1");
	            el.html(0);
                el.attr("data-original-title","Mở");
            }
        }
    });
    return false;
}

function edit_core_ol(el, role, slug) {
	var status = el.attr("rel");
	$.ajax({
		url:'/action.php',
		type: 'POST',
		data: 'url=edit_core_ol&role='+role+'&slug='+slug+'&status='+status,
		dataType: 'html',
		success: function(data){
			if(status==1) {
				el.removeClass("btn-event-close").addClass("btn-event-open");
				el.attr("rel","0");
				el.html(1);
				el.attr("data-original-title","Đóng");
			} else {
				el.removeClass("btn-event-open").addClass("btn-event-close");
				el.attr("rel","1");
				el.html(0);
				el.attr("data-original-title","Mở");
			}
		}
	});
	return false;
}

function core_dashboard(id, type) {
    var dataList = new FormData($('#'+id)[0]);
    dataList.append("url", 'core_dashboard');
    dataList.append("type", type);
	showLoader();
	$.ajax({
		url:'/action.php',
        type: 'POST',
        data: dataList,
        dataType: 'html',
		success: function(data){
			showResult(id, data);
            $('.selectpicker').selectpicker('refresh');
		},
        error: function() {
		    alert('Lỗi dữ liệu, vui lòng thực hiện lại!');
        },
        cache: false,
        contentType: false,
        processData: false
	});
	return false;
}

function backup_database(id) {
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=backup_data',
        dataType: 'html',
        success: function(data){
            showResult(id,data);
        }
    });
    return false;
}

function ol_filter(inp, result, type) {
    var dataList = $query('#'+inp);
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url='+type+'&'+dataList,
        dataType: 'html',
        success: function(data){
            showResult(result, data);
        }
    });
    return false;
}

function open_popover(type, id) {
    var result = $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=role_user&type='+type+'&id='+id,
        dataType: "html",
        cache: true,
        async: false,
        success: function(data){
            return data;
        },
        error: function() {
            return false;
        }
    });
    return result.responseText;
}

function import_file(type) {
    showLoader();
    var dataList = new FormData($('#_form_import')[0]);
    dataList.append("url", 'import');
    dataList.append("type", type);
    confirm("Thêm toàn bộ dữ liệu từ File Excel vào hệ thống.\nVới ID bị trùng thì dòng dữ liệu đó sẽ không được thêm vào hệ thống.\nBạn có chắc chắn thực hiện điều này không?", function() {
        if (this.data == true) {
            $.ajax({
                url:'/action.php',
                type: 'POST',
                data: dataList,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function (data) {
                    alert(data, function() {
                        window.location.reload(true);
                    });
                }
            });
        } else {
            closeLoader();
            return false;
        }
    });
    return false;
}

function modal_parent(type, id) {
    showLoader();
    $.ajax({
        url: '/action.php',
        type: 'POST',
        data: 'url=modal_parent&type='+type+'&id='+parseInt(id),
        dataType: 'html',
        success: function(data){
            showResult('_os_modal', data);
            $('#_os_modal').modal('show');
        }
    });
    return false;
}
function update_parent(act, type, id) {
    showLoader();
    var dataList = $query('#_parent');
    $.ajax({
        url: '/action.php',
        type: 'POST',
        data: 'url=parent&act='+act+'&type='+type+'&id='+parseInt(id)+'&'+dataList,
        dataType: 'html',
        success: function(){
            window.location.reload();
        }
    });
    return false;
}
function delete_parent(type, id) {
    showLoader();
    confirm("Bạn có chắc chắn xóa nhóm này không?", function() {
        if(this.data == true) {
            $.ajax({
                url: '/action.php',
                type: 'POST',
                data: 'url=parent&act=delete&type='+type+'&id='+ parseInt(id),
                dataType: 'html',
                success: function(){
                    window.location.reload();
                }
            });
        } else {
            closeLoader();
            return false;
        }
    });
    return false;
}

function add_jobs() {
    showLoader();
    $.ajax({
        url: '/action.php',
        type: 'POST',
        data: 'url=my_add&type=add',
        dataType: 'html',
        success: function(data){
            showResult('_os_modal', data);
        }
    });
    return false;
}
function open_jobs(type, id) {
    showLoader();
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: 'url=jobs&type='+type+'&id='+id,
        dataType: 'html',
        success: function(data){
            showResult('_os_modal', data);
            $('#_os_modal').modal({
                backdrop: 'static',
                keyboard: false,
            });
        }
    });
    return false;
}
function jobs_ac(el, type) {
    showLoader();
    var dataList = new FormData($('#'+el)[0]);
    dataList.append("url", 'jobs');
    dataList.append("type", type);
    $.ajax({
        url:'/action.php',
        type: 'POST',
        data: dataList,
        async: false,
        cache: false,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function(data){
            closeLoader();
            if(data.process==true && !isNaN(data.id)) {
                open_jobs('open', parseInt(data.id));
                $('#calendar').fullCalendar( 'refetchEvents' );
            }
            else alert(data.msg);
        },
        error: function() {
            closeLoader();
            alert('Thao tác không hợp lệ, vui lòng kiểm tra lại.');
        }
    });
    return false;
}
function jobs_done(id) {
    showLoader();
    confirm("Xác nhận công việc này đã hoàn thành?", function() {
        if(this.data == true) {
            $.ajax({
                url:'/action.php',
                type: 'POST',
                data: 'url=jobs&type=done&id='+parseInt(id),
                dataType: 'html',
                success: function(data){
                    open_jobs('open', parseInt(data));
                },
                error: function() {
                    closeLoader();
                    alert('Thao tác không hợp lệ, vui lòng kiểm tra lại.');
                }
            });
        } else {
            closeLoader();
            return false;
        }
    });
    return false;
}

function print_f(type) {
    var filter = '?filter=1';
    $( '#dataTablesList' ).find('input.filter').each(function() {
        var i = $(this).attr('data-column');
        var v = $(this).val();
        if (v.length > 0) {
            filter += '&col' + i + '=' + v;
        }
    });
    $( '#dataTablesList_filter' ).find('input[type="search"]').each(function() {
        var v = $(this).val();
        if (v.length > 0) {
            filter += '&search=' + v;
        }
    });

    window.location.href = '/print/' + type + filter;
}
function print_f_id(type,id) {
    var filter = '?filter=1';
    var valueId = '&id=' + id;
    $( '#dataTablesList' ).find('input.filter').each(function() {
        var i = $(this).attr('data-column');
        var v = $(this).val();
        if (v.length > 0) {
            filter += '&col' + i + '=' + v;
        }
    });
    $( '#dataTablesList_filter' ).find('input[type="search"]').each(function() {
        var v = $(this).val();
        if (v.length > 0) {
            filter += '&search=' + v;
        }
    });

    window.location.href = '/print/' + type + filter + valueId;
}

function modal_map(lng, lat) {
    showLoader();
    $.ajax({
        url: '/action.php',
        type: 'POST',
        data: 'url=map&lng='+lng+'&lat='+lat,
        dataType: 'html',
        success: function(data){
            showResult('_os_modal', data);
            $('#_os_modal').modal('show');
        }
    });
    return false;
}