<?php
@ob_start();
@session_start();
// System
define( 'TTH_SYSTEM', true );

$url = isset($_GET['url']) ? $_GET['url'] : 'home';
$path = array();
$path = explode('/',$url);
//----------------------------------------------------------------------------------------------------------------------
require_once(str_replace( DIRECTORY_SEPARATOR, '/', dirname( __file__ ) ) . '/define.php');
require_once(ROOT_DIR . DS ."lang" . DS . TTH_LANGUAGE . ".lang");
include_once(_F_FUNCTIONS . DS . "Function.php");
try {
    $db =  new ActiveRecord(TTH_DB_HOST, TTH_DB_USER, TTH_DB_PASS, TTH_DB_NAME);
}
catch(DatabaseConnException $e) {
    echo $e->getMessage();
}

require_once(_F_INCLUDES . DS . "_tth_constants.php");
require_once(ROOT_DIR . DS . '_ck_login.php');
$account["id"] = empty($_SESSION["user_id"]) ? 0 : intval($_SESSION["user_id"]);
require_once(_F_INCLUDES . DS . "_tth_online_daily.php");
//---------------------------------------------------
require_once(ROOT_DIR . DS . '_lock_screen.php');
//require_once(_F_INCLUDES . DS . "_tth_logs.php");
$tth[TTH_PATH] = isset($_GET[TTH_PATH]) ? $_GET[TTH_PATH] : 'home';
$tth[TTH_PATH_OP] = isset($_GET[TTH_PATH_OP]) ? $_GET[TTH_PATH_OP] : 'main';
if(isset($_GET['url'])) {
    include(_F_INCLUDES . DS . "_tth_url.php");
}
if($login_true && $lock_screen) {
    include("lock_screen.php");
} elseif(!$login_true && $tth[TTH_PATH]=='login') {
    include("login.php");
} else {
    // -- Bảng phân quyền :)
    $corePrivilegeSlug = array();
    $corePrivilegeSlug = corePrivilegeSlug($account["id"]);
    if ($tth[TTH_PATH] == 'print') {
        if (is_file(_F_MODULES . DS . $tth[TTH_PATH] . DS . str_replace('-','_', $tth[TTH_PATH_OP]) . '.php')) {
            if (!in_array($tth[TTH_PATH_OP], $corePrivilegeSlug['op'])) {
                echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
                loadPageError('Bạn không được phân quyền với chức năng này.', HOME_URL_LANG);
            } else include(_F_MODULES . DS . $tth[TTH_PATH] . DS . str_replace('-','_', $tth[TTH_PATH_OP]) . '.php');
        } else {
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
            loadPageError("Hiện tại chưa hỗ trợ chức năng này.", HOME_URL_LANG);
        }
    } else {
        $hide_left_bar = "";
        $merge_left = "";
        ?>
        <!DOCTYPE html>
        <html lang="<?php echo TTH_LANGUAGE; ?>">
        <head>
            <?php
            include(_F_INCLUDES . DS . "_tth_head.php");
            include(_F_INCLUDES . DS . "_tth_script.php");
            ?>
        </head>
        <body>
        <?php echo  getConstant('script_body') ?>
        <section id="container">
            <?php
            include(_F_INCLUDES . DS . "tth_header.php");
            include(_F_INCLUDES . DS . "tth_menu.php");
            ?>
            <section id="main-content" class="<?php echo $merge_left; ?>">
                <section id="wrapper" class="wrapper">
                    <?php
                    if (is_file(_F_MODULES . DS . $tth[TTH_PATH] . DS . str_replace('-','_', $tth[TTH_PATH_OP]) . '.php')) {
                        if (!in_array($tth[TTH_PATH], $corePrivilegeSlug['ol']) || !in_array($tth[TTH_PATH_OP], $corePrivilegeSlug['op'])) {
                            loadPageError('Bạn không được phân quyền với chức năng này.', HOME_URL_LANG);
                        } else include(_F_MODULES . DS . $tth[TTH_PATH] . DS . str_replace('-','_', $tth[TTH_PATH_OP]) . '.php');
                    } else {
                        loadPageError("Hiện tại chưa hỗ trợ chức năng này.", HOME_URL_LANG);
                    }
                    ?>
                </section>
                <?php
                include(_F_INCLUDES . DS . "tth_footer.php");
                ?>
            </section>
            <!-- Modal -->
            <div class="modal fade" id="_os_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">&nbsp;</div>
            <!-- /.Modal -->
        </section>
        <a href="javascript:void(0)" title="Lên đầu trang" id="go-top"></a>
        <div id="_loading"></div>
        <?php
        echo getConstant('google_analytics');
        echo getConstant('script_bottom');
        echo getConstant('chat_online');
        ?>
        </body>
        </html>
        <?php
    }
}