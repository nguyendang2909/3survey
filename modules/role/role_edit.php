<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['role']['link'] . '">' . $mmenu['role']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa nhóm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
$role_id        = isset($_GET['id']) ? intval($_GET['id']) : intval($role_id);
$db->table      = "core_role";
$db->condition  = "`is_active` = 1 AND `role_id` = " . $role_id;
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if($db->RowCount==0) loadPageError("Nhóm không tồn tại.", HOME_URL_LANG . $mmenu['role']['link']);


include_once (_F_TEMPLATES . DS . "role.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
	if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên nhóm.</span>';
	else {
		$db->table = "core_role";
		$data = array(
			'name'          => $db->clearText($name),
			'note'          => $db->clearText($note),
			'is_show'       => intval($is_show),
			'modified_time' => time(),
			'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`role_id` = $role_id";
		$db->update($data);

		//---- Role-User
		$user = isset($_POST['user']) ? $_POST['user'] : array();
        $user = array_keys(array_flip($user));

		$db->table = "role_user";
		$db->condition = "`role` = $role_id";
		$db->delete();
        if(count($user)>0) {
            for ($i = 0; $i < count($user); $i++) {
                $db->table = "role_user";
                $data = array(
                    'role'          => $role_id,
                    'user'          => $user[$i],
                    'created_time'  => time(),
                    'user_id'       => $_SESSION["user_id"]
                );
                $db->insert($data);
            }
        }

		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['role']['link']);
		$OK = true;

	}
}
else {
	foreach($rows as $row) {
		$name	    = $row['name'];
		$note       = $row['note'];
		$is_show    = $row['is_show'];
	}

	$user = array();
	$db->table = "role_user";
	$db->condition = "`role` = ".$role_id;
	$db->limit = "";
	$db->order = "";
	$rows = $db->select();
	foreach ($rows as $row) {
		array_push($user, $row['user']+0);
	}
}
if(!$OK) grAdmin(HOME_URL_LANG . $mmenu['role']['link'] . '/role-edit', "edit", $role_id, $name, $note, $user, $is_show, $error);