<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['role']['link'] . '">' . $mmenu['role']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm nhóm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
include_once (_F_TEMPLATES . DS . "role.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
	if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên nhóm.</span>';
	else {
		$db->table = "core_role";
		$data = array(
			'name'          => $db->clearText($name),
			'note'          => $db->clearText($note),
			'is_show'       => intval($is_show),
			'modified_time' => time(),
			'user_id'       => $_SESSION["user_id"]
		);
		$db->insert($data);
		$id_query = $db->LastInsertID;

		//---- Role-User
		$user = isset($_POST['user']) ? $_POST['user'] : array();
		$user = array_keys(array_flip($user));
		if(count($user)>0) {
            for($i=0; $i<count($user); $i++) {
                $db->table = "role_user";
                $data = array(
                    'role'          => $id_query,
                    'user'          => $user[$i],
                    'created_time'  => time(),
                    'user_id'       => $_SESSION["user_id"]
                );
                $db->insert($data);
            }
        }

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['role']['link']);
		$OK = true;
	}
}
else {
	$name	    = "";
	$note       = "";
	$is_show    = 1;
}
if(!$OK) grAdmin(HOME_URL_LANG . $mmenu['role']['link'] . '/role-add', "add", 0, $name, $note, array(), $is_show, $error);