<?php
if (!defined('TTH_SYSTEM')) {
	die('Please stop!');
}
//
$role_id = isset($_GET['id']) ? intval($_GET['id']) : intval($role_id);
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['role']['link'] . '">' . $mmenu['role']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . getTableCoreRole($role_id) . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
$db->table 		= "core_role";
$db->condition 	= "`role_id` = " . $role_id;
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if ($db->RowCount == 0) loadPageError("Nhóm quản trị không tồn tại.", HOME_URL_LANG . $mmenu['role']['link']);
include_once(_F_FUNCTIONS . DS . "CoreDashboard.php");
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dashboard fa-fw"></i> Phân quyền quản trị
				<span class="tools pull-right">
					<a href="javascript:;" class="fa fa-spinner fa-spin"></a>
					<a href="javascript:;" class="fa fa-chevron-down"></a>
					<a href="javascript:;" class="fa fa-eye"></a>
					<a href="javascript:;" class="fa fa-compress"></a>
					<a href="javascript:;" class="fa fa-times"></a>
				</span>
			</div>
			<!-- .panel-heading -->
			<div class="panel-body">
				<div class="panel-group panel-tabs-line">
					<?php
					foreach ($mmenu as $key => $val) {
						if ($key != 'home') {
							echo '<div class="panel panel-info">';
							echo '<div class="panel-heading clearfix"><h4 class="panel-title pull-left"><a data-toggle="collapse" data-parent="#accordion" href="#collapse-' . $key . '"><i class="fa ' . $val['icon'] . ' fa-fw"></i> ' . $val['title'] . '</a></h4><div class="pull-right">' . showStatusCoreOl($role_id, $key) . '</div></div>';
							if (is_file(_F_TEMPLATES . DS . "core" . DS . $key . ".php")) {
								echo '<div id="collapse-' . $key . '" class="panel-collapse collapse"><div class="panel-body">';
								include_once(_F_TEMPLATES . DS . "core" . DS . $key . ".php");
								echo '</div></div>';
							}
							echo '</div>';
						}
					}
					?>
				</div>
			</div>
			<!-- .panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script type="text/javascript">
	$(function() {
		$('.checked-all').change(function() {
			var rel = $(this).attr('rel');
			if ($(this).prop('checked')) {
				$('input.' + rel).each(function() {
					$(this).prop('checked', true);
				});
			} else {
				$('input.' + rel).each(function() {
					$(this).prop('checked', false);
				});
			}
		});
	});
</script>