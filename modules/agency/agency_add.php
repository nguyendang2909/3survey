<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['agency']['link'] . '">' . $mmenu['agency']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm đơn vị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
include_once (_F_TEMPLATES . DS . "agency.php");
if(empty($typeFunc)) $typeFunc = '-no-';

if(isset($_GET['parent']))
	$parent = intval($_GET['parent']);
elseif(isset($parent))
	intval($parent);
else
	$parent = 0;

$db->table 		= "agency";
$db->condition	= "`is_active` = 1 AND `agency_id` = " . $parent;
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0 && !empty($parent)) loadPageError("Mục không tồn tại.", HOME_URL_LANG . $mmenu['agency']['link']);

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    $user = isset($_POST['user']) ? $_POST['user'] : array();
	if(empty($symbol)) $error = '<span class="show-error">Vui lòng nhập Kí hiệu.</span>';
	elseif(empty($name)) $error = '<span class="show-error">Vui lòng nhập Tên đơn vị.</span>';
	else {
		$handleUploadImg	= false;
		$file_max_size		= FILE_MAX_SIZE;
		$dir_dest			= ROOT_DIR . DS . 'uploads' . DS . 'agency' . DS;
		$file_size			= $_FILES['img']['size'];

		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);

			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: ' . $imgUp->error . '</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
			$id_query = 0;
			$db->table = "agency";
			$data = array(
				'symbol' 		=> $db->clearText($symbol),
				'name' 			=> $db->clearText($name),
                'manager'       => $db->clearText(json_encode(array_values($user))),
				'address' 		=> $db->clearText($address),
				'phone' 		=> $db->clearText($phone),
				'email' 		=> $db->clearText($email),
				'parent' 		=> intval($parent),
                'note'          => $db->clearText($note),
				'sort' 			=> intval(sortAcs($parent) + 1),
				'is_show' 		=> intval($is_show),
				'created_time' 	=> time(),
				'user_id' 		=> $_SESSION["user_id"]
			);
			$db->insert($data);
			$id_query = $db->LastInsertID;

			if ($handleUploadImg) {
				$stringObj = new String();

				$img_name_file = $symbol . '_' . md5(uniqid());

				$imgUp->file_new_name_body = $img_name_file;
				$imgUp->image_resize = true;
				$imgUp->image_ratio_fill = true;
				$imgUp->image_x = 200;
				$imgUp->image_y = 200;
				$imgUp->Process($dir_dest);

				if ($imgUp->processed) {
					$db->table = "agency";
					$data = array(
					    'img' => $db->clearText($imgUp->file_dst_name)
					);
					$db->condition = "`agency_id` = " . $id_query;
					$db->update($data);
				} else {
					loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['agency']['link']);
				}

                $imgUp->file_new_name_body      = 'large-' . $img_name_file;
                $imgUp->image_resize            = true;
                $imgUp->image_ratio_fill        = true;
                $imgUp->image_x                 = 640;
                $imgUp->image_y                 = 400;
                $imgUp->Process($dir_dest);

				$imgUp->Clean();
			}

			loadPageSuccess("Đã thêm đơn vị thành công.", HOME_URL_LANG . $mmenu['agency']['link']);
			$OK = true;
		}
	}
}
else {
	$symbol			= "";
	$name			= "";
    $user           = array();
	$address		= "";
	$phone			= "";
	$email			= "";
	$img            = "";
	$note           = "";
	$is_show		= 1;
}
if(!$OK) agencyCore(HOME_URL_LANG . $mmenu['agency']['link'] . '/agency-add', "add", 0, $symbol, $name, $user, $address, $phone, $email, $parent, $img, $note, $is_show, $error);