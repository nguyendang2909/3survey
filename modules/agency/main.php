<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['agency']['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
if(isset($_GET['tick']) && in_array("agency;delete", $corePrivilegeSlug['op'])) {
    $tick = intval($_GET['tick']);
	if($tick>0) {
		$db->table = "agency";
		$data = array(
			'is_active'     => 0,
			'modified_time' => time(),
			'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`agency_id` = $tick";
		$db->update($data);
		loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['agency']['link']);
	}
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-left">
                        <a class="btn-add-new not-abs" href="<?php echo HOME_URL_LANG . $mmenu['agency']['link'] . '/agency-add?parent=0';?>">Thêm đơn vị</a>
                    </div>
                    <div class="pull-right btn-tool">
                        <button type="button" class="btn btn-export" onclick="return print_f('agency-export');">
                            <label class="icon">&nbsp;</label><label class="text">Xuất tệp...</label>
                        </button>
                    </div>
                </div>
            </div>
            <div id="dataTablesList" style="overflow: hidden;">
                <table class="easyui-treegrid" style="width:100%;" data-options="url: '/action.php?url=agency', method: 'get', lines: true, idField: 'id', treeField: 'name'">
                    <thead>
                    <tr>
                        <th data-options="field:'id'">ID</th>
                        <th data-options="field:'name'">Tên đơn vị</th>
                        <th data-options="field:'symbol'" width="120">Ký hiệu</th>
                        <th data-options="field:'sort'" width="100">Sắp xếp</th>
                        <th data-options="field:'show'" width="80" align="center">Hiển thị</th>
                        <th data-options="field:'tool'" width="120" align="center">Chức năng</th>
                        <th data-options="field:'link'" width="80" align="center">Nhân sự</th>
                    </tr>
                    </thead>
                </table>
            </div>
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-6 -->
</div>
<script>
    //---
    $('#dataTablesList').on('click', '.ol-confirm', function() {
        var id = parseInt($(this).attr("rel"));
        confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function () {
            if (this.data == true) window.location.href = '?tick=' + id;
        });
    }).on('click', '.ol-alert-core', function() {
        alert('Bạn không được phân quyền với chức năng này.');
    });
</script>