<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['agency']['link'] . '">' . $mmenu['agency']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa đơn vị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
include_once (_F_TEMPLATES . DS . "agency.php");
if(empty($typeFunc)) $typeFunc = '-no-';
$agency_id 		= isset($_GET['id']) ? intval($_GET['id']) : intval($agency_id);
$db->table 		= "agency";
$db->condition 	= "`is_active` = 1 AND `agency_id` = " . $agency_id;
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Mục không tồn tại.", HOME_URL_LANG . $mmenu['agency']['link']);

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    $user = isset($_POST['user']) ? $_POST['user'] : array();
	if(empty($symbol)) $error = '<span class="show-error">Vui lòng nhập Kí hiệu.</span>';
	elseif(empty($name)) $error = '<span class="show-error">Vui lòng nhập Tên đơn vị.</span>';
	else {
		$handleUploadImg = false;
		$file_max_size = FILE_MAX_SIZE;
		$dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'agency' . DS;
		$file_size = $_FILES['img']['size'];
		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);
			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: ' . $imgUp->error . '</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
			$db->table = "agency";
			$data = array(
				'symbol'		=> $db->clearText($symbol),
				'name'			=> $db->clearText($name),
                'manager'       => $db->clearText(json_encode(array_values($user))),
				'address'		=> $db->clearText($address),
				'phone'			=> $db->clearText($phone),
				'email'			=> $db->clearText($email),
                'parent' 		=> intval($parent),
                'note'          => $db->clearText($note),
				'is_show'		=> intval($is_show),
				'modified_time' => time(),
				'user_id'		=> $_SESSION["user_id"]
			);
			$db->condition = "`agency_id` = " . $agency_id;
			$db->update($data);

			if($handleUploadImg) {
				$stringObj = new String();
				if(glob($dir_dest .'*'.$img)) array_map("unlink", glob($dir_dest .'*'.$img));

				$img_name_file = $symbol . '_' . md5(uniqid());
				$imgUp->file_new_name_body      = $img_name_file;
				$imgUp->image_resize            = true;
				$imgUp->image_ratio_fill        = true;
				$imgUp->image_x                 = 200;
				$imgUp->image_y                 = 200;
				$imgUp->Process($dir_dest);
				if($imgUp->processed) {
					$db->table = "agency";
					$data = array(
                        'img' => $db->clearText($imgUp->file_dst_name)
					);
					$db->condition = "`agency_id` = " . $agency_id;
					$db->update($data);
				}
				else {
					loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['agency']['link']);
				}

                $imgUp->file_new_name_body      = 'large-' . $img_name_file;
                $imgUp->image_resize            = true;
                $imgUp->image_ratio_fill        = true;
                $imgUp->image_x                 = 640;
                $imgUp->image_y                 = 400;
                $imgUp->Process($dir_dest);

				$imgUp-> Clean();
			}

			loadPageSuccess("Đã chỉnh sửa đơn vị thành công.", HOME_URL_LANG . $mmenu['agency']['link']);
			$OK = true;
		}
	}
}
else {
	foreach($rows as $row) {
		$symbol     = $row['symbol'];
		$name       = $row['name'];
        $user      	= json_decode($row['manager']);
		$address    = $row['address'];
		$phone      = $row['phone'];
		$email      = $row['email'];
		$parent     = $row['parent'];
		$img        = $row['img'];
		$note       = $row['note'];
		$is_show	= $row['is_show'];
	}
}
if(!$OK) agencyCore(HOME_URL_LANG . $mmenu['agency']['link'] . '/agency-edit', "edit", $agency_id, $symbol, $name, $user, $address, $phone, $email, $parent, $img, $note, $is_show, $error);
