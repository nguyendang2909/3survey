<?php
include_once(_F_TEMPLATES . DS . "east_sea_report.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['opinion']['link'] . '">' . $mmenu['opinion']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Báo cáo Biển Đông</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  if (empty($startDate) || $startDate == '__/__/____'    || empty($endDate) || $endDate == '') $error = '
    <span class="show-error">Vui lòng nhập đủ thông tin.</span>
  ';
  else $OK = true;



  if ($OK) {
    $formattedStartDate = date("Y-m-d", strtotime($date->dmYtoYmd($startDate)));
    $formattedEndDate = date("Y-m-d", strtotime($date->dmYtoYmd($endDate)));

    // $db->table = "field_situation";
    // $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
    //                LEFT JOIN `" . TTH_DATA_PREFIX . "local` c ON a.`localId` = c.`local_id`
    //                LEFT JOIN `" . TTH_DATA_PREFIX . "country` d ON a.`countryId` = d.`countryId`
    //                LEFT JOIN `" . TTH_DATA_PREFIX . "ship` e ON a.`shipId` = e.`shipId`
    //                ";
    // $db->condition = '
    //   a.`isActive` = 1 AND
    //   c.`is_active` = 1 AND
    //   d.`isActive` = 1 AND
    //   e.`isActive` = 1 AND
    //   a.`time` BETWEEN DATE("' . $formattedStartDate .'") AND DATE("'. $formattedEndDate .'")
    //   ';
    // $db->order = '';
    // $db->limit = '';
    // $fieldSituationRows = $db->select("
    //   a.`fieldSituationId`,
    //   a.`time`,
    //   a.`purpose`,
    //   a.`note`,
    //   c.`title` as `local`,
    //   d.`file` AS `countryIcon`,
    //   d.`name` as `country`,
    //   e.`name` as `ship`,
    //   a.`file`
    // ");



    // $db->table = "opinion";
    // $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`
    //                LEFT JOIN `" . TTH_DATA_PREFIX . "country` d ON a.`countryId` = d.`countryId`
    //                ";
    // $db->condition = '
    // a.`isActive` = 1 AND
    // d.`isActive` = 1 AND
    // a.`time` BETWEEN DATE("' . $formattedStartDate .'") AND DATE("'. $formattedEndDate .'")
    // ';
    // $db->order = 'a.`time` ASC';
    // $db->limit = '';
    // $opinionRows = $db->select("
    //   a.`note`,
    //   d.`name` as `country`
    // ");
    // // var_dump($opinionRows);

    // $opinionCountries = array();
    // foreach($opinionRows as $key => $val) {
    //   if ($key != '-') $opinionCountries[$val['country']][] = $val['note'];
    // }


    




    
    

    
    
?>
  <div class="row">
    <div class="col-lg-12 col-md-9">
      <div class="panel">
        <div class="panel-heading text-center">
          <i class="fa fa-sitemap fa-fw"></i> Báo cáo Biển Đông từ ngày <?php echo $startDate ?> đến ngày <?php echo $endDate ?>
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td>
                    <b>1. Tình hình trên thực địa</b>
                  </td>
                </tr>

                <tr>
                  <td>
                    <b>2. Quan điểm, lập trường, chính sách về Biển Đông</b>
                    <br><?php
                    foreach($opinionCountries as $key => $val) {
                      echo '<ul class="list-group">' . $key;
                      foreach($val as $value) {
                        echo '<li class="list-group-item">- ' . $value . '</li>';
                      };
                      echo '</ul>';

    }
    ?>
                  </td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Xuất file Word</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['east-sea-report']['link']; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
 }
  // // Validate data
  // if (empty($time) || $time == '__/__/____' || empty($countryId) || $countryId == '') $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  // else {
  //   $db->table = "opinion";
  //   $data = array(
  //     'time' => date("Y-m-d", strtotime($date->dmYtoYmd($time))),
  //     'countryId' => intval($countryId),
  //     'source' => stripslashes($source),
  //     'note' => stripslashes($note),
  //     'createdBy' => $_SESSION["user_id"],
  //     'updatedBy' => $_SESSION["user_id"],
  //     'createdAt' => date("Y-m-d H:i:s"),
  //     'updatedAt' => date("Y-m-d H:i:s"),
  //   );
  //   $db->insert($data);

  //   loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['opinion']['link']);
  //   $OK = true;
  // }
} else {
  $startDate = '';
  $endDate = '';
}
if (!$OK) postEastSeaReport(
  HOME_URL_LANG . $mmenu['east-sea-report']['link'],
  "add",
  $startDate,
  $endDate,
  $error
);
