<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a  class="current">' . $mmenu['folder']['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

echo '<div class="row"><div class="col-lg-12"><iframe frameborder="0" scrolling="auto" style="border:none; width:100%; height:502px;" src="' . HOME_URL . '/editor/elfinder/elfinder.html"></iframe></div></div>';

$dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'folder';
$dirs = array_filter(glob($dir_dest . DS . '*'), 'is_dir');
$permission = array();
foreach ($dirs as $value) {
    $checked = '';
    $value = str_replace($dir_dest . DS, '', $value);
    if (!in_array($value, $corePrivilegeSlug['op'])) {
        $permission[] = array(
            'pattern' => '/' . $value . '/',
            'read' => false,
            'write' => false,
            'hidden' => true,
            'locked' => false
        );
    }
}
print_r($permission);