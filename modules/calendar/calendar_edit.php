<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link'] . '">' . $mmenu['calendar']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link'] . '">' . $mmenu['calendar']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa lịch</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$calendar_id = isset($_GET['id']) ? intval($_GET['id']) : intval($calendar_id);
$db->table      = "calendar";
$db->condition  = "`calendar_id` = $calendar_id";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link']);

include_once (_F_TEMPLATES . DS . "calendar.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
$date = new DateClass();
if($typeFunc=='edit'){
    $week 	= isset($_POST['week']) ? intval($_POST['week']) : 0;
	$month 	= isset($_POST['month']) ? intval($_POST['month']) : 0;
	$year 	= isset($_POST['year']) ? intval($_POST['year']) : 0;
    
	$file_max_size	= FILE_MAX_SIZE;
	$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'calendar' . DS;

	$file_name      = 'os' . time() . '_' . md5(microtime(true));
	$file_size      = $_FILES['files']['size'];
	if ($file_size > 0) {
		$fileUp = new Upload($_FILES['files']);
		$fileUp->file_max_size = $file_max_size;
		$fileUp->file_new_name_body = $file_name;
		$fileUp->Process($dir_dest);
		if($fileUp->processed) {
			$file_name = $fileUp->file_dst_name;
			$OK = true;
		} else {
			$error = '<span class="show-error">Lỗi tải tệp tin: '.$fileUp->error.'</span>';
		}
	} else {
		$OK = true;
		foreach($rows as $row) {
			$file_name = stripslashes($row['files']);
		}
	}
	
	if($OK) {
		$db->table = "calendar";
		$data = array(
			'type'  	    => intval($type),
			'week'       	=> $week,
			'month'       	=> $month,
			'year'       	=> $year,
			'files'       	=> $db->clearText($file_name),
			'note'          => $db->clearText($note),
			'modified_time' => time(),
			'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`calendar_id` = $calendar_id";
		$db->update($data);
		
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link']);
	}
	$OK = true;
}
else {
	foreach($rows as $row) {
        $type  		= $row['type'];
		$week    	= $row['week'];
		$month     	= $row['month'];
		$year     	= $row['year'];
        $files      = $row['files'];
        $note       = $row['note'];
	}
}
if(!$OK) calendar(HOME_URL_LANG . $mmenu['calendar']['link'] . '/calendar-edit', "edit", $calendar_id, $type, $week, $month, $year, $files, $note, $error);