<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['calendar']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['calendar']['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if(isset($_POST['tick']) && in_array("calendar;delete", $corePrivilegeSlug['op'])) {
    $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
    $tick = array_filter($tick);
    if(count($tick)>0) {
        $tick = implode(',', $tick);
        $db->table = "calendar";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`calendar_id` IN ($tick)";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link']);
    }
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-left">
						<a class="btn-add-new not-abs" href="<?php echo HOME_URL_LANG . $mmenu['calendar']['link'] . '/calendar-add';?>">Thêm lịch</a>
                    </div>
                </div>
            </div>
			<div class="table-responsive">
                <form method="post" id="_ol_delete">
					<table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
						<thead>
						<tr>
							<th>ID</th>
							<th>Loại lịch</th>
							<th>Thời gian</th>
							<th>Tệp tin</th>
                            <th width="250px">Ghi chú</th>
                            <th>Ngày cập nhật</th>
                            <th>Người cập nhật</th>
                            <th width="80px">Chọn</th>
						</tr>
						</thead>
						<thead>
						<tr>
							<td align="center">-</td>
                            <td><input type="text" data-column="1" class="form-control filter"></td>
                            <td><input type="text" data-column="2" class="form-control filter auto-number" data-a-sep="" data-v-max="9000" data-v-min="0" maxlength="4"></td>
                            <td><input type="text" data-column="3" class="form-control filter"></td>
                            <td><input type="text" data-column="4" class="form-control filter"></td>
                            <td><input type="text" data-column="5" class="form-control filter input-date text-center"></td>
                            <td><input type="text" data-column="6" class="form-control filter"></td>
                            <td align="center">-</td>
						</tr>
						</thead>
					</table>
                    <?php
                    if(in_array("calendar;delete", $corePrivilegeSlug['op']))
                        echo '<div class="row"><div class="col-sm-12" align="right"><label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label><input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá lịch" name="delete"></div></div>';
                    ?>
				</form>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-8 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTablesList tfoot th').each( function () {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );
		var table = $('#dataTablesList').DataTable( {
			"language": {
				"url": "/js/data-tables/de_DE.txt"
			},
			"lengthMenu": [100, 200, 300],
			"info":     false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: '/action.php',
				type: 'POST',
				data: {
					url: 'calendar_list',
					type: 'load'
				}
			},
            "columns": [
                { data: 'no' },
                { data: 'type' },
                { data: 'calendar' },
                { data: 'file' },
                { data: 'note' },
                { data: 'time' },
                { data: 'user' },
                { data: 'tool' }
            ],
			"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
				$('td:eq(0)', nRow).css( "text-align", "center" );
                $('td:eq(3)', nRow).css( "text-align", "center" );
                $('td:eq(5)', nRow).css( "text-align", "center" );
                $('td:eq(7)', nRow).css( "text-align", "center" );
				return nRow;
			},
            "fnDrawCallback": function () {
                $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
            },
			"order": [ 0, "desc" ],
			"aoColumnDefs" : [ {
				'targets': [7],
				'searchable':false,
				'orderable':false
			} ]
		});
		// Apply the search
        $( '.filter' ).on( 'change', function () {
            var i = $(this).attr('data-column');
            var v = $(this).val();
            table.columns(i).search(v).draw();
        });
	});
    //---
    $('.input-date').datetimepicker({
        lang: 'vi',
        timepicker: false,
        format: '<?php echo TTH_DATE_FORMAT;?>'
    });
    $(".ol-confirm").click(function() {
        confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
            if(this.data == true) document.getElementById("_ol_delete").submit();
        });
    });
    $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>