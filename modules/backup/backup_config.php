<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['backup']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['backup']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if(isset($_POST['update'])) {
	function updateConstant ($constant, $value) {
		global $db;
		$db->table = "constant";
		$data =array(
			'value'=>$db->clearText($value)
		);
		$db->condition = "constant = '".$constant."'";
		$db->update($data);
	}

	//updateConstant("backup_auto",$_POST['backup_auto']);
	updateConstant("backup_filetype", $_POST['backup_filetype']);
	updateConstant("backup_filecount",$_POST['backup_filecount']);
	updateConstant("backup_email",$_POST['backup_email']);
	loadPageSuccess("Đã cập nhật thông tin cấu hình thành công.", HOME_URL_LANG . $mmenu['backup']['link'] . $mmenu['backup']['sub'][1]['link']);
}
?>
<div class="row">
	<div class="col-lg-6 col-md-10">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-database fa-fw"></i> <?php echo $mmenu['backup']['sub'][1]['title'];?>
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form method="post" class="form-ol-3w">
						<table class="table table-no-border table-hover">
							<tr>
								<td width="200px" align="right"><label class="form-lb-tp">Tự động sao lưu:</label></td>
								<td>
									<select disabled name="backup_auto" class="form-control">
										<option value="none" <?php echo (getConstant("backup_auto")=='none')? "selected" : "" ?> >Không</option>
										<option value="day" <?php echo (getConstant("backup_auto")=='day')? "selected" : "" ?> >Hằng ngày</option>
										<option value="week" <?php echo (getConstant("backup_auto")=='week')? "selected" : "" ?> >Hằng tuần</option>
										<option value="month" <?php echo (getConstant("backup_auto")=='month')? "selected" : "" ?> >Hằng tháng</option>
									</select>
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Định dạng lưu file CSDL:</label></td>
								<td>
									<select name="backup_filetype" class="form-control">
										<option value="sql" <?php echo (getConstant("backup_filetype")=='sql')? "selected" : "" ?> >.sql</option>
										<option value="sql.gz" <?php echo (getConstant("backup_filetype")=='sql.gz')? "selected" : "" ?> >.gz</option>
									</select>
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Số file CSDL lưu lại:</label></td>
								<td><input class="form-control auto-number" type="text" maxlength="3" data-a-sep="" data-v-max="999" data-v-min="0" name="backup_filecount" value="<?php echo (getConstant("backup_filecount")+0==0) ? 1 : getConstant("backup_filecount") ?>"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Email nhận thông báo và file:</label><br>(để trống khi không muốn gửi)</td>
								<td><input class="form-control" type="email" maxlength="200" name="backup_email" value="<?php echo getConstant("backup_email")?>"></td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" name="update" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='/';">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>