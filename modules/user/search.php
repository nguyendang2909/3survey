<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Tìm kiếm dữ liệu</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
$key = isset($_GET['key']) ? trim($_GET['key']) : '';
//---
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-no-border">
            <div class="table-responsive">
                <form method="post" id="_ol_delete">
                    <table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
                        <thead>
                        <tr>
                            <th width="70px">STT</th>
                            <th>Nhóm</th>
                            <th>Tiêu đề</th>
                            <th width="80px">Chọn</th>
                        </tr>
                        </thead>
                    </table>
                </form>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-8 -->
</div>
<script>
    $(document).ready(function() {
        var table = $('#dataTablesList').DataTable( {
            "language": {
                "url": "/js/data-tables/de_DE.txt"
            },
            "lengthMenu": [100, 200, 300],
            "searching" : false,
            "info"      : false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: '/action.php',
                type: 'POST',
                data: {
                    url: 'search',
                    type: 'load',
                    key: '<?php echo $key;?>'
                }
            },
            "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                $('td:eq(0)', nRow).css( "text-align", "center" );
                $('td:eq(3)', nRow).css( "text-align", "center" );
                return nRow;
            },
            "aoColumnDefs" : [ {
                'bSortable' : false,
                'aTargets'  : [ "_all" ]
            } ]
        });
    });
</script>