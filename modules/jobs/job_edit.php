<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['jobs']['link'] . '">' . $mmenu['jobs']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa công việc</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$jobs_id = isset($_GET['id']) ? intval($_GET['id']) : intval($jobs_id);
$db->table      = "jobs";
$db->condition  = "`jobs_id` = $jobs_id";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['jobs']['link'] . $mmenu['jobs']['sub'][2]['link']);

include_once (_F_TEMPLATES . DS . "jobs.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
$date = new DateClass();
if($typeFunc=='edit'){
    $list   = isset($_POST['list']) ? $_POST['list'] : array();
    $list   = array_keys(array_flip($list));
    $begin 	= isset($_POST['begin']) ? trim($_POST['begin']) : $date->vnDateTime(time());
    $end 	= isset($_POST['end']) ? trim($_POST['end']) : $date->vnDateTime(time());
    if(count($list)==0) $error = '<span class="show-error">Vui lòng chọn người nhận việc.</span>';
    else {
        $file_max_size	= FILE_MAX_SIZE;
        $dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'jobs' . DS;

        $f_name = array();
        if(isset($_FILES['files'])) {
            $images = array();
            foreach ($_FILES['files'] as $k => $l) {
                foreach ($l as $i => $v) {
                    if (!array_key_exists($i, $images))
                        $images[$i] = array();
                    $images[$i][$k] = $v;
                }
            }
            foreach ($images as $image) {
                $filename = substr($image['name'], 0, strrpos($image['name'], '.'));
                $fileUp = new Upload($image);
                $fileUp->file_max_size = $file_max_size;
                $fileUp->file_new_name_body = $filename;
                $fileUp->Process($dir_dest);
                if($fileUp->processed) {
                    array_push($f_name, $fileUp->file_dst_name);
                    $OK = true;
                } else {
                    $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
                }
            }

        } else {
            $OK = true;
        }
		
		$begin 	= $date->dmYtoYmd($begin);
        $end 	= $date->dmYtoYmd($end);
        if(strtotime($begin) > strtotime($end))
            $end = date('Y-m-d H:i:s', strtotime($begin) + 43200);

        if(empty($f_name)) {
            foreach($rows as $row) {
                $f_name = stripslashes($row['files']);
            }
        } else
            $f_name = json_encode($f_name, JSON_UNESCAPED_UNICODE);
		
		if($OK) {
			$db->table = "jobs";
			$data = array(
                'title'         => $db->clearText($title),
				'list_to'       => $db->clearText(json_encode($list)),
				'level'         => intval($level),
				'begin'         => $db->clearText($begin),
				'end'           => $db->clearText($end),
				'files'         => $db->clearText($f_name),
				'note'          => $db->clearText($note),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->condition = "`jobs_id` = $jobs_id";
			$db->update($data);
			
			loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['jobs']['link'] . $mmenu['jobs']['sub'][2]['link']);
		}
		$OK = true;
	}
}
else {
	foreach($rows as $row) {		
		$title 		= $row['title'];
		$list		= json_decode($row['list_to']);
		$level		= $row['level'];
		$begin		= $date->vnDateTime(strtotime($row['begin']));
		$end		= $date->vnDateTime(strtotime($row['end']));
		$files		= json_decode($row['files']);
		$note		= $row['note'];
	}
}
if(!$OK) jobs(HOME_URL_LANG . $mmenu['jobs']['link'] . '/job-edit', "edit", $jobs_id, $title, $list, $level, $begin, $end, $files, $note, $error);