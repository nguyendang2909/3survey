<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['jobs']['link'] . '">' . $mmenu['jobs']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['jobs']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---


if(isset($_POST['tick']) && in_array("jobs;delete", $corePrivilegeSlug['op'])) {
    $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
    $tick = array_filter($tick);
    if(count($tick)>0) {
        $tick = implode(',', $tick);
        $db->table = "jobs";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`jobs_id` IN ($tick)";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['jobs']['link'] . $mmenu['jobs']['sub'][2]['link']);
    }
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-right btn-tool">
                        <button type="button" class="btn btn-export" onclick="print_f('device-item');">
                            <label class="icon">&nbsp;</label><label class="text">Xuất tệp...</label>
                        </button>
                    </div>
                </div>
            </div>
			<div class="table-responsive">
                <form method="post" id="_ol_delete">
					<table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
						<thead>
						<tr>
							<th>ID</th>
							<th>Tên công việc</th>
                            <th>Người nhận</th>
                            <th>Ưu tiên</th>
                            <th>Bắt đầu</th>
                            <th>Kết thức</th>
                            <th>Trạng thái</th>
                            <th width="100px">Chọn</th>
						</tr>
						</thead>
						<thead>
						<tr>
							<td align="center">-</td>
                            <td><input type="text" data-column="1" class="form-control filter"></td>
                            <td><input type="text" data-column="2" class="form-control filter"></td>
                            <td><select data-column="3" class="form-control filter"><option value="">Lọc...</option><option value="1">Bình thường</option><option value="2">Khá</option><option value="3">Cao</option></select></td>
                            <td><input type="text" data-column="4" class="form-control filter input-date text-center"></td>
                            <td><input type="text" data-column="5" class="form-control filter input-date text-center"></td>
                            <td><select data-column="6" class="form-control filter"><option value="">Lọc...</option><option value="1">Đã hoàn thành</option><option value="2">Đang tiến hành</option><option value="3">Trễ thời hạn</option></select></td>
                            <td align="center">-</td>
                        </tr>
						</thead>
					</table>
                    <?php
                    if(in_array("jobs;delete", $corePrivilegeSlug['op']))
                        echo '<div class="row"><div class="col-sm-12" align="right"><label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label><input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá công việc" name="delete"></div></div>';
                    ?>
				</form>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-8 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTablesList tfoot th').each( function () {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );
		var table = $('#dataTablesList').DataTable( {
			"language": {
				"url": "/js/data-tables/de_DE.txt"
			},
			"lengthMenu": [100, 200, 300],
			"info":     false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: '/action.php',
				type: 'POST',
				data: {
					url: 'my_add',
					type: 'load'
				}
			},
			"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
				$('td:eq(0)', nRow).css( "text-align", "center" );
                $('td:eq(3)', nRow).css( "text-align", "center" );
                $('td:eq(4)', nRow).css( "text-align", "center" );
                $('td:eq(5)', nRow).css( "text-align", "center" );
                $('td:eq(7)', nRow).css( "text-align", "center" );
				return nRow;
			},
            "fnDrawCallback": function () {
                $("#dataTablesList").on('click', '.job-open', function(e) {
                    open_jobs('open', parseInt($(this).attr("rel")));
                });
            },
			"order": [ 0, "desc" ],
			"aoColumnDefs" : [ {
				'targets': [7],
				'searchable':false,
				'orderable':false
			} ]
		});
		// Apply the search
        $( '.filter' ).on( 'change', function () {
            var i = $(this).attr('data-column');
            var v = $(this).val();
            table.columns(i).search(v).draw();
        });
	});
    //---
    $('.input-date').datetimepicker({
        lang: 'vi',
        timepicker: false,
        format: '<?php echo TTH_DATE_FORMAT;?>'
    });
    $(".ol-confirm").click(function() {
        confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
            if(this.data == true) document.getElementById("_ol_delete").submit();
        });
    });
    $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>