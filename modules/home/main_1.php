<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
    function jobsLevel($choice) {
        $color = array(
            0 => 'green',
            1 => 'orange',
            2 => 'red'
        );
        return '<span class="lb-level ' . $color[$choice] . '">&nbsp;</span>';
    }
?>
<div class="row">
    <div class="col-md-12">
        <!-- page start-->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-pencil-square-o fa-fw"></i> Lịch công việc của bạn &nbsp;
                <span class="tools pull-right">
                <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
                <a href="javascript:;" class="fa fa-chevron-down"></a>
                <a href="javascript:;" class="fa fa-eye"></a>
                <a href="javascript:;" class="fa fa-compress"></a>
                <a href="javascript:;" class="fa fa-times"></a>
            </span>
            </div>
            <div class="panel-body">
                <!-- page start-->
                <div class="row">
                    <aside class="col-lg-2 jobs-event">
                        <?php
                        $date   = new DateClass();
                        $string = array();
                        $code = '"' . intval($account["id"]) . '"';
                        $db->table = "agency";
                        $db->condition = "`is_active` = 1 AND `manager` LIKE '%$code%'";
                        $db->order = "`sort` ASC";
                        $db->limit = "";
                        $rows = $db->select("`agency_id`");
                        if($db->RowCount>0) {
                            foreach($rows as $row) {
                                array_push($string, '"a' . $row["agency_id"] . '"');
                            }
                        }

                        array_push($string, '"u' . intval($account["id"]) . '"');

                        $string = implode("|", $string);

						echo '<h4 class="drg-event-title">Công việc trong ngày</h4>';
						// ----
                        $code = '"u' . intval($account["id"]) . '"';
                        $db->table = "jobs";
                        $db->condition = "`is_active` = 1 AND `done` = 0 AND (`list_to` REGEXP '$string' OR  `forward` LIKE '%$code%') AND `end` >= '" . date("Y-m-d") . "' AND `begin` < '" .  date("Y-m-d", strtotime('+1 days')) . "'";
                        $db->order = "`begin` DESC";
                        $db->limit = "";
                        $rows = $db->select();
                        if($db->RowCount>0) {
                            echo '<ul class="event-category">';
                            foreach($rows as $row) {
                                echo '<li><a href="javascript:;" onclick="return open_jobs(\'open\', ' . $row['jobs_id'] . ');">' . jobsLevel($row['level']) . stripslashes($row['title']) . '</a></li>';
                            }
                            echo '</ul>';
                        }

                        echo '<h4 class="drg-event-title">Công việc hoàn thành</h4>';
                        // ----
                        $db->table = "jobs";
                        $db->condition = "`is_active` = 1 AND `done` = 1 AND (`list_to` REGEXP '$string' OR  `forward` LIKE '%$code%') AND `end` >= '" . date("Y-m-d") . "' AND `begin` < '" .  date("Y-m-d", strtotime('+1 days')) . "'";
                        $db->order = "`begin` DESC";
                        $db->limit = "";
                        $rows = $db->select();
                        if($db->RowCount>0) {
                            echo '<ul class="event-category">';
                            foreach($rows as $row) {
                                echo '<li><a href="javascript:;" onclick="return open_jobs(\'open\', ' . $row['jobs_id'] . ');">' . jobsLevel($row['level']) . stripslashes($row['title']) . '</a></li>';
                            }
                            echo '</ul>';
                        }
                        ?>
                    </aside>
                    <aside class="col-lg-10">
                        <div id="calendar" class="has-toolbar"></div>
                    </aside>
                </div>
                <!-- page end-->
            </div>
        </div>
        <!-- page end-->
    </div>
</div>
<!-- /.row -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/fullcalendar/fullcalendar.min.css">
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/fullcalendar/locale-all.js"></script>
<script type="text/javascript">
    $(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultView: 'basicWeek',
            disableDragging: true,
            editable: false,
            navLinks: true,
            views: {
                month: {
                    eventLimit: true
                },
                agenda: {
                    eventLimit: false
                }
            },
            scrollTime: "07:00:00",
            locale: 'vi',
            events: function(start, end, timezone, callback) {
                $.ajax({
                    url: '/action.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        url: 'my_job',
                        type: 'load',
                        start: start.unix(),
                        end: end.unix()
                    },
                    async: false,
                    success: function(s) {
                        callback(s);
                    }
                });
            },
            eventClick: function(event) {
                if (event.id) {
                    open_jobs('open', event.id);
                    return false;
                }
            },
            eventRender: function(event, eventElement) {
                if (event.icon) {
                    eventElement.find(".fc-title").prepend('<i class="fa ' + event.icon + '"></i> ');
                }
            },
            axisFormat: 'HH:mm',
            timeFormat: 'H(:mm)'
        });
    });
</script>
<?php
} else {
$stringObj = new String();

echo '<div class="row"><div class="col-lg-12">';
$db->table = "blog_post";
$db->condition = "`is_active` = 1";
$db->order = "`hot` DESC, `created_time` DESC";
$db->limit = 20;
$rows = $db->select();
if($db->RowCount>0) {
    echo '<div id="_blog" class="public-list clearfix">';
    foreach($rows as $row) {
        $photo_avt = '';
        $slug = getSlugBlogParent0($row['blog']);
        $p_user = getInfoUser2($row['user_id']);

        if(file_exists(ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS . stripslashes('post-' . $row['img'])) && !empty($row['img'])) {
            $photo_avt = '<img src="'. HOME_URL .'/uploads/blog/post-'. stripslashes($row['img']) . '" alt="' . stripslashes($row['name']) . '">';
        } else {
            $photo_avt = '<img src="'. HOME_URL .'/images/404-post.jpg" alt="' . stripslashes($row['name']) . '">';
        }
        $photo_avt = '<div class="img"><a href="'. HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . $photo_avt . '</a></div>';

        echo '<div class="blog-item"><div class="blog-box">';
        echo $photo_avt;
        echo '<div class="blog-description">';
        echo '<div class="blog-text">';
        echo '<h2 class="blog-title"><a href="'. HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
        echo '<p class="blog-comment">' . $stringObj->crop(stripslashes($row['comment']), 35) . '</p>';
        echo '</div>';
        echo '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $slug) . '</div>';
        if(!empty($p_user)) echo '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
        echo '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
        echo '</div>';
        echo '</div></div>';
    }
    echo '</div>';

    echo '<div id="fb_loading" class="clearfix">';
    echo '<div class="fb-loading-cell"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
    echo '<div class="fb-loading-cell"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
    echo '<div class="fb-loading-cell"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
    echo '</div>';
}
echo '</div></div>';
?>
<script type="text/javascript">
    var page = 1;
    var is_busy = false;
    var stopped = false;
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height()) {
            if (is_busy == true){
                return false;
            }
            if (stopped == true){
                return false;
            }
            is_busy = true;
            page++;
            $('#fb_loading').show();
            $.ajax({
                url         : '/action.php',
                type        : 'POST',
                data        : {
                    'url'   : 'post',
                    'page'  : page,
                    'menu'  : 0,
                    'gird'  : 1
                },
                dataType    : 'json',
                success     : function (data) {
                    if(parseInt(data.limit)>0)
                        stopped = false;
                    else
                        stopped = true;
                    $('#_blog').append(data.post);
                },
                statusCode: {
                    401: function() {
                        window.location.href = '/?ol=login';
                    }
                }
            }).always(function() {
                $('#fb_loading').hide();
                is_busy = false;
            });
            return false;
        }
    });
</script>
<?php
}
