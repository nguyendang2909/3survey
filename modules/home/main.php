<?php
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> PHIẾU TRƯNG CẦU Ý KIẾN

          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="militaryAgencyId" value="<?php echo $militaryAgencyId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div
                    <center>
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['local']['link']; ?>'">DÙNG CHO CÁN BỘ, CNVC THUỘC CÁC CƠ QUAN, BAN NGÀNH ĐỊA PHƯƠNG</button></center>
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['military']['link']; ?>'">DÙNG CHO SQ, QNCN, CNVCQP THUỘC CÁC CƠ QUAN, ĐƠN VỊ TRONG QUÂN ĐỘI</button></center>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
?>