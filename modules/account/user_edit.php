<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['account']['link'] . '">' . $mmenu['account']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa thành viên</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
$user_id = isset($_GET['id']) ? intval($_GET['id']) : intval($user_id);
$db->table 		= "core_user";
$db->condition	= "`is_active` = 1 AND `user_id` = $user_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0 || $user_id==1) loadPageError("Thành viên không tồn tại.", HOME_URL_LANG . $mmenu['account']['link']);

include_once (_F_TEMPLATES . DS . "user.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$date = new DateClass();
$OK = false;
$error = '';
if($typeFunc=='edit'){
    $user_error = true;
    $db->table = "core_user";
    $db->condition = "user_name LIKE '$user_name'";
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach ($rows as $row) {
            if(intval($row['user_id'])==$user_id) $user_error = false;
        }
    }

    if($user_error) $error = '<span class="show-error">Tên đăng nhập này đã tồn tại.</span>';
    else {
        $handleUploadImg = false;

        $file_max_size = FILE_MAX_SIZE;
        $dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'user';
        $file_size = $_FILES['img']['size'];
        if($file_size>0) {
            $imgUp = new Upload($_FILES['img']);
            $imgUp->file_max_size = $file_max_size;
            if ($imgUp->uploaded) {
                $handleUploadImg = true;
                $OK = true;
            }
            else {
                $error = '<span class="show-error">Lỗi tải hình: '.$imgUp->error.'</span>';
            }
        }
        else {
            $handleUploadImg = false;
            $OK = true;
        }

        if($OK) {
            $full_name = mb_convert_case($full_name, MB_CASE_TITLE, "UTF-8");

            $list = isset($_POST['list']) ? $_POST['list'] : array();
            $db->table = "core_user";
            if(empty($password)) {
                $data = array(
                    'user_name'     => $db->clearText($user_name),
                    'full_name'     => $db->clearText($full_name),
                    'gender'        => intval($gender),
                    'birthday'      => $date->dmYtoYmd2($birthday),
                    'identity_card' => $db->clearText($identity_card),
                    'date_of_issue' => $date->dmYtoYmd2($date_of_issue),
                    'email'         => $db->clearText($email),
                    'phone'         => $db->clearText($phone),
                    'address'       => $db->clearText($address),
                    'city'          => intval($city),
                    'agency'        => $db->clearText(json_encode($list)),
                    'note'          => $db->clearText($note),
                    'is_show'       => intval($is_show),
                    'modified_time' => time(),
                    'user_id_edit'  => $_SESSION["user_id"]
                );
            } else {
                $data = array(
                    'user_name'     => $db->clearText($user_name),
                    'password'      => md5($password),
                    'full_name'     => $db->clearText($full_name),
                    'gender'        => intval($gender),
                    'birthday'      => $date->dmYtoYmd2($birthday),
                    'identity_card' => $db->clearText($identity_card),
                    'date_of_issue' => $date->dmYtoYmd2($date_of_issue),
                    'email'         => $db->clearText($email),
                    'phone'         => $db->clearText($phone),
                    'address'       => $db->clearText($address),
                    'city'          => intval($city),
                    'agency'        => $db->clearText(json_encode(array_values($list))),
                    'note'          => $db->clearText($note),
                    'is_show'       => intval($is_show),
                    'modified_time' => time(),
                    'user_id_edit'  => $_SESSION["user_id"]
                );
            }
            $db->condition = "`user_id` = " . $user_id;
            $db->update($data);

            //---- Role-User
            $role_arr = isset($_POST['role']) ? $_POST['role'] : array();
            $role_arr = array_keys(array_flip($role_arr));

            $db->table = "role_user";
            $db->condition = "`user` = $user_id";
            $db->delete();
            if(count($role_arr)>0) {
                for ($i = 0; $i < count($role_arr); $i++) {
                    $db->table = "role_user";
                    $data = array(
                        'role'          => $role_arr[$i],
                        'user'          => $user_id,
                        'created_time'  => time(),
                        'user_id'       => $_SESSION["user_id"]
                    );
                    $db->insert($data);
                }
            }

            if($handleUploadImg) {
                $stringObj = new String();
                if(glob($dir_dest . DS .'*'.$img)) array_map("unlink", glob($dir_dest . DS .'*'.$img));

                $img_name_file = 'u_' . time() . "_" . md5(uniqid());
                $imgUp->file_new_name_body      = $img_name_file;
                $imgUp->image_resize            = true;
                $imgUp->image_ratio_crop        = true;
                $imgUp->image_y                 = 200;
                $imgUp->image_x                 = 200;
                $imgUp->Process($dir_dest);
                if($imgUp->processed) {
                    $name_img = $imgUp->file_dst_name;
                    $db->table = "core_user";
                    $data = array(
                        'img' => $db->clearText($name_img)
                    );
                    $db->condition = "user_id = " . $user_id;
                    $db->update($data);
                }
                else {
                    loadPageError("Lỗi tải hình: ".$imgUp->error, HOME_URL_LANG . $mmenu['account']['link']);
                }
                $imgUp->file_new_name_body      = 'sm_' . $img_name_file;
                $imgUp->image_resize            = true;
                $imgUp->image_ratio_crop        = true;
                $imgUp->image_y                 = 90;
                $imgUp->image_x                 = 90;
                $imgUp->Process($dir_dest);
                $imgUp-> Clean();
            }
            loadPageSuccess("Đã chỉnh sửa thành viên thành công.", HOME_URL_LANG . $mmenu['account']['link']);
            $OK = true;
        }
    }
}
else {
	foreach ($rows as $row) {
		$user_name          = $row['user_name'];
		$full_name          = $row['full_name'];
		$gender             = intval($row['gender']);
        if(empty(strtotime($row['birthday'])))
            $birthday       = '';
        else
		    $birthday       = $date->vnDate(strtotime($row['birthday']));
        //---
		$identity_card      = $row['identity_card'];
		if(empty(strtotime($row['date_of_issue'])))
		    $date_of_issue  = '';
		else
		    $date_of_issue  = $date->vnDate(strtotime($row['date_of_issue']));
        //---
		$email    	= $row['email'];
		$phone    	= $row['phone'];
		$address   	= $row['address'];
		$city      	= $row['city'];
		$list      	= json_decode($row['agency']);
		$note 	    = $row['note'];
		$img   		= $row['img'];
		$is_show	= intval($row['is_show']);
	}

	$role = array();
	$db->table = "role_user";
	$db->condition = "`user` = $user_id";
	$db->limit = "";
	$db->order = "";
	$rows = $db->select();
	foreach ($rows as $row) {
		array_push($role, $row['role']);
	}

}
if(!$OK)memberUser(HOME_URL_LANG . $mmenu['account']['link'] . '/user-edit', "edit", $user_id, $role, $user_name, $full_name, $gender, $birthday, $identity_card, $date_of_issue, $email, $phone, $address, $city, $list, $note, $is_show, $img, $error);