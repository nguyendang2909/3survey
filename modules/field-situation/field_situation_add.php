<?php
include_once(_F_TEMPLATES . DS . "field_situation.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . $mmenu['field-situation']['link'] . '">' . $mmenu['field-situation']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  if (empty($time) || $time == '__/__/____' || empty($localId) || $localId == '' || empty($shipId) || $shipId == '') $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'field-situation' . DS;
    $fileName      = 'field_situation' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];

    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);

      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      $fileName = '-no-';
    }
    if ($OK) {

      $db->table = "field_situation";
      $data = array(
        'time' => date("Y-m-d", strtotime($date->dmYtoYmd($time))),
        'localId' => intval($localId),
        'countryId' => intval($countryId),
        'shipId' => intval($shipId),
        'purpose' => stripslashes($purpose),
        'note' => stripslashes($note),
        'file' => $db->clearText($fileName),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['field-situation']['link']);

    }
    $OK = true;
  }
} else {
  $localId = 1;
  $note = '';
  $countryId = 1;
  $shipId = '';
  $purpose = '';
  $time = '';
  $file = '';
}

if (!$OK) postFieldSituation(
  HOME_URL_LANG . $mmenu['field-situation']['link'] . '/field-situation-add',
  "add",
  0,
  $time,
  $localId,
  $countryId,
  $shipId,
  $purpose,
  $note,
  $file,
  $error
);
