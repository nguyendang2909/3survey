<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0];?>"><?php echo $menu_ol[4];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0];?>"><?php echo $menu_op[4][0];?></a>
			</li>
			<li>
				<a class="current">Chỉnh sửa tỉnh thành</a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
//
$local_id = isset($_GET['id']) ? intval($_GET['id']) : intval($local_id);
$db->table = "local";
$db->condition = "`local_id` = $local_id";
$db->order = "";
$db->limit = 1;
$db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0]);

include_once (_F_TEMPLATES . DS . "general_local.php");
if(empty($typeFunc)) $typeFunc = "no";

$OK = false;
$error = '';
if($typeFunc=='edit'){
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên tỉnh thành.</span>';
    else {
		$db->table = "local";
		$data = array(
            'title'         => $db->clearText($title),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`local_id` = $local_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0]);
		$OK = true;
	}
}
else {
	$db->table = "local";
	$db->condition = "`local_id` = $local_id";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach($rows as $row) {
        $title      = $row['title'];
	}
}
if(!$OK) generalLocal(TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . 'general_local_edit', "edit", $local_id, $title, $error);