<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0];?>"><?php echo $menu_ol[4];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0];?>"><?php echo $menu_op[4][0];?></a>
			</li>
			<li>
				<a class="current">Thêm tỉnh thành</a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
include_once (_F_TEMPLATES . DS . "general_local.php");
if(empty($typeFunc)) $typeFunc = "no";

$OK = false;
$error = '';
if($typeFunc=='add'){
    $title = isset($_POST['title']) ? $_POST['title'] : array();
    if(empty($title[0])) $error = '<span class="show-error">Vui lòng nhập tên địa phương.</span>';
	else {
        for($i = 0; $i < count($title); $i++) {
            if(!empty($title[$i])) {
                $db->table = "local";
                $data = array(
                    'title'         => $db->clearText($title[$i]),
                    'sort'          => intval(sortAcs()+1),
                    'created_time'  => time(),
                    'modified_time' => time(),
                    'user_id'       =>  $_SESSION["user_id"]
                );
                $db->insert($data);
            }
        }

		loadPageSuccess("Đã thêm dữ liệu thành công.", TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . $link_op[4][0]);
		$OK = true;
	}
}
else {
	$title  = "";
}
if(!$OK) generalLocal(TTH_PATH_LK . $link_ol[4] . TTH_PATH_OP_LK . 'general_local_add', "add", 0, $title, $error);