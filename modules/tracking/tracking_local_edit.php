<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link'] . '">' . $mmenu['tracking']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm địa bàn</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$local_id = isset($_GET['id']) ? intval($_GET['id']) : intval($local_id);
$db->table = "local";
$db->condition = "`local_id` = $local_id";
$db->order = "";
$db->limit = 1;
$rows = $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link']);

include_once (_F_TEMPLATES . DS . "tracking_local.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên khu vực.</span>';
    else {
		$db->table = "local";
		$data = array(
            'title'         => $db->clearText($title),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`local_id` = $local_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $title      = $row['title'];
	}
}
if(!$OK) trackingLocal(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-local-edit', "edit", $local_id, $title, $error);