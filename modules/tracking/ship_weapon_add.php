<?php
include_once(_F_TEMPLATES . DS . "ship_weapon.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['sub'][7]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-weapon' . '">' . $mmenu['tracking']['sub'][7]['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-weapon-add' . '">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  // Validate data
  if (empty($name)) $error = '<span class="show-error">Vui lòng nhập tên loại tàu.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;
    $fileName      = 'weapon' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];

    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);

      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      $fileName = '-no-';
    }
    if ($OK) {
      $db->table = "ship_weapon";
      $data = array(
        'name' => $db->clearText($name),
        'order' => intval($order),
        'feature' => $db->clearText($feature),
        'weight' => floatval($weight),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-weapon');
    }
    $OK = true;
  }
} else {
  $name = "";
  $order = "";
  $feature = "";
  $weight = "";
  $file = "";
  $note = "";
}
if (!$OK) postShipWeapon(
  HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-weapon-add',
  "add",
  0,
  $name,
  $order,
  $feature,
  $weight,
  $file,
  $note,
  $error
);
