<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][0]['link'] . '">' . $mmenu['tracking']['sub'][6]['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa chủ đề</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---
$target_id    = isset($_GET['id']) ? intval($_GET['id']) : intval($target_id);
$db->table 		= "target_facebook";
$db->condition 	= "`target_id` = $target_id";
$db->order 		= "";
$db->limit 		= 1;
$rows = $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][0]['link']);
include_once (_F_TEMPLATES . DS . "target_facebook.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên trang.</span>';
    else {
		$db->table = "target_facebook";
		$data = array(
            'name'          => $db->clearText($name),
            'link'          => $db->clearText($link),
            'user'          => $db->clearText($user),
            'type'          => $db->clearText($type),
            'note'          => $db->clearText($note),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
            ); 
		$db->condition = "`target_id` = $target_id";
		$db->update($data);

		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][0]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $name       = $row['name'];
        $link       = $row['link'];
        $user       = $row['user'];
        $type       = $row['type'];
        $note       = $row['note'];
	}
}
if(!$OK) targetFacebook(HOME_URL_LANG . $mmenu['tracking']['link'] . '/target_facebook_edit', "edit", $target_id, $name, $link, $user, $type, $note, $error);