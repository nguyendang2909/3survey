<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link'] . '">' . $mmenu['tracking']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm khu vực</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "tracking_local.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $title = isset($_POST['title']) ? $_POST['title'] : array();
    if(empty($title[0])) $error = '<span class="show-error">Vui lòng nhập tên khu vực.</span>';
	else {
        for($i = 0; $i < count($title); $i++) {
            if(!empty($title[$i])) {
                $db->table = "local";
                $data = array(
                    'title'         => $db->clearText($title[$i]),
                    'sort'          => intval(sortAcs()+1),
                    'created_time'  => time(),
                    'modified_time' => time(),
                    'user_id'       =>  $_SESSION["user_id"]
                );
                $db->insert($data);
            }
        }

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link']);
		$OK = true;
	}
}
else {
	$title  = "";
}
if(!$OK) trackingLocal(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-local-add', "add", 0, $title, $error);