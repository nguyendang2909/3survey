<?php
include_once(_F_TEMPLATES . DS . "ship.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['sub'][7]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship' . '">' . $mmenu['tracking']['sub'][7]['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type-edit' . '">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$shipId = isset($_GET['id']) ? intval($_GET['id']) : intval($shipId);
$db->table      = "ship";
$db->condition  = "`shipId` = $shipId AND `isActive` = 1";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship');

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($name) || empty($mapTypeId) || empty($mmsi)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;

    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];
    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);
      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      foreach ($rows as $row) {
        $fileName = stripslashes($row['file']);
      }
    }

    if ($OK) {
      $db->table = "ship";
      $data = array(
        'name' => $db->clearText($name),
        'shipTypeId' => intval($shipTypeId),
        'countryId' => intval($countryId),
        'mmsi' => intval($mmsi),
        'callsign' => $db->clearText($callsign),
        'imo' => $db->clearText($imo),
        'length' => floatval($length),
        'width' => floatval($width),
        'height' => floatval($height),
        'weight' => floatval($weight),
        'maxSpeed' => floatval($maxSpeed),
        'capital' => $db->clearText($capital),
        'sailorCount' => $db->clearText($sailorCount),
        'workingRange' => $db->clearText($workingRange),
        'shipWeaponId' => intval($shipWeaponId),
        'mapTypeId' => intval($mapTypeId),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`shipId` = $shipId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship');
    }

    $OK = true;
  }
} else {
  foreach ($rows as $row) {
    $name = $row['name'];
    $shipTypeId = $row['shipTypeId'];
    $countryId = $row['countryId'];
    $mmsi = $row['mmsi'];
    $callsign = $row['callsign'];
    $imo = $row['imo'];
    $length = $row['length'];
    $width = $row['width'];
    $height = $row['height'];
    $weight = $row['weight'];
    $maxSpeed = $row['maxSpeed'];
    $capital = $row['capital'];
    $sailorCount = $row['sailorCount'];
    $workingRange = $row['workingRange'];
    $shipWeaponId = $row['shipWeaponId'];
    $mapTypeId = $row['mapTypeId'];
    $file = $row['file'];
    $note = $row['note'];
  }
}
if (!$OK) postShip(
  HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-edit',
  "edit",
  $shipId,
  $name,
  $shipTypeId,
  $countryId,
  $mmsi,
  $callsign,
  $imo,
  $length,
  $width,
  $height,
  $weight,
  $maxSpeed,
  $capital,
  $sailorCount,
  $workingRange,
  $shipWeaponId,
  $mapTypeId,
  $file,
  $note,
  $error
);
