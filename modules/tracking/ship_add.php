<?php
include_once(_F_TEMPLATES . DS . "ship.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['sub'][7]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship' . '">' . $mmenu['tracking']['sub'][7]['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-add' . '">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  echo 11;
  // Validate data
  if (empty($name) || empty($countryId)) $error = '<span class="show-error">Vui lòng nhập tên loại tàu.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;
    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];

    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);

      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      $fileName = '-no-';
    }
    if ($OK) {
      $db->table = "ship";
      $data = array(
        'name' => $db->clearText($name),
        'shipTypeId' => intval($shipTypeId),
        'countryId' => intval($countryId),
        'mmsi' => intval($mmsi),
        'callsign' => $db->clearText($callsign),
        'imo' => $db->clearText($imo),
        'length' => floatval($length),
        'width' => floatval($width),
        'height' => floatval($height),
        'weight' => floatval($weight),
        'maxSpeed' => floatval($maxSpeed),
        'capital' => $db->clearText($capital),
        'sailorCount' => $db->clearText($sailorCount),
        'workingRange' => $db->clearText($workingRange),
        'shipWeaponId' => intval($shipWeaponId),
        'mapTypeId' => intval($mapTypeId),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship');
    }
    $OK = true;
  }
} else {
  $name = "";

  $shipTypeId = "";
  $countryId = "";
  $mmsi = "";
  $callsign = "";
  $imo = "";
  $length = "";
  $width = "";
  $height = "";
  $weight = "";
  $maxSpeed = "";
  $capital = "";
  $sailorCount = "";
  $workingRange = "";
  $shipWeaponId = 1;
  $mapTypeId = 26;
  $file = "";
  $note = "";
}
if (!$OK) postShip(
  HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-add',
  "add",
  0,
  $name,
  $shipTypeId,
  $countryId,
  $mmsi,
  $callsign,
  $imo,
  $length,
  $width,
  $height,
  $weight,
  $maxSpeed,
  $capital,
  $sailorCount,
  $workingRange,
  $shipWeaponId,
  $mapTypeId,
  $file,
  $note,
  $error
);
