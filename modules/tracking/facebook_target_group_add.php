<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] .  $mmenu['tracking']['sub'][6]['link'] . '">' . $mmenu['tracking']['sub'][6]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][1]['link'] . '">' . $mmenu['tracking']['sub'][6]['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm nhóm đối tượng</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "facebook_target_group.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên đối tượng.</span>';
	else {
        $friends = isset($_POST['friends']) ? $_POST['friends'] : array();
        $friends = isset($_POST['tracking_facebook_target']) ? $_POST['tracking_facebook_target'] : array();

        $db->table = "facebook_target_group";
        $data = array(
            'name'          => $db->clearText($name),
            'age'           => formatNumberToInt($age),
            'address'       => $db->clearText($address),
            'religion'      => $db->clearText($religion),
            'faction'       => $db->clearText($faction),
            'social'        => $db->clearText($social),
            'local'         => intval($local),
            'friends'       => $db->clearText(json_encode($friends)),
            'note'       	=> $db->clearText($note),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"],
            'facebook_type' => $db->clearText($facebookType),
            'tracking_status' => $db->clearText($trackingStatus),
            'tracking_facebook_target' => $db->clearText(json_encode($trackingFacebookTarget)),
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][1]['link']);
		$OK = true;
	}
}
else {
    $name       = "";
    $age        = "";
    $address    = "";
    $religion   = "";
    $faction    = "";
    $social     = "";
    $local      = "";
    $friends    = array();
    $note       = "";
    $facebookType = "";
    $trackingStatus = "";
    $trackingFacebookTarget = array();
}
if(!$OK) facebookTargetGroup(HOME_URL_LANG . $mmenu['tracking']['link'] . '/facebook-target-group-add', "add", 0, $name, $age, $address, $religion, $faction, $social, $local, $friends, $note, $facebookType, $trackingStatus, $trackingFacebookTarget, $error);