<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link'] . '">' . $mmenu['tracking']['sub'][4]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa chủ đề</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$keywords_id    = isset($_GET['id']) ? intval($_GET['id']) : intval($keywords_id);
$db->table 		= "keywords";
$db->condition 	= "`keywords_id` = $keywords_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link']);

include_once (_F_TEMPLATES . DS . "tracking_keywords.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên trang.</span>';
    else {
		$db->table = "keywords";
		$data = array(
            'name'          => $db->clearText($name),
            'keyword_vi'    => $db->clearText($keyword_vi),
            'keyword_en'    => $db->clearText($keyword_en),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`keywords_id` = $keywords_id";
		$db->update($data);

		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $name       = $row['name'];
        $keyword_vi = $row['keyword_vi'];
        $keyword_en = $row['keyword_en'];
	}
}
if(!$OK) trackingKeywords(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-keywords-edit', "edit", $keywords_id, $name, $keyword_vi, $keyword_en, $error);