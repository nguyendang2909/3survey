<?php
include_once(_F_TEMPLATES . DS . "parish.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/parish' . '">Giáo xứ</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$parishId = isset($_GET['id']) ? intval($_GET['id']) : intval($parishId);
$db->table      = "parishs";
$db->condition  = "`parishId` = $parishId AND `isActive` = 1";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/parish');

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($name)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;

    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];
    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);
      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      foreach ($rows as $row) {
        $fileName = stripslashes($row['file']);
      }
    }

    if ($OK) {
      $db->table = "parishs";
      $data = array(
        'name' => $db->clearText($name),
        'address' => $db->clearText($address),
        'localId' => intval($localId),
        'yearOfEstablishment' => $db->clearText($yearOfEstablishment),
        'chanhXu' => $db->clearText($chanhXu),
        'phoXu' => $db->clearText($phoXu),
        'laityCount' => $db->clearText($laityCount),
        'phoneNumber' => $db->clearText($phoneNumber),
        'email' => $db->clearText($email),
        'website' => $db->clearText($website),
        'isWarning' => intval($isWarning),
        'mapTypeId' => intval($mapTypeId),
        'latitude'  => $db->clearText($latitude),
        'longitude' => $db->clearText($longitude),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`parishId` = $parishId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/parish');
    }

    $OK = true;
  }
} else {
  foreach ($rows as $row) {
    $name = $row['name'];
    $address = $row['address'];
    $localId = $row['localId'];

    $yearOfEstablishment = $row['yearOfEstablishment'];
    $chanhXu = $row['chanhXu'];
    $phoXu = $row['phoXu'];
    $laityCount = $row['laityCount'];
    $phoneNumber = $row['phoneNumber'];
    $email = $row['email'];
    $website = $row['website'];

    $isWarning = $row['isWarning'];
    $mapTypeId = $row['mapTypeId'];
    $latitude = $row['latitude'];
    $longitude = $row['longitude'];
    $file = $row['file'];
    $note = $row['note'];
  }
}
if (!$OK) postParish(
  HOME_URL_LANG . $mmenu['tracking']['link'] . '/parish-edit',
  "edit",
  $parishId,
  $name,
  $address,
  $localId,
  $yearOfEstablishment,
  $chanhXu,
  $phoXu,
  $laityCount,
  $phoneNumber,
  $email,
  $website,
  $isWarning,
  $mapTypeId,
  $latitude,
  $longitude,
  $file,
  $note,
  $error
);
