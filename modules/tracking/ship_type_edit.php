<?php
include_once(_F_TEMPLATES . DS . "ship_type.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['sub'][7]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type' . '">' . $mmenu['tracking']['sub'][7]['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type-edit' . '">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$shipTypeId = isset($_GET['id']) ? intval($_GET['id']) : intval($shipTypeId);
$db->table      = "ship_type";
$db->condition  = "`shipTypeId` = $shipTypeId AND `isActive` = 1";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type');

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($name) || empty($order)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;

    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];
    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);
      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      foreach ($rows as $row) {
        $fileName = stripslashes($row['file']);
      }
    }

    if ($OK) {
      $db->table = "ship_type";
      $data = array(
        'name' => $db->clearText($name),
        'order' => intval($order),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`shipTypeId` = $shipTypeId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type');
    }

    $OK = true;
  }
} else {
  foreach ($rows as $row) {
    $name = $row['name'];
    $order = $row['order'];
    $file = $row['file'];
    $note = $row['note'];
  }
}
if (!$OK) postShipType(HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-type-edit', "edit", $shipTypeId, $name, $order, $file, $note, $error);
