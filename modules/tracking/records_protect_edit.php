<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][3]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][3]['link'] . '">' . $mmenu['tracking']['sub'][5]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][3]['link'] . '">' . $mmenu['tracking']['sub'][5]['sub'][3]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa bảo vệ</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

$protect_id  = isset($_GET['id']) ? intval($_GET['id']) : intval($protect_id);
$db->table 		= "protect";
$db->condition 	= "`protect_id` = $protect_id";
$db->order 		= "";
$db->limit 		= 1;
$rows 			= $db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][3]['link']);

include_once (_F_TEMPLATES . DS . "records_protect.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='edit'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên bảo vệ.</span>';
    else {
		$db->table = "protect";
		$data = array(
            'title'      	=> $db->clearText($title),
            'local'      	=> intval($local),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`protect_id` = $protect_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][3]['link']);
		$OK = true;
	}
}
else {
	foreach($rows as $row) {
        $title	= $row['title'];
        $local 	= $row['local'];
        $lng   	= $row['lng'];
        $lat 	= $row['lat'];
	}
}
if(!$OK) recordsProtect(HOME_URL_LANG . $mmenu['tracking']['link'] . '/records-protect-edit', "edit", $protect_id, $title, $local, $lng, $lat, $error);