<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/ship-voyage' . '">Hải trình tàu</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

$shipId = isset($_GET['id']) ? intval($_GET['id']) : intval($shipId);
?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-no-border">
      <div class="row">
        <div class="col-xs-12 top-tool">
          <div class="pull-right btn-tool">
            <button type="button" class="btn btn-export" onclick="return print_f_id('ship-voyage',<?php echo $shipId ?>);">
              <label class="icon">&nbsp;</label><label class="text">Xuất tệp...</label>
            </button>
          </div>
        </div>
      </div>
      <div class="table-responsive">
        <form method="post" id="_ol_delete">
          <table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên tàu</th>
                <th>Ngày cập nhật</th>
                <th>Ghi chú</th>
                <th>Tọa độ</th>
                <!-- <th>Người cập nhật<th> -->
              </tr>
            </thead>

            <thead>
              <tr>
                <td align="center">-</td>
                <td align="center">-</td>
                <td><input type="text" data-column="2" class="form-control filter"></td>
                <td><input type="text" data-column="3" class="form-control filter"></td>
                <td><input type="text" data-column="4" class="form-control filter"></td>
                <!-- <td><input type="text" data-column="5" class="form-control filter"></td> -->
              </tr>
            </thead>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#dataTablesList tfoot th').each(function() {
      var title = $(this).text();
      $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    });
    var table = $('#dataTablesList').DataTable({
      "language": {
        url: "/js/data-tables/de_DE.txt"
      },
      "lengthMenu": [100, 200, 300],
      "info": false,
      "processing": true,
      "serverSide": true,
      "ajax": {
        url: '/action.php',
        type: 'GET',
        data: {
          url: 'ship_voyage',
          type: 'load',
          shipId: <?php echo $shipId; ?>
        }
      },
      "columns": [{
          data: 'no'
        },
        {
          data: 'name'
        },
        {
          data: 'shipUpdateTime'
        },
        {
          data: 'comment'
        },
        {
          data: 'coordinatebyradius'
        },
      ],
      // Can le cho cot
      fnRowCallback: function(nRow, aData, iDisplayIndex) {
        $('td:eq(0)', nRow).css("text-align", "center");
        $('td:eq(1)', nRow).css("text-align", "center");
        $('td:eq(2)', nRow).css("text-align", "center");
        $('td:eq(3)', nRow).css("text-align", "center");
        $('td:eq(4)', nRow).css("text-align", "center");
        return nRow;
      },

      fnDrawCallback: function() {
        $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
      },

      "order": [0, "asc"],

      "aoColumnDefs": [{
        'searchable': false,
        'orderable': false
      }]
    });

    // Apply the search
    $('.filter').on('change', function() {
      var i = $(this).attr('data-column');
      var v = $(this).val();
      table.columns(i).search(v).draw();
    });
  });

  // Datetimepicker
  $('.input-date').datetimepicker({
    lang: 'vi',
    timepicker: false,
    format: '<?php echo TTH_DATE_FORMAT; ?>'
  });

  // Confirm xoa record
  $(".ol-confirm").click(function() {
    confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
      if (this.data == true) document.getElementById("_ol_delete").submit();
    });
  });

  $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>