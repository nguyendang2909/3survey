<?php
include_once(_F_TEMPLATES . DS . "enterprise.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '/enterprise' . '">Doanh nghiệp</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  // Validate data
  if (empty($name) || empty($localId)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS;
    $fileName      = 'enterprise' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];

    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);

      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      $fileName = '-no-';
    }
    if ($OK) {
      $db->table = "enterprises";
      $data = array(
        'name' => $db->clearText($name),
        'address' => $db->clearText($address),
        'localId' => intval($localId),
        'carrier' => $db->clearText($carrier),
        'employeeCount' => $db->clearText($employeeCount),
        'yearOfEstablishment' => intval($yearOfEstablishment),
        'businessModel' => $db->clearText($businessModel),
        'owner' => $db->clearText($owner),
        'phoneNumber' => $db->clearText($phoneNumber),
        'email' => $db->clearText($email),
        'website' => $db->clearText($website),
        'isWarning' => intval($isWarning),
        'mapTypeId' => intval($mapTypeId),
        'latitude'  => $db->clearText($latitude),
        'longitude' => $db->clearText($longitude),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/enterprise');
    }
    $OK = true;
  }
} else {
  $name = "";
  $address = "";
  $localId = "";
  $carrier = "";
  $employeeCount = "";
  $yearOfEstablishment = "";
  $businessModel = "";
  $owner = "";
  $phoneNumber = "";
  $email = "";
  $website = "";
  $isWarning = "";
  $mapTypeId = 28;
  $latitude = "";
  $longitude = "";
  $file = "";
  $note = "";
}
if (!$OK) postEnterprise(
  HOME_URL_LANG . $mmenu['tracking']['link'] . '/enterprise-add',
  "add",
  0,
  $name,
  $address,
  $localId,
  $carrier,
  $employeeCount,
  $yearOfEstablishment,
  $businessModel,
  $owner,
  $phoneNumber,
  $email,
  $website,
  $isWarning,
  $mapTypeId,
  $latitude,
  $longitude,
  $file,
  $note,
  $error
);
