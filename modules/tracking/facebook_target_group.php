<?php
if (!defined('TTH_SYSTEM')) {
    die('Please stop!');
}
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] .  $mmenu['tracking']['sub'][6]['link'] . '">' . $mmenu['tracking']['sub'][6]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['tracking']['sub'][6]['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if (isset($_POST['tick'])) {
    $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
    $tick = array_filter($tick);
    if (count($tick) > 0) {
        $tick = implode(',', $tick);
        $db->table = "facebook_target_group";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`subject_id` IN ($tick)";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][6]['sub'][1]['link']);
    }
}
?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-left">
                        <a class="btn-add-new not-abs" href="<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . '/facebook-target-group-add'; ?>">Thêm nhóm đối tượng</a>
                    </div>
                    <div class="pull-right btn-tool">
                        <a class="btn btn-temp" target="_blank" href="<?php echo HOME_URL_LANG . '/documents/_temp/(Temp)_facebook_target_group.xls' ?>"><label class="icon"></label><label class="text">Mẫu nhập...</label></a>
                        <!-- <form id="_form_import" method="post" class="btn btn-import" enctype="multipart/form-data">
                            <input type="file" name="file" id="import" class="inputfile" onchange="return import_file('tracking_subject');" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                            <label class="icon" for="import">&nbsp;</label><label class="text" for="import">Chèn tệp...</label>
                        </form> -->
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <form method="post" id="_ol_delete">
                    <table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th width="100">Tên nhóm đối tượng</th>
                                <th width="100">Trạng thái</th>
                                <th>Các đối tượng theo dõi</th>
                                <th width="80px">Chọn</th>
                                <th>Xuất báo cáo</th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <td align="center">-</td>
                                <td><input type="text" data-column="1" class="form-control filter"></td>
                                <td><input type="text" data-column="2" class="form-control filter"></td>
                                <td><input type="text" data-column="3" class="form-control filter"></td>
                                <td align="center">-</td>
                                <td align="center">-</td>
                            </tr>
                        </thead>
                    </table>
                    <?php
                    if (in_array("facebook-target-group;delete", $corePrivilegeSlug['op']))
                        echo '<div class="row"><div class="col-sm-12" align="right"><label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label><input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá đối tượng" name="delete"></div></div>';
                    ?>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#dataTablesList tfoot th').each(function() {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });
        var table = $('#dataTablesList').DataTable({
            "language": {
                "url": "/js/data-tables/de_DE.txt"
            },
            "lengthMenu": [100, 200, 300],
            "info": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: '/action.php',
                type: 'POST',
                data: {
                    url: 'facebook_target_group',
                    type: 'load'
                }
            },
            "columns": [{
                    data: 'no'
                },
                {
                    data: 'name'
                },
                {
                    data: 'tracking_status'
                },
                {
                    data: 'tracking_facebook_target'
                },
                {
                    data: 'tool'
                },
                {
                    data: 'social'
                }
            ],
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                $('td:eq(0)', nRow).css("text-align", "center");
                $('td:eq(2)', nRow).css("text-align", "center");
                $('td:eq(6)', nRow).css("text-align", "center");
                $('td:eq(8)', nRow).css("text-align", "center");
                return nRow;
            },
            "order": [0, "asc"],
            "aoColumnDefs": [{
                'targets': [4],
                'searchable': false,
                'orderable': false
            }]
        });
        // Apply the search
        $('.filter').on('change', function() {
            var i = $(this).attr('data-column');
            var v = $(this).val();
            table.columns(i).search(v).draw();
        });
    });
    //---
    $('.input-date').datetimepicker({
        lang: 'vi',
        timepicker: false,
        format: '<?php echo TTH_DATE_FORMAT; ?>'
    });
    $(".ol-confirm").click(function() {
        confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
            if (this.data == true) document.getElementById("_ol_delete").submit();
        });
    });
    $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>