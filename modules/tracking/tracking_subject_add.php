<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'] . '">' . $mmenu['tracking']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm đối tượng</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "tracking_subject.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên đối tượng.</span>';
	else {
        $friends = isset($_POST['friends']) ? $_POST['friends'] : array();

        $db->table = "subject";
        $data = array(
            'name'          => $db->clearText($name),
            'age'           => formatNumberToInt($age),
            'address'       => $db->clearText($address),
            'religion'      => $db->clearText($religion),
            'faction'       => $db->clearText($faction),
            'social'        => $db->clearText($social),
            'local'         => intval($local),
            'friends'       => $db->clearText(json_encode($friends)),
            'note'       	=> $db->clearText($note),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link']);
		$OK = true;
	}
}
else {
    $name       = "";
    $age        = "";
    $address    = "";
    $religion   = "";
    $faction    = "";
    $social     = "";
    $local      = "";
    $friends    = array();
    $note       = "";
}
if(!$OK) trackingSubject(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-subject-add', "add", 0, $name, $age, $address, $religion, $faction, $social, $local, $friends, $note, $error);