<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link'] . '">' . $mmenu['tracking']['sub'][4]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm từ khóa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "tracking_keywords.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập chủ đề.</span>';
	else {
        $db->table = "keywords";
        $data = array(
            'name'          => $db->clearText($name),
            'keyword_vi'    => $db->clearText($keyword_vi),
            'keyword_en'    => $db->clearText($keyword_en),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link']);
		$OK = true;
	}
}
else {
    $name       = "";
    $keyword_vi = "";
    $keyword_en = "";
}
if(!$OK) trackingKeywords(HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-keywords-add', "add", 0, $name, $keyword_vi, $keyword_en, $error);