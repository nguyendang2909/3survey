<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][2]['link'] . '">' . $mmenu['tracking']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][2]['link'] . '">' . $mmenu['tracking']['sub'][5]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][2]['link'] . '">' . $mmenu['tracking']['sub'][5]['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm giáo xứ</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "records_parish.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên giáo xứ.</span>';
	else {
        $db->table = "parish";
        $data = array(
            'title'      	=> $db->clearText($title),
            'local'      	=> intval($local),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][2]['link']);
		$OK = true;
	}
}
else {
	$title	= "";
    $local 	= 0;
    $lng  	= "108.215";
	$lat	= "16.059";
}
if(!$OK) recordsParish(HOME_URL_LANG . $mmenu['tracking']['link'] . '/records-parish-add', "add", 0, $title, $local, $lng, $lat, $error);