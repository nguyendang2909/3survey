<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$stringObj = new String();
$P_grid = 0;
$breadcrumbs = '';
if($P_menu>0) {
    $parent = $P_menu;
    while($parent>0) {
        $db->table = "blog";
        $db->condition = "`blog_id` = $parent";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select("`blog_id`, `parent`, `grid`, `slug`, `name`");
        if ($db->RowCount > 0) {
            foreach ($rows as $row) {
                $active = '';
                $parent = intval($row['parent']);
                if($P_menu==intval($row['blog_id'])) {
                    $active = ' class="current"';
                    $P_grid = intval($row['grid']);
                }
                if($parent==0)
                    $breadcrumbs = '<li><a' . $active . ' href="' . HOME_URL_LANG . '/' .  $P_slug . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></li>' . $breadcrumbs;
                else
                    $breadcrumbs = '<li><a' . $active . ' href="' . HOME_URL_LANG . '/' .  $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></li>' . $breadcrumbs;
            }
        } else $parent = 0;
    }
}
$breadcrumbs = '<ul class="breadcrumbs-alt"><li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>' . $breadcrumbs . '</ul>';
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12 box-abs">
        <?php
        echo $breadcrumbs;
        $core_blog  = coreBlogCat($account["id"]);
        if(in_array($P_menu, $core_blog) || in_array(-1, $core_blog)) echo '<a class="btn btn-success btn-round btn-sm btn-abs" href="' . TTH_PATH_LK . $link_ol[3] . TTH_PATH_OP_LK . 'blog_post_add&blog=' . $P_menu . '"><i class="fa fa-plus"></i> Đăng bài</a>';
        ?>
	</div>
</div>
<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
        <div class="public-blog">
            <?php
            if($P_post>0) {
                $sum_view = 0;
                //---
                $db->table = "blog_post";
                $db->condition = "`is_active` = 1 AND `blog_post_id` = $P_post";
                $db->order = "`created_time` DESC";
                $db->limit = 1;
                $rows_p = $db->select("`blog_post_id`, `blog`, `name`, `slug`, `img`, `comment`, `content`, `upload`, `created_time`, `tags`, `views`, `user_id`");
                if($db->RowCount>0) {

                    $db->table = "views";
                    $db->condition = "`type` LIKE 'blog' AND `id` = $P_post  AND `user_id` = " . $account["id"];
                    $db->order = "";
                    $db->limit = 1;
                    $db->select();
                    if($db->RowCount) {
                        $db->table = "views";
                        $data = array(
                            'modified_time' => time()
                        );
                        $db->condition = "`type` LIKE 'blog' AND `id` = $P_post  AND `user_id` = " . $account["id"];
                        $db->update($data);
                    } elseif($account["id"]>0) {
                        $db->table = "views";
                        $data = array(
                            'type'          => $db->clearText('blog'),
                            'id'            => $P_post,
                            'created_time'  => time(),
                            'user_id'       => $account["id"]
                        );
                        $db->insert($data);
                    }

                    //---
                    echo '<div class="public-content clearfix">';
                    echo '<div class="blog-content">';
                    echo '<div class="blog-box">';
                    foreach($rows_p as $row) {
                        $p_user = getInfoUser2($row['user_id']);

                        echo '<div class="blog-head">';
                        if(!empty($p_user)) {
                            echo '<div class="blog-user">';
                            echo '<div class="blog-avatar">' . $p_user[4] . '</div>';
                            echo '<div class="blog-user-cap"><h4>' . $p_user[0] . '</h4>';
                            echo '<div class="blog-stats">';
                            echo '<span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span>';
                            if(in_array($P_menu, $core_blog) || in_array(-1, $core_blog)) echo '<a class="btn btn-warning btn-round btn-sm pull-right" href="' . TTH_PATH_LK . $link_ol[3] . TTH_PATH_OP_LK . 'blog_post_edit&id=' . $P_post . '"><i class="fa fa-cog"></i> Chỉnh sửa</a>';
                            echo '</div></div>';
                            echo '</div>';
                        }
                        else echo '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                        echo '</div>';

                        echo '<div class="blog-detail">';
                        echo '<h1 class="blog-title">' . stripslashes($row['name']) . '</h1>';
                        echo '<p class="blog-comment">' . stripslashes($row['comment']) . '</p>';
                        echo '<div class="blog-post">' . stripslashes($row['content']) . '</div>';

                        $list_img = '';
                        $db->table = "uploads_tmp";
                        $db->condition = "`upload_id` = ". intval($row['upload']);
                        $db->order = "";
                        $db->limit = 1;
                        $rows_i = $db->select("`list_img`");
                        if($db->RowCount>0)
                            $list_img = stripslashes($rows_i[0]['list_img']);
                        $img = array();
                        $img = explode(";", $list_img);
                        $img = array_filter($img);
                        //-----------------------------------
                        $see_all = '';
                        $total_p = count($img);
                        if($total_p>1) {
                            echo '<div class="list-photos"><ul class="photos tt-' . $total_p . ' clearfix">';
                            if ($total_p > 5) $see_all = '<div class="see-all"><div class="sa-p01"><span class="sa-p11">+' . ($total_p-4) . '</span></div></div>';
                            for ($i = 0; $i < $total_p; $i++) {
                                if($i > 4) echo '<li class="hidden"><a data-fancybox="photos" href="' . HOME_URL . '/uploads/photos/full_' . $img[$i] . '">&nbsp;</a></li>';
                                elseif ($i == 4) echo '<li><a data-fancybox="photos" href="' . HOME_URL . '/uploads/photos/full_' . $img[$i] . '">' . $see_all . '<img src="' . HOME_URL . '/uploads/photos/' . $img[$i] . '"></a></li>';
                                else echo '<li><a data-fancybox="photos" href="' . HOME_URL . '/uploads/photos/full_' . $img[$i] . '"><img src="' . HOME_URL . '/uploads/photos/' . $img[$i] . '"></a></li>';
                            }
                            echo '</ul></div>';

                        } elseif($total_p>0) {
                            echo '<div class="list-photos">';
                            echo '<div class="img"><a data-fancybox="gallery" href="' . HOME_URL . '/uploads/photos/full_' . $img[0] . '">' . $see_all . '<img src="' . HOME_URL . '/uploads/photos/full_' . $img[0] . '"></a></div>';
                            echo '</div>';
                        }
                        echo '</div>';

                        $sum_view = doubleval($row['views'] + 1);
                    }
                    $db->table = "blog_post";
                    $data = array(
                        'views' => $sum_view
                    );
                    $db->condition = "`blog_post_id` = $P_post";
                    $db->update($data);

                    echo '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $P_slug) . getTagsUser($row['tags']) . '</div>';

                    //--- List views
                    $total_views = 0;
                    $db->table = "views";
                    $db->condition = "`type` LIKE 'blog' AND `id` = $P_post";
                    $db->order = "";
                    $db->limit = 1;
                    $rows = $db->select("COUNT(*) AS `count`");
                    foreach($rows as $row) {
                        $total_views = $row['count'];
                    }

                    $db->table = "views";
                    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
                    $db->condition = "a.`type` LIKE 'blog' AND a.`id` = $P_post";
                    $db->order = "a.`created_time` DESC";
                    $db->limit = 3;
                    $rows = $db->select("a.`user_id`, b.`full_name`");
                    if ($db->RowCount > 0) {
                        $list = array();
                        foreach ($rows as $row) {
                            array_push($list, stripslashes($row['full_name']));
                        }
                        echo '<div class="ol-list-views">';
                        if ($total_views > 3) echo ' <a href="javascript:;" rel="' . $P_post . '">' . implode(', ', $list) . ' <span>và ' . ($total_views - 3) . ' người đã xem</span></a>';
                        else echo implode(', ', $list);
                        echo '</div>';
                    }

                    //--- Comment & Like
                    echo '<div class="ol-like-comment">';
                    $L_like = 0;
                    $db->table = "like";
                    $db->condition = "`type` LIKE 'blog' AND `id` = $P_post AND `user_id` = " . $account["id"];
                    $db->order = "";
                    $db->limit = 1;
                    $rows = $db->select();
                    if($db->RowCount) {
                        foreach ($rows as $row) {
                            $L_like = intval($row['c_like']);
                        }
                    }

                    $c_like = $c_dislike = 0;
                    $db->table = "like";
                    $db->condition = "`type` LIKE 'blog' AND `id` = $P_post";
                    $db->order = "";
                    $db->limit = "";
                    $rows = $db->select();
                    if($db->RowCount>0) {
                        foreach($rows as $row) {
                            if(intval($row['c_like'])==1) $c_like++;
                            elseif(intval($row['c_like'])==-1) $c_dislike++;
                        }
                    }

                    echo '<div class="ol-like">';
                    ///---
                    if($L_like==1)
                        echo '<a class="ol-like-1 ol-active" href="javascript:;" data-id="' . $P_post . '" data-role="1"><i class="fa fa-thumbs-up fa-fw"></i><span>' . formatNumberVN($c_like) . '</span></a>';
                    else
                        echo '<a class="ol-like-1" href="javascript:;" data-id="' . $P_post . '" data-role="1"><i class="fa fa-thumbs-o-up fa-fw"></i><span>' . formatNumberVN($c_like) . '</span></a>';

                    if($L_like==-1)
                        echo '<a class="ol-dislike-2 ol-active" href="javascript:;" data-id="' . $P_post . '" data-role="-1"><i class="fa fa-thumbs-down fa-fw"></i><span>' . formatNumberVN($c_dislike) . '</span></a>';
                    else
                        echo '<a class="ol-dislike-2 " href="javascript:;" data-id="' . $P_post . '" data-role="-1"><i class="fa fa-thumbs-o-down fa-fw"></i><span>' . formatNumberVN($c_dislike) . '</span></a>';
                    //---
                    echo '</div>';
                    //---Comment
                    $db->table = "comment";
                    $db->condition = "`type` LIKE 'blog' AND `is_active` = 1 AND `id` = $P_post AND `parent` = 0";
                    $db->order = "`created_time` DESC";
                    $db->limit = "";
                    $rows = $db->select("COUNT(*) AS `count`");
                    $total = $db->RowCount;
                    foreach($rows as $row) {
                        $total = $row['count'];
                    }

                    echo '<div class="ol-comment">';
                    echo '<div id="_rs_cmt" class="ol-cmt-rs">';
                    $db->table = "comment";
                    $db->condition = "`type` LIKE 'blog' AND `is_active` = 1 AND `id` = $P_post AND `parent` = 0";
                    $db->order = "`created_time` DESC";
                    $db->limit = 3;
                    $rows = array_reverse($db->select("`comment_id`, `comment`, `created_time`, `user_id`"));
                    if($db->RowCount>0) {
                        if($total>3) {
                            echo '<div class="ol-cmt-load"><a href="javascript:;" onclick="return load_comment(this, \'blog\',' . $P_post . ');">Xem các bình luận trước...</a><span>3 trong số ' . $total . '</span></div>';
                        }
                        foreach($rows as $row) {
                            $info_user = array();
                            $info_user = getInfoUser2($row["user_id"]);
                            echo '<div class="ol-cmt-item">';
                            echo '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
                            echo '<div class="ol-cmt-text cmt-parent">';
                            echo '<div class="cmt-text"><span class="ol-u-name">' . $info_user[0] . '</span>' . nl2br(stripslashes($row['comment'])) . '</div><div class="ol-cmt-rep"><a class="b-rep" href="javascript:;" data-id="' . $P_post . '" rel="' . intval($row["comment_id"]) . '">Trả lời</a><span class="space">-</span><span class="cmt-time">' . convertTimeDayAgo($row["created_time"]) . '</span></div>';
                            //---
                            $db->table = "comment";
                            $db->condition = "`type` LIKE 'blog' AND `is_active` = 1 AND `id` = $P_post AND `parent` = " . intval($row["comment_id"]);
                            $db->order = "`created_time` ASC";
                            $db->limit = "";
                            $rows2 = $db->select("`comment_id`, `comment`, `created_time`, `user_id`");
                            if($db->RowCount>0) {
                                foreach($rows2 as $row2) {
                                    $info_user = array();
                                    $info_user = getInfoUser2($row2["user_id"]);
                                    echo '<div class="ol-cmt-item ol-rep-child">';
                                    echo '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
                                    echo '<div class="ol-cmt-text"><div class="cmt-text"><span class="ol-u-name">' . $info_user[0] . '</span>' . nl2br(stripslashes($row2['comment'])) . '</div><div class="ol-cmt-rep"><a class="b-rep" href="javascript:;" data-id="' . $P_post . '" rel="' . intval($row["comment_id"]) . '">Trả lời</a><span class="space">-</span><span class="cmt-time">' . convertTimeDayAgo($row2["created_time"]) . '</span></div></div>';
                                    echo '</div>';
                                }
                            }
                            //---
                            echo '</div>';
                            echo '</div>';
                        }
                    }
                    echo '</div>';

                    if($account["id"]>0) {
                        echo '<div class="ol-box-enter">';
                        $info_user = array();
                        $info_user = getInfoUser3($account["id"]);
                        echo '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
                        echo '<div class="ol-cmt-enter"><textarea class="cmt-enter autosize" rows="1" name="cm" placeholder="Viết nội dung bình luận..." data-id="' . $P_post . '" rel="0"></textarea></div>';
                        echo '</div>';
                    }
                    echo '</div>';
                    //---
                    echo '</div>';

                    //---
                    echo '</div>';

                    if ($P_menu>0) {
                        $db->table = "blog_post";
                        $db->condition = "`is_active` = 1 AND `blog` = $P_menu AND `blog_post_id` != $P_post";
                        $db->order = "`created_time` DESC";
                        $db->limit = 6;
                        $rows = $db->select();
                        if($db->RowCount>0) {
                            echo '<div class="public-list clearfix">';
                            if($P_grid==1) {
                                foreach ($rows as $row) {
                                    $photo_avt = '';
                                    if (file_exists(ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS . stripslashes('post-' . $row['img'])) && !empty($row['img'])) {
                                        $photo_avt = '<img src="' . HOME_URL . '/uploads/blog/post-' . stripslashes($row['img']) . '" alt="' . stripslashes($row['name']) . '">';
                                    } else {
                                        $photo_avt = '<img src="' . HOME_URL . '/images/404-post.jpg" alt="' . stripslashes($row['name']) . '">';
                                    }
                                    $photo_avt = '<div class="img"><a href="' . HOME_URL_LANG . '/' . $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . $photo_avt . '</a></div>';
                                    $p_user = getInfoUser2($row['user_id']);

                                    echo '<div class="blog-item"><div class="blog-box">';
                                    echo $photo_avt;
                                    echo '<div class="blog-description">';
                                    echo '<div class="blog-text">';
                                    echo '<h2 class="blog-title"><a href="' . HOME_URL_LANG . '/' . $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
                                    echo '<p class="blog-comment">' . $stringObj->crop(stripslashes($row['comment']), 35) . '</p>';
                                    echo '</div>';
                                    echo '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $P_slug) . '</div>';
                                    if (!empty($p_user)) echo '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
                                    echo '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                                    echo '</div>';
                                    echo '</div></div>';
                                }
                            } else {
                                foreach ($rows as $row) {
                                    $p_user = getInfoUser2($row['user_id']);

                                    echo '<div class="blog-item grid"><div class="blog-box">';
                                    echo '<div class="blog-description">';
                                    echo '<div class="blog-left">';
                                    echo '<div class="blog-text">';
                                    echo '<h2 class="blog-title"><a href="' . HOME_URL_LANG . '/' . $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
                                    echo '<p class="blog-comment">' . stripslashes($row['comment']) . '</p>';
                                    echo '</div>';
                                    echo '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $P_slug) . '</div>';
                                    echo '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                                    echo '</div>';
                                    if (!empty($p_user)) {
                                        echo '<div class="blog-right">';
                                        echo '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
                                        echo '</div>';
                                    }
                                    echo '</div>';
                                    echo '</div></div>';
                                }
                            }
                            echo '</div>';
                        }
                    }
                    echo '</div>';
                    ?>
                    <script>
                        $(function(){
                            $('.ol-like').on('click', 'a[class^="ol-"]', function(){
                                var el = $(this).parent('.ol-like');
                                $.ajax({
                                    url: '/action.php',
                                    type: 'POST',
                                    data: 'url=like&type=blog&id=' + parseInt($(this).attr("data-id")) + '&choice=' + parseInt($(this).attr("data-role")),
                                    dataType: 'html',
                                    success: function(data){
                                        el.html(data);
                                    },
                                    statusCode: {
                                        401: function() {
                                            window.location.href = '/?ol=login';
                                        }
                                    }
                                });
                                return false;
                            });
                            autosize($('textarea.autosize'));
                            $('.ol-comment').on('click', 'a.b-rep', function(){
                                var ol = $(this).parents('.ol-cmt-text').find('.ol-box-enter').length;
                                if(ol==0) {
                                    var cur = $(this).parents('.ol-comment').children('.ol-box-enter');
                                    if(cur.length) {
                                        var par = parseInt($(this).attr("rel"));
                                        cur.clone().addClass('ol-rep-child').appendTo($(this).parents('.cmt-parent')).find('textarea').attr('rel', par).val('').focus();
                                        autosize($('textarea.autosize'));
                                    } else window.location.href = '/?ol=login';
                                } else if(ol>0) {
                                    $(this).parents('.ol-cmt-text').find('.ol-box-enter textarea').val('').focus();
                                }
                            });
                            $('.ol-comment').on('keydown', 'textarea.cmt-enter', function(e) {
                                var msg = $(this).val();
                                var id  = parseInt($(this).attr("data-id"));
                                var par = parseInt($(this).attr("rel"));
                                if (e.keyCode == 13 && !e.shiftKey) {
                                    if(msg=='') {
                                        return false;
                                    }
                                    else {
                                        var el = $(this);
                                        e.preventDefault();
                                        $.ajax({
                                            url: '/action.php',
                                            type: 'POST',
                                            data: {'url': 'comment', 'type': 'blog', 'id': id, 'par': par, 'msg': msg},
                                            dataType: 'html',
                                            success: function(data){
                                                if(par>0) el.parents('.ol-box-enter').before(data);
                                                else $('#_rs_cmt').append(data);
                                            },
                                            statusCode: {
                                                401: function() {
                                                    window.location.href = '/?ol=login';
                                                }
                                            }
                                        });
                                    }
                                    $(this).val('');
                                    $(this).css('height', 'auto');
                                }
                                return true;
                            });
                        });
                    </script>
                    <div class="public-sidebar">
                        <div class="panel">
                            <div class="panel-heading" style="padding: 0;">
                                <!-- Nav tabs -->
                                <ul class="nav nav-pills">
                                    <li class="active">
                                        <a href="#post_new" data-toggle="tab">Mới nhất</a>
                                    </li>
                                    <li>
                                        <a href="#post_views" data-toggle="tab">Xem nhiều nhất</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="form-responsive">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div id="post_new" class="tab-pane fade in active">
                                            <?php
                                            $db->table = "blog_post";
                                            $db->condition = "`is_active` = 1";
                                            $db->order = "`created_time` DESC";
                                            $db->limit = 10;
                                            $rows = $db->select();
                                            if($db->RowCount>0) {
                                                foreach($rows as $row) {
													$slug = getSlugBlogParent0($row['blog']);
                                                    $photo_avt = '';
                                                    if(file_exists(ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS . stripslashes('post-' . $row['img'])) && !empty($row['img'])) {
                                                        $photo_avt = '<img src="'. HOME_URL .'/uploads/blog/post-'. stripslashes($row['img']) . '" alt="' . stripslashes($row['name']) . '">';
                                                    } else {
                                                        $photo_avt = '<img src="'. HOME_URL .'/images/404-post.jpg" alt="' . stripslashes($row['name']) . '">';
                                                    }
                                                    $photo_avt = '<div class="img"><a href="'. HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . $photo_avt . '</a></div>';

                                                    echo '<div class="blog-item-left"><div class="blog-box">';
                                                    echo $photo_avt;
                                                    echo '<div class="blog-description">';
                                                    echo '<div class="blog-text">';
                                                    echo '<h3 class="blog-title"><a href="'. HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h3>';
                                                    echo '<p class="blog-comment">' . $stringObj->crop(stripslashes($row['comment']), 25) . '</p>';
                                                    echo '</div>';
                                                    echo '</div>';
                                                    echo '</div></div>';
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div id="post_views" class="tab-pane fade">
                                            <?php
                                            $db->table = "blog_post";
                                            $db->condition = "`is_active` = 1";
                                            $db->order = "`views` DESC";
                                            $db->limit = 10;
                                            $rows = $db->select();
                                            if($db->RowCount>0) {
                                                foreach($rows as $row) {
													$slug = getSlugBlogParent0($row['blog']);
                                                    $photo_avt = '';
                                                    if(file_exists(ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS . stripslashes('post-' . $row['img'])) && !empty($row['img'])) {
                                                        $photo_avt = '<img src="'. HOME_URL .'/uploads/blog/post-'. stripslashes($row['img']) . '" alt="' . stripslashes($row['name']) . '">';
                                                    } else {
                                                        $photo_avt = '<img src="'. HOME_URL .'/images/404-post.jpg" alt="' . stripslashes($row['name']) . '">';
                                                    }
                                                    $photo_avt = '<div class="img"><a href="'. HOME_URL_LANG  . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . $photo_avt . '</a></div>';

                                                    echo '<div class="blog-item-left"><div class="blog-box">';
                                                    echo $photo_avt;
                                                    echo '<div class="blog-description">';
                                                    echo '<div class="blog-text">';
                                                    echo '<h3 class="blog-title"><a href="'. HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h3>';
                                                    echo '<p class="blog-comment">' . $stringObj->crop(stripslashes($row['comment']), 25) . '</p>';
                                                    echo '</div>';
                                                    echo '</div>';
                                                    echo '</div></div>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                    <?php
                    echo '</div>';
                }
            } elseif ($P_menu>0) {
				$list = getBlogElementPlus($P_menu);

                $db->table = "blog_post";
                $db->condition = "`is_active` = 1 AND `blog` IN ($list)";
                $db->order = "`created_time` DESC";
                $db->limit = 20;
                $rows = $db->select();
                if($db->RowCount>0) {
                    if($P_grid==1) {
                        echo '<div id="_blog" class="public-list clearfix">';
                        foreach($rows as $row) {
                            $photo_avt = '';
                            if(file_exists(ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS . stripslashes('post-' . $row['img'])) && !empty($row['img'])) {
                                $photo_avt = '<img src="'. HOME_URL .'/uploads/blog/post-'. stripslashes($row['img']) . '" alt="' . stripslashes($row['name']) . '">';
                            } else {
                                $photo_avt = '<img src="'. HOME_URL .'/images/404-post.jpg" alt="' . stripslashes($row['name']) . '">';
                            }
                            $photo_avt = '<div class="img"><a href="'. HOME_URL_LANG . '/' . $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . $photo_avt . '</a></div>';
                            $p_user = getInfoUser2($row['user_id']);

                            echo '<div class="blog-item"><div class="blog-box">';
                            echo $photo_avt;
                            echo '<div class="blog-description">';
                            echo '<div class="blog-text">';
                            echo '<h2 class="blog-title"><a href="'. HOME_URL_LANG . '/' . $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
                            echo '<p class="blog-comment">' . $stringObj->crop(stripslashes($row['comment']), 45) . '</p>';
                            echo '</div>';
                            echo '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $P_slug) . '</div>';
                            if(!empty($p_user)) echo '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
                            echo '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                            echo '</div>';
                            echo '</div></div>';
                        }
						echo '</div>';

                        echo '<div id="fb_loading" class="clearfix">';
                        echo '<div class="fb-loading-cell"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
                        echo '<div class="fb-loading-cell"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
                        echo '<div class="fb-loading-cell"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
                        echo '</div>';
                    } else {
                        echo '<div id="_blog" class="public-list clearfix">';
                        foreach($rows as $row) {
                            $p_user = getInfoUser2($row['user_id']);

							echo '<div class="blog-item grid"><div class="blog-box">';
                            echo '<div class="blog-description">';
                            echo '<div class="blog-left">';
                            echo '<div class="blog-text">';
                            echo '<h2 class="blog-title"><a href="' . HOME_URL_LANG . '/' . $P_slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
                            echo '<p class="blog-comment">' . stripslashes($row['comment']) . '</p>';
                            echo '</div>';
                            echo '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $P_slug) . '</div>';
                            echo '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                            echo '</div>';
                            if (!empty($p_user)) {
                                echo '<div class="blog-right">';
                                echo '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
                                echo '</div>';
                            }
                            echo '</div>';
                            echo '</div></div>';
                        }
						echo '</div>';

                        echo '<div id="fb_loading" class="clearfix">';
                        echo '<div class="fb-loading-list"><div class="fb-cell-box"><div class="image"></div><div class="text"><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div><div class="text-line"></div></div></div></div>';
                        echo '</div>';
                    }
                    ?>
                    <script type="text/javascript">
                        var page = 1;
                        var is_busy = false;
                        var stopped = false;
                        $(window).scroll(function() {
                            if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                                if (is_busy == true){
                                    return false;
                                }
                                if (stopped == true){
                                    return false;
                                }
                                is_busy = true;
                                page++;
                                $('#fb_loading').show();
                                $.ajax({
                                    url         : '/action.php',
                                    type        : 'POST',
                                    data        : {
                                        'url'   : 'post',
                                        'page'  : page,
                                        'menu'  : <?php echo $P_menu;?>,
                                        'gird'  : <?php echo $P_grid;?>},
                                    dataType    : 'json',
                                    success     : function (data) {
                                        if(parseInt(data.limit)>0)
                                            stopped = false;
                                        else
                                            stopped = true;
                                        $('#_blog').append(data.post);
                                    },
                                    statusCode: {
                                        401: function() {
                                            window.location.href = '/?ol=login';
                                        }
                                    }
                                }).always(function() {
                                    $('#fb_loading').hide();
                                    is_busy = false;
                                });
                                return false;
                            }
                        });
                    </script>
                <?php
					
                }
				
            }
            ?>
        </div>
	</div>
</div>

