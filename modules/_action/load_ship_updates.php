<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0) {
  $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  $shipId   = isset($_REQUEST['shipId']) ? intval($_REQUEST['shipId']) : 0;
  $startDay   = isset($_REQUEST['startDay']) ? trim($_REQUEST['startDay']) : '';
  $endDay   = isset($_REQUEST['endDay']) ? trim($_REQUEST['endDay']) : '';
  $lat   = isset($_REQUEST['lat']) ? trim($_REQUEST['lat']) : '';
  $long   = isset($_REQUEST['long']) ? trim($_REQUEST['long']) : '';
  $commentUpdate   = isset($_REQUEST['commentUpdate']) ? trim($_REQUEST['commentUpdate']) : '';
  $isViolateTerritorial   = isset($_REQUEST['isViolateTerritorial']) ? intval($_REQUEST['isViolateTerritorial']) : 0;
  // $speed   = isset($_REQUEST['speed']) ? trim($_REQUEST['speed']) : '';

  if ($type == 'load') {
    $features = array();
    $db->table = "ship_report";
    $db->join = "b LEFT JOIN `olala3w_ship` a on b.`shipId` = a.`shipId`";
    $db->condition = "a.`isActive` = 1 AND b.`isActive`=1 AND b.`shipId` = " . $shipId;
    $db->order = "b.`updatedAt` DESC";
    $db->limit = 5;
    $rows = $db->select("b.`zone`, a.`name`, a.`mmsi`,b.`shipId`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`shipHeadingCourse`, b.`shipSpeed`, b.`isViolateTerritorial`, b.`comment`");

    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $coordinatesbyradius  = array($row['shipLatitude'], $row['shipLongitude']);

        $child = array(
          'type' => 'ship',
          'coordinatebyradius'        => $coordinatesbyradius,
          'shipUpdateTime'          => $row['shipUpdateTime'],
          'comment'               => $row['comment'],
        );
        $features[] = $child;
      }
    }

      $geo = array(
        'features'  => $features
      );
  } else if ($type == "loadVoyage") {
    $features = array();
    $db->table = "ship_report";
    $db->join = "b LEFT JOIN `olala3w_ship` a on b.`shipId` = a.`shipId`";
    $db->condition = "a.`isActive` = 1 AND b.`isActive`=1 AND b.`shipId` =" . $shipId . " AND DATE(b.`updatedAt`)  BETWEEN DATE('" . $startDay . "') AND DATE('" . $endDay . "')";
    $db->order = "b.`updatedAt` DESC";
    // $db->limit = 5;
    $rows = $db->select("b.`zone`, a.`name`, a.`mmsi`,b.`shipId`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`shipHeadingCourse`, b.`shipSpeed`, b.`isViolateTerritorial`, b.`comment`");

    if ($db->RowCount > 0) {

      foreach ($rows as $row) {

        $coordinates = array();
        $data = convertToGeometry($row['shipLatitude'], $row['shipLongitude']);
        $databyradius  = array($row['shipLatitude'], $row['shipLongitude']);
        $ftype = "Point";
        if (count($data) == 2 && $ftype == 'Point') {
          $coordinates = $data;
          $coordinatesbyradius = $databyradius;
        }
        $geometry = array(
          'type'          => $ftype,
          'coordinates'   => $coordinates,
          'coordinatesbyradius'   => $coordinatesbyradius
        );

        $properties = array(
          'type' => 'shipVoyage',
          'shipname'           => $row['name'],
          // 'mmsi'               => $row['mmsi'],
          // 'isViolateTerritorial'                 => $row['isViolateTerritorial'],
          // 'shipSpeed'              => $row['shipSpeed'],
          'coordinate'         => $coordinates,
          'coordinatebyradius'        => $coordinatesbyradius,
          // 'shipHeadingCourse'  => $row['shipHeadingCourse'],
          'shipUpdateTime'          => $row['shipUpdateTime'],
          'shipId'        => $row['shipId'],
          // 'comment'               => $row['comment'],
          // 'ship_id'        => $shipid,
        );

        $child = array(
          'type'          => 'Feature',
          'geometry'      => $geometry,
          'properties'    => $properties
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'type'      => 'FeatureCollection',
      'features'  => $features,
      'shipId'    => $shipId,
      'start'     => $oldDate,
      'end'       => $newDate,
    );
  } else if ($type == "insertShip") {

    $db->table = "ship_report";
    $data = array(
      'shipId' => intval($shipId),
      'shipLatitude' => $db->clearText($lat),
      'shipLongitude' => $db->clearText($long),
      // 'shipSpeed'=> $db->clearText($speed),
      'comment'=> $db->clearText($commentUpdate),
      'isViolateTerritorial' => intval($isViolateTerritorial),
      'createdAt' => date("Y-m-d H:i:s"),
      'updatedAt' => date("Y-m-d H:i:s"),
      'shipUpdateTime' => date("Y-m-d H:i:s"),
    );
    $db->insert($data);
  } else if( $type =="territBorder")
  {
    $features = array();
    $db->table = "territorial_border";
    $db->condition = '';
    $db->join = '';
    $db->order = 'id DESC';
    $db->limit = '';
    $rows = $db->select("`latitude`,`longtitude`");
    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $data = convertToGeometry($row['latitude'], $row['longtitude']);

        $child = array(
          'coordinates'        => $data,
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'features'  => $features
    );
  }
  else if ($type=="zone")
  {
    $features = array();
    $db->table = "sea_zone_report";
    $db->condition = '';
    $db->join = '';
    $db->order = 'pointId DESC';
    $db->limit = '';
    $rows = $db->select("`zoneId`,`latitude`,`longtitude`");
    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $data = convertToGeometry($row['latitude'], $row['longtitude']);

        $child = array(
          'coordinates'        => $data,
          'idZone'             => $row['zoneId'],
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'features'  => $features
    );

  }
  else if ($type=="idzone")
  {
    $features = array();
    $db->table = "sea_zone";
    $db->condition = '';
    $db->join = '';
    $db->order = '';
    $db->limit = '';
    $rows = $db->select();
    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $child = array(
          'idZone'        => $row['zoneId'],
          'nameZone'      => $row['name'],
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'features'  => $features
    );

  }

  echo json_encode($geo);
} else echo json_encode(false);
