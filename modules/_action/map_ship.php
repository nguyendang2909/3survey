<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0) {
  $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  $shipId   = isset($_REQUEST['shipId']) ? intval($_REQUEST['shipId']) : 0;

  if ($type == 'load') {
    $features = array();

    $db->table = "ship";
    $db->join = "
			a LEFT JOIN
			(SELECT b1.`source`, b1.`zone`, b1.`shipUpdateTime`, b1.`shipLatitude`, b1.`shipLongitude`, b1.`shipHeadingCourse`, b1.`shipSpeed`, b1.`isViolateTerritorial`, b1.`shipId`, b1.`comment`, b1.`isActive` FROM olala3w_ship_report b1
				JOIN (SELECT shipId, MAX(updatedAt) AS updatedAt FROM olala3w_ship_report GROUP BY shipId) b2 ON b1.shipId = b2.shipId AND b1.updatedAt = b2.updatedAt) b
      ON a.`shipId` = b.`shipId` LEFT JOIN olala3w_maps_type c ON a.`mapTypeId` = c.`maps_type_id`
      LEFT JOIN olala3w_ship_type d ON a.`shipTypeId` = d.`shipTypeId`";
    $db->condition = 'a.`isActive` = 1 AND b.`isActive`=1  AND b.`shipLatitude` != "" AND b.`shipLongitude` != ""';
    $db->order = "`b`.`zone` ASC";
    $db->limit = '';
    $rows = $db->select("a.shipId, b.`zone`, a.`name`, a.`mmsi`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`shipHeadingCourse`, b.`shipSpeed`, b.`isViolateTerritorial`, b.`comment`, b.`source`, c.`icon`, c.`color`, d.`name` AS `type`, d.`icon` AS `shipIcon`, d.`color` AS `shipColor` ");
    if ($db->RowCount > 0) {
      foreach ($rows as $row) {

        $coordinates = array();
        $data   = convertToGeometry($row['shipLatitude'], $row['shipLongitude']);
        $databyradius  = array($row['shipLatitude'], $row['shipLongitude']);
        $ftype = "Point";

        if (count($data) == 2 && $ftype == 'Point') {
          $coordinates = $data;
          $coordinatesbyradius = $databyradius;
        }

        $geometry = array(
          'type'          => $ftype,
          'coordinates'   => $coordinates,
          'coordinatesbyradius'   => $coordinatesbyradius
        );

        $properties = array(
          'type' => 'ship',
          'shipType' => $row['type'],
          'shipColor' => $row['shipColor'],
          'shipIcon' => $row['shipIcon'],
          'name'           => $row['name'],
          'MMSI'               => $row['mmsi'],
          'isViolateTerritorial'                 => $row['isViolateTerritorial'],
          'shipSpeed'              => $row['shipSpeed'],
          'coordinate'         => $coordinates,
          'coordinatebyradius'        => $coordinatesbyradius,
          'shipHeadingCourse'  => $row['shipHeadingCourse'],
          'shipUpdateTime'          => $row['shipUpdateTime'],
          'source'              => $row['source'],
          'zone' => $row['zone'],
          'comment'               => $row['comment'],
          'id'        => $row['shipId'],
          'icon' => $row['icon'],
          'color' => $row['color'],
        );

        $child = array(
          'type'          => 'Feature',
          'geometry'      => $geometry,
          'properties'    => $properties
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'type'      => 'FeatureCollection',
      'features'  => $features
    );
  }
  echo json_encode($geo);
} else echo json_encode(false);
