<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`begin`',
			1 => 'a.`title`',
            2 => 'a.`list_to`',
            3 => 'a.`level`',
            4 => 'a.`beign`',
            5 => 'a.`end`',
            6 => 'a.`done`',
            7 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`title`, a.`note`, a.`files`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`title` LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`list_to` LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND a.`level` = " . (intval($requestData['columns'][3]['search']['value'])-1);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $d = $date->dmYtoYmd2($requestData['columns'][4]['search']['value']);
            $query .= " AND a.`begin` > '" . $d . "' AND a.`begin` < '" . date('Y-m-d H:i', $d + 86400) . "'";
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $d = $date->dmYtoYmd2($requestData['columns'][5]['search']['value']);
            $query .= " AND a.`end` > '" . $d . "' AND a.`end` < '" . date('Y-m-d H:i', $d + 86400) . "'";
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {
		    $code = intval($requestData['columns'][6]['search']['value'])-1;
		    if($code==0) {
                $query .= " AND a.`done` = 1";
            } elseif($code==1) {
                $query .= " AND a.`done` = 0 AND a.`end` > '" . date("Y-m-d H:i") . "'";
            } elseif($code==2) {
                $query .= " AND a.`done` = 0 AND a.`end` < '" . date("Y-m-d H:i") . "'";
            }
        }
        if( !empty($requestData['columns'][7]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][9]['search']['value']) . "%'";
        }

        $db->table = "jobs";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
        $db->table = "jobs";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`jobs_id`, a.`title`, a.`list_to`, a.`level`, a.`begin`, a.`end`, a.`done`, b.`full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;

            $list_to = array();
            $agency = $user = array();
            $list = json_decode($row['list_to']);
            $code = 'a';
            $agency = array_filter($list, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            $code = 'u';
            $user = array_filter($list, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            if(count($agency)>0) {
                $agency = str_replace('a', '', implode(',' , $agency));
                $db->table = "agency";
                $db->condition = "`agency_id` IN ($agency)";
                $db->order = "`sort` ASC";
                $db->limit = "";
                $rows_a = $db->select("`name`");
                foreach($rows_a as $row_a) {
                    array_push($list_to, stripslashes($row_a['name']));
                }
            }
            if(count($user)>0) {
                $user = str_replace('u', '', implode(',' , $user));
                $db->table = "core_user";
                $db->condition = "`user_id` IN ($user)";
                $db->order = "`full_name` ASC";
                $db->limit = "";
                $rows_u = $db->select("`full_name`");
                foreach($rows_u as $row_u) {
                    array_push($list_to, stripslashes($row_u['full_name']));
                }
            }

			$nestedData =   array();
			$nestedData[] = $i;
			$nestedData[] = '<a class="job-open" href="javascript:;" rel="' . intval($row['jobs_id']) . '">' . stripslashes($row['title']) . '</a>';
			$nestedData[] = implode('<br>', $list_to);
			$nestedData[] = jobsLevel($row['level']);
			$nestedData[] = $date->vnDate(strtotime($row['begin']));
            $nestedData[] = $date->vnDate(strtotime($row['end']));

            $status = '';
            if($row['done']==1) {
                $status = '<span class="lb-level-circle success">&nbsp;</span> Đã hoàn thành';
            } else {
                if(strtotime($row['end']) < time())
                    $status = '<span class="lb-level-circle deadline">&nbsp;</span> Trễ thời hạn';
                else
                    $status = '<span class="lb-level-circle proceed">&nbsp;</span> Đang tiến hành';
            }

            $nestedData[] = $status;
            $nestedData[] = stripslashes($row['full_name']);

			$tool = '<button type="button" class="btn btn-info btn-xs btn-round job-open" data-toggle="tooltip" data-placement="top" title="Xem lịch" rel="' . intval($row['jobs_id']) . '"><i class="fa fa-eye"></i></button>';
            if(in_array("jobs;delete", $corePrivilegeSlug['op'])) {
                $tool .= ' &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['jobs_id']) . '"></label>';
            }
            $nestedData[] = $tool;

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);

function jobsLevel($choice) {
    $color = array(
        0 => 'green',
        1 => 'orange',
        2 => 'red'
    );
    return '<span class="lb-level ' . $color[$choice] . '">&nbsp;</span>';
}