<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`begin`',
			1 => 'a.`title`',
            2 => 'a.`list_to`',
            3 => 'a.`level`',
            4 => 'a.`beign`',
            5 => 'a.`end`',
            6 => 'a.`done`'
		);

		$query = "a.`is_active` = 1 AND `type` = 1 AND a.`user_id` = " . $account["id"];

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`title`, a.`note`, a.`files`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`list_to` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND a.`level` = " . (intval($requestData['columns'][3]['search']['value'])-1);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $d = $date->dmYtoYmd2($requestData['columns'][4]['search']['value']);
            $query .= " AND a.`begin` > '" . $d . "' AND a.`begin` < '" . date('Y-m-d H:i', $d + 86400) . "'";
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $d = $date->dmYtoYmd2($requestData['columns'][5]['search']['value']);
            $query .= " AND a.`end` > '" . $d . "' AND a.`end` < '" . date('Y-m-d H:i', $d + 86400) . "'";
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {
		    $code = intval($requestData['columns'][6]['search']['value'])-1;
		    if($code==0) {
                $query .= " AND a.`done` = 1";
            } elseif($code==1) {
                $query .= " AND a.`done` = 0 AND a.`end` > '" . date("Y-m-d H:i") . "'";
            } elseif($code==2) {
                $query .= " AND a.`done` = 0 AND a.`end` < '" . date("Y-m-d H:i") . "'";
            }
        }

        $db->table = "jobs";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
        $db->table = "jobs";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`jobs_id`, a.`title`, a.`list_to`, a.`level`, a.`begin`, a.`end`, a.`done`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;

            $list_to = array();
            $agency = $user = array();
            $list = json_decode($row['list_to']);
            $code = 'a';
            $agency = array_filter($list, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            $code = 'u';
            $user = array_filter($list, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            if(count($agency)>0) {
                $agency = str_replace('a', '', implode(',' , $agency));
                $db->table = "agency";
                $db->condition = "`agency_id` IN ($agency)";
                $db->order = "`sort` ASC";
                $db->limit = "";
                $rows_a = $db->select("`name`");
                foreach($rows_a as $row_a) {
                    array_push($list_to, stripslashes($row_a['name']));
                }
            }
            if(count($user)>0) {
                $user = str_replace('u', '', implode(',' , $user));
                $db->table = "core_user";
                $db->condition = "`user_id` IN ($user)";
                $db->order = "`full_name` ASC";
                $db->limit = "";
                $rows_u = $db->select("`full_name`");
                foreach($rows_u as $row_u) {
                    array_push($list_to, stripslashes($row_u['full_name']));
                }
            }

			$nestedData =   array();
			$nestedData[] = $i;
			$nestedData[] = '<a class="job-open" href="javascript:;" rel="' . intval($row['jobs_id']) . '">' . stripslashes($row['title']) . '</a>';
			$nestedData[] = implode('<br>', $list_to);
			$nestedData[] = jobsLevel($row['level']);
			$nestedData[] = $date->vnDate(strtotime($row['begin']));
            $nestedData[] = $date->vnDate(strtotime($row['end']));

            $status = '';
            if($row['done']==1) {
                $status = '<span class="lb-level-circle success">&nbsp;</span> Đã hoàn thành';
            } else {
                if(strtotime($row['end']) < time())
                    $status = '<span class="lb-level-circle deadline">&nbsp;</span> Trễ thời hạn';
                else
                    $status = '<span class="lb-level-circle proceed">&nbsp;</span> Đang tiến hành';
            }

            $nestedData[] = $status;

			$tool = '<button type="button" class="btn btn-info btn-xs btn-round job-open" data-toggle="tooltip" data-placement="top" title="Xem lịch" rel="' . intval($row['jobs_id']) . '"><i class="fa fa-eye"></i></button>';
            if(in_array("job-edit", $corePrivilegeSlug['op'])) {
                $tool .= ' &nbsp; <a href="' . HOME_URL_LANG . '/jobs/job-edit?id=' . intval($row['jobs_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a>&nbsp;';
            }
            if(in_array("jobs;delete", $corePrivilegeSlug['op'])) {
                $tool .= ' &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['jobs_id']) . '"></label>';
            }
            $nestedData[] = $tool;

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	} elseif($type=='add') {
		$lng = "108.215";
		$lat = "16.059";
		?>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel"><i class="fa fa-pencil-square-o fa-fw"></i> Công việc cá nhân</h4>
				</div>
				<div class="modal-body">
					<div class="form-responsive">
						<form id="add_job" method="post" enctype="multipart/form-data" onsubmit="return jobs_ac('add_job', 'add_jobs');" class="form-ol-3w">
							<input type="hidden" name="lng" value="<?php echo $lng?>">
							<input type="hidden" name="lat" value="<?php echo $lat?>">
							<table class="table table-no-border table-hover">
								<tr>
									<td width="150px" align="right"><label class="form-lb-tp">Tên công việc:</label></td>
									<td><input class="form-control" type="text" name="title" maxlength="250" value="" autocomplete="off" required></td>
								</tr>
								<tr>
									<td align="right"><label class="form-lb-tp">Bắt đầu:</label></td>
									<td><input class="form-control ip-begin-datetime" type="text" name="begin" style="width: 150px;" value="<?php echo $date->vnDateTime(time());?>" maxlength="16" autocomplete="off"></td>
								</tr>
								<tr>
									<td align="right"><label class="form-lb-tp">Kết thúc:</label></td>
									<td><input class="form-control ip-end-datetime" type="text" name="end" style="width: 150px;" value="" maxlength="16" autocomplete="off"></td>
								</tr>
								<tr>
									<td align="right"><label class="form-lb-tp">Kết quả:</label></td>
									<td><div class="b-check"><input type="checkbox" value="1" name="done" id="b_done"><label for="b_done" data-off="Chưa hoàn thành" data-on="Hoàn thành"></label></div></td>
								</tr>
								<tr>
									<td align="right" class="ver-top"><label class="form-lb-tp">Têp đính kèm:</label></td>
									<td><input class="form-control file" type="file" name="files" data-show-upload="false" data-show-preview="false" data-max-file-count="1" placeholder="Chọn file đính kèm..." value=""></td>
								</tr>
								<tr>
									<td class="ver-top" align="right"><label class="form-lb-tp">Ghi chú:</label></td>
									<td><textarea class="form-control" name="note" rows="5"></textarea></td>
								</tr>
								<tr>
									<td align="right" class="ver-top"><label class="form-lb-tp">Vị trí:</label></td>
									<td>
										<div class="checkbox">
											<label><input type="checkbox" name="map" value="1"> HIỂN THỊ VỊ TRÍ</label>
										</div>
										<div style="position: relative; width: 400px; height: 300px;"><div id="map"></div></div>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="form-ol-btn-tzc">
										<button type="submit" class="btn btn-primary btn-round" name="add">Lưu lại</button> &nbsp; 
										<button type="reset" class="btn btn-warning btn-round">Nhập lại</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
			<script>
				var map = new mapboxgl.Map({
					container: 'map',
					style: '/map/style-cdn.json',
					attributionControl: false,
					center: [<?php echo $lng;?>, <?php echo $lat;?>],
					zoom: 14,
					hash: true
				});
				map.addControl(new mapboxgl.NavigationControl());

				var marker = new mapboxgl.Marker({
					draggable: true
				}).setLngLat([<?php echo $lng;?>, <?php echo $lat;?>]).addTo(map);

				function onDragEnd() {
					var lngLat = marker.getLngLat();
					$('input[name="lng"]').val(lngLat.lng);
					$('input[name="lat"]').val(lngLat.lat);
				}

				marker.on('dragend', onDragEnd);
			</script>
			<script>
				jQuery(function(){
					$('.ip-begin-datetime').datetimepicker({
						format  : 'd/m/Y H:i',
						lang    : 'vi',
						onShow  : function( ct ){
							this.setOptions({
								minDate : '+01/01/1970',
								maxDate : $('.ip-end-datetime').val() ? $('.ip-end-datetime').val() : false,
								format  : 'd/m/Y H:i',
								formatDate : 'd/m/Y H:i'
							})
						}
					});
					$('.ip-end-datetime').datetimepicker({
						format  : 'd/m/Y H:i',
						lang    : 'vi',
						onShow  :function( ct ){
							this.setOptions({
								minDate : $('.ip-begin-datetime').val()?$('.ip-begin-datetime').val():'+01/01/1970',
								maxDate : false,
								format  : 'd/m/Y H:i',
								formatDate : 'd/m/Y H:i'
							})
						}
					});
				});
				$('.file').fileinput({
					maxFileSize: 10240
				});
			</script>
		</div>
	<?php
	}

} else echo json_encode(false);

function jobsLevel($choice) {
    $color = array(
        0 => 'green',
        1 => 'orange',
        2 => 'red'
    );
    return '<span class="lb-level ' . $color[$choice] . '">&nbsp;</span>';
}