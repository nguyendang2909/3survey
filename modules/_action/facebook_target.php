<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`name`',
			1 => 'a.`name`',
			2 => 'a.`age`',
            3 => 'a.`address`',
            4 => 'a.`religion`',
            5 => 'a.`faction`',
            6 => 'a.`modified_time`',
            7 => 'b.`full_name`',
            8 => 'a.`facebook_type`'
		);

		$query = "a.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`name`, a.`age`, a.`address`, a.`religion`, a.`faction`, a.`social`, a.`note`, b.`full_name`, a.`facebook_type`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if (!empty($requestData['columns'][2]['search']['value'])) {
            $query .= " AND a.`age` = " . formatNumberToInt($requestData['columns'][2]['search']['value']);
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND a.`address` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND a.`religion` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND a.`faction` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][6]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][7]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][8]['search']['value']) ) {
            $query .= " AND a.`address` LIKE '%" . $db->clearText($requestData['columns'][8]['search']['value']) . "%'";
        }

        

		$db->table = "facebook_target";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "facebook_target";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`subject_id`, a.`name`, a.`age`, a.`address`, a.`religion`, a.`faction`, a.`modified_time`, b.`full_name`, a.`facebook_type`, a.`social`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData 			=   array();
			$nestedData['no'] 		= $i;
			$nestedData['name'] 	= stripslashes($row['name']);
			$nestedData['age'] 	    = doubleval($row['age']);
			$nestedData['address']  = stripslashes($row['address']);
            $nestedData['religion'] = stripslashes($row['religion']);
            $nestedData['faction']  = stripslashes($row['faction']);
            $nestedData['time'] 	= $date->vnDateTime($row['modified_time']);
            $nestedData['user'] 	= stripslashes($row['full_name']);
            $nestedData['facebookType'] 	= stripslashes($row['facebook_type']);
            $nestedData['social'] 	= stripslashes($row['social']);

			$tool = '';
			if(in_array("facebook-target-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/tracking/facebook-target-edit?id=' . intval($row['subject_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("facebook-target;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['subject_id']) . '"></label>';
            }
			
            $nestedData['tool'] 	= $tool;
			
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);