<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`calendar_id`',
			1 => 'a.`type`',
            2 => 'a.`year`',
            3 => 'a.`file`',
            4 => 'a.`note`',
            5 => 'a.`modified_time`',
            6 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`note`, a.`files`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`type` = " . intval($requestData['columns'][1]['search']['value']);
        }
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`year` = " . intval($requestData['columns'][2]['search']['value']);
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND a.`files` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND a.`note` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][5]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
        }

		$db->table = "calendar";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "calendar";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`calendar_id`, a.`type`, a.`week`, a.`month`, a.`year` , a.`files`, a.`note`, a.`modified_time`, b.`full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$file = '';
			if(file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'calendar' . DS . $row['files']) && ($row['files']!='')) {
				$file = '<a target="_blank" href="' . HOME_URL . '/uploads/calendar/' . stripslashes($row['files']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-lg"></i></a>';
			}
			
			$nestedData 			=   array();
			$nestedData['no'] 		= $i;
			if($row['type']==1) {
				$nestedData['type']	= 'Lịch tuần';
				$nestedData['calendar']	= 'Tuần ' . $row['week'] . ' /' . $row['year'];
			} elseif($row['type']==2) {
				$nestedData['type']	= 'Lịch tháng';
				$nestedData['calendar']	= 'Tháng ' . $row['month'] . ' /' . $row['year'];
			} elseif($row['type']==3){
				$nestedData['type']	= 'Lịch năm';
				$nestedData['calendar']	= 'Năm ' . $row['year'];
			}
				
            $nestedData['file'] 	= $file;				
            $nestedData['note'] 	= stripslashes($row['note']);
            $nestedData['time'] 	= $date->vnDateTime($row['modified_time']);
            $nestedData['user'] 	= stripslashes($row['full_name']);
			
			$tool = '';
			if(in_array("calendar-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/calendar/calendar-edit?id=' . intval($row['calendar_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("calendar;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['calendar_id']) . '"></label>';
            }
			
            $nestedData['tool'] 	= $tool;
			
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);