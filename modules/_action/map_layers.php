<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
    $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
    if($type=='save') {
        $geo    = isset($_POST['geo']) ? trim($_POST['geo']) : '';
        $date   = new DateClass();
        //---
        /*
        $db->table = "map";
        $data = array(
            'is_active' => 0
        );
        $db->condition = "`is_active` = 1";
        $db->update($data);*/

        $db->table = "map";
        $data = array(
            'geo'           => $db->clearText($geo),
            'modified_time' => time(),
            'user_id'       => intval($account["id"])
        );
        $db->insert($data);

    } elseif($type=='load') {
        $features = array();
        $db->table = "map";
        $db->condition = "`is_active` = 1";
        $db->order = "`modified_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach ($rows as $row) {
                $rs = json_decode($row['geo'], true);
                $features = array_merge($features, $rs['features']);
            }
        }

        $geo = array(
            'type'      => 'FeatureCollection',
            'features'  => $features
        );
        echo json_encode($geo);
    }
}