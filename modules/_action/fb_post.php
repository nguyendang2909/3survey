<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 ) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $reactiveId   = isset($_GET['reactiveId']) ? $_GET['reactiveId'] : 0;
  $date   = new DateClass();

  if($type == 'load'){
    $requestData = $_REQUEST;
    $columns = array(
      0 => '`name`',
      1 => '`name`',
      2 => '`content`',
      3 => '`react`',
      4 => '`share`',
      5 => '`comment`',
      6 => '`dateTime`',
    );
  
    $db->table = "reactive";
    $db->condition = "`reactiveId` = " .$reactiveId;
    $db->order = '';
    $db->limit = '';
    $rows = $db->select('`fbTargetId`');
    $listFbTargetId = json_decode($rows[0]['fbTargetId']);
  
    $query = "(a.`fbTargetId` = " .$listFbTargetId[0];
    if(count($listFbTargetId)>1)
    {
      $dem;
      for($dem =1; $dem< count($listFbTargetId); $dem++)
      {
        $query .= " OR a.`fbTargetId` =" .$listFbTargetId[$dem];
      }
    }
    $query .= ")";
  
    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(b.`name`,a.`content`, a.`dateTime`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }
  
    if (!empty($requestData['columns'][1]['search']['value'])) {
			$query .= " AND b.`name` LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
		}

    if (!empty($requestData['columns'][2]['search']['value'])) {
			$query .= " AND a.`content` LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
		}

    if (!empty($requestData['columns'][6]['search']['value'])) {
			$query .= " AND a.`dateTime` LIKE '%" . trim($db->clearText($requestData['columns'][6]['search']['value'])) . "%'";
		}


    // Tim kiem va Count
    $db->table = "fb_report";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "fb_target` b ON a.`fbTargetId` = b.`fbTargetId`";
    $db->condition = $query;
    $db->order = "a.`dateTime` DESC";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;
  
    // Danh sach record
    $data = array();
    $db->table = "fb_report";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "fb_target` b ON a.`fbTargetId` = b.`fbTargetId`";
    $db->condition = $query;
    $db->order = "a.`dateTime` DESC";
    // $db->order =$columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit='';
    $rows = $db->select("
        b.`name`,
        a.`content`,
        a.`react`,
        a.`share`,
        a.`comment`,
        a.`dateTime`
        ");
        $i = 0;
    // $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;
  
      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['content'] = stripslashes($row['content']);
      $nestedData['react'] = stripslashes($row['react']);
      $nestedData['share'] = stripslashes($row['share']);
      $nestedData['comment'] = stripslashes($row['comment']);
      $nestedData['dateTime'] = stripslashes($row['dateTime']);
  
      $data[] = $nestedData;
    }
  
    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );
  
    echo json_encode($json_data);
  }
} else echo json_encode(false);
