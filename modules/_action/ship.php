<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_POST['type'])) {
  $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
  $date   = new DateClass();

  // TODO: search fields
  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`shipTypeId`',
      1 => 'a.`name`',
      2 => 'a.`file`',
      3 => 'a.`note`',
      4 => 'a.`updatedAt`',
      5 => 'b.`updatedBy`',
      6 => 'a.`name`',
    );

    // Tim cac active record (chua bi xoa)
    $query = "a.`isActive` = 1 AND b.`is_active` = 1";

    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`name`, a.`note`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
    }

    // Tim ten
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
    }

    // Tim note
    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND a.`note` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
    }

    // Tim ngay cap nhat
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $searchDate = $date->dmYtoYmd($requestData['columns'][4]['search']['value']);
      $query .= ' AND DATE(a.`updatedAt`) = DATE("' . $searchDate . '")';
    }

    // Tim nguoi cap nhat
    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
    }

    // Tim kiem va Count
    $db->table = "ship";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = 'ship';
    $db->join = 'a LEFT JOIN `' . TTH_DATA_PREFIX . 'ship_type` c ON a.`shipTypeId` = c.`shipTypeId`
                   LEFT JOIN `' . TTH_DATA_PREFIX . 'country` d ON a.`countryId` = d.`countryId`
                   LEFT JOIN `' . TTH_DATA_PREFIX . 'ship_weapon` e ON a.`shipWeaponId` = e.`shipWeaponId`
                   LEFT JOIN `' . TTH_DATA_PREFIX . 'core_user` b ON a.`updatedBy` = b.`user_id`';
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("a.`shipId`, a.`name`, c.`name` AS `type` , d.`file` AS `countryIcon`, d.`name` as `country`, a.`mmsi`, a.`callsign`, a.`imo`, a.`length`, a.`width`, a.`height`, a.`weight`, a.`maxSpeed`, a.`capital`, a.`sailorCount`, a.`workingRange`, e.`name` AS `weapon` , b.`full_name` AS `updatedBy`");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/tracking/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['type'] = stripslashes($row['type']);
      $nestedData['country'] = $row['countryIcon'] != '' ? '<img src="/uploads/country/' . stripslashes($row['countryIcon']) . '" / style= {width="20px" height="20px"} alt="' . stripslashes($row['country'])  . '" title="' . stripslashes($row['country']) . '">' : '-';
      $nestedData['size'] = floatval($row['length']) . ' x ' . floatval($row['width']) . ' x ' . floatval($row['height']);
      $nestedData['weight'] = floatval($row['weight']) == 0 ? '-' : floatval($row['weight']);
      $nestedData['maxSpeed'] = floatval($row['maxSpeed']) == 0 ? '-' : floatval($row['maxSpeed']);
      $nestedData['capital'] = stripslashes($row['capital']);
      $nestedData['sailorCount'] = stripslashes($row['sailorCount']);
      $nestedData['weapon'] = stripslashes($row['weapon']);
      $nestedData['file'] = $file;
      $nestedData['mmsi'] = intval($row['mmsi']);
      $nestedData['callsign'] = stripslashes($row['callsign']);
      $nestedData['imo'] = stripslashes($row['imo']);

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("ship-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/ship-edit?id=' . intval($row['shipId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("ship;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['shipId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
