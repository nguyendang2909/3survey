<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $id		= isset($_POST['id']) ? intval($_POST['id']) : 0;
    $choice	= isset($_POST['choice']) ? intval($_POST['choice']) : 0;
    $L_like = 0;

    $db->table = "like";
    $db->condition = "`type` LIKE '$type' AND `id` = $id AND `user_id` = " . $account["id"];
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select();
    if($db->RowCount) {
        foreach($rows as $row) {
            if(intval($row['c_like'])==$choice) {
                $db->table = "like";
                $db->condition = "`type` LIKE '$type' AND `id` = $id AND `user_id` = " . $account["id"];
                $db->delete();
                if($db->AffectedRows) $L_like = 0;
            } else {
                $db->table = "like";
                $data = array(
                    'c_like'        => $choice,
                    'modified_time' => time()
                );
                $db->condition = "`type` LIKE '$type' AND `id` = $id AND `user_id` = " . $account["id"];
                $db->update($data);
                if($db->AffectedRows) $L_like = $choice;
            }
        }
    } else {
        $db->table = "like";
        $data = array(
            'type'          => $type,
            'id'            => $id,
            'c_like'        => $choice,
            'created_time'  => time(),
            'user_id'       => $account["id"]
        );
        $db->insert($data);
        if($db->LastInsertID) $L_like = $choice;
    }

    $c_like = $c_dislike = 0;
    $db->table = "like";
    $db->condition = "`type` LIKE '$type' AND `id` = $id";
    $db->order = "";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            if(intval($row['c_like'])==1) $c_like++;
            elseif(intval($row['c_like'])==-1) $c_dislike++;
        }
    }

    if($L_like==1)
        echo '<a class="ol-like-1 ol-active" href="javascript:;" data-id="' . $id . '" data-role="1"><i class="fa fa-thumbs-up fa-fw"></i><span>' . formatNumberVN($c_like) . '</span></a>';
    else
        echo '<a class="ol-like-1" href="javascript:;" data-id="' . $id . '" data-role="1"><i class="fa fa-thumbs-o-up fa-fw"></i><span>' . formatNumberVN($c_like) . '</span></a>';

    if($L_like==-1)
        echo '<a class="ol-dislike-2 ol-active" href="javascript:;" data-id="' . $id . '" data-role="-1"><i class="fa fa-thumbs-down fa-fw"></i><span>' . formatNumberVN($c_dislike) . '</span></a>';
    else
        echo '<a class="ol-dislike-2 " href="javascript:;" data-id="' . $id . '" data-role="-1"><i class="fa fa-thumbs-o-down fa-fw"></i><span>' . formatNumberVN($c_dislike) . '</span></a>';

} else echo '-Error-';