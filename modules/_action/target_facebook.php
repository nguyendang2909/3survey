<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`name`',
			1 => 'a.`name`',
			2 => 'a.`link`',
            3 => 'a.`user`',
            4 => 'a.`type`',
            5 => 'a.`modified_time`',
            6 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";
		
		// if( !empty($requestData['search']['value']) ) {
		// 	$query .= " AND CONCAT(a.`name`, a.`link`, c.`full_name`, a.`type`, a.`note`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		// }
        // if( !empty($requestData['columns'][1]['search']['value']) ) {
        //     $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		// }
        // if( !empty($requestData['columns'][2]['search']['value']) ) {
        //     $query .= " AND a.`link` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
		// }
        // if( !empty($requestData['columns'][3]['search']['value']) ) {
        //     $query .= " AND c.`full_name` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        // }
        // if( !empty($requestData['columns'][4]['search']['value']) ) {
        //     $query .= " AND a.`type` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        // }
        // if (!empty($requestData['columns'][5]['search']['value'])) {
        //     $d1 = strtotime($date->dmYtoYmd($requestData['columns'][5]['search']['value']));
        //     $d2 = $d1 + 86400;
        //     $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        // }
        // if( !empty($requestData['columns'][6]['search']['value']) ) {
        //     $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
        // }

		$db->table = "reactive";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` c ON a.`user` = c.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "target_facebook";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` c ON a.`user` = c.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`target_id`, a.`name`, a.`link`, a.`user` AS `target_group`, a.`type`, a.`modified_time`, b.`full_name` AS `name2`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData 			=   array();
			$nestedData['no'] 		= $i;
			$nestedData['name'] 	= stripslashes($row['name']);
			$nestedData['link']     = '<a target="_blank" href="' . stripslashes($row['link']) . '">' . stripslashes($row['link']) . '</a>';
			$nestedData['target_group']    = stripslashes($row['target_group']);
            $nestedData['type']  = stripslashes($row['type']);
            $nestedData['time'] 	= $date->vnDateTime($row['modified_time']);
            $nestedData['user2'] 	= stripslashes($row['name2']);
			
			$tool = '';
			if(in_array("target_facebook_edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/tracking/target_facebook_edit?id=' . intval($row['target_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("tracking-reactive;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['target_id']) . '"></label>';
            }
			
            $nestedData['tool'] 	= $tool;
			
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);