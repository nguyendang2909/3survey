<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`' . TTH_DATA_PREFIX . 'local`.`sort`',
			1 => '`' . TTH_DATA_PREFIX . 'local`.`title`',
            2 => '`' . TTH_DATA_PREFIX . 'local`.`sort`',
            3 => '`' . TTH_DATA_PREFIX . 'local`.`modified_time`',
            4 => '`' . TTH_DATA_PREFIX . 'core_user`.`full_name`'
		);

		$query = "`" . TTH_DATA_PREFIX . "local`.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(`" . TTH_DATA_PREFIX . "local`.`title`, `" . TTH_DATA_PREFIX . "core_user`.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "local`.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if (!empty($requestData['columns'][2]['search']['value'])) {
            $query .= " AND `" . TTH_DATA_PREFIX . "local`.`sort` = " . formatNumberToInt($requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][3]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND `" . TTH_DATA_PREFIX . "local`.`modified_time` >= $d1 AND `" . TTH_DATA_PREFIX . "local`.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "core_user`.`full_name` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }

        $db->table = "local";
        $db->condition = "";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $total = $db->RowCount;
        foreach($rows as $row) {
            $total = $row['count'];
        }

		$db->table = "local";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "local`.`user_id`";
        $db->condition = $query;
		$db->order = "";
		$db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        //---
		$db->table = "local";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "local`.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`local_id`, `" . TTH_DATA_PREFIX . "local`.`title` AS `title`, `" . TTH_DATA_PREFIX . "local`.`sort` AS `sort`, `" . TTH_DATA_PREFIX . "local`.`modified_time` AS `modified_time`, `" . TTH_DATA_PREFIX . "core_user`.`full_name` AS `full_name`");
		$i = $requestData['start'];
        $data = array();
        foreach($rows as $row) {
		    $sort = '';
            if(in_array("tracking-local-edit", $corePrivilegeSlug['op'])) {
                $sort = showSort("sort_" . $row["local_id"] . "", $total, $row["sort"], '90%', 0, $row["local_id"], 'local', 1);
            } else {
                $sort = showSort("sort_" . $row["local_id"] . "", $total, $row["sort"], '90%', 0, $row["local_id"], 'local', 0);
            }

            $i++;
			$nestedData =   array();
			$nestedData[] = $i;
			$nestedData[] = stripslashes($row['title']);
            $nestedData[] = $sort;
            $nestedData[] = $date->vnDateTime($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);

			$nestedData[] = '<a href="' . HOME_URL_LANG . '/tracking/tracking-local-edit?id=' . intval($row['local_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Chọn xoá" class="ol-checkbox-js" name="tick[]" value="' . intval($row['local_id']) . '"></label>';
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);