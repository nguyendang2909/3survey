<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`network_admin_id`',
			1 => 'a.`name`',
			2 => 'a.`apply`',
            3 => 'c.`name`',
			4 => 'a.`tel`',
			5 => 'a.`email`',
			6 => 'a.`address`',
            7 => 'a.`modified_time`',
            8 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`name`, a.`apply`, a.`tel`, a.`email`, a.`address`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`apply` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
		}
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $query .= " AND c.`name` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND a.`tel` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND a.`email` LIKE '%" . trim($requestData['columns'][5]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $query .= " AND a.`address` LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
		}
        if (!empty($requestData['columns'][7]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][7]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][8]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][8]['search']['value']) . "%'";
        }

		$db->table = "network_admin";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "network_admin";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`network_admin_id`, a.`name`, a.`apply`, c.`name` AS `agency`, a.`tel`, a.`email`, a.`address`, a.`modified_time`, b.`full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData	  = array();
			$nestedData[] = $i;
			$nestedData[] = stripslashes($row['name']);
			$nestedData[] = stripslashes($row['apply']);
            $nestedData[] = stripslashes($row['agency']);
            $nestedData[] = stripslashes($row['tel']);
            $nestedData[] = stripslashes($row['email']);
            $nestedData[] = stripslashes($row['address']);
            $nestedData[] = $date->vnDateTime($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);
			
			$tool = '';
			if(in_array("network-admin-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/network/network-admin-edit?id=' . intval($row['network_admin_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("network-admin;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['network_admin_id']) . '"></label>';
            }
			
            $nestedData[] = $tool;
			
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);