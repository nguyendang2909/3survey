<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if(isset($_POST['page'])) {
    $page       = empty($_POST['page']) ? 0 : intval($_POST['page']);
    $menu       = isset($_POST['menu']) ? intval($_POST['menu']) : 0;
    $gird       = isset($_POST['gird']) ? intval($_POST['gird']) : 1;
    $list       = getBlogElementPlus($menu);
    $per_page   = 20;
    $start      = ($page-1) * $per_page;
    $where      = ($menu > 0) ? "`created_time` DESC" : "`hot` DESC, `created_time` DESC";
    $result     = '';
    $stringObj  = new String();

    $db->table = "blog_post";
    $db->condition = "`is_active` = 1 AND `blog` IN ($list)";
    $db->order = $where;
    $db->limit = $start . ", " . ($per_page+1);
    $rows = $db->select("`blog_post_id`, `blog`, `name`, `slug`, `img`, `comment`, `created_time`, `views`, `user_id`");
    $total = $db->RowCount;
    if($total>0) {
        $i = 0;
        if($gird==1) {
            foreach ($rows as $row) {
                $i++;
                $photo_avt = '';
                $slug = getSlugBlogParent0($row['blog']);
                $p_user = getInfoUser2($row['user_id']);

                if (file_exists(ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS . stripslashes('post-' . $row['img'])) && !empty($row['img'])) {
                    $photo_avt = '<img src="' . HOME_URL . '/uploads/blog/post-' . stripslashes($row['img']) . '" alt="' . stripslashes($row['name']) . '">';
                } else {
                    $photo_avt = '<img src="' . HOME_URL . '/images/404-post.jpg" alt="' . stripslashes($row['name']) . '">';
                }
                $photo_avt = '<div class="img"><a href="' . HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . $photo_avt . '</a></div>';

                $result .= '<div class="blog-item"><div class="blog-box">';
                $result .= $photo_avt;
                $result .= '<div class="blog-description">';
                $result .= '<div class="blog-text">';
                $result .= '<h2 class="blog-title"><a href="' . HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
                $result .= '<p class="blog-comment">' . $stringObj->crop(stripslashes($row['comment']), 37) . '</p>';
                $result .= '</div>';
                $result .= '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $slug) . '</div>';
                if (!empty($p_user)) $result .= '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
                $result .= '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                $result .= '</div>';
                $result .= '</div></div>';
                if ($i == $per_page) break;
            }
        } else {
            foreach ($rows as $row) {
                $i++;
                $slug = getSlugBlogParent0($row['blog']);
                $p_user = getInfoUser2($row['user_id']);

                $result .= '<div class="blog-item grid"><div class="blog-box">';
                $result .= '<div class="blog-description">';
                $result .= '<div class="blog-left">';
                $result .= '<div class="blog-text">';
                $result .= '<h2 class="blog-title"><a href="' . HOME_URL_LANG . '/' . $slug . '/' . stripslashes($row['slug']) . '" title="' . stripslashes($row['name']) . '">' . stripslashes($row['name']) . '</a></h2>';
                $result .= '<p class="blog-comment">' . stripslashes($row['comment']) . '</p>';
                $result .= '</div>';
                $result .= '<div class="blog-tags">' . getBlogTags(intval($row['blog']), $slug) . '</div>';
                $result .= '<div class="blog-stats"><span class="time"><i class="fa fa-calendar fa-fw"></i> ' . convertTimeDayAgo($row['created_time']) . '</span> - <span class="views">' . formatNumberVN($row['views']) . ' <i class="fa fa-eye fa-fw"></i></span></div>';
                $result .= '</div>';
                if (!empty($p_user)) {
                    $result .= '<div class="blog-right">';
                    $result .= '<div class="blog-user"><div class="blog-avatar">' . $p_user[4] . '</div><div class="blog-user-cap"><h4>' . $p_user[0] . '</h4><p>' . $p_user[1] . '</p></div></div>';
                    $result .= '</div>';
                }
                $result .= '</div>';
                $result .= '</div></div>';
                
                if ($i == $per_page) break;
            }
        }
    }

    echo json_encode( array( "limit" => $total-$per_page, "post" => $result) );

} else echo json_encode(false);