<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
	$list = loadAgency(0);
	
	echo json_encode($list, JSON_UNESCAPED_UNICODE);
}
function loadAgency($parent){
    global $db;
    $result = array();
    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
		$arr = loadAgency($row['agency_id']);
		if(count($arr) > 0)
			$result[] = array("id" => $row['agency_id'], "text" => stripslashes($row['name']), "state" => 'closed', "children" => $arr);
		else
			$result[] = array("id" => $row['agency_id'], "text" => stripslashes($row['name']), "iconCls" => 'icon-tip');
    }
    return $result;
}