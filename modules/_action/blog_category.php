<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`' . TTH_DATA_PREFIX . 'blog`.`name`',
            1 => '`' . TTH_DATA_PREFIX . 'blog`.`sort`',
            2 => '`' . TTH_DATA_PREFIX . 'blog`.`is_active`',
            3 => '`' . TTH_DATA_PREFIX . 'blog`.`hot`',
            4 => '`' . TTH_DATA_PREFIX . 'blog`.`created_time`',
            5 => '`' . TTH_DATA_PREFIX . 'core_user`.`full_name`',
            6 => '`' . TTH_DATA_PREFIX . 'blog`.`sort`'
        );

		$query = "1";

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(`" . TTH_DATA_PREFIX . "blog`.`name`, `" . TTH_DATA_PREFIX . "blog`.`slug`, `" . TTH_DATA_PREFIX . "core_user`.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}

		if( !empty($requestData['columns'][0]['search']['value']) ) {
			$query .= " AND `" . TTH_DATA_PREFIX . "blog`.`name` LIKE '%" . $db->clearText($requestData['columns'][0]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "blog`.`sort` = " . intval($requestData['columns'][1]['search']['value']);
        }
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "blog`.`is_active` = " . (intval($requestData['columns'][2]['search']['value'])-1);
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "blog`.`hot` = " . (intval($requestData['columns'][3]['search']['value'])-1);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][4]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND `" . TTH_DATA_PREFIX . "blog`.`created_time` >= $d1 AND `" . TTH_DATA_PREFIX . "blog`.`created_time` <= $d2";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "core_user`.`full_name` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }

        $db->table = "blog";
        $db->condition = "";
        $db->order = "";
        $db->limit = "";
        $db->select();
        $total = $db->RowCount;

		$db->table = "blog";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "blog`.`user_id`";
        $db->condition = $query;
		$db->order = "";
		$db->limit = "";
		$db->select();
		$totalData = $db->RowCount;
		$totalFiltered = $totalData;

		$db->table = "blog";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "blog`.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`blog_id`, `" . TTH_DATA_PREFIX . "blog`.`name` AS `name`, `" . TTH_DATA_PREFIX . "blog`.`sort` AS `sort`, `" . TTH_DATA_PREFIX . "blog`.`is_active` AS `active`, `" . TTH_DATA_PREFIX . "blog`.`hot` AS `hot`, `" . TTH_DATA_PREFIX . "blog`.`created_time` AS `created_time`, `" . TTH_DATA_PREFIX . "core_user`.`full_name` AS `full_name`");
		$i = $requestData['start'];
        $data = array();
        foreach($rows as $row) {
			$i++;
			$nestedData =   array();

			$active = $hot = $sort = $delete = '';
            if(in_array("blog_post_edit", $corePrivilegeSlug['op'])) {
                $sort = showSort("sort_" . $row["blog_id"] . "", $total, $row["sort"], '90%', 0, $row["blog_id"], 'blog', 1);
                $active = (intval($row['active']) == 0) ?
                    '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status($(this), ' . intval($row["blog_id"]) . ', \'is_active\', \'blog\');" rel="1">0</div>'
                    :
                    '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status($(this), ' . intval($row["blog_id"]) . ', \'is_active\', \'blog\');" rel="0">1</div>';

                $hot = (intval($row['hot']) == 0) ?
                    '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status($(this), ' . intval($row["blog_id"]) . ', \'hot\', \'blog\');" rel="1">0</div>'
                    :
                    '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status($(this), ' . intval($row["blog_id"]) . ', \'hot\', \'blog\');" rel="0">1</div>';
            } else {
                $sort = showSort("sort_" . $row["blog_id"] . "", $total, $row["sort"], '90%', 0, $row["blog_id"], 'blog', 0);
                $active = (intval($row['active']) == 0) ?
                    '<div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div>'
                    :
                    '<div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div>';

                $hot = (intval($row['hot']) == 0) ?
                    '<div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div>'
                    :
                    '<div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div>';
            }

            $nestedData[] = stripslashes($row['name']);
            $nestedData[] = $sort;
            $nestedData[] = $active;
            $nestedData[] = $hot;
            $nestedData[] = $date->vnDateTime($row['created_time']);
            $nestedData[] = stripslashes($row['full_name']);
            $nestedData[] = '<a href=""><img data-toggle="tooltip" data-placement="left" title="Thêm mục" src="images/add.png"></a> &nbsp; <a href="/?ol=blog&op=blog_category_edit&id=' . intval($row['blog_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="images/edit.png"></a> &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Chọn xoá" class="ol-checkbox-js" name="tick[]" value="' . intval($row['blog_id']) . '"></label><a href="/?ol=blog&op=blog_post_add&blog=' . intval($row["blog_id"]) . '"><img data-toggle="tooltip" data-placement="top" title="Thêm một bài viết mới" src="images/list.png"></a>';

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);