<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`content`',
			1 => 'a.`content`',
			2 => 'a.`archives`',
            3 => 'a.`created_date`',
            4 => 'a.`keywords`',
			5 => 'a.`attached`',
            6=> 'a.`modified_time`',
            7 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`content`, a.`archives`, a.`keywords`, a.`note`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`content` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`archives` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
		}
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $d = $date->dmYtoYmd2($requestData['columns'][3]['search']['value']);
            $query .= " AND a.`created_date` = '" . $d . "'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND a.`keywords` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
		if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND a.`attached` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][6]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][7]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
        }

		$db->table = "topic";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "topic";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`topic_id`, a.`content`, a.`archives`, a.`created_date`, a.`keywords`,a.`attached`, a.`modified_time`, b.`full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$file = '';
			if(file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'topic' . DS . $row['attached']) && ($row['attached']!='')) {
				$file = '<a target="_blank" href="' . HOME_URL . '/uploads/topic/' . stripslashes($row['attached']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-lg"></i></a>';
			}
			$nestedData 			=   array();
			$nestedData['no'] 		= $i;
			$nestedData['content'] 	= stripslashes($row['content']);
			$nestedData['archives'] = stripslashes($row['archives']);
			$nestedData['created']  = $date->vnDate(strtotime($row['created_date']));
            $nestedData['keywords'] = stripslashes($row['keywords']);
			$nestedData['attached'] = $file;
            $nestedData['time'] 	= $date->vnDateTime($row['modified_time']);
            $nestedData['user'] 	= stripslashes($row['full_name']);
			
			$tool = '';
			if(in_array("tracking-topic-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/tracking/tracking-topic-edit?id=' . intval($row['topic_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("tracking-topic;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['topic_id']) . '"></label>';
            }
			
            $nestedData['tool'] 	= $tool;
			
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);