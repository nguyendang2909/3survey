<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type       = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date       = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`created_time`',
            1 => 'a.`icon`',
            2 => 'a.`title`',
            3 => 'a.`note`',
            4 => 'a.`modified_time`',
            5 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`title`, a.`note`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}

		if( !empty($requestData['columns'][1]['search']['value']) ) {
			$query .= " AND a.`icon` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`title` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND a.`note` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][5]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][6]['search']['value']) . "%'";
        }

		$db->table = "maps_type";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;

		$db->table = "maps_type";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`user_id` = b.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`maps_type_id`, a.`icon`, a.`title`, a.`note`, a.`modified_time`, a.`user_id`, b.`full_name`");
		$data = array();
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData =   array();

			$icon = '';
			if($row['icon']=='-no-') $icon = '<img data-toggle="tooltip" data-placement="top" title="Không có hình" src="/images/error.png">';
			else $icon = '<img data-toggle="tooltip" data-placement="top" title="' . stripslashes($row['title']) . '" src="' . HOME_URL_LANG . '/uploads/maps/' . stripslashes($row['icon']) . '">';

			$nestedData[] = $i;
            $nestedData[] = $icon;
			$nestedData[] = stripslashes($row['title']);
            $nestedData[] = stripslashes($row['note']);
            $nestedData[] = $date->vnDateTime($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);
            $nestedData[] = '<a href="' . HOME_URL_LANG . '/maps/maps-type-edit?id=' . intval($row['maps_type_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Chọn xoá" class="ol-checkbox-js" name="tick[]" value="' . intval($row['maps_type_id']) . '"></label>';

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);