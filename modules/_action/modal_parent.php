<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $id     = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $result = '';
    if($type=='cash' && in_array("cash_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'cash\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm sổ quỹ</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'cash\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm sổ quỹ</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    } elseif($type=='cost' && in_array("cost_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'cost\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục chi phí</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'cost\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục chi phí</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    } elseif($type=='asset' && in_array("asset_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'asset\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục tài sản</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'asset\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục tài sản</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    } elseif($type=='quiz' && in_array("quiz_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'quiz\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục câu hỏi trắc nghiệm</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'quiz\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục câu hỏi trắc nghiệm</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    } elseif($type=='document' && in_array("document_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'document\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục tài liệu</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'document\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục tài liệu</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    } elseif($type=='rules' && in_array("rules_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'rules\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm nội quy</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'rules\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm nội quy</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    } elseif($type=='customer' && in_array("customer_parent", $corePrivilegeSlug['op'])) {
        $db->table = "parents";
        $db->condition = "`is_active` = 1 AND `parents_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $result .= '<div class="modal-dialog"><div class="modal-content">';
                $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'edit\', \'customer\', ' . intval($row['parents_id']) .');">';
                $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục khách hàng</h4></div>';
                $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
                $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="' . stripslashes($row['title']) . '" maxlength="250" required></td></tr>';
                $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment">' . stripslashes($row['comment']) . '</textarea></td></tr>';
                $result .= '</table></div></div>';
                $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Chỉnh sửa nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
                $result .= ' </form>';
                $result .= '</div></div>';
            }
        } else {
            $result .= '<div class="modal-dialog"><div class="modal-content">';
            $result .= '<form id="_parent" class="form-ol-3w" method="post" onsubmit="return update_parent(\'add\', \'customer\', 0);">';
            $result .= '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">Nhóm danh mục khách hàng</h4></div>';
            $result .= '<div class="modal-body"><div class="form-responsive report-top"><table class="table table-no-border table-hover">';
            $result .= '<tr><td width="120px" align="right"><label class="form-lb-tp">Tiêu đề nhóm:</label></td><td><input class="form-control" type="text" name="title" value="" maxlength="250" required></td></tr>';
            $result .= '<tr><td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td><td><textarea class="form-control" rows="3" name="comment"></textarea></td></tr>';
            $result .= '</table></div></div>';
            $result .= '<div class="modal-footer"><button type="submit" class="btn btn-primary btn-round">Thêm nhóm</button><button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Thoát</button></div>';
            $result .= ' </form>';
            $result .= '</div></div>';
        }
    }
	
    echo $result;
	
} else echo '-Error-';
