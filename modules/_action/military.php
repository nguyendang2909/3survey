<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`age`',
      1 => 'a.`age`',
      2 => 'a.`education`',
      3 => 'a.`type`',
      4 => 'a.`social`',
      5 => 'a.`internetUsageTime`',
      6 => 'a.`internetUsagePurpose`',
      7 => 'a.`postStatus`',
      8 => 'a.`postAction`',
      9 => 'a.`identifyAuthen`',
      10 => 'a.`assessOnlineCommunity`',
      11 => 'a.`followingPage`',
      12 => 'a.`pageEffective`',
      13 => 'a.`note`',
      14 => 'a.`age`',
    );

    $query = "a.`isActive` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`age`, a.`education`, a.`sex`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND " . $columns[1] . " LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND " . $columns[2] . " LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND " . $columns[3] . " LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND " . $columns[4] . " LIKE '%" . trim($db->clearText($requestData['columns'][4]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND " . $columns[5] . " LIKE '%" . trim($db->clearText($requestData['columns'][5]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][6]['search']['value'])) {
      $query .= " AND " . $columns[6] . " LIKE '%" . trim($db->clearText($requestData['columns'][6]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][7]['search']['value'])) {
      $query .= " AND " . $columns[7] . " LIKE '%" . trim($db->clearText($requestData['columns'][7]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][8]['search']['value'])) {
      $query .= " AND " . $columns[8] . " LIKE '%" . trim($db->clearText($requestData['columns'][8]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][9]['search']['value'])) {
      $query .= " AND " . $columns[9] . " LIKE '%" . trim($db->clearText($requestData['columns'][9]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][10]['search']['value'])) {
      $query .= " AND " . $columns[10] . " LIKE '%" . trim($db->clearText($requestData['columns'][10]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][11]['search']['value'])) {
      $query .= " AND " . $columns[11] . " LIKE '%" . trim($db->clearText($requestData['columns'][11]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][12]['search']['value'])) {
      $query .= " AND " . $columns[12] . " LIKE '%" . trim($db->clearText($requestData['columns'][12]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][13]['search']['value'])) {
      $query .= " AND " . $columns[13] . " LIKE '%" . trim($db->clearText($requestData['columns'][13]['search']['value'])) . "%'";
    }

    // Tim kiem va Count
    $db->table = "military_agency";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "military_agency";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("
      a.`militaryAgencyId`,
      a.`age`,
      a.`education`,
      a.`type`,
      a.`social`,
      a.`internetUsageTime`,
      a.`internetUsagePurpose`,
      a.`postStatus`,
      a.`postAction`,
      a.`identifyAuthen`,
      a.`assessOnlineCommunity`,
      a.`followingPage`,
      a.`pageEffective`,
      a.`note`
      ");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['age'] = intval($row['age']);
      $nestedData['education'] = stripslashes($row['education']);
      $nestedData['type'] = stripslashes($row['sex']) == 0 ? 'Nam': 'Nữ';
      $nestedData['social'] = implode(', ', json_decode($row['social']));
      $nestedData['internetUsageTime'] = intval($row['internetUsageTime']);
      $nestedData['internetUsagePurpose'] = implode(', ', json_decode($row['internetUsagePurpose']));
      $nestedData['postStatus'] = intval($row['postStatus']);
      $nestedData['postAction'] = implode(', ', json_decode($row['postAction']));
      $nestedData['identifyAuthen'] = intval($row['identifyAuthen']);
      $nestedData['assessOnlineCommunity'] = intval($row['assessOnlineCommunity']);
      $nestedData['followingPage'] = intval($row['followingPage']);
      $nestedData['pageEffective'] = intval($row['pageEffective']);
      $nestedData['note'] = '
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#noteModal'. $i .'">Xem</button>
      <div class="modal fade" id="noteModal'. $i .'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Ý kiến cá nhân</h5></div>
            <div class="modal-body">' . stripslashes($row['note']) . '</div>
          </div>
        </div>
      </div>';

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("military-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/military/military-edit?id=' . intval($row['militaryAgencyId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("military;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['militaryAgencyId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
