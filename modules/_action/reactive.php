<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => '`name`',
      1 => '`name`',
      2 => '`hometown`',
      3 => '`address`',
      4 => '`birthdayYear`',
      5 => '`religion`',
      6 => '`name`',
      7 => '`contact`',
      8 => '`isWarning`',
      9 => '`name`',
      10 => '`name`',
    );

    $query = "`isActive` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(`name`, `hometown`, `address`, `birthdayYear`, `religion`, `name`, `contact`, `note`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }
    // Tim ten
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND " . $columns[1] . " LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
    }
    // Tim dia ban
    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND " . $columns[2] . " LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
    }
    // Tim nam thanh lap
    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND " . $columns[3] . " LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
    }
    // Tim chanh xu
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $query .= " AND " . $columns[4] . " LIKE '%" . trim($db->clearText($requestData['columns'][4]['search']['value'])) . "%'";
    }
    // Tim pho xu
    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND " . $columns[5] . " LIKE '%" . trim($db->clearText($requestData['columns'][5]['search']['value'])) . "%'";
    }
    // Tim giao dan
    if (!empty($requestData['columns'][6]['search']['value'])) {
      $query .= " AND " . $columns[6] . " LIKE '%" . trim($db->clearText($requestData['columns'][6]['search']['value'])) . "%'";
    }
    // Tim SDT
    if (!empty($requestData['columns'][7]['search']['value'])) {
      $query .= " AND " . $columns[7] . " LIKE '%" . trim($db->clearText($requestData['columns'][7]['search']['value'])) . "%'";
    }
    // Tim email
    if (!empty($requestData['columns'][8]['search']['value'])) {
      $query .= " AND " . $columns[8] . " LIKE '%" . trim($db->clearText($requestData['columns'][8]['search']['value'])) . "%'";
    }
    // Tim website
    if (!empty($requestData['columns'][9]['search']['value'])) {
      $query .= " AND " . $columns[9] . " LIKE '%" . trim($db->clearText($requestData['columns'][9]['search']['value'])) . "%'";
    }

    // Tim kiem va Count
    $db->table = "reactive";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "reactive";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];

    $rows = $db->select('
    `reactiveId`,
    `name`,
    `hometown`,
    `address`,
    `birthdayYear`,
    `religion`,
    `fbTargetId`,
    `contact`,
    `isWarning`,
    `note`,
    `file`
    ');

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/tracking/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['hometown'] = stripslashes($row['hometown']);
      $nestedData['address'] = stripslashes($row['address']);
      $nestedData['birthdayYear'] = stripslashes($row['birthdayYear']);
      $nestedData['religion'] = stripslashes($row['religion']);
      $nestedData['fbTargetId'] = listFbInGroup(json_decode($row['fbTargetId']));
      $nestedData['contact'] = stripslashes($row['contact']);
      $nestedData['isWarning'] = getWarning(stripslashes($row['isWarning']));
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['file'] = $file;

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("reactive-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/reactive-edit?id=' . intval($row['reactiveId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("reactive;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['reactiveId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
