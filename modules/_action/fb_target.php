<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_GET['type'])) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`name`',
      1 => 'a.`name`',
      2 => 'a.`type`',
      3 => 'a.`link`',
      4 => 'a.`name`',
      5 => 'a.`name`',
    );

    $query = "a.`isActive` = 1";

    // Tim
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(
        a.`name`,
        a.`type`,
        a.`link`) LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND " . $columns[1] . " LIKE '%" . trim($db->clearText($requestData['columns'][1]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND " . $columns[2] . " LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
    }

    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND " . $columns[3] . " LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
    }

    // Tim kiem va Count
    $db->table = "fb_target";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach record
    $data = array();
    $db->table = "fb_target";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON a.`updatedBy` = b.`user_id`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("
      a.`fbTargetId`,
      a.`name`,
      a.`type`,
      a.`link`,
      a.`file`,
      a.`note`");

    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if ($row['file'] !=  '' && file_exists(ROOT_DIR . DS . 'uploads' . DS . 'tracking' . DS . $row['file'])) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/tracking/' . stripslashes($row['file']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống">
          <i class="fa fa-paperclip fa-lg"></i>
        </a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['type'] = stripslashes($row['type']);
      $nestedData['link'] = stripslashes($row['link']);
      $nestedData['note'] = stripslashes($row['note']);
      $nestedData['file'] = $file;

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("fb-target-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/tracking/fb-target-edit?id=' . intval($row['fbTargetId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("fb-target;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['fbTargetId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
