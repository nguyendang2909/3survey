<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
    $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $agency = isset($_POST['agency']) ? intval($_POST['agency']) : 0;
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`user_id`',
			1 => '`user_name`',
			2 => '`full_name`',
            3 => '`apply`',
            4 => '`agency`',
			5 => '`is_show`',
            6 => '`modified_time`'
		);

		$query = "`is_active` = 1";
		if($agency>0) {
            $agency  = '"' . $agency . '"';
            $query .= " AND `agency` LIKE '%$agency%'";
        }
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(`user_name`, `full_name`, `comment`, `email`, `phone`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if (!empty($requestData['columns'][1]['search']['value'])) {
            $query .= " AND `user_name` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][2]['search']['value'])) {
            $query .= " AND `full_name` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND `apply` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND `is_show` = " . intval($requestData['columns'][5]['search']['value'] - 1);
        }
        if (!empty($requestData['columns'][6]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][6]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND `modified_time` >= $d1 AND `modified_time` <= $d2";
        }

		$db->table = "core_user";
        $db->condition = $query;
		$db->order = "";
		$db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
		$totalFiltered = $totalData;

        $db->table = "core_user";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`user_id` AS `user_id`, `user_name` AS `user_name`, `full_name` AS `full_name`, `apply` AS `apply`, `agency` AS `agency`, `is_show` AS `is_show`, `modified_time` AS `modified_time`");
		$i = $requestData['start'];
        $data = array();
        foreach($rows as $row) {
            $active = $show = $tool = '';
            if(in_array("user-edit", $corePrivilegeSlug['op'])) {
				$show = (intval($row['is_show']) == 0) ?
                    '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status_core($(this), ' . $row['user_id'] . ', \'is_show\', \'core_user\', \'user\');" rel="1">0</div>'
                    :
                    '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status_core($(this), ' . $row['user_id'] . ', \'is_show\', \'core_user\', \'user\');" rel="0">1</div>';

                $tool .= '<a href="' . HOME_URL_LANG . '/account/user-edit?id=' . intval($row['user_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; ';
            } else {
                $show = (intval($row['is_show']) == 0) ?
                    '<div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div>'
                    :
                    '<div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div>';

            }
            if(in_array("user;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['user_id']) . '"></label>';
            }

			$i++;
			$nestedData =   array();
			$nestedData[] = $row['user_id'];
			$nestedData[] = '<a href="javascript:;" class="btn-popover" title="' . stripslashes($row['full_name']) . '" rel="' . intval($row['user_id']) . '">' . stripslashes($row['user_name']) . '</a>';
            $nestedData[] = stripslashes($row['full_name']);
            $nestedData[] = stripslashes($row['apply']);
            $nestedData[] = groupAgency($row['agency']);
            $nestedData[] = $show;
            $nestedData[] = $date->vnDateTime($row['modified_time']);
            $nestedData[] = $tool;
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);