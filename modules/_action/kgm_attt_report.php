<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0 && isset($_POST['type'])) {
  $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
  $date   = new DateClass();

  if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => 'a.`newsSummary`',
      1 => 'a.`newsSummary`',
      2 => 'a.`categoryName`',
      3 => 'a.`files`',
      4 => 'a.`reportDate`',
      5 => 'a.`reportBy`',
    );

    // Tim cac active record (chua bi xoa)
    $query = "a.`isActive` = 1 AND d.`is_active` = 1";

    // Tim kiem
    if (!empty($requestData['search']['value'])) {
      $query .= " AND CONCAT(a.`newSummary`, a.newsContent, a.categoryName, d.full_name AS reportBy, a.reportDate) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
    }
    // Tim tom tat tin
    if (!empty($requestData['columns'][1]['search']['value'])) {
      $query .= " AND a.`newsSummary` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
    }
    // Tim category
    if (!empty($requestData['columns'][2]['search']['value'])) {
      $query .= " AND b.`categoryName` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
    }
    // Tim anh
    if (!empty($requestData['columns'][3]['search']['value'])) {
      $query .= " AND a.`files` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
    }
    // Tim ngay bao cao: dang bi loi
    if (!empty($requestData['columns'][4]['search']['value'])) {
      $d1 = date(strtotime($date->dmYtoYmd($requestData['columns'][4]['search']['value'])));
      $d2 = $d1 + 86400;
      $query .= " AND a.`reportDate` >= $d1 AND a.`reportDate` <= $d2";
    }
    // Tim nguoi bao cao
    if (!empty($requestData['columns'][5]['search']['value'])) {
      $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
    }

    // Tim kiem va Count
    $db->table = "kgm_attt_reports";
    $db->join = "a LEFT JOIN " . TTH_DATA_PREFIX . "kgm_attt_report_categories b ON a.categoryId = b.categoryId LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` d ON d.`user_id` = a.`createdBy`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("COUNT(*) AS `count`");
    $totalData = $db->RowCount;
    foreach ($rows as $row) {
      $totalData = $row['count'];
    }
    $totalFiltered = $totalData;

    // Danh sach bao cao ATTT KGM
    $data = array();
    $db->table = "kgm_attt_reports";
    $db->join = "a LEFT JOIN " . TTH_DATA_PREFIX . "kgm_attt_report_categories b ON a.categoryId = b.categoryId LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` d ON d.`user_id` = a.`createdBy`";
    $db->condition = $query;
    $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select("a.newsId, a.newsSummary, a.newsContent, b.categoryName, d.full_name AS reportBy, a.reportDate, a.files");
    $i = $requestData['start'];
    foreach ($rows as $row) {
      $i++;

      $file = '';
      if (file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'network' . DS . $row['files']) && ($row['files'] != '')) {
        $file = '<a target="_blank" href="' . HOME_URL . '/uploads/report/' . stripslashes($row['files']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-lg"></i></a>';
      }

      $nestedData =   array();
      $nestedData['no'] = $i;
      $nestedData['newsSummary'] = stripslashes($row['newsSummary']);
      $nestedData['newsCategory'] = stripslashes($row['categoryName']);
      $nestedData['newsImage'] = $file;
      $nestedData['reportDate'] = $date->convertYmdTodmY($row['reportDate']);
      $nestedData['reportBy'] = stripslashes($row['reportBy']);

      // Show/hide nut chinh sua, xoa record
      $tool = '';
      if (in_array("kgm-attt-report-edit", $corePrivilegeSlug['op'])) {
        $tool .= '<a href="' . HOME_URL_LANG . '/report/kgm-attt-report-edit?id=' . intval($row['newsId']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
      }

      if (in_array("kgm-attt-report;delete", $corePrivilegeSlug['op'])) {
        $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['newsId']) . '"></label>';
      }
      $nestedData['tool'] = $tool;

      $data[] = $nestedData;
    }

    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
      "recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    );

    echo json_encode($json_data);
  }
} else echo json_encode(false);
