<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';

	if($type=='device_category' && isset($_FILES['file'])) {
		// ---- FILE
		$date           = new DateClass();
		$OK             = false;
		$file_max_size  = FILE_MAX_SIZE;
		$u_file         = '-no-';
		$dir_dest       = ROOT_DIR . DS . "documents" . DS;
		$file_type      = $_FILES['file']['type'];
		$file_name      = $_FILES['file']['name'];
		$file_size      = $_FILES['file']['size'];
		$file_type      = trim(strrchr($file_name,'.'));
		$file_full_name = "tmp_".time().$file_type;

		if (($file_size > 0) && ($file_size <= $file_max_size)) {
			if ($file_type == ".xlsx" || $file_type == ".xls") {
				if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
					$u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
					@rename($dir_dest . $file_full_name, $dir_dest . $u_file);
					$OK = true;
				} else $OK = false;
			} else $OK = false;
		} else {
			$OK = false;
		}
		// Insert Database.
		$count = 0;
		if($OK===true) {

			include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
			$objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$id 	= intval($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$title 	= trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$note 	= trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());

					//---------------------
					if($id>0) {
						$db->table = "device";
						$db->condition = "`device_id` = $id";
						$db->order = "";
						$db->limit = 1;
						$rws = $db->select();
						if($db->RowCount>0) {
							foreach($rws as $rw) {
								if($rw['is_active']==0) {
									$db->table = "device";
									$data = array(
										'title' 		=> $db->clearText($title),
										'note' 			=> $db->clearText($note),
										'is_active' 	=> 1,
										'modified_time' => time(),
										'user_id' 		=> intval($account["id"])
									);
									$db->condition = "`device_id` = $id";
									$db->update($data);
									if ($db->AffectedRows > 0) $count++;
								}
							}
						} else {
							$db->table = "device";
							$data = array(
								'device_id' 	=> $id,
								'title' 		=> $db->clearText($title),
								'note' 			=> $db->clearText($note),
								'is_active' 	=> 1,
								'created_time' 	=> time(),
								'modified_time' => time(),
								'user_id' 		=> intval($account["id"])
							);
							$db->insert($data);
							if($db->LastInsertID>0) $count++;
						}
					}
				}
			}
		}
		if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
		else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

	} elseif($type=='device_item' && isset($_FILES['file'])) {
		// ---- FILE
		$date           = new DateClass();
		$OK             = false;
		$file_max_size  = FILE_MAX_SIZE;
		$u_file         = '-no-';
		$dir_dest       = ROOT_DIR . DS . "documents" . DS;
		$file_type      = $_FILES['file']['type'];
		$file_name      = $_FILES['file']['name'];
		$file_size      = $_FILES['file']['size'];
		$file_type      = trim(strrchr($file_name,'.'));
		$file_full_name = "tmp_".time().$file_type;

		if (($file_size > 0) && ($file_size <= $file_max_size)) {
			if ($file_type == ".xlsx" || $file_type == ".xls") {
				if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
					$u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
					@rename($dir_dest . $file_full_name, $dir_dest . $u_file);
					$OK = true;
				} else $OK = false;
			} else $OK = false;
		} else {
			$OK = false;
		}
		// Insert Database.
		$count = 0;
		if($OK===true) {

			include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
			$objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$id 		= intval($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$title 		= trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$device 	= intval($worksheet->getCellByColumnAndRow(2, $row)->getValue());
					$seri		= trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
					$specs		= trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
					$agency 	= intval($worksheet->getCellByColumnAndRow(5, $row)->getValue());
					$user 		= intval($worksheet->getCellByColumnAndRow(6, $row)->getValue());
					$perform	= trim($worksheet->getCellByColumnAndRow(7, $row)->getValue());
					$status		= trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
					$history	= trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());
					$note		= trim($worksheet->getCellByColumnAndRow(10, $row)->getValue());

					//---------------------
					if($id>0) {
						$db->table = "device_item";
						$db->condition = "`device_item_id` = $id";
						$db->order = "";
						$db->limit = 1;
						$rws = $db->select();
						if($db->RowCount>0) {
							foreach($rws as $rw) {
								if($rw['is_active']==0) {
									$db->table = "device_item";
									$data = array(
										'device'  	    => intval($device),
										'title'       	=> $db->clearText($title),
										'seri'       	=> $db->clearText($seri),
										'specs'       	=> $db->clearText($specs),
										'agency'        => intval($agency),
										'user'          => intval($user),
										'perform'       => $db->clearText($perform),
										'status'        => $db->clearText($status),
										'history'       => $db->clearText($history),
										'note'          => $db->clearText($note),
										'modified_time' => time(),
										'user_id' 		=> intval($account["id"])
									);
									$db->condition = "`device_item_id` = $id";
									$db->update($data);
									if ($db->AffectedRows > 0) $count++;
								}
							}
						} else {
							$db->table = "device_item";
							$data = array(
								'device_item_id'	=> $id,
								'device'  	    => intval($device),
								'title'       	=> $db->clearText($title),
								'seri'       	=> $db->clearText($seri),
								'specs'       	=> $db->clearText($specs),
								'agency'        => intval($agency),
								'user'          => intval($user),
								'perform'       => $db->clearText($perform),
								'status'        => $db->clearText($status),
								'history'       => $db->clearText($history),
								'note'          => $db->clearText($note),
								'created_time'  => time(),
								'modified_time' => time(),
								'user_id'       => intval($account["id"])
							);
							$db->insert($data);
							if($db->LastInsertID>0) $count++;
						}
					}
				}
			}
		}
		if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
		else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

	} elseif($type=='software_category' && isset($_FILES['file'])) {
		// ---- FILE
		$date           = new DateClass();
		$OK             = false;
		$file_max_size  = FILE_MAX_SIZE;
		$u_file         = '-no-';
		$dir_dest       = ROOT_DIR . DS . "documents" . DS;
		$file_type      = $_FILES['file']['type'];
		$file_name      = $_FILES['file']['name'];
		$file_size      = $_FILES['file']['size'];
		$file_type      = trim(strrchr($file_name,'.'));
		$file_full_name = "tmp_".time().$file_type;

		if (($file_size > 0) && ($file_size <= $file_max_size)) {
			if ($file_type == ".xlsx" || $file_type == ".xls") {
				if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
					$u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
					@rename($dir_dest . $file_full_name, $dir_dest . $u_file);
					$OK = true;
				} else $OK = false;
			} else $OK = false;
		} else {
			$OK = false;
		}
		// Insert Database.
		$count = 0;
		if($OK===true) {

			include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
			$objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$id 	= intval($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$title 	= trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$note 	= trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());

					//---------------------
					if($id>0) {
						$db->table = "software";
						$db->condition = "`software_id` = $id";
						$db->order = "";
						$db->limit = 1;
						$rws = $db->select();
						if($db->RowCount>0) {
							foreach($rws as $rw) {
								if($rw['is_active']==0) {
									$db->table = "software";
									$data = array(
										'title' 		=> $db->clearText($title),
										'note' 			=> $db->clearText($note),
										'is_active' 	=> 1,
										'modified_time' => time(),
										'user_id' 		=> intval($account["id"])
									);
									$db->condition = "`software_id` = $id";
									$db->update($data);
									if ($db->AffectedRows > 0) $count++;
								}
							}
						} else {
							$db->table = "software";
							$data = array(
								'software_id' 	=> $id,
								'title' 		=> $db->clearText($title),
								'note' 			=> $db->clearText($note),
								'is_active' 	=> 1,
								'created_time' 	=> time(),
								'modified_time' => time(),
								'user_id' 		=> intval($account["id"])
							);
							$db->insert($data);
							if($db->LastInsertID>0) $count++;
						}
					}
				}
			}
		}
		if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
		else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

	} elseif($type=='software_item' && isset($_FILES['file'])) {
		// ---- FILE
		$date           = new DateClass();
		$OK             = false;
		$file_max_size  = FILE_MAX_SIZE;
		$u_file         = '-no-';
		$dir_dest       = ROOT_DIR . DS . "documents" . DS;
		$file_type      = $_FILES['file']['type'];
		$file_name      = $_FILES['file']['name'];
		$file_size      = $_FILES['file']['size'];
		$file_type      = trim(strrchr($file_name,'.'));
		$file_full_name = "tmp_".time().$file_type;

		if (($file_size > 0) && ($file_size <= $file_max_size)) {
			if ($file_type == ".xlsx" || $file_type == ".xls") {
				if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
					$u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
					@rename($dir_dest . $file_full_name, $dir_dest . $u_file);
					$OK = true;
				} else $OK = false;
			} else $OK = false;
		} else {
			$OK = false;
		}
		// Insert Database.
		$count = 0;
		if($OK===true) {

			include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
			$objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$id 		= intval($worksheet->getCellByColumnAndRow(0, $row)->getValue());
					$title 		= trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$software 	= intval($worksheet->getCellByColumnAndRow(2, $row)->getValue());
					$perform	= trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
					$owner		= trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
					$address	= trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
					$agency 	= intval($worksheet->getCellByColumnAndRow(6, $row)->getValue());
					$user 		= intval($worksheet->getCellByColumnAndRow(7, $row)->getValue());
					$history	= trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());
					$note		= trim($worksheet->getCellByColumnAndRow(9, $row)->getValue());

					//---------------------
					if($id>0) {
						$db->table = "software_item";
						$db->condition = "`software_item_id` = $id";
						$db->order = "";
						$db->limit = 1;
						$rws = $db->select();
						if($db->RowCount>0) {
							foreach($rws as $rw) {
								if($rw['is_active']==0) {
									$db->table = "software_item";
									$data = array(
										'software'      => intval($software),
										'title'       	=> $db->clearText($title),
										'perform'       => $db->clearText($perform),
										'owner'       	=> $db->clearText($owner),
										'address'       => $db->clearText($address),
										'agency'        => intval($agency),
										'user'          => intval($user),
										'history'       => $db->clearText($history),
										'note'          => $db->clearText($note),
										'modified_time' => time(),
										'user_id' 		=> intval($account["id"])
									);
									$db->condition = "`device_item_id` = $id";
									$db->update($data);
									if ($db->AffectedRows > 0) $count++;
								}
							}
						} else {
							$db->table = "software_item";
							$data = array(
								'software_item_id'	=> $id,
								'software'      => intval($software),
								'title'       	=> $db->clearText($title),
								'perform'       => $db->clearText($perform),
								'owner'       	=> $db->clearText($owner),
								'address'       => $db->clearText($address),
								'agency'        => intval($agency),
								'user'          => intval($user),
								'history'       => $db->clearText($history),
								'note'          => $db->clearText($note),
								'created_time'  => time(),
								'modified_time' => time(),
								'user_id'       => intval($account["id"])
							);
							$db->insert($data);
							if($db->LastInsertID>0) $count++;
						}
					}
				}
			}
		}
		if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
		else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

	} elseif($type=='network_ip' && isset($_FILES['file'])) {
		// ---- FILE
		$date           = new DateClass();
		$OK             = false;
		$file_max_size  = FILE_MAX_SIZE;
		$u_file         = '-no-';
		$dir_dest       = ROOT_DIR . DS . "documents" . DS;
		$file_type      = $_FILES['file']['type'];
		$file_name      = $_FILES['file']['name'];
		$file_size      = $_FILES['file']['size'];
		$file_type      = trim(strrchr($file_name,'.'));
		$file_full_name = "tmp_".time().$file_type;

		if (($file_size > 0) && ($file_size <= $file_max_size)) {
			if ($file_type == ".xlsx" || $file_type == ".xls") {
				if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
					$u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
					@rename($dir_dest . $file_full_name, $dir_dest . $u_file);
					$OK = true;
				} else $OK = false;
			} else $OK = false;
		} else {
			$OK = false;
		}
		// Insert Database.
		$count = 0;
		if($OK===true) {

			include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
			$objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
			foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				for ($row = 2; $row <= $highestRow; $row++) {
					$ip 		= trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$device 	= intval($worksheet->getCellByColumnAndRow(2, $row)->getValue());
					$agency 	= intval($worksheet->getCellByColumnAndRow(3, $row)->getValue());
					
					//---------------------
					if(!empty($ip)) {
						$db->table = "network_ip";
						$data = array(
							'ip'            => $db->clearText($ip),
							'device_item'   => intval($device),
							'agency'        => intval($agency),
							'created_time'  => time(),
							'modified_time' => time(),
							'user_id'       => intval($account["id"])
						);
						$db->insert($data);
						if($db->LastInsertID>0) $count++;
					}
				}
			}
		}
		if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
		else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

	} elseif($type=='network_admin' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $name		= trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $apply 	    = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $agency 	= intval($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                    $tel 	    = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $email 	    = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                    $address    = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());

                    //---------------------
                    if(!empty($name)) {
                        $db->table = "network_admin";
                        $data = array(
                            'name'          => $db->clearText($name),
                            'apply'         => $db->clearText($apply),
                            'agency'        => intval($agency),
                            'tel'           => $db->clearText($tel),
                            'email'         => $db->clearText($email),
                            'address'       => $db->clearText($address),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    } elseif($type=='network_profile' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $title	    = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $user 	    = intval($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $agency 	= intval($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                    $history    = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $note 	    = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());

                    //---------------------
                    if(!empty($title)) {
                        $db->table = "network_profile";
                        $data = array(
                            'title'         => $db->clearText($title),
                            'user'          => intval($user),
                            'agency'        => intval($agency),
                            'history'       => $db->clearText($history),
                            'note'          => $db->clearText($note),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    } elseif($type=='tracking_local' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $title	    = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());

                    //---------------------
                    if(!empty($title)) {
                        $db->table = "local";
                        $db->condition = "`is_active` = 1";
                        $db->order = "";
                        $db->limit = 1;
                        $rows_c = $db->select("COUNT(*) AS `count`");
                        $count_r = 0;
                        foreach($rows_c as $row_c) {
                            $count_r = $row_c['count'];
                        }

                        $db->table = "local";
                        $data = array(
                            'title'         => $db->clearText($title),
                            'sort'          => intval($count_r + 1),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    } elseif($type=='tracking_subject' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $name	    = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $age	    = intval($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $address    = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                    $religion   = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $faction    = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());
                    $social     = trim($worksheet->getCellByColumnAndRow(6, $row)->getValue());
                    $local      = trim($worksheet->getCellByColumnAndRow(7, $row)->getValue());
                    $note       = trim($worksheet->getCellByColumnAndRow(8, $row)->getValue());

                    //---------------------
                    if(!empty($name)) {
                        $db->table = "local";
                        $db->condition = "`is_active` = 1 AND `title` LIKE '$local'";
                        $db->order = "";
                        $db->limit = 1;
                        $rows_c = $db->select("`local_id`");
                        $local = 0;
                        foreach($rows_c as $row_c) {
                            $local = $row_c['local_id'];
                        }

                        $db->table = "subject";
                        $data = array(
                            'name'          => $db->clearText($name),
                            'age'           => intval($age),
                            'address'       => $db->clearText($address),
                            'religion'      => $db->clearText($religion),
                            'faction'       => $db->clearText($faction),
                            'social'        => $db->clearText($social),
                            'local'         => intval($local),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    } elseif($type=='tracking_topic' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $content    = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $archives   = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $created    = $date->dmYtoYmd2($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                    $note       = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $keywords   = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());

                    //---------------------
                    if(!empty($content)) {
                        $db->table = "topic";
                        $data = array(
                            'content'       => $db->clearText($content),
                            'archives'      => $db->clearText($archives),
                            'created_date'  => $db->clearText($created),
                            'note'          => $db->clearText($note),
                            'keywords'      => $db->clearText($keywords),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    } elseif($type=='tracking_reactive' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $name       = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $link       = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $user       = intval($worksheet->getCellByColumnAndRow(3, $row)->getValue());
                    $faction    = trim($worksheet->getCellByColumnAndRow(4, $row)->getValue());
                    $note       = trim($worksheet->getCellByColumnAndRow(5, $row)->getValue());

                    //---------------------
                    if(!empty($name)) {
                        $db->table = "reactive";
                        $data = array(
                            'name'          => $db->clearText($name),
                            'link'          => $db->clearText($link),
                            'user'          => intval($user),
                            'faction'       => $db->clearText($faction),
                            'note'          => $db->clearText($note),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    } elseif($type=='tracking_keywords' && isset($_FILES['file'])) {
        // ---- FILE
        $date           = new DateClass();
        $OK             = false;
        $file_max_size  = FILE_MAX_SIZE;
        $u_file         = '-no-';
        $dir_dest       = ROOT_DIR . DS . "documents" . DS;
        $file_type      = $_FILES['file']['type'];
        $file_name      = $_FILES['file']['name'];
        $file_size      = $_FILES['file']['size'];
        $file_type      = trim(strrchr($file_name,'.'));
        $file_full_name = "tmp_".time().$file_type;

        if (($file_size > 0) && ($file_size <= $file_max_size)) {
            if ($file_type == ".xlsx" || $file_type == ".xls") {
                if (@move_uploaded_file($_FILES['file']['tmp_name'], $dir_dest . $file_full_name)) {
                    $u_file = 'Excel_' . time() . '_' . md5(uniqid()) . $file_type;
                    @rename($dir_dest . $file_full_name, $dir_dest . $u_file);
                    $OK = true;
                } else $OK = false;
            } else $OK = false;
        } else {
            $OK = false;
        }
        // Insert Database.
        $count = 0;
        if($OK===true) {

            include(_F_CLASSES . DS . "PHPExcel" . DS . "IOFactory.php");
            $objPHPExcel = PHPExcel_IOFactory::load($dir_dest . $u_file);
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                $highestRow = $worksheet->getHighestRow();
                for ($row = 2; $row <= $highestRow; $row++) {
                    $name   = trim($worksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $vie    = trim($worksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $eng    = trim($worksheet->getCellByColumnAndRow(3, $row)->getValue());

                    //---------------------
                    if(!empty($name)) {
                        $db->table = "keywords";
                        $data = array(
                            'name'          => $db->clearText($name),
                            'keyword_vi'    => $db->clearText($vie),
                            'keyword_en'    => $db->clearText($eng),
                            'created_time'  => time(),
                            'modified_time' => time(),
                            'user_id'       => intval($account["id"])
                        );
                        $db->insert($data);
                        if($db->LastInsertID>0) $count++;
                    }
                }
            }
        }
        if($count>0) echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-green">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';
        else echo 'ĐÃ THÊM THÀNH CÔNG <strong class="c-red">' . $count . ' (dòng)</strong> DỮ LIỆU MỚI.';

    }

} else echo json_encode(false);