<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`role_id`',
			1 => 'a.`name`',
			2 => 'a.`is_show`',
            3 => '`count`',
            4 => 'a.`modified_time`',
            5 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`name`, a.`note`, b.`full_name`) LIKE '%" . $requestData['search']['value'] . "%'";
		}
        if (!empty($requestData['columns'][1]['search']['value'])) {
            $query .= " AND a.`name` LIKE '%" . trim($requestData['columns'][1]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`is_show` = " . intval($requestData['columns'][2]['search']['value'] - 1);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $query .= " AND a.`sort` = " . formatNumberToInt($requestData['columns'][3]['search']['value']);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][4]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $requestData['columns'][5]['search']['value'] . "%'";
        }

		$db->table = "core_role";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "role_user` c ON c.`role` = a.`role_id`";
        $db->condition = $query . " GROUP BY a.`role_id`";
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;

        $db->table = "core_role";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "role_user` c ON c.`role` = a.`role_id`";
        $db->condition = $query . " GROUP BY a.`role_id`";
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`role_id` AS `role_id`, a.`name` AS `name`, a.`is_active` AS `is_active`, a.`is_show` AS `is_show`, COUNT(distinct c.`user`) AS `count`, a.`modified_time` AS `modified_time`, b.`full_name` AS `full_name`");
		$i = $requestData['start'];
        $data = array();
        foreach($rows as $row) {
            $active = $show = $tool = '';
            if(in_array("role-edit", $corePrivilegeSlug['op'])) {
                $show = ($row['is_show'] == 0) ?
                    '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status_core($(this), ' . $row['role_id'] . ', \'is_show\', \'core_role\', \'role\');" rel="1">0</div>'
                    :
                    '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status_core($(this), ' . $row['role_id'] . ', \'is_show\', \'core_role\', \'role\');" rel="0">1</div>';

                $tool .= '<a href="' . HOME_URL_LANG . '/role/role-edit?id=' . intval($row['role_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            } else {
                $show = ($row['is_show'] == 0) ?
                    '<div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div>'
                    :
                    '<div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div>';
            }

            if(in_array("role;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['role_id']) . '"></label>';
            }
            $tool .= '<a href="' . HOME_URL_LANG . '/role/dashboard?id=' .  intval($row['role_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chức năng" src="/images/list.png"></a>';

            $i++;
			$nestedData =   array();
			$nestedData[] = $row['role_id'];
			$nestedData[] = '<a href="javascript:;" class="btn-popover" title="' . stripslashes($row['name']) . '" rel="' . intval($row['role_id']) . '">' . stripslashes($row['name']) . '</a>';
            $nestedData[] = $show;
            $nestedData[] = formatNumberVN($row['count']);
            $nestedData[] = $date->vnDateTime($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);
            $nestedData[] = $tool;
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);