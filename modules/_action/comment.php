<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $type       = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $id		    = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $parent     = isset($_POST['par']) ? intval($_POST['par']) : 0;
    $message    = isset($_POST['msg']) ? trim($_POST['msg']) : 0;
    $created    = time();

    $db->table = "comment";
    $data = array(
        'type'          => $type,
        'id'            => $id,
        'parent'        => $parent,
        'comment'       => $db->clearText($message),
        'created_time'  => $created,
        'user_id'       => $account["id"]
    );
    $db->insert($data);
    if($db->LastInsertID) {
        $info_user = array();
        $info_user = getInfoUser2($account["id"]);
        if($parent>0) {
            echo '<div class="ol-cmt-item ol-rep-child">';
            echo '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
            echo '<div class="ol-cmt-text cmt-parent"><div class="cmt-text"><span class="ol-u-name">' . $info_user[0] . '</span>' . nl2br($message) . '</div><div class="ol-cmt-rep"><a class="b-rep" href="javascript:;" data-id="' . $id . '" rel="' . $parent . '">Trả lời</a><span class="space">-</span><span class="cmt-time">' . convertTimeDayAgo($created) . '</span></div></div>';
            echo '</div>';
        } else {
            echo '<div class="ol-cmt-item">';
            echo '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
            echo '<div class="ol-cmt-text cmt-parent"><div class="cmt-text"><span class="ol-u-name">' . $info_user[0] . '</span>' . nl2br($message) . '</div><div class="ol-cmt-rep"><a class="b-rep" href="javascript:;" data-id="' . $id . '" rel="' . $db->LastInsertID . '">Trả lời</a><span class="space">-</span><span class="cmt-time">' . convertTimeDayAgo($created) . '</span></div></div>';
            echo '</div>';
        }
    } else echo '';

} else echo '-Error-';