<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`network_profile_id`',
            1 => 'a.`title`',
            2 => 'd.`full_name`',
            3 => 'c.`name`',
            4 => 'a.`history`',
            5 => 'a.`files`',
            6 => 'a.`modified_time`',
            7 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1 AND b.`is_active` = 1";

		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`title`, c.`name`, d.`full_name`, a.`files`, b.`full_name`, a.`history`, a.`note`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND d.`full_name` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][3]['search']['value']) ) {
            $query .= " AND c.`name` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][4]['search']['value']) ) {
            $query .= " AND a.`history` LIKE '%" . $db->clearText($requestData['columns'][4]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND a.`files` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }
        if( !empty($requestData['columns'][6]['search']['value']) ) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][6]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][7]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][7]['search']['value']) . "%'";
        }

        $db->table = "network_profile";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` d ON d.`user_id` = a.`user`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "network_profile";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` d ON d.`user_id` = a.`user`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("a.`network_profile_id`, a.`title`, c.`name`, d.`full_name` AS `name_u`, a.`history`, a.`files`, a.`modified_time`, b.`full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			
			$file = '';
			if(file_exists(ROOT_DIR  . DS . 'uploads' . DS . 'network' . DS . $row['files']) && ($row['files']!='')) {
				$file = '<a target="_blank" href="' . HOME_URL . '/uploads/network/' . stripslashes($row['files']) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-lg"></i></a>';
			}
			
			$nestedData =   array();
			$nestedData[] = $row['network_profile_id'];
			$nestedData[] = stripslashes($row['title']);
            $nestedData[] = stripslashes($row['name_u']);
            $nestedData[] = stripslashes($row['name']);
            $nestedData[] = stripslashes($row['history']);
            $nestedData[] = $file;
            $nestedData[] = $date->vnDate($row['modified_time']);
            $nestedData[] = stripslashes($row['full_name']);

			$tool = '';
            if(in_array("network-profile-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/network/network-profile-edit?id=' . intval($row['network_profile_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("network-profile;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['network_profile_id']) . '"></label>';
            }
            $nestedData[] = $tool;

			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);