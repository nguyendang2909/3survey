<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if(isset($_POST['role_id'])){
	$category   = array();
	$role       = intval($_POST['role_id']);
	$category   = (isset($_POST['variable'])) ? $_POST['variable'] : array();
	$type       = (isset($_POST['type'])) ? $_POST['type'] : '-no-';

    $db->table = "core_privilege";
    $db->condition = "`role_id` = $role AND `type` LIKE '" . $db->clearText($type) . "'";
    $db->delete();

	if(count($category)>0){
		for($i=0; $i<count($category); $i++) {
			$db->table = "core_privilege";
			$data = array(
				'role_id' => $role,
				'type' => $db->clearText($type),
				'privilege_slug' => $db->clearText($category[$i])
			);
			$db->insert($data);
		}
	}

	include_once(_F_FUNCTIONS . DS . "CoreDashboard.php");
	echo "<script>alert('Cập nhật quyền quản trị thành công.')</script>";
    if($type == 'account') echo showCoreAccount($role);
    if($type == 'account') echo showCoreFieldSituation($role);
    if($type == 'account') echo showCoreOpinion($role);
    if($type == 'agency') echo showCoreAgency($role);
    if($type == 'role') echo showCoreRole($role);
    if($type == 'jobs') echo showCoreJobs($role);
    if($type == 'calendar') echo showCoreCalendar($role);
    if($type == 'network') echo showCoreNetwork($role);
    if($type == 'maps') echo showCoreMaps($role);
    if($type == 'device') echo showCoreDevice($role);
    if($type == 'software') echo showCoreSoftware($role);
    if($type == 'tracking') echo showCoreTracking($role);
    if($type == 'folder') echo showCoreFolder($role);
    if($type == 'blog') echo showCoreBlog($role);
    if($type == 'config') echo showCoreConfig($role);
    if($type == 'backup') echo showCoreBackup($role);

} else echo "<script>alert('Thao tác không hợp lệ.')</script>";