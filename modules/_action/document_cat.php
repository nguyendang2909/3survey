<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`' . TTH_DATA_PREFIX . 'document`.`parent`',
			1 => '`' . TTH_DATA_PREFIX . 'document`.`title`',
            2 => '`' . TTH_DATA_PREFIX . 'document`.`comment`',
            3 => '`' . TTH_DATA_PREFIX . 'document`.`count`',
            4 => '`' . TTH_DATA_PREFIX . 'document`.`modified_time`',
            5 => '`' . TTH_DATA_PREFIX . 'core_user`.`full_name`'
		);

		$query = "`" . TTH_DATA_PREFIX . "document`.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(`" . TTH_DATA_PREFIX . "document`.`title`, `" . TTH_DATA_PREFIX . "document`.`comment`, `" . TTH_DATA_PREFIX . "core_user`.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document`.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document`.`comment` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $query .= " AND `" . TTH_DATA_PREFIX . "document`.`count` = " . formatNumberToInt($requestData['columns'][3]['search']['value']);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][4]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND `" . TTH_DATA_PREFIX . "document`.`modified_time` >= $d1 AND `" . TTH_DATA_PREFIX . "document`.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND `" . TTH_DATA_PREFIX . "core_user`.`full_name` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }

		$db->table = "document";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "document`.`user_id`";
        $db->condition = $query;
		$db->order = "";
		$db->limit = "";
        $rows = $db->select();
		$totalData = $db->RowCount;
		$totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "document";
        $db->join = "LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` ON `" . TTH_DATA_PREFIX . "core_user`.`user_id` = `" . TTH_DATA_PREFIX . "document`.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`document_id`, `" . TTH_DATA_PREFIX . "document`.`title` AS `title`, `" . TTH_DATA_PREFIX . "document`.`comment` AS `comment`, `" . TTH_DATA_PREFIX . "document`.`count` AS `count`, `" . TTH_DATA_PREFIX . "document`.`modified_time` AS `modified_time`, `" . TTH_DATA_PREFIX . "core_user`.`full_name` AS `full_name`, `" . TTH_DATA_PREFIX . "document`.`parent` AS `parent`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData =   array();
			$nestedData['no'] = $i;
			$nestedData['title'] = stripslashes($row['title']);
            $nestedData['comment'] = stripslashes($row['comment']);
            $nestedData['count'] = formatNumberVN($row['count']);
            $nestedData['time'] = $date->vnDateTime($row['modified_time']);
            $nestedData['user'] = stripslashes($row['full_name']);
			$nestedData['tool'] = '<a href="/?ol=document&op=document_cat_edit&id=' . intval($row['document_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="images/edit.png"></a> &nbsp; <label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Chọn xoá" class="ol-checkbox-js" name="tick[]" value="' . intval($row['document_id']) . '"></label> &nbsp; <a href="/?ol=document&op=document_item_add&id=' . intval($row['document_id']) .'"><img data-toggle="tooltip" data-placement="top" title="Thêm tài liệu" src="images/add.png"></a> &nbsp;<a href="/?ol=document&op=document_item&id=' . intval($row['document_id']) .'"><img data-toggle="tooltip" data-placement="top" title="Danh sách tài liệu" src="images/list.png"></a>';
            $nestedData['group'] = getTitleParent($row['parent']);
            $nestedData['parent'] = empty($nestedData['group']) ? 0 : intval($row['parent']);
            $data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);