<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
    $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
	$code	= isset($_REQUEST['code']) ? intval($_REQUEST['code']) : 0;
	
    if($type=='load') {
        $features = array();
        $db->table = "maps";
        $db->condition = "`is_active` = 1 AND `type` =".$code;
        $db->order = "`modified_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach ($rows as $row) {
                $coordinates = array();
				$data   = array_map('floatval', explode(',', $row['f_coordinates']));
				$ftype = "";
				switch($row['f_type']){
                    case 'fiber':
                    case 'cable': case 'note':
                        $ftype = 'LineString';
						break;
					case 'Polygon':
						$ftype= 'Polygon';
						break;
					default:
						$ftype = 'Point';
				}
				if(count($data)==2 && $ftype=='Point') {
                    $coordinates = $data;
                } elseif($ftype=='LineString') {
                    $items  = array();
                    for($i=0; $i<count($data); $i++) {
                        array_push($items, $data[$i]);
                        if($i%2!==0) {
                            $coordinates[] = $items;
                            $items = array();
                        }
                    }
                } elseif($ftype=='Polygon') {
                    $items  = array();
                    for($i=0; $i<count($data); $i++) {
                        array_push($items, $data[$i]);
                        if($i%2!==0) {
                            $coordinates[] = $items;
                            $items = array();
                        }
                    }
                    $coordinates = array($coordinates);
                }
				$geometry = array(
					'type' 			=> $ftype,
					'coordinates'	=> $coordinates
				);

                $properties = array(
                    'id'            => $row['maps_id'],
                    'name'          => stripslashes($row['title']),
                    'coordinate'    => $coordinates,
                    'imgurl'		=> $row['imgurl'],
                    'note'          => $row['note'],
					'map_id'		=> $code,
                    'attached'		=> $row['attached'],
                    'object_type'	=> $row['f_type'],
                    'warning'        =>$row['warning'],
                    'warningnote'        =>$row['warningnote']

                );

				$child = array(
					'type'		    => 'Feature',
					'geometry'	    => $geometry,
                    'properties'    => $properties
				);
                $features[] = $child;
				
            }
        }

        $geo = array(
            'type'      => 'FeatureCollection',
            'features'  => $features
        );
	
		echo json_encode($geo);

    } elseif($type=='delete') {
        $db->table = "maps";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => intval($account["id"])
        );
        $db->condition = "`is_active` = 1 AND `maps_id` = $code";
        $db->update($data);
    }
    elseif($type=='update') {

        $geo_ = isset($_REQUEST['geo']) ? $_REQUEST['geo']: '';
		$note_ = isset($_REQUEST['note']) ? $_REQUEST['note']: '';
		$mapType = isset($_REQUEST['maptype']) ? $_REQUEST['maptype']: '';
        $tittle = isset($_REQUEST['tittle']) ? $_REQUEST['tittle']: '';
        $warning = $_REQUEST['warning'];
        $warningnote = isset($_REQUEST['warningnote']) ? $_REQUEST['warningnote']: '';

		$file = isset($_FILES['file']) ? $_FILES['file']: '';
		$msg = '';
				
        if($geo_ == '')
            return;
        $db->table = "maps";
        $data = array(
            'modified_time' => time(),
            'f_coordinates'       => $geo_
        );
		if($note_ != '')
			$data['note'] = $note_;
        if($mapType!= '')
			$data['type'] = $mapType;
		if($tittle!= '')
			$data['title'] = $tittle;
        if($warning!= '')
            $data['warning'] = $warning;
        if($warningnote!= '')
			$data['warningnote'] = $warningnote;
		if($file['name'] != ''){
				$target_dir = "uploads/maps/doc/";
				$target_file = $target_dir.basename($file["name"]);
				$msg= $target_file;
				
				if(move_uploaded_file($file["tmp_name"], $target_file)){
					$data['attached'] = basename($target_file);							
				}
				else{
				}
			}
		$db->condition = "`is_active` = 1 AND `maps_id` =". $code;		
        $db->update($data);
    }

} else echo json_encode(false);