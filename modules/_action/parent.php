<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $action	    = isset($_POST['act']) ? trim($_POST['act']) : '-no-';
    $type	    = isset($_POST['type']) ?  trim($_POST['type']) : '-no-';
    $id 	    = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $title	    = isset($_POST['title']) ? trim($_POST['title']) : '';
    $comment    = isset($_POST['comment']) ? trim($_POST['comment']) : '';
	
	if($action=='add') {
		$db->table = "parents";
		$data = array(
            'type'          => $db->clearText($type),
            'title'          => $db->clearText($title),
            'comment'       => $db->clearText($comment),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
		$db->insert($data);
	} elseif($action=='edit') {
        $db->table = "parents";
        $data = array(
            'title'          => $db->clearText($title),
            'comment'       => $db->clearText($comment),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`parents_id` = $id";
        $db->update($data);
    } elseif($action=='delete') {
        $db->table = "parents";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`parents_id` = $id";
        $db->update($data);

        if($type=='cash') {
            $db->table = "cash_book";
            $data = array(
                'parent' => 0
            );
            $db->condition = "`parent` = $id";
            $db->update($data);

        } elseif($type=='cost') {
            $db->table = "cost";
            $data = array(
                'parent' => 0
            );
            $db->condition = "`parent` = $id";
            $db->update($data);
        } elseif($type=='asset') {
            $db->table = "asset";
            $data = array(
                'parent' => 0
            );
            $db->condition = "`parent` = $id";
            $db->update($data);
        } elseif($type=='quiz') {
            $db->table = "quiz";
            $data = array(
                'parent' => 0
            );
            $db->condition = "`parent` = $id";
            $db->update($data);
        } elseif($type=='rules') {
            $db->table = "discipline_rulesdiscipline_rules";
            $data = array(
                'parent' => 0
            );
            $db->condition = "`parent` = $id";
            $db->update($data);
        } elseif($type=='customer') {
            $db->table = "customer";
            $data = array(
                'parent' => 0
            );
            $db->condition = "`parent` = $id";
            $db->update($data);
        }

    }
	
} else echo '-Error-';
