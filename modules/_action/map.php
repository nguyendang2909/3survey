<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$lng	= isset($_POST['lng']) ? trim($_POST['lng']) : '';
    $lat	= isset($_POST['lat']) ? trim($_POST['lat']) : '';
	?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel"><i class="fa fa-map-marker fa-fw"></i> Xem vị trí trên bản đồ</h4>
			</div>
			<div class="modal-body">
				<div style="position: relative; width: 100%; min-width: 300px; height: 300px;"><div id="map"></div></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-round btn-sm" data-dismiss="modal">Thoát</button>
			</div>
		</div>
	</div>
	<script>
	var map = new mapboxgl.Map({
		container: 'map',
		style: '/map/style-cdn.json',
		attributionControl: false,
		center: [<?php echo $lng;?>, <?php echo $lat;?>],
		zoom: 14,
		hash: true
	  });
	map.addControl(new mapboxgl.NavigationControl());
    map.addControl(new mapboxgl.FullscreenControl());
		  
	var marker = new mapboxgl.Marker({
		draggable: false
	}).setLngLat([<?php echo $lng;?>, <?php echo $lat;?>]).addTo(map);
	</script>
    <?php
}