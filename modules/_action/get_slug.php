<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if(isset($_POST['table'])) {
	$table  = $_POST['table'];
	$name   = trim($_POST['name']);
	$stringObj = new String();
	$slug = $stringObj->getSlug($name);

	$db->table = $table;
	$db->condition = "`slug` LIKE '$slug'";
	$db->order = "";
	$db->limit = "";
	$db->select();
	if($db->RowCount > 0) {
		$slug = $slug . '-' . $stringObj->getSlug(getRandomString(10));
	}
	echo $slug;
}