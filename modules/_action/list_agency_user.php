<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
	$list = loadAgency(0);
	
	echo json_encode($list, JSON_UNESCAPED_UNICODE);
}
function loadAgency($parent){
    global $db;
    $result = array();
    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {		
        $code = '"' . intval($row['agency_id']) . '"';
		$child = array();
		$db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '%$code%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows_u = $db->select("`user_id`, `full_name`");
        if($db->RowCount>0) {
            foreach($rows_u as $row_u) {
				$child[] = array("id" => 'u'. $row_u['user_id'], "text" => stripslashes($row_u['full_name']), "iconCls" => 'icon-man');
            }
        }
		
		$arr = loadAgency($row['agency_id']);
		$arr = array_merge($arr, $child);
		if(count($arr) > 0)
			$result[] = array("id" => 'a'. $row['agency_id'], "text" => stripslashes($row['name']), "state" => 'closed', "children" => $arr);
		else
			$result[] = array("id" => 'a'. $row['agency_id'], "text" => stripslashes($row['name']), "iconCls" => 'icon-tip');
    }
    return $result;
}