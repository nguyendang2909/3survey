<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
	$list = loadAgency(0);
	
	echo json_encode($list, JSON_UNESCAPED_UNICODE);
}
function loadAgency($parent){
    global $db, $corePrivilegeSlug;
    $result = array();
    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    $count = $db->RowCount;
    foreach($rows as $row) {
        $arr = loadAgency($row['agency_id']);
        $sort = $show = $tool = $link = '';
        if (in_array("agency-add", $corePrivilegeSlug['op'])) {
            $tool .= '<a href="' . HOME_URL_LANG . '/agency/agency-add?parent=' . intval($row["agency_id"]) . '"><img data-toggle="tooltip" data-placement="left" title="Thêm mục" src="./images/add.png"></a> &nbsp; ';
        }
        if (in_array("agency-edit", $corePrivilegeSlug['op'])) {
            $sort = showSort("sort_" . $row["agency_id"] . "", $count, $row["sort"], '100%', 0, $row["agency_id"], 'agency', 1);
            if($row["is_show"] == 0)
                $show = '<div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status($(this), ' . $row["agency_id"] . ', \'is_show\', \'agency\');" rel="1">0</div>';
            else
                $show = '<div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status($(this), ' . $row["agency_id"] . ', \'is_show\', \'agency\');" rel="0">1</div>';

            $tool .= '<a href="' . HOME_URL_LANG . '/agency/agency-edit?id=' . intval($row["agency_id"]) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="./images/edit.png"></a> &nbsp; ';
        } else {
            $sort = showSort("sort_" . $row["agency_id"] . "", $count, $row["sort"], '100%', 0, $row["agency_id"], 'agency', 0);

            if($row["is_show"] == 0)
                $show = '<div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div>';
            else
                $show = '<div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div>';
        }

        if (in_array("agency;delete", $corePrivilegeSlug['op'])) {
            $tool .= '<a class="ol-confirm" href="javascript:;" rel="' . intval($row["agency_id"]) . '"><img data-toggle="tooltip" data-placement="right" title="Xóa đơn vị" src="./images/remove.png"></a>';
        }

        $link = '<a href="' . HOME_URL_LANG . '/account?agency=' . intval($row["agency_id"]) . '"><img data-toggle="tooltip" data-placement="top" title="Danh sách nhân viên" src="./images/list.png"></a>';

		if(count($arr) > 0)
			$result[] = array("id" => $row['agency_id'], "name" => stripslashes($row['name']), "symbol" => stripslashes($row['symbol']), "sort" => $sort, "show" => $show, "tool" => $tool, "link" => $link, "children" => $arr);
		else
			$result[] = array("id" => $row['agency_id'], "name" => stripslashes($row['name']), "symbol" => stripslashes($row['symbol']), "sort" => $sort, "show" => $show, "tool" => $tool, "link" => $link);
    }
    return $result;
}