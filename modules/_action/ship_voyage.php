<?php
if (!defined('TTH_SYSTEM')) {
	die('Please stop!');
}
//
if ($account["id"] > 0) {
  $type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
  $shipId   = isset($_GET['shipId']) ? $_GET['shipId'] : 0;
  $date   = new DateClass();
	//
	if ($type == 'load') {
    $requestData = $_REQUEST;
    $columns = array(
      0 => '`name`',
      1 => '`name`',
      2 => '`shipUpdateTime`',
      3 => '`comment`',
      4 => '`shipLatitude`'
    );

    $query = $shipId. "= b.`shipId` AND a.`isActive` = 1 AND b.`isActive`=1 ";

    
		if (!empty($requestData['search']['value'])) {
			$query .= " AND CONCAT( b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`comment`) 
      LIKE '%" . trim($db->clearText($requestData['search']['value'])) . "%'";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			$query .= " AND b.`shipUpdateTime` LIKE '%" . trim($db->clearText($requestData['columns'][2]['search']['value'])) . "%'";
		}

    if (!empty($requestData['columns'][3]['search']['value'])) {
			$query .= " AND b.`comment` LIKE '%" . trim($db->clearText($requestData['columns'][3]['search']['value'])) . "%'";
		}

		if (!empty($requestData['columns'][4]['search']['value'])) {
			$searchText = trim($db->clearText($requestData['columns'][5]['search']['value']));
			$query .= " AND b.`shipLatitude` LIKE '%" . $searchText . "%' OR b.`shipLongitude` LIKE '%" . $searchText . "%'";
		}


		// Tinh so record
    $db->table = "ship_report";
    $db->join = "b LEFT JOIN `olala3w_ship` a on b.`shipId` = a.`shipId`";
    $db->condition = $query;
    $db->order = "";
    $db->limit = "";
		$rows = $db->select("COUNT(*) AS `count`");
		$totalData = $db->RowCount;
		foreach ($rows as $row) {
			$totalData = $row['count'];
		}
		$totalFiltered = $totalData;

		$data = array();

		// Data
		$db->table = "ship_report";
    $db->join = "b LEFT JOIN `olala3w_ship` a ON b.`shipId` = a.`shipId`";
    $db->condition = $query;
    $db->order = "b.`updatedAt` DESC";
    // $db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
    $db->limit='';
    // $db->limit = $requestData['start'] . " ," . $requestData['length'];
    $rows = $db->select(" a.`name`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`comment`");
        
    $i = 0;
    foreach ($rows as $row) {
      $i++;
			$nestedData 			=   array();
			$nestedData['no'] 		= $i;
      $nestedData['name'] = stripslashes($row['name']);
      $nestedData['shipUpdateTime'] = stripslashes($row['shipUpdateTime']);
      $nestedData['comment'] = stripslashes($row['comment']);
      $nestedData['coordinatebyradius'] = stripslashes($row['shipLatitude']) . "<br />" . stripslashes($row['shipLongitude']);
        
      $data[] = $nestedData;
    }
        
    $json_data = array(
      "draw"            => intval($requestData['draw']),
      "recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
      "data"            => $data
    ); 

		echo json_encode($json_data);
	}
} else echo json_encode(false);