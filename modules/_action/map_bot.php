<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

if ($account["id"] > 0) {
  $type   = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
  if ($type == 'load') {
    $features = array();

    $db->table = 'bots';
    $db->join = 'a LEFT JOIN olala3w_maps_type c ON a.`mapTypeId` = c.`maps_type_id`
                   LEFT JOIN olala3w_local b ON a.`localId` = b.`local_id`
    ';
    $db->condition = 'a.`isActive` = 1 AND a.`latitude` != "" AND a.`longitude` != ""';
    $db->order = '';
    $db->limit = '';
    $rows = $db->select('
      a.botId,
      a.`name`,
      a.`address`,
      a.`investor`,
      a.`transferStatus`,
      a.`latitude`,
      a.`longitude`,
      a.`isWarning`,
      a.`note`,
      b.`title` AS local,
      c.`icon`,
      c.`color`
      ');

    if ($db->RowCount > 0) {
      foreach ($rows as $row) {
        $coordinates = array();
  
        $data = convertToGeometry($row['latitude'], $row['longitude']);
        $databyradius  = array($row['latitude'], $row['longitude']);
        $ftype = "Point";

        if (count($data) == 2 && $ftype == 'Point') {
          $coordinates = $data;
          $coordinatesbyradius = $databyradius;
        }

        $geometry = array(
          'type'          => $ftype,
          'coordinates'   => $coordinates,
          'coordinatesbyradius'   => $coordinatesbyradius
        );

        $properties = array(
          'id' => $row['botId'],
          'type' => 'bot',
          'name' => $row['name'],
          'address' => $row['address'],
          'local' => $row['local'],
          'investor' => $row['investor'],
          'transferStatus' => $row['transferStatus'],
          'isWarning' => $row['isWarning'],
          'note' => $row['note'],
          'icon' => $row['icon'],
          'color' => $row['color'],
          'coordinate'         => $coordinates,
          'coordinatebyradius'        => $coordinatesbyradius,
        );

        $child = array(
          'type'          => 'Feature',
          'geometry'      => $geometry,
          'properties'    => $properties
        );
        $features[] = $child;
      }
    }

    $geo = array(
      'type'      => 'FeatureCollection',
      'features'  => $features
    );
  }
  echo json_encode($geo);
} else echo json_encode(false);
