<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

if($account["id"]>0) {
    $type       = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $id		    = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $string     = '';

    $db->table = "comment";
    $db->condition = "`type` LIKE '$type' AND `is_active` = 1 AND `id` = $id AND `parent` = 0";
    $db->order = "`created_time` DESC";
    $db->limit = "3, 3000";
    $rows = array_reverse($db->select());
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $info_user = array();
            $info_user = getInfoUser2($row["user_id"]);
            $string .= '<div class="ol-cmt-item">';
            $string .= '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
            $string .= '<div class="ol-cmt-text cmt-parent">';
            $string .= '<div class="cmt-text"><span class="ol-u-name">' . $info_user[0] . '</span>' . nl2br(stripslashes($row['comment'])) . '</div><div class="ol-cmt-rep"><a class="b-rep" href="javascript:;" rel="' . intval($row["comment_id"]) . '">Trả lời</a><span class="space">-</span><span class="cmt-time">' . convertTimeDayAgo($row["created_time"]) . '</span></div>';
            //---
            $db->table = "comment";
            $db->condition = "`type` LIKE '$type' AND `is_active` = 1 AND `id` = $id AND `parent` = " . intval($row["comment_id"]);
            $db->order = "`created_time` ASC";
            $db->limit = "";
            $rows2 = $db->select();
            if($db->RowCount>0) {
                foreach($rows2 as $row2) {
                    $info_user = array();
                    $info_user = getInfoUser2($row2["user_id"]);
                    $string .= '<div class="ol-cmt-item ol-rep-child">';
                    $string .= '<div class="ol-cmt-avt"><div class="img-avt">' . $info_user[4] . '</div></div>';
                    $string .= '<div class="ol-cmt-text"><div class="cmt-text"><span class="ol-u-name">' . $info_user[0] . '</span>' . nl2br(stripslashes($row2['comment'])) . '</div><div class="ol-cmt-rep"><a class="b-rep" href="javascript:;" data-id="' . $id . '" rel="' . intval($row["comment_id"]) . '">Trả lời</a><span class="space">-</span><span class="cmt-time">' . convertTimeDayAgo($row2["created_time"]) . '</span></div></div>';
                    $string .= '</div>';
                }
            }
            //---
            $string .= '</div>';
            $string .= '</div>';
        }
    }

    echo $string;

} else echo '-Error-';