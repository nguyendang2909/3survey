<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
    $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();

    if($type=='load') {
        $start  = isset($_POST['start']) ? $date->vnOther(intval($_POST['start']), 'Y-m-d') : date('Y-m-d', strtotime('-15 days'));
        $end    = isset($_POST['end']) ? $date->vnOther(intval($_POST['end']), 'Y-m-d') : date('Y-m-d', strtotime('+15 days'));

        $string = array();
        $code = '"' . intval($account["id"]) . '"';
        $db->table = "agency";
        $db->condition = "`is_active` = 1 AND `manager` LIKE '%$code%'";
        $db->order = "`sort` ASC";
        $db->limit = "";
        $rows = $db->select("`agency_id`");
        if($db->RowCount>0) {
            foreach($rows as $row) {
                array_push($string, '"a' . $row["agency_id"] . '"');
            }
        }

        array_push($string, '"u' . intval($account["id"]) . '"');

        $code = '"u' . intval($account["id"]) . '"';
        $string = implode("|", $string);
        $events = array();
        $db->table = "jobs";
        $db->condition = "`is_active` = 1 AND (`list_to` REGEXP '$string' OR `forward` LIKE '%$code%') AND `end` >= '$start' AND `begin` <= '$end'";
        $db->order = "`begin` DESC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            foreach($rows as $row) {
                $e 			= array();
                $e['id'] 	= $row['jobs_id'];
                $e['title'] = stripslashes($row['title']);
                $e['start'] = $row['begin'];
                $e['end'] 	= $row['end'];
                if($row['done']==1)
                    $e['color'] = '#20c510';
                elseif($row['level']==0)
                    $e['color'] = '#00918e';
                elseif($row['level']==1)
                    $e['color'] = '#fcb322';
                elseif($row['level']==2)
                    $e['color'] = '#f73323';
                $e['allDay'] = false;
                array_push($events, $e);
            }
        }

        echo json_encode($events);

    } elseif($type=='add') {

    }

} else echo json_encode(false);