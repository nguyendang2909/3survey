<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
	$type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $date   = new DateClass();
    //
	if($type=='load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => 'a.`device_id`',
			1 => 'a.`title`',
            2 => 'a.`note`',
            3 => 'a.`count`',
            4 => 'a.`modified_time`',
            5 => 'b.`full_name`'
		);

		$query = "a.`is_active` = 1";
		
		if( !empty($requestData['search']['value']) ) {
			$query .= " AND CONCAT(a.`title`, a.`note`, b.`full_name`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][1]['search']['value']) ) {
            $query .= " AND a.`title` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}
        if( !empty($requestData['columns'][2]['search']['value']) ) {
            $query .= " AND a.`note` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {
            $query .= " AND a.`count` = " . formatNumberToInt($requestData['columns'][3]['search']['value']);
        }
        if (!empty($requestData['columns'][4]['search']['value'])) {
            $d1 = strtotime($date->dmYtoYmd($requestData['columns'][4]['search']['value']));
            $d2 = $d1 + 86400;
            $query .= " AND a.`modified_time` >= $d1 AND a.`modified_time` <= $d2";
        }
        if( !empty($requestData['columns'][5]['search']['value']) ) {
            $query .= " AND b.`full_name` LIKE '%" . $db->clearText($requestData['columns'][5]['search']['value']) . "%'";
        }

		$db->table = "device";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = "";
        $db->limit = 1;
        $rows = $db->select("COUNT(*) AS `count`");
        $totalData = $db->RowCount;
        foreach($rows as $row) {
            $totalData = $row['count'];
        }
        $totalFiltered = $totalData;
		
        $data = array();
        //---
		$db->table = "device";
        $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` b ON b.`user_id` = a.`user_id`";
        $db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		$db->limit = $requestData['start'] . " ," . $requestData['length'];
		$rows = $db->select("`device_id`, a.`title` AS `title`, a.`note` AS `note`, a.`count` AS `count`, a.`modified_time` AS `modified_time`, b.`full_name` AS `full_name`");
		$i = $requestData['start'];
		foreach($rows as $row) {
			$i++;
			$nestedData 			=   array();
			$nestedData['no'] 		= $row['device_id'];
			$nestedData['title'] 	= stripslashes($row['title']);
            $nestedData['note'] 	= stripslashes($row['note']);
            $nestedData['count'] 	= formatNumberVN($row['count']);
            $nestedData['time'] 	= $date->vnDateTime($row['modified_time']);
            $nestedData['user'] 	= stripslashes($row['full_name']);
			
			$tool = '';
			if(in_array("device-category-edit", $corePrivilegeSlug['op'])) {
                $tool .= '<a href="' . HOME_URL_LANG . '/network/device-category-edit?id=' . intval($row['device_id']) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; &nbsp;';
            }

            if(in_array("device-category;delete", $corePrivilegeSlug['op'])) {
                $tool .= '<label class="checkbox-inline"><input type="checkbox" data-toggle="tooltip" data-placement="top" title="Xóa" class="ol-checkbox-js" name="tick[]" value="' . intval($row['device_id']) . '"></label>';
            }
			
            $nestedData['tool'] 	= $tool;
			
			$data[] = $nestedData;
		}

		$json_data = array(
				"draw"            => intval( $requestData['draw'] ),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"            => $data
		);

		echo json_encode($json_data);

	}

} else echo json_encode(false);