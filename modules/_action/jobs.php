<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0 && isset($_POST['type'])) {
    $type   = isset($_POST['type']) ? $_POST['type'] : '-no-';
    $id     = isset($_POST['id']) ? intval($_POST['id']) : 0;
    $date   = new DateClass();
    //
    if ($type=='open') {
        $db->table = "jobs";
        $db->condition = "`jobs_id` = $id";
        $db->order = "";
        $db->limit = 1;
        $rows = $db->select();
        foreach($rows as $row) {
            $list_to = $list_forward = array();
            $agency = $user = array();
            $list = array_values(json_decode($row['list_to']));
            $code = 'a';
            $agency = array_filter($list, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            $code = 'u';
            $user = array_filter($list, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            if(count($agency)>0) {
                $code = str_replace('a', '', implode(',' , $agency));
                $db->table = "agency";
                $db->condition = "`agency_id` IN ($code)";
                $db->order = "`sort` ASC";
                $db->limit = "";
                $rows_a = $db->select("`name`");
                foreach($rows_a as $row_a) {
                    array_push($list_to, stripslashes($row_a['name']));
                }
            }
            if(count($user)>0) {
                $code = str_replace('u', '', implode(',' , $user));
                $db->table = "core_user";
                $db->condition = "`user_id` IN ($code)";
                $db->order = "`full_name` ASC";
                $db->limit = "";
                $rows_u = $db->select("`full_name`");
                foreach($rows_u as $row_u) {
                    array_push($list_to, stripslashes($row_u['full_name']));
                }
            }
            $forward = json_decode($row['forward']);
            $code = 'u';
            $forward = array_filter($forward, function ($item) use ($code) {
                if (stripos($item, $code) !== false) {
                    return true;
                }
                return false;
            });
            if(count($forward)>0) {
                $code = str_replace('u', '', implode(',' , $forward));
                $db->table = "core_user";
                $db->condition = "`user_id` IN ($code)";
                $db->order = "`full_name` ASC";
                $db->limit = "";
                $rows_u = $db->select("`full_name`");
                foreach($rows_u as $row_u) {
                    array_push($list_forward, stripslashes($row_u['full_name']));
                }
            }

            $status = '';
            if($row['done']==1) {
                $status = '<span class="lb-level success">ĐÃ HOÀN THÀNH</span>';
            } else {
                if(strtotime($row['end']) < time())
                    $status = '<span class="lb-level deadline">TRỄ THỜI HẠN</span>';
                else
                    $status = '<span class="lb-level proceed">ĐANG TIẾN HÀNH</span>';
            }


            $my_a = $my_a1 = array();
            $code = '"' . intval($account["id"]) . '"';
            $db->table = "agency";
            $db->condition = "`is_active` = 1 AND `manager` LIKE '%$code%'";
            $db->order = "`sort` ASC";
            $db->limit = "";
            $rows_a = $db->select("`agency_id`");
            if($db->RowCount>0) {
                foreach($rows_a as $row_a) {
                    array_push($my_a, 'a' . $row_a["agency_id"]);
                    array_push($my_a1, 'a' . $row_a["agency_id"]);
                }
            }
            array_push($my_a, 'u' . intval($account["id"]));
            $jobs_a =  array_merge($agency, $user);
            $views = json_decode($row['views']);
            ?>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-pencil-square-o fa-fw"></i> Xem lịch công việc</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-responsive modal-mn">
                            <div class="item-1">
                                <?php
                                $files = json_decode($row['files']);
                                $r_files = '';
                                if(!empty($files)) {
                                    foreach($files as $val) {
                                        $r_files .= '<a target="_blank" href="' . HOME_URL . '/uploads/jobs/' . stripslashes($val) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-fw"></i> ' . stripslashes($val) . '</a><br>';
                                    }
                                }

                                echo '<table class="table table-no-border">';
                                echo '<tr><td align="right" width="120px">Người giao việc:</td><td><strong>' . getUserFullName($row['user_id']) . '</strong></td></tr>';
                                echo '<tr><td align="right">Tên công việc:</td><td><strong>' . stripslashes($row['title']) . '</strong></td></tr>';
                                echo '<tr><td align="right">Mức độ:</td><td><strong>' . jobsLevel($row['level']) . '</strong></td></tr>';
                                echo '<tr><td align="right" style="vertical-align: top;">Người nhận:</td><td><strong>' . implode('<br>', $list_to) . '</strong></td></tr>';
                                if(count($list_forward)>0)
                                    echo '<tr><td align="right" style="vertical-align: top;">Người nhận tiếp:</td><td><strong>' . implode('<br>', $list_forward) . '</strong></td></tr>';
                                echo '<tr><td align="right">Bắt đầu:</td><td><strong>' . $date->vnFull(strtotime($row['begin'])) . '</strong></td></tr>';
                                echo '<tr><td align="right">Hạn chốt:</td><td><strong>' . $date->vnFull(strtotime($row['end'])) . '</strong></td></tr>';
                                echo '<tr><td align="right">Tệp đính kèm:</td><td>' . $r_files . '</td></tr>';
                                echo '<tr><td align="right">Ghi chú:</td><td>' . stripslashes($row['note']) . '</td></tr>';
                                if(!empty($row['map']) && !empty($row['lng']) && !empty($row['lat']))
                                    echo '<tr><td align="right" class="ver-top">Vị trí:</td><td><div style="position: relative; width: 100%; min-width: 300px; height: 300px;"><div id="map"></div></td></tr>';
                                echo '<tr><td align="right">Trạng thái:</td><td>' . $status . '</td></tr>';
                                echo '</table>';

                                if(count($views)) {
                                    $list_view = array();
                                    $code = implode(',', $views);
                                    $db->table = "core_user";
                                    $db->condition = "`is_active` = 1 AND `user_id` IN ($code)";
                                    $db->order = "`full_name` ASC";
                                    $db->limit = "";
                                    $rows_vw = $db->select("`full_name`");
                                    if($db->RowCount>0) {
                                        foreach($rows_vw as $row_vw) {
                                            array_push($list_view, stripslashes($row_vw['full_name']));
                                        }
                                        echo '<div class="ol-list-views"><i class="fa fa-eye fa-fw"></i> ' . implode('; ', $list_view) . '</div>';
                                    }
                                }

                                $db->table = "jobs_report";
                                $db->condition = "`is_active` = 1 AND `jobs` = $id";
                                $db->order = "`created_time` ASC";
                                $db->limit = "";
                                $rows_r = $db->select();
                                if($db->RowCount>0) {
                                    echo '<h4 class="drg-event-title">Báo cáo công việc:</h4>';
                                    echo '<table class="table table-bordered table-report">';
                                    echo '<thead><tr><th>Thời gian</th><th>Người báo cáo</th><th>Kết quả</th><th>Tệp tin</th><th>Ghi chú</th></tr></thead>';
                                    foreach ($rows_r as $row_r) {
                                        $done = '<span class="lb-level-circle deadline">&nbsp;</span> Chưa hoàn thành';
                                        $files = json_decode($row_r['files']);
                                        $r_files = '';
                                        if(!empty($files)) {
                                            foreach($files as $val) {
                                                $r_files .= '<a target="_blank" href="' . HOME_URL . '/uploads/jobs/' . stripslashes($val) . '" data-toggle="tooltip" data-placement="top" title="Tải tệp xuống"><i class="fa fa-paperclip fa-fw"></i> ' . stripslashes($val) . '</a><br>';
                                            }
                                        }

                                        if($row_r['done']==1)
                                            $done = '<span class="lb-level-circle success">&nbsp;</span> Hoàn thành';
                                        elseif($row_r['done']==-1)
                                            $done = '<span class="lb-level-circle proceed">&nbsp;</span> Chuyển việc';
                                        echo '<tr><td align="center">' . $date->vnDateTime($row_r['created_time']) . '</td><td>' . getUserFullName($row_r['user_id']) . '</td><td>' . $done . '</td><td align="center">' . $r_files . '</td><td>' . stripslashes($row_r['note']) . '</td></tr>';
                                    }
                                    echo '</table>';
                                }
                                ?>
                            </div>
                            <?php
                            $code = 'u' . intval($account["id"]);
                            if($row['done']==0) {
                                if((count(array_intersect($my_a, $jobs_a))>0) || in_array($code, $forward)) { ?>
                                    <div class="item-2" style="display: none;">
                                        <form id="jobs_report" onsubmit="return jobs_ac('jobs_report', 'report');" method="post" enctype="multipart/form-data" class="form-ol-3w">
                                            <input type="hidden" name="id" value="<?php echo $id;?>">
                                            <table class="table table-no-border table-hover">
                                                <tr>
                                                    <td width="120px" align="right"><label class="form-lb-tp">Kết quả:</label></td>
                                                    <td><div class="b-check"><input type="checkbox" value="1" name="done" id="b_done"><label for="b_done" data-off="Chưa hoàn thành" data-on="Hoàn thành"></label></div></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="ver-top"><label class="form-lb-tp">Têp đính kèm:</label></td>
                                                    <td><input class="form-control file" type="file" name="files[]" data-show-upload="false" data-show-preview="false" multiple data-max-file-count="10" placeholder="Chọn file đính kèm..." value=""></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                                    <td><textarea class="form-control" name="note" rows="5"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="form-ol-btn-tzc">
                                                        <button type="submit" class="btn btn-success btn-round">GỬI BÁO CÁO</button>
                                                        <button type="button" class="btn btn-default btn-round btn-sm btn-back"><i class="fa fa-reply"></i></button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                <?php }
                                if(count(array_intersect($my_a, $jobs_a))>0) {
                                    ?>
                                    <div class="item-3" style="display: none;">
                                        <form id="jobs_forward" onsubmit="return jobs_ac('jobs_forward', 'forward');" method="post" enctype="multipart/form-data" class="form-ol-3w">
                                            <input type="hidden" name="id" value="<?php echo $id;?>">
                                            <table class="table table-no-border table-hover">
                                                <tr>
                                                    <td width="120px" align="right"><label class="form-lb-tp">Người nhận:</label></td>
                                                    <td><?php echo selectUserForward(array_merge($agency, $my_a1), $forward);?></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="ver-top"><label class="form-lb-tp">Têp đính kèm:</label></td>
                                                    <td><input class="form-control file" type="file" name="files[]" data-show-upload="false" data-show-preview="false" multiple data-max-file-count="10" placeholder="Chọn file đính kèm..." value=""></td>
                                                </tr>
                                                <tr>
                                                    <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                                    <td><textarea class="form-control" name="note" rows="5"></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="form-ol-btn-tzc">
                                                        <button type="submit" class="btn btn-warning btn-round">CHUYỂN NGƯỜI KHÁC</button>
                                                        <button type="button" class="btn btn-default btn-round btn-sm btn-back"><i class="fa fa-reply"></i></button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                <?php } } ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <?php
                        $code = 'u' . intval($account["id"]);
                        if($row['done']==0) {
                            if(intval($account["id"])==$row['user_id']) {
                                echo '<a type="button" class="btn btn-info btn-round btn-sm" href="' . HOME_URL_LANG . '/jobs/job-edit?id=' . intval($row['jobs_id']) . '">Chỉnh sửa</a>';
                                echo '<button type="button" class="btn btn-success btn-round btn-sm" onclick="return jobs_done(' . intval($row['jobs_id']) . ');">Hoàn thành</button>';
                            }
                            if((count(array_intersect($my_a, $jobs_a))>0) || in_array($code, $forward)) {
                                echo '<button type="button" class="btn btn-success btn-round btn-sm">Báo cáo</button>';
                            }
                            if(count(array_intersect($my_a, $jobs_a))>0)
                                echo '<button type="button" class="btn btn-warning btn-round btn-sm">Chuyển việc</button>';
                        }
                        ?>
                        <button type="button" class="btn btn-danger btn-round btn-sm" data-dismiss="modal">Thoát</button>
                    </div>
                </div>
                <script>
                    $(function(){
                        autosize($('textarea.form-control'));
                        $('.file').fileinput();
                        $(".modal-footer .btn-success").click(function(){
                            $(".item-1").slideUp();
                            $(".item-2").slideDown();
                            $(".item-3").slideUp();
                            $(".modal-footer").hide();
                        });
                        $(".modal-footer .btn-warning").click(function(){
                            $(".item-1").slideUp();
                            $(".item-2").slideUp();
                            $(".item-3").slideDown();
                            $(".modal-footer").hide();
                            $('.selectpicker').selectpicker('refresh');
                        });
                        $(".item-2 .btn-back").click(function(){
                            $(".item-1").slideDown();
                            $(".item-2").slideUp();
                            $(".item-3").slideUp();
                            $(".modal-footer").show();
                        });
                        $(".item-3 .btn-back").click(function(){
                            $(".item-1").slideDown();
                            $(".item-2").slideUp();
                            $(".item-3").slideUp();
                            $(".modal-footer").show();
                        });
                    });
                </script>
                <?php
                if(!empty($row['map']) && !empty($row['lng']) && !empty($row['lat'])) {
                    ?>
                    <script>
                        var map = new mapboxgl.Map({
                            container: 'map',
                            style: '/map/style-cdn.json',
                            attributionControl: false,
                            center: [<?php echo $row['lng'];?>, <?php echo $row['lat'];?>],
                            zoom: 14,
                            hash: true
                        });
                        map.addControl(new mapboxgl.NavigationControl());
                        map.addControl(new mapboxgl.FullscreenControl());

                        var marker = new mapboxgl.Marker({
                            draggable: false
                        }).setLngLat([<?php echo $row['lng'];?>, <?php echo $row['lat'];?>]).addTo(map);
                    </script>
                <?php } ?>
            </div>
            <?php
            if(!in_array($account["id"], $views)) {
                array_push($views, $account["id"] . "");
                $db->table = "jobs";
                $data = array(
                    'views' => $db->clearText(json_encode(array_values($views)))
                );
                $db->condition = "`jobs_id` = $id";
                $db->update($data);
            }
        }

    } elseif ($type=='report')  {
        $done       = isset($_POST['done']) ? intval($_POST['done']) : 0;
        $note       = isset($_POST['note']) ? trim($_POST['note']) : '';
        $OK         = false;
        $msg        = '';
        $file_max_size	= FILE_MAX_SIZE;
        $dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'jobs' . DS;
        $f_name = array();
        if(isset($_FILES['files'])) {
            $images = array();
            foreach ($_FILES['files'] as $k => $l) {
                foreach ($l as $i => $v) {
                    if (!array_key_exists($i, $images))
                        $images[$i] = array();
                    $images[$i][$k] = $v;
                }
            }
            foreach ($images as $image) {
                $filename = substr($image['name'], 0, strrpos($image['name'], '.'));
                $fileUp = new Upload($image);
                $fileUp->file_max_size = $file_max_size;
                $fileUp->file_new_name_body = $filename;
                $fileUp->Process($dir_dest);
                if($fileUp->processed) {
                    array_push($f_name, $fileUp->file_dst_name);
                    $OK = true;
                } else {
                    $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
                }
            }

        } else {
            $OK = true;
        }

        $f_name = json_encode($f_name, JSON_UNESCAPED_UNICODE);

        if($OK) {
            $db->table = "jobs_report";
            $data = array(
                'jobs'  	    => intval($id),
                'done'  	    => intval($done),
                'files'         => $db->clearText($f_name),
                'note'          => $db->clearText($note),
                'created_time'  => time(),
                'user_id'       =>  $_SESSION["user_id"]
            );
            $db->insert($data);
            if($db->LastInsertID>0) {
                $OK = true;
            } else {
                $OK = false;
                $msg = 'Lỗi lữu cơ sở dữ liệu, vui lòng thực hiện lại.';
            }
        }
        echo json_encode(array("process" => $OK, "id" => $id, "msg" => $msg));

    } elseif ($type=='done')  {
        $id       	= isset($_POST['id']) ? intval($_POST['id']) : 0;
        $OK         = false;
        $msg        = '';
        $db->table = "jobs";
        $data = array(
            'done'    	=> 1,
            'done_time' => time()
        );
        $db->condition = "`is_active` = 1 AND `jobs_id` = $id";
        $db->update($data);
        echo $id;

    } elseif ($type=='forward')  {
        $user       = isset($_POST['user']) ? $_POST['user'] : array();
        $user       = array_keys(array_flip($user));
        $user       = array_keys(array_flip($user));
        $note       = isset($_POST['note']) ? trim($_POST['note']) : '';
        $file_size  = isset($_FILES['files']) ? $_FILES['files']['size'] : 0;
        $OK         = false;
        $msg        = '';
        $file_max_size	= FILE_MAX_SIZE;
        $dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'jobs' . DS;
        $f_name = array();
        if(isset($_FILES['files'])) {
            $images = array();
            foreach ($_FILES['files'] as $k => $l) {
                foreach ($l as $i => $v) {
                    if (!array_key_exists($i, $images))
                        $images[$i] = array();
                    $images[$i][$k] = $v;
                }
            }
            foreach ($images as $image) {
                $filename = substr($image['name'], 0, strrpos($image['name'], '.'));
                $fileUp = new Upload($image);
                $fileUp->file_max_size = $file_max_size;
                $fileUp->file_new_name_body = $filename;
                $fileUp->Process($dir_dest);
                if($fileUp->processed) {
                    array_push($f_name, $fileUp->file_dst_name);
                    $OK = true;
                } else {
                    $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
                }
            }

        } else {
            $OK = true;
        }

        $f_name = json_encode($f_name, JSON_UNESCAPED_UNICODE);


        if($OK) {
            $db->table = "jobs";
            $data = array(
                'forward'       => $db->clearText(json_encode($user)),
                'modified_time' => time()
            );
            $db->condition = "`is_active` = 1 AND `jobs_id` = $id";
            $db->update($data);
            //---
            $db->table = "jobs_report";
            $data = array(
                'jobs'  	    => intval($id),
                'done'  	    => -1,
                'files'         => $db->clearText($f_name),
                'note'          => $db->clearText($note),
                'created_time'  => time(),
                'user_id'       =>  $_SESSION["user_id"]
            );
            $db->insert($data);
            if($db->LastInsertID>0) {
                $OK = true;
            } else {
                $OK = false;
                $msg = 'Lỗi lữu cơ sở dữ liệu, vui lòng thực hiện lại.';
            }
        }
        echo json_encode(array("process" => $OK, "id" => $id, "msg" => $msg));

    } else die(http_response_code(404));

} else echo json_encode(false);

function jobsLevel($choice) {
    $color = array(
        0 => 'green',
        1 => 'orange',
        2 => 'red'
    );
    $text = array(
        0 => 'Bình thường',
        1 => 'Khá',
        2 => 'Cao'
    );
    return '<span class="lb-level ' . $color[$choice] . '">' . $text[$choice] . '</span>';
}

function selectUserForward(array $agency, array $choice) {
    global $db;
    $result = '';
    if(count($agency)>0)
        $agency = str_replace('a', '', implode(',', $agency));
    else
        $agency = 0;
    if(count($choice)>0) {
        $choice = str_replace('u', '', implode(',', $choice));
        $choice = explode(',', $choice);
    } else
        $choice = array();

    $result .= '<select class="form-control selectpicker" name="user[]" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn nhân viên..." required>';

    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `agency_id` IN ($agency)";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows_ag = $db->select("`agency_id`, `symbol`, `name`");
    foreach ($rows_ag as $row_ag) {
        $code = '"' . intval($row_ag['agency_id']) . '"';
        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '%$code%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_ag['symbol']) . ' - ' . stripslashes($row_ag['name']) . '">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  $row['gender'] . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="u' . $row["user_id"] . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }
    }

    $result .= '</select>';
    return $result;
}