<?php
if (!defined('TTH_SYSTEM')) {
	die('Please stop!');
}
//
if ($account["id"] > 0 && isset($_GET['type'])) {
	$type   = isset($_GET['type']) ? $_GET['type'] : '-no-';
	$date   = new DateClass();
	//
	if ($type == 'load') {
		$requestData = $_REQUEST;
		$columns = array(
			0 => '`name`',
			1 => '`zone`',
			2 => '`name`',
			3 => '`mmsi`',
			4 => '`shipUpdateTime`',
			5 => '`shipLatitude`',
			6 => '`shipHeadingCourse`',
			7 => '`shipSpeed`',
			8 => '`isViolateTerritorial`',
			9 => '`comment`'
		);

		$query = 'a.`isActive` = 1 AND b.`isActive`=1';

		if (!empty($requestData['search']['value'])) {
			$query .= " AND CONCAT(a.`name`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`shipHeadingCourse`, b.`shipSpeed`, b.`comment`) LIKE '%" . $db->clearText($requestData['search']['value']) . "%'";
		}

		if (!empty($requestData['columns'][1]['search']['value'])) {
			$query .= " AND b.`zone` LIKE '%" . $db->clearText($requestData['columns'][1]['search']['value']) . "%'";
		}

		if (!empty($requestData['columns'][2]['search']['value'])) {
			$query .= " AND a.`name` LIKE '%" . $db->clearText($requestData['columns'][2]['search']['value']) . "%'";
		}

		if (!empty($requestData['columns'][3]['search']['value'])) {
			$query .= " AND a.`mmsi` LIKE '%" . $db->clearText($requestData['columns'][3]['search']['value']) . "%'";
		}

		if (!empty($requestData['columns'][5]['search']['value'])) {
			$searchText = $db->clearText($requestData['columns'][5]['search']['value']);
			$query .= " AND b.`shipLatitude` LIKE '%" . $searchText . "%' OR b.`shipLongitude` LIKE '%" . $searchText . "%'";
		}

		if (!empty($requestData['columns'][9]['search']['value'])) {
			$query .= " AND b.`comment` LIKE '%" . $db->clearText($requestData['columns'][9]['search']['value']) . "%'";
		}

		// Tinh so record
		$db->table = "ship";
		$db->join = "
			a LEFT JOIN
			(SELECT b1.`zone`, b1.`shipUpdateTime`, b1.`shipLatitude`, b1.`shipLongitude`, b1.`shipHeadingCourse`, b1.`shipSpeed`, b1.`isViolateTerritorial`, b1.`shipId`, b1.`comment`, b1.`isActive` FROM olala3w_ship_report b1
				JOIN (SELECT shipId, MAX(updatedAt) AS updatedAt FROM olala3w_ship_report GROUP BY shipId) b2 ON b1.shipId = b2.shipId AND b1.updatedAt = b2.updatedAt) b
			ON a.`shipId` = b.`shipId`";
		$db->condition = $query;
		$db->order = "`b`.`zone` ASC";
		$db->limit = 1;
		$rows = $db->select("COUNT(*) AS `count`");
		$totalData = $db->RowCount;
		foreach ($rows as $row) {
			$totalData = $row['count'];
		}
		$totalFiltered = $totalData;

		$data = array();

		// Data
		$db->table = "ship";
		$db->join = "
			a LEFT JOIN
			(SELECT b1.`zone`, b1.`shipUpdateTime`, b1.`shipLatitude`, b1.`shipLongitude`, b1.`shipHeadingCourse`, b1.`shipSpeed`, b1.`isViolateTerritorial`, b1.`shipId`, b1.`comment`, b1.`isActive` FROM olala3w_ship_report b1
				JOIN (SELECT shipId, MAX(updatedAt) AS updatedAt FROM olala3w_ship_report GROUP BY shipId) b2 ON b1.shipId = b2.shipId AND b1.updatedAt = b2.updatedAt) b
			ON a.`shipId` = b.`shipId`";
		$db->condition = $query;
		$db->order = $columns[$requestData['order'][0]['column']] . " " . $requestData['order'][0]['dir'];
		$db->limit = '';
		$rows = $db->select("b.`zone`, a.`name`, a.`mmsi`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`shipHeadingCourse`, b.`shipSpeed`, b.`isViolateTerritorial`, b.`comment`");

		$i = 0;
		foreach ($rows as $row) {
			$i++;
			$nestedData 			=   array();
			$nestedData['no'] 		= $i;
			$nestedData['name'] 	= stripslashes($row['name']);
			$nestedData['updatedDate']  = stripslashes($row['shipUpdateTime']);
			$nestedData['coordinate'] 	= stripslashes($row['shipLatitude']) . "<br />" . stripslashes($row['shipLongitude']);
			$nestedData['direction'] 	= stripslashes($row['shipHeadingCourse']);
			$nestedData['speed'] 	= stripslashes($row['shipSpeed']);
			$nestedData['comment'] = stripslashes($row['comment']);
			$nestedData['mmsi'] = stripslashes($row['mmsi']);
			$nestedData['isViolateTerritorial'] = checkViolateTerritorial(stripslashes($row['isViolateTerritorial']));
			$nestedData['zone'] = stripslashes($row['zone']);

			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval($requestData['draw']),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}
} else echo json_encode(false);


function checkViolateTerritorial($status)
{
	if ($status == 1) return 'Có';
	if ($status == 0) return 'Không';
	return "Không xác định";
}
