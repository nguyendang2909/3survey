<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function documentCat($act, $typeFunc, $document_id, $title, $parent, $comment, $error) {
	global $db, $link_ol,  $link_op;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-cloud fa-fw"></i> Danh mục tài liệu
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="document_id" value="<?php echo $document_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Tên danh mục:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" autocomplete="off" required></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Nhóm:</label></td>
                                <td><?php echo listParent($db, $parent);?></td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                <td>
                                    <textarea class="form-control" rows="3" name="comment"><?php echo stripslashes($comment)?></textarea>
                                </td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}

function listParent($db, $choice) {
    $result = '<select name="parent" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn nhóm...">';
    $db->table = "parents";
    $db->condition = "`type` = 'document' AND `is_active` = 1";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $selected = '';
        if(intval($row["parents_id"])==$choice) $selected = ' selected';
        $result .= '<option value="' . intval($row["parents_id"]) . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
    }
    $result .= '</select>';

    return $result;
}
