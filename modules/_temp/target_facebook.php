<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function targetFacebook($act, $typeFunc, $target_id, $name, $link, $user, $type, $note, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dot-circle-o fa-fw"></i> Trang Facebook phản động
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="reactive_id" value="<?php echo $target_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
                            <tr>
                                <td align="right" width="150px"><label class="form-lb-tp">Tên facebook:</label></td>
                                <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name);?>" maxlength="250" required autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td align="right" width="150px"><label class="form-lb-tp">Liên kết facebook:</label></td>
                                <td><input class="form-control" type="text" name="link" value="<?php echo stripslashes($link);?>" maxlength="250" autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Dạng facebook:</label></td>
                                <!-- <td><input class="form-control" type="text" name="faction" value="<?php echo stripslashes($type);?>"></td> -->
                                <td><select class="form-control selectpicker" name="type" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn dạng facebook">
                                    <option value = "fanpage" <?php if ($type == "fanpage") echo " selected" ?>>Fanpage</option>
                                    <option value = "page" <?php if ($type == "page") echo " selected" ?>>Page cá nhân</option>
                                    <option value = "group" <?php if ($type == "group") echo " selected" ?>>Group</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Nhóm đối tượng</label></td>
                                <td><?php echo selectFacebookGroupTarget(0, $user);?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Bài đăng:</label></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"><textarea id="note" class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][3]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    CKEDITOR.replace('note', {
        height: 150,
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ],
            [ 'Image' ]
        ]
    });
</script>
<?php
}
