<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function assetItem($act, $typeFunc, $asset_item_id, $asset, $code, $content, $amount, $created_date, $error) {
	global $db, $link_ol,  $link_op;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-diamond fa-fw"></i> Nội dung tài sản
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="asset_item_id" value="<?php echo $asset_item_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="170px" align="right"><label class="form-lb-tp">Danh mục tài sản:</label></td>
								<td><?php echo listAsset($asset);?></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Mã tài sản:</label></td>
                                <td><input class="form-control" type="text" name="code" value="<?php echo stripslashes($code);?>" maxlength="50" required></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tài sản:</label></td>
                                <td><input class="form-control" type="text" name="content" value="<?php echo stripslashes($content);?>" required></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Giá trị:</label></td>
                                <td><input class="form-control auto-number" type="text" name="amount" data-a-sep=" " data-v-max="999999999999" data-v-min="0" maxlength="20" value="<?php echo stripslashes($amount)?>" autocomplete="off" required></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Ngày nhập vào:</label></td>
                                <td><input class="form-control input-date" type="text" name="created_date" style="width: 180px;" value="<?php echo $created_date?>" maxlength="10" required></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo TTH_PATH_LK . $link_ol[21] . TTH_PATH_OP_LK . $link_op[21][0];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.input-date').datetimepicker({
    mask: '39/19/9999',
    lang: 'vi',
    timepicker: false,
    format: '<?php echo TTH_DATE_FORMAT;?>',
    maxDate: '+01/01/1970'
});
</script>
<?php
}

function listAsset($choice) {
    global $db;

    $result = '<select name="asset" class="selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn danh mục..." required>';
    $db->table = "parents";
    $db->condition = "`type` LIKE 'asset' AND `is_active` = 1";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows_p = $db->select();
    foreach ($rows_p as $row_p) {
        $db->table = "asset";
        $db->condition = "`is_active` = 1 AND `parent` = " . intval($row_p['parents_id']);
        $db->order = "`created_time` DESC";
        $db->limit = "";
        $rows = $db->select();
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_p['title']) . '">';
            foreach($rows as $row) {
                $selected = '';
                if(intval($row['asset_id'])==$choice) $selected = ' selected';
                $result .= '<option value="' . intval($row['asset_id']) . '"' . $selected . '>' . stripslashes($row['title']) . '</option>';
            }
            $result .= '</optgroup>';
        }

    }

    $db->table = "asset";
    $db->condition = "`is_active` = 1 AND `parent` = 0";
    $db->order = "`created_time` DESC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $selected = '';
        if(intval($row['asset_id'])==$choice) $selected = ' selected';
        $result .= '<option value="' . intval($row['asset_id']) . '"' . $selected . '>' . stripslashes($row['title']) . '</option>';
    }
    $result .= '</select>';

    return $result;
}