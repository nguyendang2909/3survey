<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function grAdmin($act, $typeFunc, $role_id, $name, $note, array $user, $is_show, $error) {
	global $db, $mmenu;
?>
<div class="row">
	<div class="col-lg-7 col-md-10">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-group fa-fw"></i> Nhóm chức năng
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form class="form-ol-3w" action="<?php echo $act?>" method="post">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="role_id" value="<?php echo $role_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="150px" align="right"><label class="form-lb-tp">Tên nhóm:</label></td>
								<td><input class="form-control" type="text" name="name" maxlength="255" value="<?php echo stripslashes($name)?>" required="required" ></td>
							</tr>
							<tr>
								<td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td>
								<td>
									<textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea>
								</td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Thành viên:</label></td>
								<td><?php echo groupUserSelectAt(0, $user); ?></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Hiển thị:</label></td>
								<td>
									<label class="radio-inline"><input type="radio" name="is_show" value="0" <?php echo $is_show==0?"checked":""?> > Đóng</label>
									<label class="radio-inline"><input type="radio" name="is_show" value="1" <?php echo $is_show==1?"checked":""?> > Mở</label>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round" id="user">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['role']['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}

function groupUserSelectAt($parent, array $choice) {
    global $db;
    $result = '';
    if($parent==0)  {
        $result .= '<select name="user[]" class="form-control selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn nhân viên...">';

        $db->table = "core_user";
        $db->condition = "`agency` LIKE '[]'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `user_name`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="0 - ROOT">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  intval($row['gender']) . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . intval($row["user_id"]) . '"' . $selected . '>' . stripslashes($row["full_name"]) . ' (' . stripslashes($row["user_name"])  . ')</option>';
            }
            $result .= '</optgroup>';
        }
    }

    $db->table = "agency";
    $db->condition = "`parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows_ag = $db->select("`agency_id`, `symbol`, `name`");
    foreach ($rows_ag as $row_ag) {
        $code = '"' . intval($row_ag['agency_id']) . '"';
        $db->table = "core_user";
        $db->condition = "`agency` LIKE '%$code%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `user_name`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_ag['symbol']) . ' - ' . stripslashes($row_ag['name']) . '">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  intval($row['gender']) . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . intval($row["user_id"]) . '"' . $selected . '>' . stripslashes($row["full_name"]) . ' (' . stripslashes($row["user_name"]) . ')</option>';
            }
            $result .= '</optgroup>';
        }

        $result .= groupUserSelectAt($row_ag['agency_id'], $choice);
    }

    if($parent==0) $result .= '</select>';
    return $result;
}
