<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function trackingKeywords($act, $typeFunc, $keywords_id, $name, $keyword_vi, $keyword_en, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dot-circle-o fa-fw"></i> Từ khóa theo chủ đề
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="keywords_id" value="<?php echo $keywords_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
                            <tr>
                                <td align="right" width="150px"><label class="form-lb-tp">Chủ đề:</label></td>
                                <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name);?>" maxlength="250" required autocomplete="off"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Từ khóa (VIE):</label></td>
                                <td><input class="form-control" type="text" name="keyword_vi" data-role="tagsinput" maxlength="255" value="<?php echo stripslashes($keyword_vi)?>"></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Từ khóa (ENG):</label></td>
                                <td><input class="form-control" type="text" name="keyword_en" data-role="tagsinput" maxlength="255" value="<?php echo stripslashes($keyword_en)?>"></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][4]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}
