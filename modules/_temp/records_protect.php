<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function recordsProtect($act, $typeFunc, $protect_id, $title, $local, $lng, $lat, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dot-circle-o fa-fw"></i> Bảo vệ
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>">
						<input type="hidden" name="protect_id" value="<?php echo $protect_id?>">
						<input type="hidden" name="lng" value="<?php echo $lng?>">
						<input type="hidden" name="lat" value="<?php echo $lat?>">
						<div class="panel-show-error"><?php echo $error?></div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Tên bảo vệ:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" autocomplete="off" required></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Địa bàn:</label></td>
                                <td><?php echo listLocal($local);?></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][5]['sub'][3]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}

function listLocal($choice) {
    global $db;
    $result = '';

    $result .= '<select name="local" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn địa bàn...">';
    $db->table = "local";
    $db->condition = "`is_active` = 1";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $selected = '';
            if ($row["local_id"]==$choice) $selected = ' selected';

            $result .= '<option value="' . intval($row["local_id"]) . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
        }
    }
    $result .= '</select>';
    return $result;
}