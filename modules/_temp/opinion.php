<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postOpinion(
  $act,
  $typeFunc,
  $parishId,
  $name,
  $address,
  $localId,
  $yearOfEstablishment,
  $chanhXu,
  $phoXu,
  $laityCount,
  $phoneNumber,
  $email,
  $website,
  $isWarning,
  $mapTypeId,
  $latitude,
  $longitude,
  $file,
  $note,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Phiếu trưng cầu ý kiến
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="parishId" value="<?php echo $parishId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Tên giáo xứ(*):</label></td>
                  <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Địa chỉ:</label></td>
                  <td><input class="form-control" type="text" name="address" value="<?php echo stripslashes($address); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Địa bàn(*):</label></td>
                  <td><?php echo selectSingleLocal($localId); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Năm thành lập:</label></td>
                  <td><input class="form-control" type="number" min='0' max='2100' name="yearOfEstablishment" value="<?php if (stripslashes($yearOfEstablishment) != 0) echo stripslashes($yearOfEstablishment);
                                                                                                                      else {
                                                                                                                        echo '';
                                                                                                                      }; ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Chánh xứ:</label></td>
                  <td><input class="form-control" type="text" name="chanhXu" value="<?php echo stripslashes($chanhXu); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Phó xứ:</label></td>
                  <td><input class="form-control" type="text" name="phoXu" value="<?php echo stripslashes($phoXu); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Số giáo dân:</label></td>
                  <td><input class="form-control" type="text" name="laityCount" value="<?php echo stripslashes($laityCount); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">SĐT</label></td>
                  <td><input class="form-control" type="text" name="phoneNumber" value="<?php echo stripslashes($phoneNumber); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">email</label></td>
                  <td><input class="form-control" type="text" name="email" value="<?php echo stripslashes($email); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Website:</label></td>
                  <td><input class="form-control" type="text" name="website" value="<?php echo stripslashes($website); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Cảnh báo:</label></td>
                  <td><?php echo selectWarning($isWarning, 'required'); ?></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Vĩ độ:</label></td>
                  <td><input class="form-control" type="text" name="latitude" value="<?php echo stripslashes($latitude); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Kinh độ:</label></td>
                  <td><input class="form-control" type="text" name="longitude" value="<?php echo stripslashes($longitude); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Icon(*):</label></td>
                  <td><?php echo selectMapTypeSingle($mapTypeId, 'required'); ?></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Tệp tin:</label></td>
                  <td><input class="form-control file" type="file" name="file" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $file; ?>" placeholder="Chọn file..."></td>
                </tr>

                <tr>
                  <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                  <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . '/parish'; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.file').fileinput({
      <?php if ($file && $file !== '' && $file != '-no-') echo 'initialPreview: ["' . $file . '"]'; ?>,
    });
  </script>

<?php
}