<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postMilitary(
  $act,
  $typeFunc,
  $militaryAgencyId,
  $age,
  $education,
  $type,
  $social,
  $internetUsageTime,
  $internetUsagePurpose,
  $postStatus,
  $postAction,
  $identifyAuthen,
  $assessOnlineCommunity,
  $followingPage,
  $pageEffective,
  $note,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-12 col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> PHIẾU TRƯNG CẦU Ý KIẾN DÙNG CHO CÁN BỘ, CNVC THUỘC CÁC CƠ QUAN, BAN NGÀNH ĐỊA PHƯƠNG

          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="militaryAgencyId" value="<?php echo $militaryAgencyId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td width="170px" align="left"><label class="form-lb-tp">Tuổi(*):</label></td>
                  <td><input  class="form-control" type="number" name="age" min="1" max="100" value="<?php echo stripslashes($age); ?>" required></td>
                </tr>

                <tr>
                  <td width="170px" align="left"><label class="form-lb-tp">Trình độ học vấn(*):</label></td>
                  <td><input class="form-control" type="text" name="education" maxlength="50" value="<?php echo stripslashes($education); ?>"></td>
                </tr>

                <tr>
                  <td width="170px" align="left"><label class="form-lb-tp">Đồng chí là(*):</label></td>
                  <td><?php echo selectRadioBox('type', $type, ['Sỹ quan', 'Quân nhân chuyên nghiệp', 'Công nhân viên chức quốc phòng'], 'required'); ?></td>
                </tr>
              </table>


              <table class="table table-no-border table-hover">
                  <tr><td>
                    <label class="form-check-label"><i>Câu 1. Đồng chí đã tham gia các trang mạng xã hội nào?</i></label>
                    <br /><?php echo selectCheckBox('social', $social, ['Zalo', 'Facebook', 'Youtube', 'Twitter', 'Instagram', 'Khác'], '') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 2. Đồng chí sử dụng internet, mạng xã hội bao nhiêu giờ trong ngày?</i></label>
                    <br /><?php echo selectRadioBox('internetUsageTime', $internetUsageTime, ['Dưới 01 giờ', 'Từ 01 giờ đến 02 giờ', 'Trên 2 giờ'], 'required') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 3. Đồng chí thường sử dụng internet, mạng xã hội cho việc gì?</i></label>
                    <br /><?php echo selectCheckBox('internetUsagePurpose', $internetUsagePurpose, ['Đọc báo, xem tin tức', 'Giải trí', 'Tìm kiếm thông tin cho công việc', 'Kết nối với bạn bè, người thân', 'Mua, bán hàng online', 'Làm việc khác'], '') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 4. Đồng chí có đăng tải hình ảnh cá nhân, công việc, các bài viết lên internet, mạng xã hội không?</i></label>
                    <br /><?php echo selectRadioBox('postStatus', $postStatus, ['Chưa bao giờ', 'Thỉnh thoảng', 'Thường xuyên'], 'required') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 5. Đồng chí thường có hành động nào (dưới đây) khi đọc các bài viết trên internet, mạng xã hội?</i></label>
                    <br /><?php echo selectCheckBox('postAction', $postAction, ['Thích', 'Bình luận', 'Chia sẻ với bạn bè, người thân', 'Chỉ xem cho có', 'Lướt qua'], '') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 6. Khi xem các thông tin, chia sẻ, bình luận trên internet, mạng xã hội đồng chí có xác định nguồn gốc, tính xác thực của bài viết, tin tức không?</i></label>
                    <br /><?php echo selectRadioBox('identifyAuthen', $identifyAuthen, ['Chưa bao giờ', 'Thỉnh thoảng', 'Thường xuyên', 'Chỉ những bài viết, tin tức, tài liệu quan trọng'], 'required') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 7. Đồng chí đánh giá vai trò, mức độ ảnh hưởng của cộng đồng mạng với các vấn đề, sự kiện chính trị, xã hội lớn (Ví dụ như: đợt Covid-19 vừa qua; phiên tòa xét xử vụ án tại Đồng Tâm…)?</i></label>
                    <br /><?php echo selectRadioBox('assessOnlineCommunity', $assessOnlineCommunity, ['Tác động tốt', 'Trung bình', 'Không có vai trò gì', 'Làm xấu thêm tình hình'], 'required') ?>
                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 8. Đồng chí có tham gia vào các kênh thông tin của cơ quan, đơn vị để tuyên truyền, đấu tranh với các thông tin xấu độc trên internet, mạng xã hội không?</i></label>
                    <br /><?php echo selectRadioBox('followingPage', $followingPage, ['Có', 'Không'], 'required') ?>

                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Đồng chí đánh giá tác động, hiệu quả của các kênh, thông tin đó thế nào?</i></label>
                    <br /><?php echo selectRadioBox('pageEffective', $pageEffective, ['Tốt', 'Trung bình', 'Chưa hiệu quả'], 'required') ?>

                  </td></tr>

                  <tr><td>
                    <label class="form-check-label"><i>Câu 9. Theo đồng chí để đấu tranh có hiệu quả với các thông tin xấu độc và  xây dựng  một môi trường thông tin tích cực, lành mạnh trên internet, mạng xã hội  cần phải làm gì và làm như thế nào (hoặc vui lòng cung cấp các biện pháp mà đồng chí thấy hiệu quả)?</i></label>
                    <br /><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note) ?></textarea>
                  </td></tr>

                </tr>
              </table>

                    <center><button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['military']['link']; ?>'">Thoát</button></center>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
}