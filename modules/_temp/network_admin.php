<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function networkAdmin($act, $typeFunc, $network_admin_id, $name, $apply, $agency, $tel, $email, $address, $error) {
	global $mmenu;
    if(empty($agency)) $agency = "''";
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-sitemap fa-fw"></i> Quản trị mạng
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="network_admin_id" value="<?php echo $network_admin_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Họ và tên:</label></td>
                                <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name);?>" autocomplete="off" required></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Vị trí: (Chức vụ)</label></td>
                                <td><input class="form-control" type="text" name="apply" value="<?php echo stripslashes($apply);?>" autocomplete="off"></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Đơn vị:</label></td>
                                <td><input class="form-control easyui-combotree" data-options="url:'/action.php?url=list_agency',method:'get',value:<?php echo $agency;?>,animate:true,lines:true" name="agency" style="width: 100%;"></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Số điện thoại:</label></td>
                                <td><input class="form-control" type="text" name="tel" value="<?php echo stripslashes($tel);?>" autocomplete="off"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Email:</label></td>
                                <td><input class="form-control" type="text" name="email" value="<?php echo stripslashes($email);?>" autocomplete="off"></td>
							</tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Địa chỉ:</label></td>
                                <td><input class="form-control" type="text" name="address" value="<?php echo stripslashes($address);?>" autocomplete="off"></td>
							</tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][4]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}