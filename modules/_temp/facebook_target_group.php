<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function facebookTargetGroup($act, $typeFunc, $subject_id, $name, $age, $address, $religion, $faction, $social, $local, array $friends, $note, $facebookType, $trackingStatus, array $trackingFacebookTarget, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-dot-circle-o fa-fw"></i> Thông tin đối tượng theo dõi
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="subject_id" value="<?php echo $subject_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Tên đối tượng:</label></td>
                                <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name);?>" autocomplete="off" required></td>
                            </tr>

                            <tr>
                                <td align="right"><label class="form-lb-tp">Trạng thái:</label></td>
                                <td><select class="form-control selectpicker" name="trackingStatus" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn trạng thái">
                                    <option value = "1" <?php if ($trackingStatus == 1) echo " selected" ?>>Quét</option>
                                    <option value = "0" <?php if ($trackingStatus != 1) echo " selected" ?>>Dừng quét</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td align="right"><label class="form-lb-tp">Đối tượng theo dõi:</label></td>
                                <td><?php echo listTrackingFacebookTarget($trackingFacebookTarget);?></td>
                            </tr>

                            <tr>
                                <td align="right"><label class="form-lb-tp">Thông tin thêm:</label></td>
                                <td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td colspan="2"><textarea id="note" class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>

							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    CKEDITOR.replace('note', {
        height: 150,
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ],
            [ 'Image' ]
        ]
    });
</script>
<?php
}

function listLocal($choice) {
    global $db;
    $result = '';

    $result .= '<select name="local" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn địa bàn...">';
    $db->table = "local";
    $db->condition = "`is_active` = 1";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $selected = '';
            if ($row["local_id"]==$choice) $selected = ' selected';

            $result .= '<option value="' . intval($row["local_id"]) . '"' . $selected . '>' . stripslashes($row["title"]) . '</option>';
        }
    }
    $result .= '</select>';
    return $result;
}

function listTrackingFacebookTarget(array $choice) {
    global $db;
    $result = '';

    $result .= '<select name="trackingFacebookTarget[]" class="form-control selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn đối tượng facebook theo dõi...">';
    $db->table = "fb_target";
    $db->condition = "`isActive` = 1";
    $db->order = "`name` ASC";
    $db->limit = "";
    $rows = $db->select();
    if($db->RowCount>0) {
        foreach($rows as $row) {
            $selected = '';
            if (in_array($row["fbTargetId"], $choice)) $selected = ' selected';

            $result .= '<option value="' . intval($row["fbTargetId"]) . '"' . $selected . '>' . stripslashes($row["name"]) . ' (' . $row["link"] . ')</option>';
        }
    }
    $result .= '</select>';
    return $result;
}