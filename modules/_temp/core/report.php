<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}
//
global  $role_id;
?>

<form id="core_report" method="post" onsubmit="return core_dashboard('core_report', 'report');">
  <?php
  echo showCoreReport($role_id);
  ?>
</form>