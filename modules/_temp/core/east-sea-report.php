<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}
//
global  $role_id;
?>

<form id="core_east_sea_report" method="post" onsubmit="return core_dashboard('core_east_sea_report', 'east-sea-report');">
  <?php
  echo showCoreEastSeaReport($role_id);
  ?>
</form>