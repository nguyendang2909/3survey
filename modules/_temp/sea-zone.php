<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

function postSeaZone(
  $act,
  $typeFunc,
  $zoneId,
  $name,
  $file,
  $error
) {
  global $mmenu;
  if (empty($agency)) $agency = "''";
?>
  <div class="row">
    <div class="col-lg-6 col-md-9">
      <div class="panel">
        <div class="panel-heading">
          <i class="fa fa-sitemap fa-fw"></i> Vùng biển
          <span class="tools pull-right">
            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
            <a href="javascript:;" class="fa fa-chevron-down"></a>
            <a href="javascript:;" class="fa fa-eye"></a>
            <a href="javascript:;" class="fa fa-compress"></a>
            <a href="javascript:;" class="fa fa-times"></a>
          </span>
        </div>
        <div class="panel-body">
          <div class="form-responsive">
            <form action="<?php echo $act ?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
              <input type="hidden" name="typeFunc" value="<?php echo $typeFunc ?>" />
              <input type="hidden" name="zoneId" value="<?php echo $zoneId ?>" />
              <div class="panel-show-error">
                <?php echo $error ?>
              </div>
              <table class="table table-no-border table-hover">

                <tr>
                  <td width="170px" align="right"><label class="form-lb-tp">Tên vùng biển(*):</label></td>
                  <td><input class="form-control" type="text" name="name" value="<?php echo stripslashes($name); ?>" required></td>
                </tr>

                <tr>
                  <td align="right"><label class="form-lb-tp">Tệp tin:</label></td>
                  <td><input class="form-control file" type="file" name="file" data-show-upload="false" data-show-preview="false" data-max-file-count="1" value="<?php echo $file; ?>" placeholder="Chọn file..."></td>
                </tr>

                <tr>
                  <td colspan="2" class="form-ol-btn-tzc">
                    <button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
                    <button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
                    <button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . '/sea-zone'; ?>'">Thoát</button>
                  </td>
                </tr>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    $('.file').fileinput({
      <?php if ($file && $file !== '' && $file != '-no-') echo 'initialPreview: ["' . $file . '"]'; ?>,
    });
  </script>

<?php
}