<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function mapsType($act, $typeFunc, $maps_type_id, $icon, $title, $note, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-5 col-md-7">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-globe fa-fw"></i> Thể loại vị trí
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>">
						<input type="hidden" name="maps_type_id" value="<?php echo $maps_type_id?>">
                        <input type="hidden" name="icon" value="<?php echo $icon?>">
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="70px" align="right" class="ver-top"><label class="form-lb-tp">Icon:</label></td>
								<td><input class="form-control file file-img" type="file" name="icon" data-show-upload="false" data-max-file-count="1" accept="image/*"></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tên loại:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" maxlength="150" required></td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['maps']['link'] . $mmenu['maps']['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.file-img').fileinput({
	<?php if($icon!='-no-' && $icon!='') { ?>
	initialPreview: [
		"<img src='../uploads/maps/<?php echo $icon;?>' class='file-preview-image' alt='<?php echo $icon?>'>"
	],
	<?php } ?>
	allowedFileExtensions : ['jpg', 'png','gif']
});
</script>
<?php
}