<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function blogCore($act, $typeFunc, $blog_post_id, $blog, $name, $slug, $img, $comment, $content, $hot, $title, $description, $keywords, $upload_img_id, array $tags, $error) {
	global $db, $mmenu;
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-newspaper-o fa-fw"></i> Nội dung bài viết
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
                    <div class="panel-show-error"><?php echo $error?></div>
					<form action="<?php echo $act?>" method="post" enctype="multipart/form-data" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>">
						<input type="hidden" name="blog_post_id" value="<?php echo $blog_post_id?>">
						<input type="hidden" name="img" value="<?php echo $img?>">
                        <input type="hidden" name="upload_img_id" value="<?php echo $upload_img_id?>">
						<table class="table table-no-border table-hover">
							<tr>
								<td width="12%" align="right"><label class="form-lb-tp">Tên bài viết:</label></td>
								<td width="88%" colspan="3"><input class="form-control" type="text" id="name" name="name" value="<?php echo stripslashes($name)?>" required></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Liên kết tĩnh:</label></td>
                                <td colspan="3" class="element-relative"><input class="form-control" type="text" id="slug" name="slug" value="<?php echo stripslashes($slug)?>"><div data-toggle="tooltip" data-placement="top" title="Tạo liên kết tĩnh" class="btn-get-slug" onclick="return get_slug('blog');"></div></td>
                            </tr>
                            <tr>
                                <td width="12%" align="right" class="ver-top"><label class="form-lb-tp">Chuyên mục:</label></td>
                                <td width="38%" class="ver-top"><?php echo blogCategory($blog);?></td>
								<td width="12%" align="right" class="ver-top"><label class="form-lb-tp">Hình đại diện:</label></td>
								<td width="38%">
									<input class="form-control file file-img" type="file" name="img" data-show-upload="false" data-max-file-count="1" accept="image/*">
								</td>
							</tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Mô tả:</label></td>
                                <td colspan="3">
                                    <textarea class="form-control" rows="3" name="comment"><?php echo stripslashes($comment)?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Nội dung chi tiết:</label></td>
                                <td colspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4"><textarea class="form-control ckeditor" name="content" required><?php echo stripslashes($content)?></textarea></td>
                            </tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Thư viện ảnh:</label></td>
                                <td colspan="3"><input id="album" class="form-control file" type="file" name="images[]" data-max-file-count="20" accept="image/*" multiple></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Tag User:</label></td>
                                <td colspan="3"><?php echo groupUserSelectAt(0, $tags); ?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Nổi bật:</label></td>
                                <td colspan="3">
                                    <label class="radio-inline"><input type="radio" name="hot" value="0" <?php if($hot==0) echo 'checked';?>> Đóng</label>
                                    <label class="radio-inline"><input type="radio" name="hot" value="1" <?php if($hot==1) echo 'checked';?>> Mở</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tth-bg-df">&nbsp;</td>
                                <td class="tth-bg-df" colspan="3"><label class="form-lb-tp">SEO</label> - <span class="tth-gp-text">Không bắt buộc phải nhập, dữ liệu được lấy tự động nếu rỗng.</span></td>
                            </tr>
							<tr>
								<td class="tth-gp-l" align="right"><label class="form-lb-tp">Title:</label></td>
								<td class="tth-gp-r" colspan="3"><input class="form-control" type="text" name="title" maxlength="255" value="<?php echo stripslashes($title)?>"></td>
							</tr>
                            <tr>
                                <td class="tth-gp-l" align="right"><label class="form-lb-tp">Description:</label></td>
                                <td class="tth-gp-r" colspan="3"><input class="form-control" type="text" name="description" maxlength="255" value="<?php echo stripslashes($description)?>"></td>
                            </tr>
                            <tr>
                                <td class="tth-gp-l tth-gp-b" align="right"><label class="form-lb-tp">Keywords:</label></td>
                                <td class="tth-gp-r tth-gp-b" colspan="3"><input class="form-control" type="text" name="keywords" data-role="tagsinput" maxlength="255" value="<?php echo stripslashes($keywords)?>"></td>
                            </tr>
							<tr>
								<td colspan="4" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$dir_dest = ROOT_DIR . DS .'uploads'. DS .'photos' . DS;
$list_img = '';
$p1 = $p2 = array();
$db->table = "uploads_tmp";
$db->condition = "`upload_id` = $upload_img_id";
$db->order = "";
$db->limit = 1;
$rows = $db->select();
foreach ($rows as $row){
    $list_img = stripslashes($row['list_img']);
}

$files_img = explode(";", $list_img);
if(count($files_img)>0) {
    for ($i = 0; $i < count($files_img); $i++) {
        if ($files_img[$i] != '' && file_exists($dir_dest . $files_img[$i])) {
            $src    = '../uploads/photos/' . $files_img[$i];
            $src2   = '../uploads/photos/full_' . $files_img[$i];
            $l_key  = explode("_", $files_img[$i]);
            $key    = $l_key[0];
            $url    = '../uploads/upload.php?type=2&id='.$upload_img_id.'&item='.$files_img[$i].'&lang='.TTH_LANGUAGE;
            $p1[$i] = '"<a href=\''.$src2.'\' data-fancybox=\'gallery\'><img src=\''.$src.'\' class=\'file-preview-image\'></a>"';
            $p2[$i] = '{url: "'.$url.'", key: '.$key.'}';
        }
    }
}
?>
<script>
	$('.file-img').fileinput({
		<?php if($img!='-no-' && $img!='') { ?>
		initialPreview: [
			"<img src='../uploads/blog/<?php echo $img;?>' class='file-preview-image' alt='<?php echo $img?>'>"
		],
		<?php } ?>
		allowedFileExtensions : ['jpg', 'png','gif']
	});
    $("#album").fileinput({
        uploadUrl: "/uploads/upload.php?type=1&id=<?php echo $upload_img_id?>&lang=<?php echo TTH_LANGUAGE?>",
        uploadAsync: false,
        initialPreview: [<?php echo implode(',', $p1);?>],
        initialPreviewConfig: [<?php echo implode(',', $p2);?>],
        minFileCount: 1
    }).on("filebatchselected", function() {
        $("#album").fileinput('upload');
    }).on('filebatchpreupload', function() {
        window.onbeforeunload = function() {
            return 'Thư viện ảnh của bạn chưa tải lên hoàn tất, bạn có muốn thoát không?';
        };
    }).on('filebatchuploadsuccess', function() {
        window.onbeforeunload = null;
    });
</script>
<?php
}

function blogCategory($choice) {
	global $db;
    $result = "";
    $result .= '<select class="form-control selectpicker" name="blog" required>';
    $result .= loadItemBlog($db, 0, 0, $choice);
    $result .= '</select>';

    return $result;
}

function loadItemBlog($db, $level, $parent, $choice){
    $result = $space = '';
    $db->table = "blog";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $space = '&nbsp; ';
        for($i=0; $i<$level; $i++) {
            $space = $space . '&nbsp; ';
        }
        if ($level < 3){
            $selected = '';
            if(intval($row["blog_id"])==$choice) $selected = ' selected';

            $result .= '<option value="' . intval($row["blog_id"]) . '"' . $selected . '>' . $space . '&rarr; ' . stripslashes($row["name"]) . '</option>';
            $result .= loadItemblog($db, $level+1, intval($row["blog_id"]), $choice);
        }
    }
    return $result;
}

function groupUserSelectAt($parent, array $choice) {
    global $db;
    $result = '';
    if($parent==0)  {
        $result .= '<select name="tags[]" class="selectpicker" multiple data-live-search="true" data-selected-text-format="count" data-live-search-placeholder="Tìm..." title="Chọn nhân viên...">';

        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '[]'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="0 - ROOT">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  intval($row['gender']) . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . intval($row["user_id"]) . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }
    }

    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows_ag = $db->select("`agency_id`, `symbol`, `name`");
    foreach ($rows_ag as $row_ag) {
        $code = '"' . intval($row_ag['agency_id']) . '"';
        $db->table = "core_user";
        $db->condition = "`is_active` = 1 AND `is_show` = 1 AND `agency` LIKE '%$code%'";
        $db->order = "`full_name` ASC";
        $db->limit = "";
        $rows = $db->select("`user_id`, `full_name`, `gender`, `img`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row_ag['symbol']) . ' - ' . stripslashes($row_ag['name']) . '">';
            foreach($rows as $row) {
                $avatar = '';
                if($row['img']=='-no-' || $row['img']=='' ) {
                    $avatar = '/uploads/user/no-avatar-' .  intval($row['gender']) . '.png';
                } else {
                    $avatar = '/uploads/user/sm_' . $row['img'];
                }
                $avatar = ' data-thumbnail="' . $avatar . '"';

                $selected = '';
                if (in_array($row["user_id"], $choice)) $selected = ' selected';
                $result .= '<option' . $avatar . ' value="' . intval($row["user_id"]) . '"' . $selected . '>' . stripslashes($row["full_name"]) . '</option>';
            }
            $result .= '</optgroup>';
        }

        $result .= groupUserSelectAt($row_ag['agency_id'], $choice);
    }

    if($parent==0) $result .= '</select>';
    return $result;
}