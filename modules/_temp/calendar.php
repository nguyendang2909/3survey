<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function calendar($act, $typeFunc, $calendar_id, $type, $week, $month, $year, $files, $note, $error) {
	global $db, $mmenu;
    if(empty($agency)) $agency = "''";
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-calendar fa-fw"></i> Nội dung lịch
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w" enctype="multipart/form-data">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="calendar_id" value="<?php echo $calendar_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td width="170px" align="right"><label class="form-lb-tp">Loại lịch:</label></td>
								<td>
                                    <label class="radio-inline"><input type="radio" name="type" value="1" <?php if($type==1) echo 'checked';?>> Tuần</label>
                                    <label class="radio-inline"><input type="radio" name="type" value="2" <?php if($type==2) echo 'checked';?>> Tháng</label>
                                    <label class="radio-inline"><input type="radio" name="type" value="3" <?php if($type==3) echo 'checked';?>> Năm</label>
								</td>
							</tr>
                            <tr id="_week">
                                <td align="right"><label class="form-lb-tp">Tuần:</label></td>
                                <td><input class="form-control auto-number" style="width: 70px;" type="text" name="week" data-v-min="0" data-v-max="55" data-a-sep="" value="<?php echo stripslashes($week);?>" maxlength="2" required></td>
                            </tr>
                            <tr id="_month">
                                <td align="right"><label class="form-lb-tp">Tháng:</label></td>
                                <td><input class="form-control auto-number" style="width: 70px;" type="text" name="month" data-v-min="0" data-v-max="12" data-a-sep="" value="<?php echo stripslashes($month);?>" maxlength="2" required></td>
                            </tr>
                            <tr id="_year">
                                <td align="right"><label class="form-lb-tp">Năm:</label></td>
                                <td><input class="form-control auto-number" style="width: 70px;" type="text" name="year" data-v-min="0" data-v-max="9000" data-a-sep="" value="<?php echo stripslashes($year);?>" maxlength="4" required></td>
                            </tr>
							<tr>
								<td align="right"><label class="form-lb-tp">Têp tin: (PDF)</label></td>
								<td><input class="form-control file" type="file" name="files" data-show-upload="false" data-show-preview="false" data-max-file-count="1" accept="pdf" value="<?php echo $files;?>" placeholder="Chọn file..."></td>
							</tr>
                            <tr>
                                <td align="right" class="ver-top"><label class="form-lb-tp">Ghi chú:</label></td>
                                <td><textarea class="form-control" rows="3" name="note"><?php echo stripslashes($note)?></textarea></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['calendar']['link'] . $mmenu['calendar']['sub'][3]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$('.file').fileinput({
	<?php if($files !='' && $files !='-no-') echo 'initialPreview: ["' . $files . '"],'; ?>
	allowedFileExtensions : ['pdf']
});

$(function () {
	var type = parseInt($('input[name="type"]:checked').val());
	if(type==1) {
        $('#_week, #_year').show();
        $('#_month').hide();
	} else if(type==2) {
		$('#_month, #_year').show();
		$('#_week').hide();
	} else if(type==3) {
		$('#_year').show();
		$('#_week,#_month').hide();
	}
    $('.form-ol-3w').on('change', 'input[name="type"]', function() {
        var type = parseInt($(this).val());
        if(type==1) {
            $('#_week, #_year').show();
            $('#_month').hide();
        } else if(type==2) {
            $('#_month, #_year').show();
            $('#_week').hide();
        } else if(type==3) {
            $('#_year').show();
            $('#_week, #_month').hide();
        }
    });
});

</script>
<?php
}