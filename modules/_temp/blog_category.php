<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function blogCore($act, $typeFunc, $blog_id, $parent, $name, $slug, $img, $hot, $grid, $title, $description, $keywords, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-7 col-md-10">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-newspaper-o fa-fw"></i> Nội dung chuyên mục
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
                    <div class="panel-show-error"><?php echo $error?></div>
					<form action="<?php echo $act?>" method="post" enctype="multipart/form-data" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>">
						<input type="hidden" name="blog_id" value="<?php echo $blog_id?>">
						<input type="hidden" name="img" value="<?php echo $img?>">
						<table class="table table-no-border table-hover">
							<tr>
								<td width="150px" align="right"><label class="form-lb-tp">Tên chuyên mục:</label></td>
								<td><input class="form-control" type="text" id="name" name="name" value="<?php echo stripslashes($name)?>" required></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Liên kết tĩnh:</label></td>
                                <td class="element-relative"><input class="form-control" type="text" id="slug" name="slug" value="<?php echo stripslashes($slug)?>"><div data-toggle="tooltip" data-placement="top" title="Tạo liên kết tĩnh" class="btn-get-slug" onclick="return get_slug('blog');"></div></td>
                            </tr>
							<tr>
								<td align="right" class="ver-top"><label class="form-lb-tp">Hình đại diện:</label></td>
								<td>
									<input class="form-control file file-img" type="file" name="img" data-show-upload="false" data-max-file-count="1" accept="image/*">
								</td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Mục cha:</label></td>
                                <td><?php echo parentBlog($parent);?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Nổi bật:</label></td>
                                <td>
                                    <label class="radio-inline"><input type="radio" name="hot" value="0" <?php if($hot==0) echo 'checked';?>> Đóng</label>
                                    <label class="radio-inline"><input type="radio" name="hot" value="1" <?php if($hot==1) echo 'checked';?>> Mở</label>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Hiển thị:</label></td>
                                <td>
                                    <label class="radio-inline"><input type="radio" name="grid" value="0" <?php if($grid==0) echo 'checked';?>> List (Dạng danh sách)</label>
                                    <label class="radio-inline"><input type="radio" name="grid" value="1" <?php if($grid==1) echo 'checked';?>> Grid (Dạng lưới)</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="tth-bg-df">&nbsp;</td>
                                <td class="tth-bg-df"><label class="form-lb-tp">SEO</label> - <span class="tth-gp-text">Không bắt buộc phải nhập, dữ liệu được lấy tự động nếu rỗng.</span></td>
                            </tr>
							<tr>
								<td class="tth-gp-l" align="right"><label class="form-lb-tp">Title:</label></td>
								<td class="tth-gp-r"><input class="form-control" type="text" name="title" maxlength="255" value="<?php echo stripslashes($title)?>"></td>
							</tr>
                            <tr>
                                <td class="tth-gp-l" align="right"><label class="form-lb-tp">Description:</label></td>
                                <td class="tth-gp-r"><input class="form-control" type="text" name="description" maxlength="255" value="<?php echo stripslashes($description)?>"></td>
                            </tr>
                            <tr>
                                <td class="tth-gp-l tth-gp-b" align="right"><label class="form-lb-tp">Keywords:</label></td>
                                <td class="tth-gp-r tth-gp-b"><input class="form-control" type="text" name="keywords" data-role="tagsinput" maxlength="255" value="<?php echo stripslashes($keywords)?>"></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('.file-img').fileinput({
		<?php if($img!='-no-' && $img!='') { ?>
		initialPreview: [
			"<img src='../uploads/blog/<?php echo $img?>' class='file-preview-image' title='<?php echo $img?>' alt='<?php echo $img?>'>"
		],
		<?php } ?>
		allowedFileExtensions : ['jpg', 'png','gif']
	});
</script>
<?php
}
function parentBlog($choice) {
    global $db;
    $result = '';
    $result .= '<select class="form-control selectpicker" name="parent" required>';
    $selected = '';
    if($choice==0) $selected = ' selected';
    $result .= '<option value="0"' . $selected . '>Chuyên mục gốc...</option>';
    $result .= loadItemBlog($db, 0, 0, $choice);
    $result .= '</select>';

    return $result;
}
function loadItemBlog($db, $level, $parent, $choice){
    $result = $space = '';
    $db->table = "blog";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $space = '&nbsp; ';
        for($i=0; $i<$level; $i++) {
            $space = $space . '&nbsp; ';
        }
        if ($level < 3){
            $selected = '';
            if($choice==intval($row["blog_id"])) $selected = ' selected';

            $result .= '<option value="' . intval($row["blog_id"]) . '"' . $selected . '>' . $space . '&rarr; ' . stripslashes($row["name"]) . '</option>';
            $result .= loadItemblog($db, $level+1, intval($row["blog_id"]), $choice);
        }
    }
    return $result;
}

function sortAcs($parent){
	global $db;
	$db->table = "blog";
	$db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "";
	$db->limit = "";
	$db->select();
	return $db->RowCount;
}
