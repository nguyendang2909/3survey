<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function trackingLocal($act, $typeFunc, $local_id, $title, $error) {
	global $mmenu;
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-map-marker fa-fw"></i> Khu vực
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="local_id" value="<?php echo $local_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<?php
							if($typeFunc=='add') {
							?>
							<tr class="inactive">
								<td align="right" width="150px"><label class="form-lb-tp">Tên khu vực:</label></td>
                                <td><input class="form-control required" type="text" name="title[]" value="" autocomplete="off"></td>
                                <td>
                                    <button type="button" class="btn btn-danger btn-xs btn-round btn-delete"><i class="fa fa-trash" title="Xóa"></i></button>
                                </td>
                            </tr>
							<?php
							} else {
							?>
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">Tên địa phương:</label></td>
                                <td><input class="form-control" type="text" name="title" value="<?php echo stripslashes($title);?>" autocomplete="off" required></td>
							</tr>
							<?php
							}
							?>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['tracking']['link'] . $mmenu['tracking']['sub'][0]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    $(function(){
        $("table.table").on("click focus", ".inactive", function() {
            $(this).clone().insertAfter($(this));
            $(this).removeClass("inactive").find("input:first").focus();
            $(this).find("input.required").attr('required', 'required');
            $(this).find('input.tags-input').tagsinput();
            $('.auto-number').autoNumeric('init');
        }).on("click", ".btn-delete", function() {
            $(this).closest("tr").remove();
        });
    });
</script>
<?php
}
function sortAcs(){
	global $db;
	$db->table = "local";
	$db->condition = "`is_active` = 1";
    $db->order = "";
	$db->limit = "";
	$db->select();
	return $db->RowCount;
}