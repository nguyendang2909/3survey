<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
function networkIP($act, $typeFunc, $network_ip_id, $ip, $device_item, $agency, $error) {
	global $mmenu;
    if(empty($agency)) $agency = "''";
?>
<div class="row">
	<div class="col-lg-6 col-md-9">
		<div class="panel">
			<div class="panel-heading">
				<i class="fa fa-sitemap fa-fw"></i> Địa chỉ IP
				<span class="tools pull-right">
		            <a href="javascript:;" class="fa fa-spinner fa-spin"></a>
		            <a href="javascript:;" class="fa fa-chevron-down"></a>
		            <a href="javascript:;" class="fa fa-eye"></a>
		            <a href="javascript:;" class="fa fa-compress"></a>
		            <a href="javascript:;" class="fa fa-times"></a>
	            </span>
			</div>
			<div class="panel-body">
				<div class="form-responsive">
					<form action="<?php echo $act?>" method="post" class="form-ol-3w">
						<input type="hidden" name="typeFunc" value="<?php echo $typeFunc?>" />
						<input type="hidden" name="network_ip_id" value="<?php echo $network_ip_id?>" />
						<div class="panel-show-error">
							<?php echo $error?>
						</div>
						<table class="table table-no-border table-hover">
							<tr>
								<td align="right" width="150px"><label class="form-lb-tp">IP:</label></td>
                                <td><input class="form-control" type="text" name="ip" value="<?php echo stripslashes($ip);?>" autocomplete="off" required></td>
							</tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Thiết bị:</label></td>
                                <td><?php echo listDevice($device_item);?></td>
                            </tr>
                            <tr>
                                <td align="right"><label class="form-lb-tp">Đơn vị:</label></td>
                                <td><input class="form-control easyui-combotree" data-options="url:'/action.php?url=list_agency',method:'get',value:<?php echo $agency;?>,animate:true,lines:true" name="agency" style="width: 100%;"></td>
                            </tr>
							<tr>
								<td colspan="2" class="form-ol-btn-tzc">
									<button type="submit" class="btn btn-primary btn-round">Lưu lại</button> &nbsp;
									<button type="reset" class="btn btn-warning btn-round">Nhập lại</button> &nbsp;
									<button type="button" class="btn btn-danger btn-round" onclick="location.href='<?php echo HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link'];?>'">Thoát</button>
								</td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
}

function listDevice($choice) {
    global $db;

    $result = '<select name="device_item" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Tìm..." title="Chọn nhóm..." required>';
    $db->table = "device";
    $db->condition = "`is_active` = 1";
    $db->order = "`title` ASC";
    $db->limit = "";
    $rows = $db->select("`device_id`, `title`");
    foreach($rows as $row) {
        if(intval($row['device_id'])==$choice) $selected = ' selected';
        $db->table = "device_item";
        $db->condition = "`is_active` = 1 AND `device` = " . intval($row['device_id']);
        $db->order = "`title` ASC";
        $db->limit = "";
        $rows_d = $db->select("`device_item_id`, `title`");
        if($db->RowCount>0) {
            $result .= '<optgroup label="' . stripslashes($row['title']) . '">';
            foreach($rows_d as $row_d) {
                $selected = '';
                if(intval($row_d['device_item_id'])==$choice) $selected = ' selected';
                $result .= '<option value="' . intval($row_d['device_item_id']) . '"' . $selected . '>' . stripslashes($row_d['title']) . '</option>';
            }
            $result .= '</optgroup>';
        }
    }

    $db->table = "device_item";
    $db->condition = "`is_active` = 1 AND `device` = 0";
    $db->order = "`title` ASC";
    $db->limit = "";
    $rows_d = $db->select("`device_item_id`, `title`");
    if($db->RowCount>0) {
        foreach($rows_d as $row_d) {
            $selected = '';
            if(intval($row_d['device_item_id'])==$choice) $selected = ' selected';
            $result .= '<option value="' . intval($row_d['device_item_id']) . '"' . $selected . '>' . stripslashes($row_d['title']) . '</option>';
        }
    }

    $result .= '</select>';

    return $result;
}