<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link'] . '">' . $mmenu['blog']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link'] . '">' . $mmenu['blog']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm bài viết</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "blog_post.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $tags = isset($_POST['tags']) ? $_POST['tags'] : array();
	if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên bài viết.</span>';
	elseif(empty($blog)) $error = '<span class="show-error">Vui lòng chọn chuyên mục bài viết.</span>';
    else {
		$handleUploadImg = false;
		$file_max_size  = FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS;
		$file_size      = $_FILES['img']['size'];

		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);

			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: '.$imgUp->error.'</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
            $stringObj = new String();

            $slug = (empty($slug)) ? $name : $slug;
            $slug = $stringObj->getSlug($slug);
            $db->table = "blog_post";
            $db->condition = "`slug` LIKE '$slug'";
            $db->order = "";
            $db->limit = "";
            $db->select();
            if($db->RowCount > 0) {
                $slug = $slug . '-' . $stringObj->getSlug(getRandomString(10));
            }

			$id_query = 0;
			$db->table = "blog_post";
			$data = array(
                'blog'          => $db->clearText($blog),
				'name'          => $db->clearText($name),
                'slug'          => $db->clearText($slug),
                'comment'       => $db->clearText($comment),
                'content'       => $db->clearText($content),
                'upload'        => doubleval($upload_img_id),
                'tags'          => $db->clearText(json_encode($tags)),
				'title'         => $db->clearText($title),
				'description'   => $db->clearText($description),
				'keywords'      => $db->clearText($keywords),
                'hot'           => intval($hot),
				'created_time'  => time(),
				'user_id'       => $_SESSION["user_id"]
			);
			$db->insert($data);
			$id_query = $db->LastInsertID;

			if ($handleUploadImg) {
                $name_image = $stringObj->getSlug(mb_substr($name, 0, 50, 'UTF-8') . '-' . $id_query . '-' . time());

				$imgUp->file_new_name_body = $name_image;
				$imgUp->image_resize = true;
				$imgUp->image_ratio_crop = true;
                $imgUp->image_x = 490;
				$imgUp->image_y = 256;
				$imgUp->Process($dir_dest);

				if ($imgUp->processed) {
					$db->table = "blog_post";
					$data = array(
                        'img' => $db->clearText($imgUp->file_dst_name)
					);
					$db->condition = "`blog_post_id` = $id_query";
					$db->update($data);
				} else {
					loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link']);
				}

                $imgUp->file_new_name_body = 'post-' . $name_image;
                $imgUp->image_resize = true;
                $imgUp->image_ratio_crop = true;
                $imgUp->image_x = 320;
                $imgUp->image_y = 220;
                $imgUp->Process($dir_dest);

				$imgUp->Clean();
			}

            $db->table = "uploads_tmp";
            $data = array(
                'status' => 1
            );
            $db->condition = "`upload_id` = $upload_img_id";
            $db->update($data);

			loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][0]['link']);
			$OK = true;
		}
	}
}
else {
    $upload_img_id  = 0;
    if($upload_img_id==0) {
        $db->table = "uploads_tmp";
        $data = array(
            'created_time' => time()
        );
        $db->insert($data);
        $upload_img_id = $db->LastInsertID;
    }
    //---
    $blog           = isset($_GET['blog']) ? intval($_GET['blog']) : 0;
	$name			= '';
    $slug           = '';
    $img            = '';
    $comment        = '';
    $content        = '';
    $tags           = array();
    $hot		    = 0;
    $title          = '';
    $description    = '';
    $keywords       = '';
}
if(!$OK) blogCore(HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-post-add', 'add', 0, $blog, $name, $slug, $img, $comment, $content, $hot, $title, $description, $keywords, $upload_img_id, $tags, $error);