<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link'] . '">' . $mmenu['blog']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link'] . '">' . $mmenu['blog']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm chuyên mục</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "blog_category.php");
if(empty($typeFunc)) $typeFunc = '-no-';

if(isset($_GET['parent']))
	$parent = intval($_GET['parent']);
elseif(isset($parent))
	intval($parent);
else
	$parent = 0;

$db->table = "blog";
$db->condition = "`blog_id` = $parent";
$rows = $db->select();
$db->order = "";
$db->limit = 1;
if($db->RowCount==0 && !empty($parent)) loadPageError("Mục không tồn tại.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);

$OK = false;
$error = '';
if($typeFunc=='add'){
	if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên chuyên mục.</span>';
	else {
		$handleUploadImg = false;
		$file_max_size = FILE_MAX_SIZE;
		$dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS;
		$file_size = $_FILES['img']['size'];

		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);

			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: ' . $imgUp->error . '</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
            $stringObj = new String();

            $slug = (empty($slug)) ? $name : $slug;
            $slug = $stringObj->getSlug($slug);
            $db->table = "blog";
            $db->condition = "`slug` LIKE '$slug'";
            $db->order = "";
            $db->limit = "";
            $db->select();
            if($db->RowCount > 0) {
                $slug = $slug . '-' . $stringObj->getSlug(getRandomString(10));
            }

			$id_query = 0;
			$db->table = "blog";
			$data = array(
			    'parent'        => intval($parent),
				'name'          => $db->clearText($name),
                'slug'          => $db->clearText($slug),
				'title'         => $db->clearText($title),
				'description'   => $db->clearText($description),
				'keywords'      => $db->clearText($keywords),
				'sort'          => intval(sortAcs($parent) + 1),
                'hot'           => intval($hot),
				'grid'          => intval($grid),
				'created_time'  => time(),
				'user_id'       => $_SESSION["user_id"]
			);
			$db->insert($data);
			$id_query = $db->LastInsertID;

			if ($handleUploadImg) {
                $name_image = $stringObj->getSlug(mb_substr($name, 0, 50, 'UTF-8') . '-' . $id_query . '-' . time());

				$imgUp->file_new_name_body = $name_image;
				$imgUp->image_resize = true;
				$imgUp->image_ratio_crop = true;
                $imgUp->image_x = 490;
				$imgUp->image_y = 256;
				$imgUp->Process($dir_dest);

				if ($imgUp->processed) {
                    $name_image = $imgUp->file_dst_name;
					$db->table = "blog";
					$data = array(
                        'img' => $db->clearText($name_image)
					);
					$db->condition = "`blog_id` = $id_query";
					$db->update($data);
				} else {
					loadPageError("Lỗi tải hình: " . $imgUp->error, HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);
				}

				$imgUp->Clean();
			}

			loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);
			$OK = true;
		}
	}
}
else {
    $name			= '';
    $slug           = '';
    $img            = '';
    $hot		    = 0;
    $grid           = 1;
    $title          = '';
    $description    = '';
    $keywords       = '';
}
if(!$OK) blogCore(HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-category-add', 'add', 0, $parent, $name, $slug, $img, $hot, $grid, $title, $description, $keywords, $error);