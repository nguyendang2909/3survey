<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link'] . '">' . $mmenu['blog']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link'] . '">' . $mmenu['blog']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Chỉnh sửa chuyên mục</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "blog_category.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$blog_id = isset($_GET['id']) ? intval($_GET['id']) : intval($blog_id);
$db->table = "blog";
$db->condition = "`blog_id` = $blog_id";
$db->order = "";
$db->limit = 1;
$rows = $db->select();
if($db->RowCount==0) loadPageError("Mục không tồn tại.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);

$OK = false;
$error = '';
if($typeFunc=='edit'){
    if(empty($name)) $error = '<span class="show-error">Vui lòng nhập tên chuyên mục.</span>';
    else {
		$handleUploadImg = false;
		$file_max_size = FILE_MAX_SIZE;
		$dir_dest = ROOT_DIR . DS . 'uploads' . DS . 'blog' . DS;
		$file_size = $_FILES['img']['size'];
		if($file_size>0) {
			$imgUp = new Upload($_FILES['img']);
			$imgUp->file_max_size = $file_max_size;
			if ($imgUp->uploaded) {
				$handleUploadImg = true;
				$OK = true;
			}
			else {
				$error = '<span class="show-error">Lỗi tải hình: ' . $imgUp->error . '</span>';
			}
		}
		else {
			$handleUploadImg = false;
			$OK = true;
		}

		if($OK) {
            $stringObj = new String();

            $slug = (empty($slug)) ? $name : $slug;
            $slug = $stringObj->getSlug($slug);
            $db->table = "blog";
            $db->condition = "`slug` LIKE '$slug'";
            $db->order = "";
            $db->limit = "";
            $db->select();
            if($db->RowCount > 0) {
                foreach ($rows as $row) {
                    if($row['blog_id']!=$blog_id)
                        $slug = $slug . '-' . $stringObj->getSlug(getRandomString(10));
                }
            }

			$db->table = "blog";
			$data = array(
                'parent'        => intval($parent),
                'name'          => $db->clearText($name),
                'slug'          => $db->clearText($slug),
                'title'         => $db->clearText($title),
                'description'   => $db->clearText($description),
                'keywords'      => $db->clearText($keywords),
                'hot'           => intval($hot),
                'grid'          => intval($grid),
				'modified_time' => time(),
				'user_id'       => $_SESSION["user_id"]
			);
			$db->condition = "`blog_id` = $blog_id";
			$db->update($data);

			if($handleUploadImg) {
				if(glob($dir_dest .'*'.$img)) array_map("unlink", glob($dir_dest .'*'.$img));
				$name_image = $stringObj->getSlug(mb_substr($name, 0, 50, 'UTF-8') . '-' . $blog_id . '-' . time());

                $imgUp->file_new_name_body      = $img_name_file;
				$imgUp->image_resize            = true;
				$imgUp->image_ratio_crop        = true;
                $imgUp->image_x                 = 490;
				$imgUp->image_y                 = 256;
				$imgUp->Process($dir_dest);
				if($imgUp->processed) {
					$name_img = $imgUp->file_dst_name;
					$db->table = "blog";
					$data = array(
                        'img' => $db->clearText($name_img)
					);
					$db->condition = "`blog_id` = $blog_id";
					$db->update($data);
				}
				else {
					loadPageError("Lỗi tải hình: ".$imgUp->error, HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);
				}
				$imgUp-> Clean();
			}

			loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);
			$OK = true;
		}
	}
}
else {
	$db->table = "blog";
	$db->condition = "`blog_id` = $blog_id";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach($rows as $row) {
        $parent         = intval($row['parent']);
		$name           = $row['name'];
		$slug           = $row['slug'];
		$img            = $row['img'];
        $hot            = intval($row['hot']);
        $grid           = intval($row['grid']);
        $title          = $row['title'];
        $description    = $row['description'];
        $keywords       = $row['keywords'];
	}
}
if(!$OK) blogCore(HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-category-edit', 'edit', $blog_id, $parent, $name, $slug, $img, $hot, $grid, $title, $description, $keywords, $error);
