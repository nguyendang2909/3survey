<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['blog']['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['blog']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if(isset($_GET['tick']) && in_array("blog-category;delete", $corePrivilegeSlug['op'])) {
    $tick = intval($_GET['tick']);
    if($tick>0) {
        $db->table = "blog";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`blog_id` = $tick";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['blog']['link'] . $mmenu['blog']['sub'][1]['link']);
    }
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
			<a class="btn-add-new" href="<?php echo HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-category-add&parent=0';?>">Thêm chuyên mục</a>
			<div class="table-responsive">
				<table class="table table-manager" cellspacing="0" cellpadding="0" id="dataTablesList">
					<thead>
						<tr>
							<th>Chuyên mục</th>
							<th>Sắp xếp</th>
                            <th>Nổi bật</th>
							<th>Chức năng</th>
							<th>Thêm bài viết</th>
						</tr>
					</thead>
					<tbody>
					<?php
					echo loadMenuCategory($db, 0 , 0);
					?>
					</tbody>
				</table>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-6 -->
</div>
<?php
function loadMenuCategory($db, $level, $parent) {
	global  $mmenu, $corePrivilegeSlug;
	$result = '';
	$db->table = "blog";
	$db->condition = "`is_active` = 1 AND `parent` = $parent";
	$db->order = "`sort` ASC";
	$db->limit = "";
	$rows = $db->select();
	$countList = $db->RowCount;
	foreach($rows as $row) {
		if($parent==0) $width = '100%';
		else $width = '80%';

		$result .= '<tr>';
		if($parent==0) $result .= '<td style="font-weight: bold;">' . stripslashes($row['name']) . '</td>';
		else $result .= '<td style="padding: 0 8px 0 ' . $level . 'px;"><img src="/images/node.png"> ' . stripslashes($row['name']) . '</td>';
        if(in_array('blog-category-edit', $corePrivilegeSlug['op'])) {
            $result .= '<td align="right">' . showSort('sort_' . $row["blog_id"], $countList, $row["sort"], $width, 0, $row["blog_id"], 'blog', 1) . '</td>';
            if($row["hot"]==0)
                $result .= '<td><div class="btn-event-close" data-toggle="tooltip" data-placement="top" title="Mở" onclick="edit_status($(this), ' . $row["blog_id"] . ', \'hot\', \'blog\');" rel="1">0</div></td>';
            else
                $result .= '<td><div class="btn-event-open" data-toggle="tooltip" data-placement="top" title="Đóng" onclick="edit_status($(this), ' . $row["blog_id"] . ', \'hot\', \'blog\');" rel="0">1</div></td>';
        } else {
            $result .= '<td align="right">' . showSort('sort_' . $row["blog_id"], $countList, $row["sort"], $width, 0, $row["blog_id"], 'blog', 0) . '</td>';
            if($row["hot"]==0)
                $result .= '<td><div class="btn-event-close ol-alert-core" data-toggle="tooltip" data-placement="top" title="Mở" rel="1">0</div></td>';
            else
                $result .= '<td><div class="btn-event-open ol-alert-core" data-toggle="tooltip" data-placement="top" title="Đóng" rel="0">1</div></td>';
        }

        $add = $delete = '';
        if($level < 30)
            $add = '<a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-category-add&parent=' . intval($row["blog_id"]) . '"><img data-toggle="tooltip" data-placement="left" title="Thêm mục" src="/images/add.png"></a> &nbsp; ';
        else
            $add = '<span style="width: 25px; height: 1px; display: inline-block;"></span>';
        if(in_array('blog-category;delete', $corePrivilegeSlug['op']))
            $delete = '<a class="ol-confirm" style="cursor: pointer;" href="javascript:;" rel="' . intval($row["blog_id"]) . '"><img data-toggle="tooltip" data-placement="right" title="Xóa mục" src="/images/remove.png"></a>';
        else
            $delete = '<a class="ol-alert-core" style="cursor: pointer;"><img data-toggle="tooltip" data-placement="right" title="Xóa mục" src="/images/remove.png"></a>';

        $result .= '<td align="center">' . $add . '<a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-category-edit&id=' . intval($row["blog_id"]) . '"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa" src="/images/edit.png"></a> &nbsp; ' . $delete . '</td>';
        $result .= '<td align="center"><a href="' . HOME_URL_LANG . $mmenu['blog']['link'] . '/blog-post-add&blog=' . intval($row["blog_id"]) . '"><img data-toggle="tooltip" data-placement="top" title="Thêm bài viết mới" src="/images/list.png"></a></td>';
        $result .= '</tr>';

        $result .= loadMenuCategory($db, $level+30, intval($row["blog_id"]));
	}
	return $result;
}
?>
<script>
	$(document).ready(function() {
		$('#dataTablesList').dataTable( {
			"language": {
				"url": "/js/data-tables/de_DE.txt"
			},
			"ordering": false,
			"paging":   false,
			"info":     false
		});
	});
    //---
	$(".ol-confirm").click(function() {
		var id = parseInt($(this).attr("rel"));
		confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
			if(this.data == true) window.location.href = '?tick=' + id;
		});
	});
	$(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>