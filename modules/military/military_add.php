<?php
include_once(_F_TEMPLATES . DS . "military.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['military']['link'] . '">' . $mmenu['military']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  // Validate data
  if (empty($age) || empty($education)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
      $db->table = "military_agency";
      $data = array(
        'age' => intval($age),
        'education' => $db->clearText($education),
        'type' => intval($type),
        'social' => $db->clearText(json_encode($social)),
        'internetUsageTime' => intval($internetUsageTime),
        'internetUsagePurpose' => $db->clearText(json_encode($internetUsagePurpose)),
        'postStatus' => intval($postStatus),
        'postAction' => $db->clearText(json_encode($postAction)),
        'identifyAuthen' => intval($identifyAuthen),
        'assessOnlineCommunity' => intval($assessOnlineCommunity),
        'followingPage' => $db->clearText($followingPage),
        'pageEffective' => $db->clearText($pageEffective),
        'note' => $db->clearText($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['military']['link']);
  }
} else {
  $age = '';
  $education = '';
  $type = '';
  $social = '';
  $internetUsageTime = '';
  $internetUsagePurpose = '';
  $postStatus = '';
  $postAction = '';
  $identifyAuthen = '';
  $assessOnlineCommunity = '';
  $followingPage = '';
  $pageEffective = '';
  $note = '';
}
if (!$OK) postMilitary(
  HOME_URL_LANG . $mmenu['military']['link'] . '/military-add',
  "add",
  0,
  $age,
  $education,
  $type,
  $social,
  $internetUsageTime,
  $internetUsagePurpose,
  $postStatus,
  $postAction,
  $identifyAuthen,
  $assessOnlineCommunity,
  $followingPage,
  $pageEffective,
  $note,
  $error
);
