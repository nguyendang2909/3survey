<?php
include_once(_F_TEMPLATES . DS . "military.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['military']['link'] . '">' . $mmenu['military']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$militaryAgencyId = isset($_GET['id']) ? intval($_GET['id']) : intval($militaryAgencyId);
$db->table      = "military_agency";
$db->condition  = "`militaryAgencyId` = $militaryAgencyId AND `isActive` = 1";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['military']['link']);

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($age) || empty($education)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
      $db->table = "military_agency";
      $data = array(
        'age' => intval($age),
        'education' => $db->clearText($education),
        'type' => intval($type),
        'social' => $db->clearText(json_encode($social)),
        'internetUsageTime' => intval($internetUsageTime),
        'internetUsagePurpose' => $db->clearText(json_encode($internetUsagePurpose)),
        'postStatus' => intval($postStatus),
        'postAction' => $db->clearText(json_encode($postAction)),
        'identifyAuthen' => intval($identifyAuthen),
        'assessOnlineCommunity' => intval($assessOnlineCommunity),
        'followingPage' => $db->clearText($followingPage),
        'pageEffective' => $db->clearText($pageEffective),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`militaryAgencyId` = $militaryAgencyId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['military']['link']);
  }
} else {
  foreach ($rows as $row) {
    $age = $row['age'];
    $education = $row['education'];
    $sex = $row['type'];
    $social = json_decode($row['social']);
    $internetUsageTime = $row['internetUsageTime'];
    $internetUsagePurpose = json_decode($row['internetUsagePurpose']);
    $postStatus = $row['postStatus'];
    $postAction = json_decode($row['postAction']);
    $identifyAuthen = $row['identifyAuthen'];
    $assessOnlineCommunity = $row['assessOnlineCommunity'];
    $followingPage = $row['followingPage'];
    $pageEffective = $row['pageEffective'];
    $note = $row['note'];
  }
}
if (!$OK) postMilitary(
  HOME_URL_LANG . $mmenu['military']['link'] . '/military-edit',
  "edit",
  $militaryAgencyId,
  $age,
  $education,
  $sex,
  $social,
  $internetUsageTime,
  $internetUsagePurpose,
  $postStatus,
  $postAction,
  $identifyAuthen,
  $assessOnlineCommunity,
  $followingPage,
  $pageEffective,
  $note,
  $error
);
