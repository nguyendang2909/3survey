<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Xoa record
if (isset($_POST['tick']) && in_array("military;delete", $corePrivilegeSlug['op'])) {
  $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
  $tick = array_filter($tick);
  if (count($tick) > 0) {
    $tick = implode(',', $tick);
    $db->table = "military_agency";
    $data = array(
      'isActive' => 0,
      'updatedBy' => $_SESSION["user_id"],
      'updatedAt' => time(),
    );
    $db->condition = "`militaryAgencyId` IN ($tick)";
    $db->update($data);
    loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['military']['link']);
  }
}
?>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-no-border">
      <div class="row">
        <div class="col-xs-12 top-tool">
          <div class="pull-left">
            <a class="btn-add-new not-abs" href="<?php echo HOME_URL_LANG . $mmenu['military']['link'] . '/military-add'; ?>">Thêm</a>
          </div>
        </div>
      </div>
      <div class="table-responsive">
        <form method="post" id="_ol_delete">
          <table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
            <thead>
              <tr>
                <th>ID</th>
                <th>Tuổi</th>
                <th>Trình độ</th>
                <th>Giới tính</th>
                <th>Câu 1</th>
                <th>Câu 2</th>
                <th>Câu 3</th>
                <th>Câu 4</th>
                <th>Câu 5</th>
                <th>Câu 6</th>
                <th>Câu 7</th>
                <th>Câu 8a</th>
                <th>Câu 8b</th>
                <th>Câu 9</th>
                <th width="80px">Chọn</th>
              </tr>
            </thead>

            <thead>
              <tr>
                <td align="center">-</td>
                <td><input type="text" data-column="1" class="form-control filter"></td>
                <td><input type="text" data-column="2" class="form-control filter"></td>
                <td><input type="text" data-column="3" class="form-control filter"></td>
                <td><input type="text" data-column="4" class="form-control filter"></td>
                <td><input type="text" data-column="5" class="form-control filter"></td>
                <td><input type="text" data-column="6" class="form-control filter"></td>
                <td><input type="text" data-column="7" class="form-control filter"></td>
                <td><input type="text" data-column="8" class="form-control filter"></td>
                <td><input type="text" data-column="9" class="form-control filter"></td>
                <td><input type="text" data-column="10" class="form-control filter"></td>
                <td><input type="text" data-column="11" class="form-control filter"></td>
                <td><input type="text" data-column="12" class="form-control filter"></td>
                <td><input type="text" data-column="13" class="form-control filter"></td>

                <td align="center">-</td>
              </tr>
            </thead>
          </table>

          <!-- Nut xoa -->
          <?php
          if (in_array("military;delete", $corePrivilegeSlug['op']))
            echo '
            <div class="row">
              <div class="col-sm-12" align="right">
                <label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label>
                <input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá hồ sơ" name="delete">
              </div>
            </div>';
          ?>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#dataTablesList tfoot th').each(function() {
      var title = $(this).text();
      $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    });
    var table = $('#dataTablesList').DataTable({
      language: {
        url: "/js/data-tables/de_DE.txt"
      },
      lengthMenu: [100, 200, 300],
      info: false,
      processing: true,
      serverSide: true,
      ajax: {
        url: '/action.php',
        type: 'GET',
        data: {
          url: 'military',
          type: 'load'
        }
      },
      "columns": [{
          data: 'no'
        },
        {
          data: 'age'
        },
        {
          data: 'education'
        },
        {
          data: 'type'
        },
        {
          data: 'social'
        },
        {
          data: 'internetUsageTime'
        },
        {
          data: 'internetUsagePurpose'
        },
        {
          data: 'postStatus'
        },
        {
          data: 'postAction'
        },
        {
          data: 'identifyAuthen'
        },
        {
          data: 'assessOnlineCommunity'
        },
        {
          data: 'followingPage'
        },
        {
          data: 'pageEffective'
        },
        {
          data: 'note'
        },
        {
          data: 'tool'
        }
      ],
      // Can le cho cot
      fnRowCallback: function(nRow, aData, iDisplayIndex) {
        $('td:eq(0)', nRow).css("text-align", "center");
        $('td:eq(1)', nRow).css("text-align", "center");
        $('td:eq(2)', nRow).css("text-align", "center");
        $('td:eq(3)', nRow).css("text-align", "center");
        $('td:eq(4)', nRow).css("text-align", "center");
        $('td:eq(5)', nRow).css("text-align", "center");
        $('td:eq(6)', nRow).css("text-align", "center");
        $('td:eq(7)', nRow).css("text-align", "center");
        $('td:eq(8)', nRow).css("text-align", "center");
        $('td:eq(9)', nRow).css("text-align", "center");
        $('td:eq(10)', nRow).css("text-align", "center");
        $('td:eq(11)', nRow).css("text-align", "center");
        $('td:eq(12)', nRow).css("text-align", "center");
        $('td:eq(13)', nRow).css("text-align", "center");
        $('td:eq(14)', nRow).css("text-align", "center");

        

        return nRow;
      },

      fnDrawCallback: function() {
        $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
      },

      order: [0, "asc"],

      aoColumnDefs: [{
        searchable: false,
        orderable: false
      }]
    });

    // Apply the search
    $('.filter').on('change', function() {
      var i = $(this).attr('data-column');
      var v = $(this).val();
      table.columns(i).search(v).draw();
    });
  });

  // Datetimepicker
  $('.input-date').datetimepicker({
    lang: 'vi',
    timepicker: false,
    format: '<?php echo TTH_DATE_FORMAT; ?>'
  });

  // Confirm xoa record
  $(".ol-confirm").click(function() {
    confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
      if (this.data == true) document.getElementById("_ol_delete").submit();
    });
  });

  $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>