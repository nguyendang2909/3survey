<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0];?>"><?php echo $menu_ol[23];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0];?>"><?php echo $menu_op[23][0];?></a>
			</li>
			<li>
				<a class="current">Thêm tài sản</a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
include_once (_F_TEMPLATES . DS . "document_item.php");
if(empty($typeFunc)) $typeFunc = "no";

$date = new DateClass();
$OK = false;
$error = '';
if($typeFunc=='add'){
    if(empty($document)) $error = '<span class="show-error">Vui lòng chọn danh mục tài liệu.</span>';
	elseif(empty($code)) $error = '<span class="show-error">Vui lòng nhập mã tài liệu.</span>';
	elseif(empty($title)) $error = '<span class="show-error">Vui lòng nhập tiêu đề tài liệu.</span>';
	else {
		$file_max_size	= FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'residence' . DS;
		
		$file_name      = 'bt' . time() . '_' . md5(microtime(true));
        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
				$error = '<span class="show-error">Lỗi tải tệp tin: '.$fileUp->error.'</span>';
            }
        } else {
            $OK = true;
            $file_name = '-no-';
        }
		
		if($OK) {			
			$db->table = "document_item";
			$data = array(
				'document'  	=> intval($document),
				'code'          => $db->clearText($code),
				'title'       	=> $db->clearText($title),
				'comment'       => $db->clearText($comment),
				'file'       	=> $db->clearText($file_name),
				'created_time'  => time(),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->insert($data);
			
			loadPageSuccess("Đã thêm dữ liệu thành công.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0]);
		}
		$OK = true;
	}
}
else {
    $document  	= isset($_GET['id']) ? intval($_GET['id']) : 0;
    $code      	= "";
    $title    	= "";
    $comment	= "";
    $files    	= "";
}
if(!$OK) documentItem(TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . 'document_item_add', "add", 0, $document, $code, $title, $comment, $files, $error);