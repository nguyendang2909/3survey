<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0];?>"><?php echo $menu_ol[23];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0];?>"><?php echo $menu_op[23][0];?></a>
			</li>
			<li>
				<a class="current">Chỉnh sửa tài sản</a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
//
$document_item_id = isset($_GET['id']) ? intval($_GET['id']) : intval($document_item_id);
$db->table = "document_item";
$db->condition = "`document_item_id` = $document_item_id";
$db->order = "";
$db->limit = 1;
$db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0]);

include_once (_F_TEMPLATES . DS . "document_item.php");
if(empty($typeFunc)) $typeFunc = "no";

$OK = false;
$error = '';
if($typeFunc=='edit'){
    if(empty($document)) $error = '<span class="show-error">Vui lòng chọn danh mục tài liệu.</span>';
	elseif(empty($code)) $error = '<span class="show-error">Vui lòng nhập mã tài liệu.</span>';
	elseif(empty($title)) $error = '<span class="show-error">Vui lòng nhập tiêu đề tài liệu.</span>';
    else {
        $file_max_size	= FILE_MAX_SIZE;
        $dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'residence' . DS;

        $file_name      = 'bt' . time() . '_' . md5(microtime(true));
        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
                $error = '<span class="show-error">Lỗi tải tệp tin: '.$fileUp->error.'</span>';
            }
        } else {
            $OK = true;
            foreach($rows as $row) {
                $file_name = stripslashes($row['file']);
            }
        }
		
		if($OK) {
			$db->table = "document_item";
			$data = array(
				'document'  	=> intval($document),
				'code'          => $db->clearText($code),
				'title'       	=> $db->clearText($title),
				'comment'       => $db->clearText($comment),
				'file'       	=> $db->clearText($file_name),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->condition = "`document_item_id` = $document_item_id";
			$db->update($data);
			
			loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0]);
		}
		$OK = true;
	}
}
else {
	$db->table = "document_item";
	$db->condition = "`document_item_id` = $document_item_id";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach($rows as $row) {
        $document  	= intval($row['document']);
        $code    	= $row['code'];
        $title   	= $row['title'];
        $comment 	= $row['comment'];
        $files   	= $row['file'];
	}
}
if(!$OK) documentItem(TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . 'document_item_edit', "edit", $document_item_id, $document, $code, $title, $comment, $files, $error);