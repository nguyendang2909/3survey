<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//

$db->table = "document";
$db->condition = "`is_active` = 1";
$db->order = "";
$db->limit = "";
$rows = $db->select();
foreach ($rows as $row) {
    $db->table = "document_item";
    $db->condition = "`is_active` = 1 AND `document` = " . intval($row['document_id']);
    $db->order = "";
    $db->limit = "";
    $rows_c = $db->select();
    $count = $db->RowCount;
    //--- Cập nhật dữ liệu
    $db->table = "document";
    $data = array(
        'count' => $count
    );
    $db->condition = "`document_id` = " . intval($row['document_id']);
    $db->update($data);
}
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a><?php echo $menu_ol[23];?></a>
			</li>
			<li>
				<a class="current"><?php echo $menu_op[23][1];?></a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
if(isset($_POST['tick'])){
    $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
    $tick = array_filter($tick);
    if(count($tick)>0) {
        $tick = implode(',', $tick);
        $db->table = "document";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`document_id` IN ($tick)";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1]);
    }
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-left">
                        <a class="btn-add-new not-abs" href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . 'document_cat_add';?>">Thêm danh mục</a>
                        <?php
                        if(in_array("document_parent", $corePrivilegeSlug['op'])) echo '<a class="btn btn-ac-group btn-round btn-sm" href="javascript:;" onclick="return modal_parent(\'document\', 0);"><i class="fa fa-plus"></i> Thêm nhóm danh mục</a>';
                        ?>
                    </div>
                </div>
            </div>
			<div class="table-responsive">
                <form method="post" id="_ol_open">
					<table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
						<thead>
						<tr>
							<th>STT</th>
							<th>Danh mục</th>
                            <th width="250px">Ghi chú</th>
                            <th>Số tài liệu</th>
                            <th>Ngày cập nhật</th>
                            <th>Người cập nhật</th>
                            <th width="120px">Chọn</th>
						</tr>
						</thead>
						<thead>
						<tr>
							<td align="center">-</td>
                            <td><input type="text" data-column="1" class="form-control filter"></td>
                            <td><input type="text" data-column="2" class="form-control filter"></td>
                            <td><input type="text" data-column="3" class="form-control filter auto-number text-right" data-a-sep=" " data-v-max="999999999999" data-v-min="0" maxlength="20"></td>
                            <td><input type="text" data-column="4" class="form-control filter input-date text-center"></td>
                            <td><input type="text" data-column="5" class="form-control filter"></td>
                            <td align="center">-</td>
						</tr>
						</thead>
					</table>
                    <?php
                    if(in_array("document_cat;delete", $corePrivilegeSlug['op']))
                        echo '<div class="row"><div class="col-sm-12" align="right"><label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label><input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá" name="delete"></div></div>';
                    ?>
				</form>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-8 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTablesList tfoot th').each( function () {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );
		var table = $('#dataTablesList').DataTable( {
			"language": {
				"url": "/js/data-tables/de_DE.txt"
			},
			"lengthMenu": [100, 200, 300],
			"info":     false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: '/action.php',
				type: 'POST',
				data: {
					url: 'document_cat',
					type: 'load'
				}
			},
            "columns": [
                { data: 'no' },
                { data: 'title' },
                { data: 'comment' },
                { data: 'count' },
                { data: 'time' },
                { data: 'user' },
                { data: 'tool' }
            ],
			"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
				$('td:eq(0)', nRow).css( "text-align", "center" );
                $('td:eq(3)', nRow).css( "text-align", "right" );
                $('td:eq(4)', nRow).css( "text-align", "center" );
                $('td:eq(6)', nRow).css( "text-align", "center" );
				return nRow;
			},
            rowGroup: {
                startRender: function ( rows, group ) {
                    var count = rows
                        .data()
                        .pluck('count')
                        .reduce( function (a, b) {
                            return a + b.replace(/[^\d]/g, '')*1;
                        }, 0);
                    count = $.fn.dataTable.render.number(' ', '.', 0).display( count );

                    var parent = rows
                        .data()
                        .pluck('parent')
                        .reduce( function (a, b) {
                            return b;
                        }, 0);

                    var tool = '';
                    <?php if(in_array("document_parent", $corePrivilegeSlug['op'])) { ?>
                    if(parseInt(parent)>0) tool = '<a href="javascript:;" onclick="return modal_parent(\'document\', ' + parent + ');"><img data-toggle="tooltip" data-placement="top" title="Chỉnh sửa nhóm" src="./images/edit.png"></a> &nbsp; <a href="javascript:;" onclick="return delete_parent(\'document\', ' + parent + ');"><img data-toggle="tooltip" data-placement="top" title="Xóa nhóm" src="./images/remove.png"></a> &nbsp; <a href="/?ol=document&op=document_cat_add&parent=' + parent + '"><img data-toggle="tooltip" data-placement="top" title="Thêm danh mục" src="./images/add.png"></a>';
                    <?php } ?>
                    return $('<tr/>')
                        .append( '<td colspan="3">'+group+'</td>' )
                        .append( '<td align="right">'+count+'</td>' )
                        .append( '<td></td>' )
                        .append( '<td></td>' )
                        .append( '<td align="center">' + tool + '</td>' );
                },
                dataSrc: 'group'
            },
			"order": [ 0, "desc" ],
			"aoColumnDefs" : [ {
				'targets': [6],
				'searchable':false,
				'orderable':false
			} ]
		});
		// Apply the search
        $( '.filter' ).on( 'change', function () {
            var i = $(this).attr('data-column');
            var v = $(this).val();
            table.columns(i).search(v).draw();
        });
	});
    $('.input-date').datetimepicker({
        lang:'vi',
        timepicker: false,
        format:'<?php echo TTH_DATE_FORMAT;?>'
    });
    $(".ol-confirm").click(function() {
        confirm("Tất cả dữ liệu được chọn sẽ xoá hoàn toàn và không thể khôi phục lại.\nBạn có muốn thực hiện không?", function() {
            if(this.data == true) document.getElementById("_ol_open").submit();
        });
    });
    $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>