<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a><?php echo $menu_ol[23];?></a>
			</li>
			<li>
				<a class="current"><?php echo $menu_op[23][0];?></a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
if(isset($_POST['tick'])){
    $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
    $tick = array_filter($tick);
    if(count($tick)>0) {
        $tick = implode(',', $tick);
        $db->table = "document_item";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`document_item_id` IN ($tick)";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][0]);
    }
}
$document = isset($_GET['id']) ? intval($_GET['id']) : 0;
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-left">
                        <a class="btn-add-new not-abs" href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . 'document_item_add';?>">Thêm tài liệu</a>
                    </div>
                </div>
            </div>
			<div class="table-responsive">
                <form method="post" id="_ol_open">
					<table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
						<thead>
						<tr>
							<th>STT</th>
							<th>Danh mục</th>
                            <th>Mã tài liệu</th>
                            <th>Tiêu đề</th>
                            <th>Ghi chú</th>
                            <th>Tệp tin</th>
                            <th>Ngày cập nhật</th>
                            <th>Người cập nhật</th>
                            <th width="60px">Chọn</th>
						</tr>
						</thead>
						<thead>
						<tr>
							<td align="center">-</td>
                            <td><input type="text" data-column="1" class="form-control filter"></td>
                            <td><input type="text" data-column="2" class="form-control filter"></td>
                            <td><input type="text" data-column="3" class="form-control filter"></td>
                            <td><input type="text" data-column="4" class="form-control filter"></td>
                            <td><input type="text" data-column="5" class="form-control filter"></td>
                            <td><input type="text" data-column="6" class="form-control filter input-date text-center"></td>
                            <td><input type="text" data-column="7" class="form-control filter"></td>
                            <td align="center">-</td>
                        </tr>
						</thead>
					</table>
                    <?php
                    if(in_array("document_item;delete", $corePrivilegeSlug['op']))
                        echo '<div class="row"><div class="col-sm-12" align="right"><label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label><input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá" name="delete"></div></div>';
                    ?>
				</form>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-8 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTablesList tfoot th').each( function () {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );
		var table = $('#dataTablesList').DataTable( {
			"language": {
				"url": "/js/data-tables/de_DE.txt"
			},
			"lengthMenu": [100, 200, 300],
			"info":     false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: '/action.php',
				type: 'POST',
				data: {
					url: 'document_item',
					type: 'load',
                    document: <?php echo $document;?>
				}
			},
			"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
				$('td:eq(0)', nRow).css( "text-align", "center" );
                $('td:eq(5)', nRow).css( "text-align", "center" );
                $('td:eq(6)', nRow).css( "text-align", "center" );
                $('td:eq(8)', nRow).css( "text-align", "center" );
				return nRow;
			},
			"order": [ 0, "desc" ],
			"aoColumnDefs" : [ {
				'targets': [8],
				'searchable':false,
				'orderable':false
			} ]
		});
		// Apply the search
        $( '.filter' ).on( 'change', function () {
            var i = $(this).attr('data-column');
            var v = $(this).val();
            table.columns(i).search(v).draw();
        });
	});
    $('.input-date').datetimepicker({
        lang: 'vi',
        timepicker: false,
        format: '<?php echo TTH_DATE_FORMAT;?>'
    });
    $(".ol-confirm").click(function() {
        confirm("Tất cả dữ liệu được chọn sẽ xoá hoàn toàn và không thể khôi phục lại.\nBạn có muốn thực hiện không?", function() {
            if(this.data == true) document.getElementById("_ol_open").submit();
        });
    });
    $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>