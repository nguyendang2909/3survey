<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
?>
<!-- Menu path -->
<div class="row">
	<div class="col-md-12">
		<ul class="breadcrumbs-alt">
			<li>
				<a href="/"><?php echo $menu_ol[0];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1];?>"><?php echo $menu_ol[23];?></a>
			</li>
			<li>
				<a href="<?php echo TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1];?>"><?php echo $menu_op[23][1];?></a>
			</li>
			<li>
				<a class="current">Chỉnh sửa danh mục</a>
			</li>
		</ul>
	</div>
</div>
<!-- /.row -->
<?php
//
$document_id = isset($_GET['id']) ? intval($_GET['id']) : intval($document_id);
$db->table = "document";
$db->condition = "`document_id` = $document_id";
$db->order = "";
$db->limit = 1;
$db->select();
if($db->RowCount==0) loadPageError("Dữ liệu không tồn tại.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1]);

include_once (_F_TEMPLATES . DS . "document_cat.php");
if(empty($typeFunc)) $typeFunc = "no";

$OK = false;
$error = '';
if($typeFunc=='edit'){
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên danh mục.</span>';
    else {
		$db->table = "document";
		$data = array(
            'title'         => $db->clearText($title),
            'parent'        => intval($parent),
            'comment'       => $db->clearText($comment),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
		);
		$db->condition = "`document_id` = $document_id";
		$db->update($data);
		loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . $link_op[23][1]);
		$OK = true;
	}
}
else {
	$db->table = "document";
	$db->condition = "`document_id` = $document_id";
	$db->order = "";
	$db->limit = 1;
	$rows = $db->select();
	foreach($rows as $row) {
        $title      = $row['title'];
        $parent     = intval($row['parent']);
        $comment    = $row['comment'];
	}
}
if(!$OK) documentCat(TTH_PATH_LK . $link_ol[23] . TTH_PATH_OP_LK . 'document_cat_edit', "edit", $document_id, $title, $parent, $comment, $error);