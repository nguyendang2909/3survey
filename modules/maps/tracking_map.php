<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}
?>


<link rel="stylesheet" href="<?php echo HOME_URL; ?>/css/ol/ol.css" type="text/css" />
<link rel="stylesheet" href="<?php echo HOME_URL; ?>/css/ol-layerswitcher/src/ol-layerswitcher.css" type="text/css" />
<link rel="stylesheet" href="<?php echo HOME_URL; ?>/css/ol/ol-ext.css" type="text/css" />



<style>
  html,
  body {
    margin: 0;
    height: 100%;
  }

  .map {
    position: absolute;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 640px;
  }


  .sidebar {
    display: block;
    height: 80%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    background-color: white;
    overflow-x: hidden;
    margin-top: 140px;
    padding-top: 20px;
    transition: 0.5s;
    color: #00918e;
    overflow-y: auto;
  }

  .sidebar a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: black;
    transition: 0.3s;
  }


  .sidebar a:hover {
    color: #00900e;
  }

  .closebtn {
    position: absolute;
    right: 0px;
    top: 0px;
    font-size: 20px;
  }

  .openbtn {
    font-size: 20px;
    cursor: pointer;
    background-color: #111;
    color: white;
    padding: 10px 15px;
    border: none;
  }

  .sidebar li {
    margin-left: 15px;
    margin-bottom: 3px;
  }

  .sidebar li:hover {
    color: black;
    cursor: pointer;
  }

  .sidebar img {
    margin-right: 5px;
    width: 18px;
    height: auto;
  }

  .fcontainer {
    display: block;
    float: right;
  }

  #search {
    border: 1px solid #05708b;
    border-radius: 4px;
    width: 260px;
    height: 30px;
  }

  .ol-popup-content {
    /* display: inline-block; */
    /* display: block; */
    /* position: absolute; */
    /* float: right; */
    /* clear: both; */
    /* overflow: auto; */
    /* border-radius: 10px; */
    max-height: 350px;
  }

  #info {
    z-index: 1;
    opacity: 0;
    position: absolute;
    bottom: 0;
    right: 0;
    margin: 0;
    background: rgba(0, 60, 136, 0.7);
    color: white;
    border: 0;
  }

  .searchObject {
    /* background-color: #66ffcc; */
    background-color: white;
    background-image: url(../images/search-icon.png);
    background-position: left 9px center;
    background-repeat: no-repeat no-repeat;
    border: 1px solid black;
    border-radius: 100px;
    box-shadow: none;
    box-sizing: border-box;
    color: #4f4f4f;
    display: block;
    font-size: 14px;
    line-height: 1.42857;
    margin: 0;
    outline: 1px solid red;
    padding: 5px 10px 5px 30px;
    transition: all .3s;
    width: 170px;
  }

  .popup {
    margin-top: 25px;
    right: 0px;
    width: 300px;
    height: 600px;
    background-color: lightblue;
    display: block;
    position: absolute;
  }

  #popup-div {
    font-size: 11px;
  }

  .shipPicture {
    width: 300px;
    height: 200px;
    background-color: red;
  }

  .shipDetail {
    width: 300px;
    height: 400px;
    background-color: yellow;
  }

  a {
    color: white;
  }

  a:hover {
    color: red;
    font-weight: bold;
  }


  /* ---------------TEST---------------- */

  /*the container must be positioned relative:*/
  .autocomplete {
    position: relative;
    display: inline-block;
  }

  input {
    border: 1px solid transparent;
    background-color: #f1f1f1;
    padding: 10px;
    font-size: 16px;
  }

  input[type=text] {
    background-color: #f1f1f1;
    width: 100%;
  }

  input[type=submit] {
    background-color: DodgerBlue;
    color: #fff;
    cursor: pointer;
  }

  .autocomplete-items {
    position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    /*position the autocomplete items to be the same width as the container:*/
    top: 100%;
    left: 0;
    right: 0;
  }

  .autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
  }

  /*when hovering an item:*/
  .autocomplete-items div:hover {
    background-color: #e9e9e9;
  }

  /*when navigating through the items using the arrow keys:*/
  .autocomplete-active {
    background-color: DodgerBlue !important;
    color: #ffffff;
  }

  .container {
    padding: 5px;
  }

  .search-box {
    display: block;
    float: right;
    width: 27%;
    height: 40px;
    border-radius: 3px;
    padding: 4px 55px 4px 10px;
    position: relative;
    background: #fff;
    border: 1px solid #ddd;
    -webkit-transition: all 200ms ease-in-out;
    -moz-transition: all 200ms ease-in-out;
    transition: all 200ms ease-in-out;
  }

  .search-box.hovered,
  .search-box:hover,
  .search-box:active {
    border: 1px solid #aaa;
  }

  .search-box input[type=text] {
    border: 0;
    box-shadow: none;
    display: inline-block;
    padding: 0;
    background: transparent;
    font-size: 14px;
  }

  .search-box input[type=text]:hover,
  .search-box input[type=text]:focus,
  .search-box input[type=text]:active {
    box-shadow: none;
  }

  .search-box .search-btn {
    position: absolute;
    right: 2px;
    top: 2px;
    color: #fff;
    border-radius: 1px;
    font-size: 17px;
    padding: 5px 5px 1px;
    background-color: #abada0;
  }

  .search-box .search-btn:hover {
    color: #fff;
    background-color: #abada0;
  }

  .popover {
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1010;
    display: none;
    padding: 1px;
    background-color: #fff;
    border: 1px solid #002a5c;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    -webkit-background-clip: padding-box;
    -moz-background-clip: padding;
    background-clip: padding-box;
    min-width: 400px;
    max-width: 500px;
    max-height: 500px;
  }

  .popover.top {
    margin-bottom: 10px;
  }

  .popover.right {
    margin-left: 10px;
  }

  .popover.bottom {
    margin-top: 10px;
  }

  .popover.left {
    left: calc(100% - 10px) !important;
    /* margin-right: 10px; */
  }

  .popover-content {
    padding: 9px 14px;
  }

  .popover-content p,
  .popover-content ul,
  .popover-content ol {
    margin-bottom: 0;
  }
</style>


<div id="web_map" class="map">
  <!-- <div style="float:right;" > -->
  <!-- <button id="searchButton" style="margin-right:11px">Search</button> -->
  <!-- <button id="searchButton" type="button" class="btn btn-info">
      <span class="glyphicon glyphicon-search"></span> Search
    </button>
  </div>
  <div class="fcontainer autocomplete">
    <input id="search" type='text' placeholder='Tìm kiếm dữ liệu'>
  </div> -->
  <button type = "button" name="defaultView" id="defaultView" class="btn btn-success" style="background-color: #abada0; float: left; margin-top:5px; border-radius: 20px; -moz-border-radius: 20px; -webkit-border-radius: 20px;">Việt Nam</button>
  <button type = "button" name="seaZone" id="seaZone" class="btn btn-success" style="background-color: #abada0; float: left; margin-top:5px; border-radius: 20px; -moz-border-radius: 20px; -webkit-border-radius: 20px;">Vùng Biển</button>
  <div class="search-box">
    <input class="form-control" id="search" placeholder="Tìm kiếm dữ liệu..." type="text">
    <button class="btn btn-link search-btn" id="searchButton"> <i class="glyphicon glyphicon-search"></i>
    </button>
  </div>
  <div class="modal fade" id="addSeaZone" tabindex="-1" role="dialog" aria-labelledby="addSeaZoneLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" id = "phantudiv">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h5 class="modal-title" id="addSeaZoneLabel">Thêm vùng biển </h4>
          <div class="form-inline">
            <label>Tên vùng: </label>
            <input type="text" class="form-control " id="textZoneName"></input>
          </div>
          <div class="form-inline">
            <label>Add:</label>
            <input type="number" class="form-control" style="width : 90px;" id="numberCoordinates"></input>
            <label>Coordinates: </label>
            <button type="button" class="btn btn-primary" name="addCoordinates" id="addCoordinates">Thêm</button>
          </div>
        </div>
        <!-- <div class="modal-body form-inline" id="formCoordinates0">
          <label>Vĩ độ: </label>
          <input type="text" class="form-control" id="latitute0"></input>
          <label>Kinh độ: </label>
          <input type="text" class="form-control" id="longtitute0"></input>
        </div>
        <div class="modal-body form-inline" id="formCoordinates1">
          <label>Vĩ độ: </label>
          <input type="text" class="form-control" id="latitute1"></input>
          <label>Kinh độ: </label>
          <input type="text" class="form-control" id="longtitute1"></input>
        </div>
        <div class="modal-body form-inline" id="formCoordinates2">
          <label>Vĩ độ: </label>
          <input type="text" class="form-control" id="latitute2"></input>
          <label>Kinh độ: </label>
          <input type="text" class="form-control" id="longtitute2"></input>
        </div> -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" name="add" id="add">Thêm</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>

        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h5 class="modal-title" id="myModalLabel">Cập nhật vị trí </h4>
        </div>
        <div class="modal-body" id="">
          <label>Vĩ độ: </label>
          <input type="text" class="form-control" id="lat"></input>
          <label>Kinh độ: </label>
          <input type="text" class="form-control" id="long"></input>
          <label>Ghi chú: </label>
          <input type="text" class="form-control" id="comment"></input>
          <label>Vi phạm lãnh hải: </label>
          <select name="isViolateTerritorial" class="form-control" id="isViolateTerritorial">
            <option value="0">Không vi phạm</option>
            <option value="1">Vi phạm</option>
          </select>
        </div>
        <div class="modal-footer form-inline">
          <button type="button" class="btn btn-primary" name="update">Cập nhật</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
        </div>
      </div>
    </div>
  </div>
  <pre id="info" />
  </div>
  <div style="display: none;">
      <!-- Popup -->
      <div id="feature-click-popup"></div>
      <div id="feature-popup-hover"></div>
    </div>
<script>

</script>

<script src="<?php echo HOME_URL; ?>/js/map/index.js"></script>