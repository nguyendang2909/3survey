<?php
include_once(_F_TEMPLATES . DS . "local.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['local']['link'] . '">' . $mmenu['local']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  // Validate data
  if (empty($age) || empty($education)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
      $db->table = "local_agency";
      $data = array(
        'age' => intval($age),
        'education' => $db->clearText($education),
        'sex' => intval($sex),
        'social' => $db->clearText(json_encode($social)),
        'internetUsageTime' => intval($internetUsageTime),
        'internetUsagePurpose' => $db->clearText(json_encode($internetUsagePurpose)),
        'postStatus' => intval($postStatus),
        'postAction' => $db->clearText(json_encode($postAction)),
        'identifyAuthen' => intval($identifyAuthen),
        'assessOnlineCommunity' => intval($assessOnlineCommunity),
        'followingPage' => $db->clearText($followingPage),
        'note' => $db->clearText($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['local']['link']);
  }
} else {
  $age = '';
  $education = '';
  $sex = '';
  $social = '';
  $internetUsageTime = '';
  $internetUsagePurpose = '';
  $postStatus = '';
  $postAction = '';
  $identifyAuthen = '';
  $assessOnlineCommunity = '';
  $followingPage = '';
  $note = '';
}
if (!$OK) postLocal(
  HOME_URL_LANG . $mmenu['local']['link'] . '/local-add',
  "add",
  0,
  $age,
  $education,
  $sex,
  $social,
  $internetUsageTime,
  $internetUsagePurpose,
  $postStatus,
  $postAction,
  $identifyAuthen,
  $assessOnlineCommunity,
  $followingPage,
  $note,
  $error
);
