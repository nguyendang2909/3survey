<?php
include_once(_F_TEMPLATES . DS . "local.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['local']['link'] . '">' . $mmenu['local']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$localAgencyId = isset($_GET['id']) ? intval($_GET['id']) : intval($localAgencyId);
$db->table      = "local_agency";
$db->condition  = "`localAgencyId` = $localAgencyId AND `isActive` = 1";
$db->order      = "";
$db->limit      = 1;
$rows           = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['local']['link']);

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($age) || empty($education)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
      $db->table = "local_agency";
      $data = array(
        'age' => intval($age),
        'education' => $db->clearText($education),
        'sex' => intval($sex),
        'social' => $db->clearText(json_encode($social)),
        'internetUsageTime' => intval($internetUsageTime),
        'internetUsagePurpose' => $db->clearText(json_encode($internetUsagePurpose)),
        'postStatus' => intval($postStatus),
        'postAction' => $db->clearText(json_encode($postAction)),
        'identifyAuthen' => intval($identifyAuthen),
        'assessOnlineCommunity' => intval($assessOnlineCommunity),
        'followingPage' => $db->clearText($followingPage),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`localAgencyId` = $localAgencyId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['local']['link']);
  }
} else {
  foreach ($rows as $row) {
    $age = $row['age'];
    $education = $row['education'];
    $sex = $row['sex'];
    $social = json_decode($row['social']);
    $internetUsageTime = $row['internetUsageTime'];
    $internetUsagePurpose = json_decode($row['internetUsagePurpose']);
    $postStatus = $row['postStatus'];
    $postAction = json_decode($row['postAction']);
    $identifyAuthen = $row['identifyAuthen'];
    $assessOnlineCommunity = $row['assessOnlineCommunity'];
    $followingPage = $row['followingPage'];
    $note = $row['note'];
  }
}
if (!$OK) postLocal(
  HOME_URL_LANG . $mmenu['local']['link'] . '/local-edit',
  "edit",
  $localAgencyId,
  $age,
  $education,
  $sex,
  $social,
  $internetUsageTime,
  $internetUsagePurpose,
  $postStatus,
  $postAction,
  $identifyAuthen,
  $assessOnlineCommunity,
  $followingPage,
  $note,
  $error
);
