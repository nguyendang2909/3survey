<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/device_item.xls");

    $db->table = "device_item";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "device` b ON b.`device_id` = a.`device` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` d ON d.`user_id` = a.`user`";
	$db->condition = "a.`is_active` = 1 AND b.`is_active` = 1";
	$db->order = "b.`title` ASC, a.`title` ASC";
	$db->limit = "";
	$data = $db->select("a.`device_item_id`, b.`title` AS `title1`, a.`title` AS `title2`, a.`seri`, a.`specs`, c.`name` AS `name1`, d.`full_name` AS `name2`, a.`perform`, a.`status`, a.`history`, a.`note`, a.`files`");

	$baseRow = 3;
	foreach($data as $r => $dataRow) {
        $f_link = $f_text = '';
        if (file_exists(ROOT_DIR . DS . 'uploads' . DS . 'device' . DS . stripslashes($dataRow['files'])) && !empty($dataRow['files'])) {
            $f_link = HOME_URL . '/uploads/device/' . stripslashes($dataRow['files']);
            $f_text = 'Tải về';
        }


		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
        if(empty($f_link))
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
				->setCellValue('B'.$row, stripslashes($dataRow['title1']))
                ->setCellValue('C'.$row, stripslashes($dataRow['title2']))
                ->setCellValue('D'.$row, stripslashes($dataRow['seri']))
                ->setCellValue('E'.$row, stripslashes($dataRow['specs']))
				->setCellValue('F'.$row, stripslashes($dataRow['name1']))
				->setCellValue('G'.$row, stripslashes($dataRow['name2']))
				->setCellValue('H'.$row, stripslashes($dataRow['perform']))
				->setCellValue('I'.$row, stripslashes($dataRow['status']))
				->setCellValue('J'.$row, stripslashes($dataRow['history']))
				->setCellValue('K'.$row, stripslashes($dataRow['note']));
        else
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
				->setCellValue('B'.$row, stripslashes($dataRow['title1']))
                ->setCellValue('C'.$row, stripslashes($dataRow['title2']))
                ->setCellValue('D'.$row, stripslashes($dataRow['seri']))
                ->setCellValue('E'.$row, stripslashes($dataRow['specs']))
				->setCellValue('F'.$row, stripslashes($dataRow['name1']))
				->setCellValue('G'.$row, stripslashes($dataRow['name2']))
				->setCellValue('H'.$row, stripslashes($dataRow['perform']))
				->setCellValue('I'.$row, stripslashes($dataRow['status']))
				->setCellValue('J'.$row, stripslashes($dataRow['history']))
				->setCellValue('K'.$row, stripslashes($dataRow['note']))
                ->setCellValue('L'.$row, $f_text)
                ->getCell('L'.$row)->getHyperlink()->setUrl($f_link);
	}
	//$objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Thiet-bi-phan-cung_(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);