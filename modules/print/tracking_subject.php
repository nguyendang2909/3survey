<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/tracking_subject.xls");

    $db->table = "subject";
    $db->condition = "`is_active` = 1";
	$db->order = "`name` ASC";
	$db->limit = "";
	$data = $db->select("`subject_id`, `name`, `age`, `address`, `religion`, `faction`, `social`, `local`, `friends`, `note`");

	$baseRow = 3;
	foreach($data as $r => $dataRow) {
		$list = array();
		$friends = json_decode($dataRow['friends']);
		if(count($friends)>0) {
			$friends = implode(',', $friends);
			$db->table = "subject";
			$db->condition = "`is_active` = 1 AND `subject_id` IN ($friends)";
			$db->order = "`name` ASC";
			$db->limit = "";
			$rows_c = $db->select("`name`");
			foreach($rows_c as $row_c) {
				array_push($list, stripslashes($row_c['name']));
			}
			
		}
		$list = implode('; ', $list);
		//---
		$local = '';
		$db->table = "local";
		$db->condition = "`is_active` = 1 AND `local_id` = " . intval($dataRow['local']);
		$db->order = "`title` ASC";
		$db->limit = "";
		$rows_c = $db->select("`title`");
		foreach($rows_c as $row_c) {
			$local = stripslashes($row_c['title']);
		}

		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
			->setCellValue('B'.$row, stripslashes($dataRow['name']))
			->setCellValue('C'.$row, intval($dataRow['age']))
			->setCellValue('D'.$row, stripslashes($dataRow['address']))
			->setCellValue('E'.$row, stripslashes($dataRow['religion']))
			->setCellValue('F'.$row, stripslashes($dataRow['faction']))
			->setCellValue('G'.$row, stripslashes($dataRow['social']))
			->setCellValue('H'.$row, $local)
			->setCellValue('I'.$row, $list)
			->setCellValue('J'.$row, stripslashes($dataRow['note']));
	}
	$objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Doi-tuong-theo-doi_(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);