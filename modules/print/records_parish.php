<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/software_item.xls");

    $db->table = "software_item";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "software` b ON b.`software_id` = a.`software` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency` LEFT JOIN `" . TTH_DATA_PREFIX . "core_user` d ON d.`user_id` = a.`user`";
	$db->condition = "a.`is_active` = 1 AND b.`is_active` = 1";
	$db->order = "b.`title` ASC, a.`title` ASC";
	$db->limit = "";
	$data = $db->select("a.`software_item_id`, b.`title` AS `title1`, a.`title` AS `title2`, a.`perform`, a.`owner`, a.`address`, c.`name` AS `name1`, d.`full_name` AS `name2`, a.`history`, a.`note`, a.`files`");

	$baseRow = 3;
	foreach($data as $r => $dataRow) {
        $f_link = $f_text = '';
        if (file_exists(ROOT_DIR . DS . 'uploads' . DS . 'software' . DS . stripslashes($dataRow['files'])) && !empty($dataRow['files'])) {
            $f_link = HOME_URL . '/uploads/software/' . stripslashes($dataRow['files']);
            $f_text = 'Tải về';
        }


		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
        if(empty($f_link))
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
				->setCellValue('B'.$row, stripslashes($dataRow['title1']))
                ->setCellValue('C'.$row, stripslashes($dataRow['title2']))
                ->setCellValue('D'.$row, stripslashes($dataRow['perform']))
                ->setCellValue('E'.$row, stripslashes($dataRow['owner']))
                ->setCellValue('F'.$row, stripslashes($dataRow['address']))
				->setCellValue('G'.$row, stripslashes($dataRow['name1']))
				->setCellValue('H'.$row, stripslashes($dataRow['name2']))
				->setCellValue('I'.$row, stripslashes($dataRow['history']))
				->setCellValue('J'.$row, stripslashes($dataRow['note']));
        else
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
				->setCellValue('B'.$row, stripslashes($dataRow['title1']))
                ->setCellValue('C'.$row, stripslashes($dataRow['title2']))
                ->setCellValue('D'.$row, stripslashes($dataRow['perform']))
                ->setCellValue('E'.$row, stripslashes($dataRow['owner']))
                ->setCellValue('F'.$row, stripslashes($dataRow['address']))
				->setCellValue('G'.$row, stripslashes($dataRow['name1']))
				->setCellValue('H'.$row, stripslashes($dataRow['name2']))
				->setCellValue('I'.$row, stripslashes($dataRow['history']))
				->setCellValue('J'.$row, stripslashes($dataRow['note']))
                ->setCellValue('K'.$row, $f_text)
                ->getCell('K'.$row)->getHyperlink()->setUrl($f_link);
	}
	$objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Phan-mem-ung-dung_(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);