<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
    $date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
    $objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/ship_voyage.xls");
    
    $shipId = isset($_GET['id']) ? intval($_GET['id']) : intval($shipId);
    $search   = isset($_GET['search']) ? $_GET['search'] : '-no-';
    $col2   = isset($_GET['col2']) ? $_GET['col2'] : '-no-';
    $col3   = isset($_GET['col3']) ? $_GET['col3'] : '-no-';
    $col4   = isset($_GET['col4']) ? $_GET['col4'] : '-no-';

    $query = $shipId. "= b.`shipId` AND a.`isActive` = 1 AND b.`isActive`=1 ";

    if($search != '-no-')
    {
        $query .= " AND CONCAT(b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`comment`) LIKE '%" . trim($db->clearText($search)) . "%'";
    }
    if($col2 != '-no-')
    {
        $query .= " AND b.`shipUpdateTime` LIKE '%" . trim($db->clearText($col2)) . "%'";
    }
    if($col3 != '-no-')
    {
        $query .= " AND b.`comment` LIKE '%" . trim($db->clearText($col3)) . "%'";
    }
    if($col4 != '-no-')
    {
        $searchText = trim($db->clearText($col4));
        $query .= " AND b.`shipLatitude` LIKE '%" . $searchText . "%' OR b.`shipLongitude` LIKE '%" . $searchText . "%'";
    }

    $data = array();
    $db->table = "ship_report";
    $db->join = "b LEFT JOIN `olala3w_ship` a ON b.`shipId` = a.`shipId`";
    $db->condition = $query;
    $db->order = "b.`updatedAt` DESC";
    $db->limit='';
    $data = $db->select(" a.`name`, b.`shipUpdateTime`, b.`shipLatitude`, b.`shipLongitude`, b.`comment`");

    $baseRow = 3;
    foreach($data as $r => $dataRow) {
        $row = $baseRow + $r;
        $objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
            ->setCellValue('B'.$row, stripslashes($dataRow['name']))
            ->setCellValue('C'.$row, stripslashes($dataRow['shipUpdateTime']))
            ->setCellValue('D'.$row, stripslashes($dataRow['comment']))
            ->setCellValue('E'.$row, stripslashes($dataRow['shipLatitude']) . ',' . stripslashes($dataRow['shipLongitude']));
    }
    $objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

    $time = $date->vnOther(time(), 'd-m-Y_H-i');
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Hai_trinh(' . $time . ').xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);