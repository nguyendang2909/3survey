<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
  $objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/fb_post.xls");
    
  $reactiveId = isset($_GET['id']) ? intval($_GET['id']) : intval($reactiveId);
  $search   = isset($_GET['search']) ? $_GET['search'] : '-no-';
  $col1   = isset($_GET['col1']) ? $_GET['col1'] : '-no-';
  $col2   = isset($_GET['col2']) ? $_GET['col2'] : '-no-';
  $col6   = isset($_GET['col6']) ? $_GET['col6'] : '-no-';

  $db->table = "reactive";
  $db->condition = "`reactiveId` = " .$reactiveId;
  $db->order = '';
  $db->limit = '';
  $rows = $db->select('`fbTargetId`');
  $listFbTargetId = json_decode($rows[0]['fbTargetId']);
  
  $query = "(a.`fbTargetId` = " .$listFbTargetId[0];
  if(count($listFbTargetId)>1)
  {
    $dem;
    for($dem =1; $dem< count($listFbTargetId); $dem++)
    {
      $query .= " OR a.`fbTargetId` =" .$listFbTargetId[$dem];
    }
  }
  $query .= ")";

  if($search != '-no-')
  {
    $query .= " AND CONCAT(b.`name`,a.`content`, a.`dateTime`) LIKE '%" . trim($db->clearText($search)) . "%'";
  }
  if($col1 != '-no-')
  {
    $query .= " AND b.`name` LIKE '%" . trim($db->clearText($col1)) . "%'";
  }
  if($col2 != '-no-')
  {
    $query .= " AND a.`content` LIKE '%" . trim($db->clearText($col2)) . "%'";
  }
  if($col6 != '-no-')
  {
    $query .= " AND a.`dateTime` LIKE '%" . trim($db->clearText($col6)) . "%'";
  }


  $data = array();
  $db->table = "fb_report";
  $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "fb_target` b ON a.`fbTargetId` = b.`fbTargetId`";
  $db->condition = $query;
  $db->order = "a.`dateTime` DESC";
  $db->limit='';
  $data = $db->select("
      b.`name`,
      a.`content`,
      a.`react`,
      a.`share`,
      a.`comment`,
      a.`dateTime`
      ");

	$baseRow = 3;
	foreach($data as $r => $dataRow) {

		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
			->setCellValue('B'.$row, stripslashes($dataRow['name']))
			->setCellValue('C'.$row, stripslashes($dataRow['content']))
			->setCellValue('D'.$row, stripslashes($dataRow['react']))
      ->setCellValue('E'.$row, stripslashes($dataRow['share']))
      ->setCellValue('F'.$row, stripslashes($dataRow['comment']))
      ->setCellValue('G'.$row, stripslashes($dataRow['dateTime']));
	}
	$objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Fb_post(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);