<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/agency.xls");

	$data = loadAgency(0);

	$baseRow = 3;
	foreach($data as $r => $dataRow) {
		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
			->setCellValue('B'.$row, $dataRow['agency_id'])
			->setCellValue('C'.$row, $dataRow['name'])
			->setCellValue('D'.$row, $dataRow['symbol'])
			->setCellValue('E'.$row, $dataRow['sort'])
			->setCellValue('F'.$row, $dataRow['address'])
			->setCellValue('G'.$row, $dataRow['phone'])
			->setCellValue('H'.$row, $dataRow['email'])
			->setCellValue('I'.$row, $dataRow['note']);
	}
	$objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Quan-ly-don-vi_(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);


function loadAgency($parent){
    global $db;
    $result = array();
    $db->table = "agency";
    $db->condition = "`is_active` = 1 AND `parent` = $parent";
    $db->order = "`sort` ASC";
    $db->limit = "";
    $rows = $db->select();
    foreach($rows as $row) {
        $arr = loadAgency($row['agency_id']);
        $result[] = array("id" => $row['agency_id'], "name" => stripslashes($row['name']), "symbol" => stripslashes($row['symbol']), "sort" => intval($row['sort']), "address" => stripslashes($row['address']), "phone" => stripslashes($row['phone']), "email" => stripslashes($row['email']), "note" => strip_tags(stripslashes($row['note'])));
        if(count($arr) > 0) $result[] = $arr;
    }
    return $result;
}