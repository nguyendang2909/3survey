<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
if($account["id"]>0) {
	$date   = new DateClass();
	$objReader = PHPExcel_IOFactory::createReader('Excel5');
	$objPHPExcel = $objReader->load(_F_MODULES . DS . "print/_temp/network_ip.xls");

    $db->table = "network_ip";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "device_item` b ON b.`device_item_id` = a.`device_item` LEFT JOIN `" . TTH_DATA_PREFIX . "agency` c ON c.`agency_id` = a.`agency`";
	$db->condition = "a.`is_active` = 1";
	$db->order = "a.`ip` ASC";
	$db->limit = "";
	$data = $db->select("a.`network_ip_id`, a.`ip`, b.`title`, c.`name` AS `agency`");

	$baseRow = 3;
	foreach($data as $r => $dataRow) {
		$row = $baseRow + $r;
		$objPHPExcel->getActiveSheet()->insertNewRowBefore($row, 1);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $r+1)
			->setCellValue('B'.$row, stripslashes($dataRow['ip']))
			->setCellValue('C'.$row, stripslashes($dataRow['title']))
			->setCellValue('D'.$row, stripslashes($dataRow['agency']));
	}
	$objPHPExcel->getActiveSheet()->removeRow($baseRow-1, 1);

	$time = $date->vnOther(time(), 'd-m-Y_H-i');
	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Dia-chi-IP_(' . $time . ').xls"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;


}  else loadPageError('Lỗi! Dữ liệu truy xuất không hợp lệ.', HOME_URL_LANG);