<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' .  $mmenu['network']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">' . $mmenu['network']['sub'][2]['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

if(isset($_POST['tick'])){
    $tick = empty($_POST['tick']) ? array() : $_POST['tick'];
    $tick = array_filter($tick);
    if(count($tick)>0) {
        $tick = implode(',', $tick);
        $db->table = "device";
        $data = array(
            'is_active'     => 0,
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->condition = "`device_id` IN ($tick)";
        $db->update($data);
        loadPageSuccess("Đã xoá dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][1]['link']);
    }
}

$db->table = "device";
$db->condition = "`is_active` = 1";
$db->order = "";
$db->limit = "";
$rows = $db->select();
foreach ($rows as $row) {
    $db->table = "device_item";
    $db->condition = "`is_active` = 1 AND `device` = " . intval($row['device_id']);
    $db->order = "";
    $db->limit = 1;
    $rows_s = $db->select("COUNT(*) AS `count`");
    $count = $db->RowCount;
    foreach($rows_s as $row_s) {
        $count = $row_s['count'];
    }
    //--- Cập nhật dữ liệu
    $db->table = "device";
    $data = array(
        'count' => $count
    );
    $db->condition = "`device_id` = " . intval($row['device_id']);
    $db->update($data);
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-no-border">
            <div class="row">
                <div class="col-xs-12 top-tool">
                    <div class="pull-left">
						<a class="btn-add-new not-abs" href="<?php echo HOME_URL_LANG . $mmenu['network']['link'] . '/device-category-add';?>">Thêm nhóm thiết bị</a>
                    </div>
                    <div class="pull-right btn-tool">
                        <a class="btn btn-temp" target="_blank" href="<?php echo HOME_URL_LANG . '/documents/_temp/(Temp)_Device-Category.xls'?>"><label class="icon"></label><label class="text">Mẫu nhập...</label></a>
                        <button type="button" class="btn btn-export" onclick="return print_f('device-category');">
                            <label class="icon">&nbsp;</label><label class="text">Xuất tệp...</label>
                        </button>
                        <form id="_form_import" method="post" class="btn btn-import" enctype="multipart/form-data">
                            <input type="file" name="file" id="import" class="inputfile" onchange="return import_file('device_category');" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                            <label class="icon" for="import">&nbsp;</label><label class="text" for="import">Chèn tệp...</label>
                        </form>
                    </div>
                </div>
            </div>
			<div class="table-responsive">
                <form method="post" id="_ol_delete">
					<table class="table display table-bordered" cellspacing="0" cellpadding="0" id="dataTablesList">
						<thead>
						<tr>
							<th>ID</th>
							<th>Nhóm thiết bị</th>
                            <th width="250px">Ghi chú</th>
                            <th>Số thiết bị</th>
                            <th>Ngày cập nhật</th>
                            <th>Người cập nhật</th>
                            <th width="80px">Chọn</th>
						</tr>
						</thead>
						<thead>
						<tr>
							<td align="center">-</td>
                            <td><input type="text" data-column="1" class="form-control filter"></td>
                            <td><input type="text" data-column="2" class="form-control filter"></td>
                            <td><input type="text" data-column="3" class="form-control filter auto-number text-center" data-a-sep=" " data-v-max="999999999999" data-v-min="0" maxlength="20"></td>
                            <td><input type="text" data-column="4" class="form-control filter input-date text-center"></td>
                            <td><input type="text" data-column="5" class="form-control filter"></td>
                            <td align="center">-</td>
						</tr>
						</thead>
					</table>
                    <?php
                    if(in_array("device-category;delete", $corePrivilegeSlug['op']))
                        echo '<div class="row"><div class="col-sm-12" align="right"><label class="radio-inline"><input type="checkbox" id="_ol_select_all"  data-toggle="tooltip" data-placement="top" title="Chọn xóa tất cả" ></label><input type="button" class="btn btn-danger btn-round btn-xs ol-confirm" value="Xoá nhóm" name="delete"></div></div>';
                    ?>
				</form>
			</div>
			<!-- /.table-responsive -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-8 -->
</div>
<script>
	$(document).ready(function() {
		$('#dataTablesList tfoot th').each( function () {
			var title = $(this).text();
			$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
		} );
		var table = $('#dataTablesList').DataTable( {
			"language": {
				"url": "/js/data-tables/de_DE.txt"
			},
			"lengthMenu": [100, 200, 300],
			"info":     false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				url: '/action.php',
				type: 'POST',
				data: {
					url: 'device_category',
					type: 'load'
				}
			},
            "columns": [
                { data: 'no' },
                { data: 'title' },
                { data: 'note' },
                { data: 'count' },
                { data: 'time' },
                { data: 'user' },
                { data: 'tool' }
            ],
			"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
				$('td:eq(0)', nRow).css( "text-align", "center" );
                $('td:eq(3)', nRow).css( "text-align", "center" );
                $('td:eq(4)', nRow).css( "text-align", "center" );
                $('td:eq(6)', nRow).css( "text-align", "center" );
				return nRow;
			},
            "fnDrawCallback": function () {
                $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
            },
			"order": [ 1, "asc" ],
			"aoColumnDefs" : [ {
				'targets': [6],
				'searchable':false,
				'orderable':false
			} ]
		});
		// Apply the search
        $( '.filter' ).on( 'change', function () {
            var i = $(this).attr('data-column');
            var v = $(this).val();
            table.columns(i).search(v).draw();
        });
	});
    //---
    $('.input-date').datetimepicker({
        lang: 'vi',
        timepicker: false,
        format: '<?php echo TTH_DATE_FORMAT;?>'
    });
    $(".ol-confirm").click(function() {
        confirm("Tất cả các dữ liệu liên quan sẽ được xóa.\nBạn có muốn thực hiện không?", function() {
            if(this.data == true) document.getElementById("_ol_delete").submit();
        });
    });
    $(".ol-alert-core").boxes('alert', 'Bạn không được phân quyền với chức năng này.');
</script>