<?php
include_once(_F_TEMPLATES . DS . "website.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . '/website' . '">' . $mmenu['network']['sub'][5]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Sửa</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

// Get record to edit from db
$websiteId = isset($_GET['id']) ? intval($_GET['id']) : intval($websiteId);
$db->table = 'websites';
$db->condition = "`websiteId` = $websiteId AND `isActive` = 1";
$db->order = "";
$db->limit = 1;
$rows = $db->select();
if ($db->RowCount == 0) loadPageError("Dữ liệu không tồn tại.", HOME_URL_LANG . $mmenu['tracking']['link'] . '/tracking-fleet-type');

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'edit') {
  // Validate data
  if (empty($name) || empty($websiteUrl)) $error = '<span class="show-error">Vui lòng nhập đủ thông tin.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'network' . DS;

    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];
    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);
      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      foreach ($rows as $row) {
        $fileName = stripslashes($row['file']);
      }
    }

    if ($OK) {
      $db->table = "websites";
      $data = array(
        'name' => $db->clearText($name),
        'place' => $db->clearText($place),
        'url' => $db->clearText($websiteUrl),
        'ip' => $db->clearText($ip),
        'latitude' => $db->clearText($latitude),
        'longitude' => $db->clearText($longitude),
        'mapTypeId' => $db->clearText($mapTypeId),
        'owner' => $db->clearText($owner),
        'isWarning' => intval($isWarning),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'updatedAt' => date("Y-m-d H:i:s"),
        'updatedBy' =>  $_SESSION["user_id"],
      );
      $db->condition = "`websiteId` = $websiteId";
      $db->update($data);

      loadPageSuccess("Đã chỉnh sửa dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . '/website');
    }
    $OK = true;
  }
} else {
  foreach ($rows as $row) {
    $name = $row['name'];
    $place = $row['place'];
    $websiteUrl = $row['url'];
    $ip = $row['ip'];
    $latitude = $row['latitude'];
    $longitude = $row['longitude'];
    $mapTypeId = $row['mapTypeId'];
    $owner = $row['owner'];
    $isWarning = $row['isWarning'];
    $file = $row['file'];
    $note = $row['note'];
  }
}
if (!$OK) website(
  HOME_URL_LANG . $mmenu['network']['link'] . '/website-edit',
  "edit",
  $websiteId,
  $name,
  $place,
  $websiteUrl,
  $ip,
  $latitude,
  $longitude,
  $mapTypeId,
  $owner,
  $isWarning,
  $file,
  $note,
  $error
);
