<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link'] . '">' . $mmenu['network']['sub'][1]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm IP</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "network_ip.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($ip)) $error = '<span class="show-error">Vui lòng nhập địa chỉ IP.</span>';
	else {
        $db->table = "network_ip";
        $data = array(
            'ip'            => $db->clearText($ip),
            'device_item'   => $db->clearText($device_item),
            'agency'        => intval($agency),
            'created_time'  => time(),
            'modified_time' => time(),
            'user_id'       => $_SESSION["user_id"]
        );
        $db->insert($data);

		loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][1]['link']);
		$OK = true;
	}
}
else {
	$ip	            = "";
    $device_item    = 0;
    $agency         = 0;
}
if(!$OK) networkIP(HOME_URL_LANG . $mmenu['network']['link'] . '/network-ip-add', "add", 0, $ip, $device_item, $agency, $error);