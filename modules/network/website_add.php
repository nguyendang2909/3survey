<?php
include_once(_F_TEMPLATES . DS . "website.php");

if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

// Menu
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="javascript:;">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . '/website' . '">' . $mmenu['network']['sub'][5]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';

if (empty($typeFunc)) $typeFunc = '-no-';
$OK = false;
$error = '';

if ($typeFunc == 'add') {
  // Validate data
  if (empty($name)) $error = '<span class="show-error">Vui lòng nhập tên.</span>';
  else {
    $fileMaxSize  = FILE_MAX_SIZE;
    $dirDest       = ROOT_DIR . DS . 'uploads' . DS . 'network' . DS;
    $fileName      = 'os' . time() . '_' . md5(microtime(true));
    $fileSize      = $_FILES['file']['size'];

    if ($fileSize > 0) {
      $fileUp = new Upload($_FILES['file']);
      $fileUp->file_max_size = $fileMaxSize;
      $fileUp->file_new_name_body = $fileName;
      $fileUp->Process($dirDest);

      if ($fileUp->processed) {
        $fileName = $fileUp->file_dst_name;
        $OK = true;
      } else {
        $error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
      }
    } else {
      $OK = true;
      $fileName = '-no-';
    }

    if ($OK) {
      $db->table = "websites";
      $data = array(
        'name' => $db->clearText($name),
        'place' => $db->clearText($place),
        'url' => $db->clearText($websiteUrl),
        'ip' => $db->clearText($ip),
        'latitude' => $db->clearText($latitude),
        'longitude' => $db->clearText($longitude),
        'mapTypeId' => $db->clearText($mapTypeId),
        'owner' => $db->clearText($owner),
        'isWarning' => intval($isWarning),
        'file' => $db->clearText($fileName),
        'note' => $db->clearText($note),
        'createdBy' => $_SESSION["user_id"],
        'updatedBy' => $_SESSION["user_id"],
        'createdAt' => date("Y-m-d H:i:s"),
        'updatedAt' => date("Y-m-d H:i:s"),
      );
      $db->insert($data);

      loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . '/website');
    }

    $OK = true;
  }
} else {
  $name = '';
  $place = '';
  $websiteUrl = '';
  $ip='';
  $latitude = '';
  $longitude = '';
  $mapTypeId = 17;
  $owner = '';
  $isWarning = '';
  $file = '';
  $note = '';
}
if (!$OK) website(
  HOME_URL_LANG . $mmenu['network']['link'] . '/website-add',
  "add",
  0,
  $name,
  $place,
  $websiteUrl,
  $ip,
  $latitude,
  $longitude,
  $mapTypeId,
  $owner,
  $isWarning,
  $file,
  $note,
  $error
);
