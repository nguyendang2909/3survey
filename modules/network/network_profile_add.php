<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][0]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][0]['link'] . '">' . $mmenu['network']['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm hồ sơ</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "network_profile.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên hồ sơ.</span>';
    elseif(empty($user)) $error = '<span class="show-error">Vui lòng chọn người tạo.</span>';
	else {
		$file_max_size	= FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'network' . DS;
		
		$file_name      = 'os' . time() . '_' . md5(microtime(true));
        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
				$error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
            }
        } else {
            $OK = true;
            $file_name = '-no-';
        }
		
		if($OK) {			
			$db->table = "network_profile";
			$data = array(
				'title'       	=> $db->clearText($title),
                'user'          => intval($user),
				'agency'        => intval($agency),
				'history'       => $db->clearText($history),
				'note'          => $db->clearText($note),
				'files'       	=> $db->clearText($file_name),
				'created_time'  => time(),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->insert($data);
			
			loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][0]['link']);
		}
		$OK = true;
	}
}
else {
    $title      = "";
    $user       = 0;
    $agency     = 0;
    $history    = "";
    $note       = "";
    $files      = "";
}
if(!$OK) networkProfile(HOME_URL_LANG . $mmenu['network']['link'] . '/network-profile-add', "add", 0, $title, $user, $agency, $history, $note, $files, $error);