<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//
$breadcrumbs = '<ul class="breadcrumbs-alt">';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . '">' . $mmenu['home']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link'] . '">' . $mmenu['network']['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link'] . '">' . $mmenu['network']['sub'][2]['title'] . '</a></li>';
$breadcrumbs .= '<li><a href="' . HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link'] . '">' . $mmenu['network']['sub'][2]['sub'][0]['title'] . '</a></li>';
$breadcrumbs .= '<li><a class="current">Thêm thiết bị</a></li>';
$breadcrumbs .= '</ul>';
echo '<div class="row"><div class="col-md-12">' . $breadcrumbs . '</div></div>';
//---

include_once (_F_TEMPLATES . DS . "device_item.php");
if(empty($typeFunc)) $typeFunc = '-no-';

$OK = false;
$error = '';
if($typeFunc=='add'){
    $date = new DateClass();
    if(empty($device)) $error = '<span class="show-error">Vui lòng chọn nhóm thiết bị.</span>';
	elseif(empty($title)) $error = '<span class="show-error">Vui lòng nhập tên thiết bị.</span>';
	else {
		$file_max_size	= FILE_MAX_SIZE;
		$dir_dest       = ROOT_DIR . DS . 'uploads' . DS . 'device' . DS;
		
		$file_name      = 'os' . time() . '_' . md5(microtime(true));
        $file_size      = $_FILES['files']['size'];
        if ($file_size > 0) {
            $fileUp = new Upload($_FILES['files']);
            $fileUp->file_max_size = $file_max_size;
            $fileUp->file_new_name_body = $file_name;
            $fileUp->Process($dir_dest);
            if($fileUp->processed) {
                $file_name = $fileUp->file_dst_name;
                $OK = true;
            } else {
				$error = '<span class="show-error">Lỗi tải tệp tin: ' . $fileUp->error . '</span>';
            }
        } else {
            $OK = true;
            $file_name = '-no-';
        }
		
		if($OK) {			
			$db->table = "device_item";
			$data = array(
				'device'  	    => intval($device),
				'title'       	=> $db->clearText($title),
				'seri'       	=> $db->clearText($seri),
				'specs'       	=> $db->clearText($specs),
				'agency'        => intval($agency),
				'user'          => intval($user),
				'perform'       => $db->clearText($perform),
				'status'        => $db->clearText($status),
				'history'       => $db->clearText($history),
				'note'          => $db->clearText($note),
				'files'       	=> $db->clearText($file_name),
				'created_time'  => time(),
				'modified_time' => time(),
				'user_id'       =>  $_SESSION["user_id"]
			);
			$db->insert($data);
			
			loadPageSuccess("Đã thêm dữ liệu thành công.", HOME_URL_LANG . $mmenu['network']['link'] . $mmenu['network']['sub'][2]['sub'][0]['link']);
		}
		$OK = true;
	}
}
else {
    $device  	= isset($_GET['id']) ? intval($_GET['id']) : 0;
    $title    	= "";
    $seri       = "";
    $specs      = "";
    $agency     = 0;
    $user       = 0;
    $perform    = "";
    $status     = "";
    $history    = "";
    $note       = "";
    $files      = "";
}
if(!$OK) deviceItem(HOME_URL_LANG . $mmenu['network']['link'] . '/device-item-add', "add", 0, $device, $title, $seri, $specs, $agency, $user, $perform, $status, $history, $note, $files, $error);