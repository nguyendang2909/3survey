<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
/* Count login of USER. */
$date = new DateClass();
$time = $date->vnOther(time(), 'Y-m-d');
$session = md5(session_id());
$acc_u = $account["id"];
$sum = 0;

if($acc_u>0) {
	$db->table = "logs";
	$db->condition = "`user_id` = $acc_u AND `date` = '$time'";
	$db->order = "";
	$db->limit = "";
	$rows = $db->select();
	if ($db->RowCount > 0) {
		foreach ($rows as $row) {
			$sum = $row['count'];
			if ($row['session'] != $session) {
				$db->table = "logs";
				$data = array(
						'count'     => intval($sum + 1),
						'session'   => $session
				);
				$db->condition = "`logs_id` = " . intval($row['logs_id']);
				$db->update($data);
			}
		}
	} else {
		$db->table = "logs";
		$data = array(
				'user_id'       => $acc_u,
				'date'          => $time,
				'count'         => 1,
				'session'       => $session,
				'modified_time' => time()
		);
		$db->insert($data);
	}
}