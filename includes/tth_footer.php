<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }

?>

<footer class="footer">
	<div class="box-wrap clearfix">
        <div class="copyright">
            <p>2020 &copy;Báo cáo Biển Đông</p>
        </div>
        <div class="olala-team">
            <p>Developed by <a title="Lu doan 3/BTL86" href="http://btl86.bqp" style="color: inherit; font-weight: bold;" target="_blank">Lu doan 3/BTL86</a>.</p>
        </div>
	</div>
</footer>