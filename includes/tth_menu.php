<?php
if (!defined('TTH_SYSTEM')) {
  die('Please stop!');
}

$mmenu = array();
$mmenu['home'] = array(
  'link'  => '/home',
  'title' => 'Phiếu ý kiến',
  'icon'  => 'fa-sticky-note-o'
);
$mmenu['local'] = array(
  'link'  => '/local',
  'title' => 'Cơ quan, ban ngành địa phương',
  'icon'  => 'fa-sticky-note-o'
);

$mmenu['military'] = array(
  'link'  => '/military',
  'title' => 'Cơ quan, đơn vị quân đội',
  'icon'  => 'fa-sticky-note-o'
);

?>
<aside>
  <div id="sidebar" class="nav-collapse <?php echo $hide_left_bar; ?>">
    <!-- sidebar menu start-->
    <div class="leftside-navigation">
      <ul class="sidebar-menu" id="nav-accordion">
        <?php
        foreach ($mmenu as $key => $value) {
          if (in_array(substr($value['link'], 1), $corePrivilegeSlug['ol'])) {
            $active = '';
            if ($tth[TTH_PATH] == substr($value['link'], 1)) $active = ' class="active"';
            if (empty($value['sub'])) {
              echo '<li><a' . $active . ' href="' . HOME_URL_LANG . $value['link'] . '" title="' . $value['title'] . '"><i class="fa ' . $value['icon'] . '"></i><span>' . $value['title'] . '</span></a></li>';
            } else {
              echo '<li class="sub-menu">';
              echo '<a' . $active . ' href="javascript:;" title="' . $value['title'] . '"><i class="fa ' . $value['icon'] . '"></i><span>' . $value['title'] . '</span></a>';
              echo '<ul class="sub">';
              foreach ($value['sub'] as $k => $v) {
                if (in_array(substr($v['link'], 1), $corePrivilegeSlug['op'])) {
                  if (empty($v['sub'])) {
                    $active = '';
                    if (($tth[TTH_PATH] == substr($value['link'], 1)) && ($tth[TTH_PATH_OP] == substr($v['link'], 1))) $active = ' class="active"';
                    echo '<li' . $active . '><a href="' . HOME_URL_LANG . $value['link'] . $v['link'] . '" title="' . $v['title'] . '">' . $v['title'] . '</a></li>';
                  } else {
                    $active = '';
                    if (($tth[TTH_PATH] == substr($value['link'], 1)) && in_array('/' . $tth[TTH_PATH_OP], array_column($v['sub'], 'link'))) $active = ' class="active"';
                    echo '<li class="sub-menu">';
                    echo '<a' . $active . ' href="javascript:;" title="' . $v['title'] . '">' . $v['title'] . '</a>';
                    echo '<ul class="sub">';
                    foreach ($v['sub'] as $kk => $vv) {
                      $active = '';
                      if (($tth[TTH_PATH] == substr($value['link'], 1)) && ($tth[TTH_PATH_OP] == substr($vv['link'], 1))) $active = ' class="active"';
                      echo '<li' . $active . '><a href="' . HOME_URL_LANG . $value['link'] . $vv['link'] . '" title="' . $vv['title'] . '">' . $vv['title'] . '</a></li>';
                    }
                    echo '</ul>';
                    echo '</li>';
                  }
                }
              }
              echo '</ul>';
              echo '</li>';
            }
          }
        }

        ?>
      </ul>
    </div>
    <!-- sidebar menu end-->
  </div>
</aside>