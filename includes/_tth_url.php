<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }

$url = trim($_GET['url']);
$path = array();
$path = explode('/', $url);
if(!empty($path[0])) {
    $tth[TTH_PATH] = $path[0];
    if(!empty($path[1])) {
        $tth[TTH_PATH_OP] = $path[1];
    }
    $P_slug = '';
    $P_category = $P_menu = $P_post = 0;
    //---
    $db->table = "blog";
    $db->condition = "`is_active` = 1 AND `parent` = 0 AND `slug` LIKE '" . $path[0] . "'";
    $db->order = "";
    $db->limit = 1;
    $rows = $db->select("`blog_id`, `slug`");
    if($db->RowCount > 0) {
        //---
        $tth[TTH_PATH] = 'public';
        $tth[TTH_PATH_OP] = 'post';
        foreach($rows as $row) {
            $P_slug = stripslashes($row['slug']);
            $P_menu = intval($row['blog_id']);
        }

        if(!empty($path[1])) {
            $db->table = "blog_post";
            $db->condition = "`is_active` = 1 AND `slug` LIKE '" . $path[1] . "'";
            $db->order = "";
            $db->limit = 1;
            $rows = $db->select("`blog_post_id`, `blog`");
            if($db->RowCount > 0) {
                foreach($rows as $row) {
                    $P_post = intval($row['blog_post_id']);
                    $P_menu = intval($row['blog']);
                }
            } else {
                $db->table = "blog";
                $db->condition = "`is_active` = 1 AND `slug` LIKE '" . $path[1] . "'";
                $db->order = "";
                $db->limit = 1;
                $rows = $db->select("`blog_id`");
                if($db->RowCount > 0) {
                    foreach($rows as $row) {
                        $P_menu = intval($row['blog_id']);
                    }
                }
            }
        }
    }
}