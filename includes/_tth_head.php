<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//--
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="language" content="<?php echo TTH_LANGUAGE;?>">
<meta http-equiv="Refresh" content="3600">
<title><?php echo getConstant('title'); ?></title>
<meta name="description" content="<?php echo getConstant('description'); ?>">
<meta name="keywords" content="<?php echo getConstant('keywords'); ?>">
<meta name="copyright" content="<?php echo getConstant('copyright'); ?>">
<meta name="author" content="Olala App | Olala Group">
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo HOME_URL; ?>/images/logos/57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo HOME_URL; ?>/images/logos/60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo HOME_URL; ?>/images/logos/72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo HOME_URL; ?>/images/logos/76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo HOME_URL; ?>/images/logos/114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo HOME_URL; ?>/images/logos/120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo HOME_URL; ?>/images/logos/144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo HOME_URL; ?>/images/logos/152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo HOME_URL; ?>/images/logos/180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo HOME_URL; ?>/images/logos/192x192.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo HOME_URL; ?>/images/logos/96x96.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo HOME_URL; ?>/images/logos/32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo HOME_URL; ?>/images/logos/16x16.png">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo HOME_URL; ?>/images/logos/favicon.ico">
<link rel="icon" type="image/x-icon" href="<?php echo HOME_URL; ?>/images/logos/favicon.ico">

<!--Core CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/bs3/css/bootstrap.min.css" charset="utf-8" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/jquery-ui/jquery-ui-1.10.1.custom.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/animate.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/fileinput.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/jquery.calendar/jquery.datetimepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL;?>/js/daterangepicker/daterangepicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- DataTables CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/data-tables/dataTables.bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/data-tables/jquery.dataTables.css">
<!-- Bootstrap-select CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/bootstrap-select/css/bootstrap-select.min.css">
<!-- Custom styles for this template -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/style.css?ver=3.291118">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/style-responsive.css?ver=3.291118" media="all">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/fancybox/jquery.fancybox.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/jquery-easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/jquery-easyui/themes/icon.css">
<!-- Bootstrap-reset CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/bootstrap-reset.css?ver=3.291118">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/openlayers/ol.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/openlayers/ol-popup.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/js/openlayers/ol-layerswitcher.css">
