<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }

$p_role = $p_user = array();
$db->table = "core_privilege";
$db->condition = "`type` LIKE '" . $tth[TTH_PATH] . "' AND `privilege_slug` LIKE '" . $tth[TTH_PATH_OP] . "'";
$db->order = "";
$db->limit = "";
$rows = $db->select('role_id');
foreach($rows as $row) {
    array_push($p_role, intval($row['role_id']));
}
if(count($p_role)>0) {
    $p_role = implode(',', $p_role);

    $db->table = "core_user";
    $db->join = "a LEFT JOIN `" . TTH_DATA_PREFIX . "role_user` b ON a.`user_id` = b.`user`";
    $db->condition = "a.`is_active` = 1 AND b.`role` IN ($p_role) GROUP BY a.`user_id`";
    $db->order = "a.`sort` ASC";
    $db->limit = "";
    $rows = $db->select("a.`user_id`, a.full_name");
    foreach($rows as $row) {
        if($row['user_id']==1) array_push($p_user, stripslashes($row['full_name']));
        else array_push($p_user, '<a target="_blank" href="/?ol=user&op=portal&id=' . intval($row['user_id']) . '" data-toggle="tooltip" data-placement="top" title="Portal nhân viên">' . stripslashes($row['full_name']) . '</a>');
    }
}
if(count($p_user)) {
    echo '<div class="row"><div class="col-xs-12 bottom-privileged"><span class="lb-level">Privileged:</span> ' . implode($p_user, ' / '). '</div></div>';
}