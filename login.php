<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//--
?>
<!DOCTYPE html>
<html lang="<?php echo TTH_LANGUAGE;?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="language" content="<?php echo TTH_LANGUAGE;?>">
	<meta http-equiv="Refresh" content="3600">
	<title><?php echo getConstant('title'); ?></title>
	<meta name="description" content="<?php echo getConstant('description'); ?>">
	<meta name="keywords" content="<?php echo getConstant('keywords'); ?>">
	<meta name="copyright" content="<?php echo getConstant('copyright'); ?>">
	<meta name="author" content="Olala Apps | Olala Group">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo HOME_URL; ?>/images/logos/57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo HOME_URL; ?>/images/logos/60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo HOME_URL; ?>/images/logos/72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo HOME_URL; ?>/images/logos/76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo HOME_URL; ?>/images/logos/114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo HOME_URL; ?>/images/logos/120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo HOME_URL; ?>/images/logos/144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo HOME_URL; ?>/images/logos/152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo HOME_URL; ?>/images/logos/180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo HOME_URL; ?>/images/logos/192x192.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo HOME_URL; ?>/images/logos/96x96.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo HOME_URL; ?>/images/logos/32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo HOME_URL; ?>/images/logos/16x16.png">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo HOME_URL; ?>/images/logos/favicon.ico">
	<link rel="icon" type="image/x-icon" href="<?php echo HOME_URL; ?>/images/logos/favicon.ico">
	<!--Core CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/bs3/css/bootstrap.min.css" charset="utf-8" media="all">
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/bootstrap-reset.css">
	<!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/style-responsive.css" media="all">
</head>
<body class="login-body">
	<section id="container">
		<header class="login-header clearfix">
			<!--logo start-->
            <a href="<?php echo HOME_URL_LANG;?>" class="logo" title="East Sea"><img src="<?php echo HOME_URL;?>/images/logos/east-sea.png" alt="Logo East Sea" width="100px"></a>
			<!--logo end-->
		</header>
		<section class="login-wrap">
			<div class="row box-wrap">
				<div class="login-form">
					<form id="sign-in-form" method="post">
						<div class="title">
							<h3>Đăng nhập vào tài khoản</h3>
						</div>
						<div class="login-input">
							<p class="field">
								<input type="text" name="ol_login_username" maxlength="30" placeholder="Tên đăng nhập (hoặc Số điện thoại)..." required="required" title="Hãy điền tên đăng nhập." autocomplete="off" autocapitalize="off" autocorrect="off">
								<i class="fa fa-user fa-1x"></i>
							</p>
							<p class="field">
								<input type="password" name="ol_login_password" maxlength="50" placeholder="Mật khẩu..." required="required" title="Hãy điền mật khẩu." autocomplete="off" autocapitalize="off" autocorrect="off">
								<i class="fa fa-key fa-1x"></i>
							</p>
                            <?php
                            if(isset($_GET['change_password']) && !isset($_POST['login_admin'])) {
                                echo $notify_account['change_password_success'];
                            } else {
                                echo $login_failed;
                            }
                            ?>
						</div>
                        <div class="form-actions clearfix">
                            <a class="go-home" href="<?php echo HOME_URL_LANG;?>" title="Về lại trang chủ"><i class="fa fa-long-arrow-left fa-fw"></i> Về trang chủ</a>
                            <button type="submit" class="btn btn-primary pull-right" name="login_admin">Đăng nhập <i class="m-icon-swapright m-icon-white"></i></button>
                        </div>
                        <div class="forget-password">
                            <h4>Quên mật khẩu?</h4>
                            <p>Bấm <a href="javascript:void(0)" id="click-forgot-password">vào đây</a>, để lấy lại mật khẩu của bạn.</p>
                        </div>
					</form>
					<form id="forgot-password-form" method="post" style="display: none;" onsubmit="return send_lost_forgot('forgot-password-form');">
						<div class="title">
							<h3>Thiết lập mật khẩu mới</h3>
						</div>
						<div class="login-input">
							<p class="field">
								<input type="text" name="forgot_user_email" maxlength="50" placeholder="Tên đăng nhập / E-mail ..." required="required" title="Hãy điền Tên đăng nhập hoặc E-mail." autocomplete="off" autocapitalize="off" autocorrect="off">
								<i class="fa fa-support fa-1x"></i>
							</p>
							<?php echo $notify_account['reset_password'];?>
						</div>
                        <div class="form-actions clearfix">
                            <a href="javascript:void(0)" class="btn btn-default pull-left" id="click-sign-in"><i class="m-icon-swapleft"></i> Quay lại</a>
                            <button type="submit" class="btn btn-info pull-right" name="forgot">Thiết lập <i class="m-icon-swapright m-icon-white"></i></button>
                        </div>
					</form>
				</div>
			</div>
		</section>
		<?php
		include (_F_INCLUDES . DS . 'tth_footer.php');
		?>
	</section>
	<!-- Modal -->
	<div class="modal fade" id="_pages" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
	<!-- /.Modal -->
	<div id="_loading"></div>
<!--Core js-->
<!--[if lt IE 9]>
<script src="./js/ie8-responsive-file-warning.js"></script>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/bs3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/backstretch/jquery.backstretch.min.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/login.js"></script>
<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.boxes.js"></script>
<script>
    jQuery(document).ready(function() {
        $.backstretch([
                "<?php echo HOME_URL;?>/images/1.jpg",
                "<?php echo HOME_URL;?>/images/2.jpg",
                "<?php echo HOME_URL;?>/images/3.jpg",
                "<?php echo HOME_URL;?>/images/4.jpg"
            ], {
                fade: 1000,
                duration: 8000
            }
        );
    });
</script>
</body>
</html>