<?php
if (!defined('TTH_SYSTEM')) { die('Please stop!'); }
//--
?>
<!DOCTYPE html>
<html lang="<?php echo TTH_LANGUAGE;?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="language" content="<?php echo TTH_LANGUAGE;?>">
	<meta http-equiv="Refresh" content="3600">
	<title><?php echo getConstant('title'); ?></title>
	<meta name="description" content="<?php echo getConstant('description'); ?>" />
	<meta name="keywords" content="<?php echo getConstant('keywords'); ?>" />
	<meta name="copyright" content="<?php echo getConstant('copyright'); ?>" />
    <meta name="author" content="Olala Apps | Olala Group">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo HOME_URL; ?>/images/logos/57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo HOME_URL; ?>/images/logos/60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo HOME_URL; ?>/images/logos/72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo HOME_URL; ?>/images/logos/76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo HOME_URL; ?>/images/logos/114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo HOME_URL; ?>/images/logos/120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo HOME_URL; ?>/images/logos/144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo HOME_URL; ?>/images/logos/152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo HOME_URL; ?>/images/logos/180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo HOME_URL; ?>/images/logos/192x192.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo HOME_URL; ?>/images/logos/96x96.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo HOME_URL; ?>/images/logos/32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo HOME_URL; ?>/images/logos/16x16.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo HOME_URL; ?>/images/logos/favicon.ico">
    <link rel="icon" type="image/x-icon" href="<?php echo HOME_URL; ?>/images/logos/favicon.ico">
    <!--Core CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/bs3/css/bootstrap.min.css" charset="utf-8" media="all">
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/bootstrap-reset.css">
	<!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo HOME_URL; ?>/css/style-responsive.css" media="all">

	<!-- Core js-->
	<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo HOME_URL;?>/bs3/js/bootstrap.min.js"></script>
	<!-- Common script init for all pages-->
	<script type="text/javascript" src="<?php echo HOME_URL;?>/js/auto-numeric.js"></script>
	<script type="text/javascript" src="<?php echo HOME_URL;?>/js/scripts.js"></script>
	<script type="text/javascript" src="<?php echo HOME_URL;?>/js/jquery.boxes.js"></script>
	<?php echo getConstant('google_analytics')?>
	<!--[if lt IE 9]>
	<script src="<?php echo HOME_URL; ?>/js/ie8-responsive-file-warning.js"></script>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body class="lock-screen" onload="startTime()">
<div class="lock-panel"><span class="move"></span></div>
<div class="lock-wrapper">
	<div id="time"></div>
	<div class="lock-box text-center">
		<?php
		$info_user = array();
		$info_user = getInfoUser($account["id"]);
		?>
		<div class="lock-name"><a title="Đăng xuất" href="javascript:void(0);" onclick="window.location.href = '?logout=OK'"><i class="fa fa-sign-out"></i></a> <span><?php echo $info_user[0];?></span></div>
		<?php echo $info_user[9];?>
		<div class="lock-pwd">
			<form role="lock-screen" method="post">
				<div class="form-group">
					<input type="password" class="form-control lock-input" name="ol_lock_password" placeholder="Nhập mật khẩu..." maxlength="30" required autocomplete="off" autocapitalize="off" autocorrect="off">
					<button class="btn btn-lock" type="submit" name="unlock_screen">
						<i class="fa fa-unlock"></i>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!--Core js-->
<script>
	function startTime() {
		var today=new Date();
		var h=today.getHours();
		var m=today.getMinutes();
		var s=today.getSeconds();
		m=checkTime(m);
		s=checkTime(s);
		document.getElementById('time').innerHTML=h+":"+m+":"+s;
		t=setTimeout(function(){startTime()},500);
	}

	function checkTime(i) {
		if (i<10)
		{
			i="0" + i;
		}
		return i;
	}
</script>
</body>
</html>